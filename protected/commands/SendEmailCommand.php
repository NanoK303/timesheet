<?php

class SendEmailCommand extends CConsoleCommand
{

    public function run($args)
    {
        $datetime = new DateTime("now", new DateTimeZone('Asia/Bangkok'));
        echo "\n\n Start SendEmail ".$datetime->format('Y-m-d H:i:s')."\n\n";
        $data=Workdetail::getNotComplete();
        $dataStaff=Staff::model()->findAll('Position=? || Position=?',array('admin','manage'));
        foreach ($dataStaff as $key => $value) {
            if($value->remove=='N' && !empty($value->Email)) {
                Yii::import('application.extensions.yii-mail.YiiMailMessage');
                $message = new YiiMailMessage; //เรียกใช้ YiiMailMessage ผ่าน $message
                $message->view = 'mail';
                $message->setBody(array('data' => $data), 'text/html'); //ใส่ข้อความลงให้เมลที่เราจะส่ง
                $message->subject = 'แจ้งเตือน จากระบบ TimeSheet'; //หัวข้อของ Email ที่จะส่ง
                $message->addTo($value->Email); //ใส่ email ปลายทาง
                $message->from = 'sootipong303@gmail.com'; //ใส่ Email ผู้ส่ง
                Yii::app()->mail->send($message); //ส่ง Email
            }
        }
        echo "\n END SendEmail ".$datetime->format('Y-m-d H:i:s')."\n\n";
    }
}
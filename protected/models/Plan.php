<?php

Yii::import('application.models._base.BasePlan');

class Plan extends BasePlan
{
    public $detail;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	protected function beforeSave()
	{
		if ($this->Plan_create_date == null) {
			$this->Plan_create_date = new CDbExpression('NOW()');
		}

//        if($this->Plan_status=='success'){
//            $this->Plan_create_date = new CDbExpression('NOW()');
//        }

		if($this->Plan_start_date!==null){
			$this->Plan_start_date = date('Y-m-d H:i:s', strtotime($this->Plan_start_date));
		}

		if($this->Plan_end_date!==null){
			$this->Plan_end_date = date('Y-m-d H:i:s', strtotime($this->Plan_end_date));
		}

		if($this->UserName==null){
			$this->UserName=Yii::app()->user->id;
		}

        $this->Plan_count_day= self::getCountDay($this->Plan_start_date,$this->Plan_end_date);

//        if(!empty($this->Plan_start_date) && !empty($this->Plan_end_date))
//            $this->Plan_hour=((strtotime($this->Plan_end_date)-strtotime($this->Plan_start_date))/3600)+24;

		return parent::beforeSave();
	}

    public static function getCountDay($startTime,$endTime){
        $count=0;
//        $begin = new DateTime( $startTime );
//        $end = new DateTime( $endTime );
//        $interval = DateInterval::createFromDateString('1 day');
//        $period = new DatePeriod($begin, $interval, $end);
        for ( $i = strtotime($startTime); $i <= strtotime($endTime); $i = $i + 86400 ) {
            if(!Holiday::CheckHoliday(date( 'Y', $i ),date( 'm', $i ),date( 'd', $i )))
                $count++;
        }
        return $count;
    }

    public static function maxLevel($Plan_id){
        $model=Plan::model()->findByPk($Plan_id);
        if(!empty($model->Plan_level_three)){
            $maxLevel = Yii::app()->db->createCommand("select count(*) from plan where plan.Plan_level_three=".$model->Plan_level_three." and Plan_remove='N'")->queryScalar();
        }elseif(!empty($model->Plan_level_two)){
            $maxLevel = Yii::app()->db->createCommand("select count(*) from plan where plan.Plan_level_two=".$model->Plan_level_two." and Plan_remove='N'")->queryScalar();
        }elseif(!empty($model->Plan_level_one)){
            $maxLevel = Yii::app()->db->createCommand("select count(*) from plan where plan.Plan_level_one=".$model->Plan_level_one." and Plan_remove='N'")->queryScalar();
        }elseif(!empty($model->Project_id)){
            $maxLevel = Yii::app()->db->createCommand("select count(*) from plan where plan.Project_id=".$model->Project_id." and Plan_remove='N'")->queryScalar();
        }
        $model->Plan_level=$maxLevel;
        $model->save();
    }

    public static function sumHourPlanId($Plan_id,$type){
        $sum=0;
        if($type=='Plan_level_three'){
            $sumPrice = Yii::app()->db->createCommand("select sum(Plan_hour) as hour from plan where Plan_level_three=".$Plan_id." and Plan_remove='N'")->queryScalar();
        }elseif($type=='Plan_level_two'){
            $sumPrice = Yii::app()->db->createCommand("select sum(Plan_hour) as hour from plan where Plan_level_two=".$Plan_id." and Plan_level_three is null and Plan_remove='N'")->queryScalar();
        }elseif($type=='Plan_level_one'){
            $sumPrice = Yii::app()->db->createCommand("select sum(Plan_hour) as hour from plan where Plan_level_one=".$Plan_id." and Plan_level_two is null and Plan_level_three is null and Plan_remove='N'")->queryScalar();
        }
        $sum=intval($sumPrice);
        return $sum;
    }

    public static function updateHourPlan($Plan_id){
        $dataPlan=Plan::model()->findByPk($Plan_id);
        if (!empty($dataPlan->Plan_level_three)) {
            Plan::model()->updateByPk($dataPlan->Plan_level_three, array('Plan_hour' => self::sumHourPlanId($dataPlan->Plan_level_three,'Plan_level_three')));
        }
        if (!empty($dataPlan->Plan_level_two)) {
            Plan::model()->updateByPk($dataPlan->Plan_level_two, array('Plan_hour' => self::sumHourPlanId($dataPlan->Plan_level_two,'Plan_level_two')));
        }
        if (!empty($dataPlan->Plan_level_one)) {
           Plan::model()->updateByPk($dataPlan->Plan_level_one, array('Plan_hour' => self::sumHourPlanId($dataPlan->Plan_level_one,'Plan_level_one')));
        }
    }

    public static function checkSubPlan($Plan_id){
        if(!empty($Plan_id)) {
            $dataPlan = Plan::model()->findAll('t.Plan_level_one=? or t.Plan_level_two=? or t.Plan_level_three=?', array($Plan_id, $Plan_id, $Plan_id));
            if (!empty($dataPlan)) {
                return true;
            } else {
                return false;
            }
        }else{
            return false;
        }
    }

    public static function checkPlanDate($Plan_id,$year,$month,$day){
        $date = new DateTime();
        $date->setDate($year,$month,$day);
        $criteriawork = new CDbCriteria;
        $criteriawork->select='Plan_start_date,Plan_end_date,Plan_status,Plan_create_date,Plan_hour';
        $criteriawork->compare('Plan_remove', 'N', FALSE);
        $criteriawork->compare('Plan_id', $Plan_id, FALSE);
        if(!empty($typeSearch) && !empty($dateWeek['start']) && !empty($dateWeek['end'])) {
            $criteriawork->addCondition('Plan_start_date <= "' . $date->format('Y-m-d 00:00:00') . '"');
            $criteriawork->addCondition('Plan_end_date >= "' . $date->format('Y-m-d 23:59:59') . '"');
        }
        return Plan::model()->find($criteriawork);
    }



    public static function checkDatePlan($id,$day,$month,$year){
        $status=false;
        $dataPlan=self::checkPlanDate($id,$day,$month,$year);
        if(!empty($dataPlan)){
                $dateNow = new DateTime();
                $dayNow=$dateNow->format('d');
                $monthNow=$dateNow->format('m');
                $yearNow=$dateNow->format('Y');
                $dateNow->setDate($yearNow, $monthNow, $dayNow);
                $date = new DateTime();
                $date->setDate($year, $month, $day);
                $paymentDate = strtotime($date->format('Y-m-d'));
                $contractDateNow = strtotime($dateNow->format('Y-m-d'));
                $contractDateBegin = strtotime($dataPlan->Plan_start_date);
                $contractDateEnd = strtotime($dataPlan->Plan_end_date);
                $contractDateCreate = strtotime($dataPlan->Plan_create_date);
                    if ($paymentDate >= $contractDateBegin && $paymentDate <= $contractDateEnd) {

                        $userTime = $paymentDate - $contractDateBegin;
                        $maxTime = $contractDateEnd - $contractDateBegin;
                        if(((self::CheckWorkPlanUser($id)/3600)>$dataPlan->Plan_hour)){
                            $status = 'warning';
                        }else if ($userTime !== 0 && $maxTime !== 0 && $contractDateNow==$paymentDate) {
                            $status = 'success';
                        } elseif ($dataPlan->Plan_status == 'success') {
                            $status = 'success';
                        } elseif ($day > $contractDateEnd && $contractDateNow > $contractDateEnd) {
                            $status = 'danger';
                        }elseif($contractDateNow==$paymentDate){
                            $status = 'success';
                        } else {
                            $status = 'info';
                        }
                    } else {
                        if($dataPlan->Plan_status == 'success' and $contractDateNow==$paymentDate){
                            $status = "success";
                        }elseif ($dataPlan->Plan_status == 'plan' and $day == $dayNow and $contractDateEnd < $contractDateNow and $contractDateBegin <= $paymentDate) {
                            $status = "success";
                        } else if ($dataPlan->Plan_status == 'plan' and $paymentDate <= $contractDateNow and $contractDateEnd < $contractDateNow and $contractDateBegin <= $paymentDate) {
                            $status = 'danger';
                        }else if($dataPlan->Plan_status == 'success' and $paymentDate > $contractDateEnd and $paymentDate<$contractDateCreate){
                            $status = 'danger';
                        }
                    }
            }
        return $status;
    }

    public static function GetTitlePlan($id){
        $dataPlan=Plan::model()->findByPk($id);
        return $dataPlan->Plan_title;
    }

    public static function GetPathPlan($id){
        $string="";
        $plan=Plan::model()->findByPk($id);
        if(!empty($plan->Project_id))
            $string.=Project::GetNameProject($plan->Project_id);

        if(!empty($plan->Plan_level_one))
            $string.='>'.Plan::GetTitlePlan($plan->Plan_level_one);

        if(!empty($plan->Plan_level_two))
            $string.='>'.Plan::GetTitlePlan($plan->Plan_level_two);

        if(!empty($plan->Plan_level_three))
            $string.='>'.Plan::GetTitlePlan($plan->Plan_level_three);

        $string.='>'.$plan->Plan_title;
        return $string;
    }

    public static function getArrayMonth($Plan_id,$monthNow,$yearNow){
        $month=Workdetail::getDateTimeNowMonth();
        $year=Workdetail::getDateTimeNowYear();
        $dataReturn=array();
        $icon='';
        $status='';
        if($month==$monthNow && $year==$yearNow){
            $day=Workdetail::getDateTimeNowDay();
        }else{
            $day=Workdetail::checkNumberOfDays($year,$month);
        }
        for ($i = 1; $i <= Workdetail::checkNumberOfDays($yearNow,$monthNow); $i++) {
            $icon='';
            $status='';
            if (Holiday::CheckHoliday($yearNow, $monthNow, $i)) {
                $status = 'active';
            } else {
                $status = Plan::checkDatePlan($Plan_id, $i, $monthNow, $yearNow);
//                if($status!==false && $i==$day && $status=='warning'){
//                    $icon='glyphicon glyphicon-fire';
//                }elseif($status!==false && $i==$day){
//                    $icon='glyphicon glyphicon-time';
//                }
            }
//            $dataReturn[$i]['icon']=$icon;
            $dataReturn[$i]['status']=$status;
        }
        return $dataReturn;
    }

    public static function GetPlanByProjectId($id,$monthNow,$yearNow){
        $criteriaPlan = new CDbCriteria;
        $criteriaPlan->compare('Plan_remove', 'N', FALSE);
        $criteriaPlan->compare('Project_id', $id, FALSE);
        $criteriaPlan->order=('Plan_level_one,Plan_level_two,Plan_level_three,Plan_level');
        $dataPlan=Plan::model()->findAll($criteriaPlan);
        $dataReturn=array();
        if(!empty($dataPlan)) {
            foreach ($dataPlan as $key => $value) {
                if (!empty($value->Plan_id) && empty($value->Plan_level_one) && empty($value->Plan_level_two) && empty($value->Plan_level_three)) {
                    $dataReturn[$value->Plan_id]['data'] = $value;
                    $dataReturn[$value->Plan_id]['day']=self::getArrayMonth($value->Plan_id,$monthNow,$yearNow);
                }elseif(!empty($value->Plan_id) && !empty($value->Plan_level_one) && empty($value->Plan_level_two) && empty($value->Plan_level_three)){
                    if(!empty($dataReturn[$value->Plan_level_one])){
                        $dataReturn[$value->Plan_level_one][$value->Plan_id]['data'] = $value;
                        $dataReturn[$value->Plan_level_one][$value->Plan_id]['day'] = self::getArrayMonth($value->Plan_id,$monthNow,$yearNow);
                    }
                }elseif(!empty($value->Plan_id) && !empty($value->Plan_level_one) && !empty($value->Plan_level_two) && empty($value->Plan_level_three)){
                    if(!empty($dataReturn[$value->Plan_level_one][$value->Plan_level_two])){
                        $dataReturn[$value->Plan_level_one][$value->Plan_level_two][$value->Plan_id]['data'] = $value;
                        $dataReturn[$value->Plan_level_one][$value->Plan_level_two][$value->Plan_id]['day'] = self::getArrayMonth($value->Plan_id,$monthNow,$yearNow);
                    }
                }elseif(!empty($value->Plan_id) && !empty($value->Plan_level_one) && !empty($value->Plan_level_two) && !empty($value->Plan_level_three)){
                    if(!empty($dataReturn[$value->Plan_level_one][$value->Plan_level_two][$value->Plan_level_three])){
                        $dataReturn[$value->Plan_level_one][$value->Plan_level_two][$value->Plan_level_three][$value->Plan_id]['data'] = $value;
                        $dataReturn[$value->Plan_level_one][$value->Plan_level_two][$value->Plan_level_three][$value->Plan_id]['day'] = self::getArrayMonth($value->Plan_id,$monthNow,$yearNow);
                    }
                }
            }
        }

//        foreach($dataReturn as $key=>$value){
//            for ($i = 1; $i <= 31; $i++) {
//                var_dump($value['day'][$i]['icon']);
//                echo "<br><br>";
//            }
//        }
//        exit;

        return $dataReturn;
    }

    public static function CheckWorkPlanUser($Plan_id){


        $stringSQL="select sum(plan_staff.Plan_staff_hour)/3600 as sumHour
            from plan
            INNER JOIN plan_staff on (plan_staff.Plan_id=plan.Plan_id)
            INNER JOIN staff on (staff.UserName=plan_staff.UserName)
            where plan.Plan_remove='N' and (plan.Plan_id=".$Plan_id." || plan.Plan_level_one=".$Plan_id." || plan.Plan_level_two=".$Plan_id." || plan.Plan_level_three=".$Plan_id.")";

//        $dataPlan=Plan::model()->findByPk($Plan_id);
//        $stringSQL="select sum(plan_staff.Plan_staff_hour) as sumHouse from plan INNER JOIN plan_staff on (plan_staff.Plan_id=plan.Plan_id) where";
//        if(!empty($dataPlan->Plan_id) && !empty($dataPlan->Project_id) && empty($dataPlan->Plan_level_one) && empty($dataPlan->Plan_level_two) && empty($dataPlan->Plan_level_three)){
//            $stringSQL.=" plan.Project_id=".$dataPlan->Project_id." and plan.Plan_level_one=".$dataPlan->Plan_id;
//        }elseif(!empty($dataPlan->Plan_id) && !empty($dataPlan->Project_id) && !empty($dataPlan->Plan_level_one) && empty($dataPlan->Plan_level_two) && empty($dataPlan->Plan_level_three)) {
//            $stringSQL .= " plan.Project_id=" . $dataPlan->Project_id . " and plan.Plan_level_one=" . $dataPlan->Plan_level_one . " and plan.Plan_level_two=" . $dataPlan->Plan_id;
//        }elseif(!empty($dataPlan->Plan_id) && !empty($dataPlan->Project_id) && !empty($dataPlan->Plan_level_one) && !empty($dataPlan->Plan_level_two) && empty($dataPlan->Plan_level_three)){
//            $stringSQL .= " plan.Project_id=" . $dataPlan->Project_id . " and plan.Plan_level_one=" . $dataPlan->Plan_level_one . " and plan.Plan_level_two=" . $dataPlan->Plan_level_two." and plan.Plan_level_three=".$dataPlan->Plan_id;
//        }elseif(!empty($dataPlan->Plan_id) && !empty($dataPlan->Project_id) && !empty($dataPlan->Plan_level_one) && !empty($dataPlan->Plan_level_two) && !empty($dataPlan->Plan_level_three)){
//            $stringSQL .= " plan.Project_id=" . $dataPlan->Project_id . " and plan.Plan_level_one=" . $dataPlan->Plan_level_one . " and plan.Plan_level_two=" . $dataPlan->Plan_level_two." and plan.Plan_level_three=".$dataPlan->Plan_level_three." and plan.Plan_id=".$dataPlan->Plan_id;
//        }

//        if($Plan_id==20){
//            echo $stringSQL;
//            exit;
//        }


        $list= Yii::app()->db->createCommand($stringSQL)->queryRow();
        return intval($list['sumHour']);
    }

    public static function GetAllPlanByProjectId($id){
        $dataReturn=array();
        $dateNew=new DateTime('Now');
        $dateNew->setDate($dateNew->format('Y'),1,1);
        $criteriaPlan = new CDbCriteria;
        $criteriaPlan->condition="Project_id = '".$id."' and Plan_remove='N'";
//        $criteriaPlan->compare('Plan_remove', 'N', FALSE);
//        $criteriaPlan->compare('Project_id', $id, FALSE);
        $criteriaPlan->order=('Plan_level_one,Plan_level_two,Plan_level_three,Plan_level');
        $dataPlan=Plan::model()->findAll($criteriaPlan);
        if(!empty($dataPlan)) {
            foreach ($dataPlan as $key => $value) {
                $dateStart = new DateTime($value->Plan_start_date);
                $dateEnd = new DateTime($value->Plan_end_date);
                $data = array();
                $data['Plan_id'] = $value->Plan_id;
                $data['Plan_level_one'] = $value->Plan_level_one;
                $data['Plan_level_two'] = $value->Plan_level_two;
                $data['Plan_level_three'] = $value->Plan_level_three;
                $data['Plan_title'] = $value->Plan_title;
                $data['minMargin'] = 0;
                $data['min'] = 0;
                $data['margin-left'] = (((strtotime($dateStart->format('Y-m-d')) - strtotime($dateNew->format('Y-m-d'))) / (60 * 60 * 24)) * 40);
                $data['width'] = (((strtotime($dateEnd->format('Y-m-d')) - strtotime($dateStart->format('Y-m-d'))) / (60 * 60 * 24)) * 40);
                if (empty($value->Plan_level_one) && empty($value->Plan_level_two) && empty($value->Plan_level_three)) {
                    $dataReturn[$value->Plan_id]['data'] = $data;
                } elseif (!empty($value->Plan_level_one) && !empty($value->Plan_level_two) && !empty($value->Plan_level_three)) {
                    if (($dataReturn[$value->Plan_level_one]['data']['min'] < ($data['margin-left'] + $data['width'])) or $dataReturn[$value->Plan_level_one]['data']['min'] == 0) {
                        $dataReturn[$value->Plan_level_one]['data']['min'] = ($data['margin-left'] + $data['width']);
                    }
                    if (($dataReturn[$value->Plan_level_one][$value->Plan_level_two]['data']['min'] < ($data['margin-left'] + $data['width'])) or $dataReturn[$value->Plan_level_one][$value->Plan_level_two]['data']['min'] == 0) {
                        $dataReturn[$value->Plan_level_one][$value->Plan_level_two]['data']['min'] = ($data['margin-left'] + $data['width']);
                    }
                    if (($dataReturn[$value->Plan_level_one][$value->Plan_level_two][$value->Plan_level_three]['data']['minMargin'] > $data['margin-left']) or $dataReturn[$value->Plan_level_one][$value->Plan_level_two][$value->Plan_level_three]['data']['minMargin'] == 0) {
                        $dataReturn[$value->Plan_level_one][$value->Plan_level_two][$value->Plan_level_three]['data']['minMargin'] = $data['margin-left'];
                    }
                    if ($dataReturn[$value->Plan_level_one][$value->Plan_level_two][$value->Plan_level_three]['data']['min'] < ($data['margin-left'] + $data['width']) or $dataReturn[$value->Plan_level_one][$value->Plan_level_two][$value->Plan_level_three]['data']['min'] == 0) {
                        $dataReturn[$value->Plan_level_one][$value->Plan_level_two][$value->Plan_level_three]['data']['min'] = $data['margin-left'];
                    }
                    $dataReturn[$value->Plan_level_one][$value->Plan_level_two][$value->Plan_level_three][$value->Plan_id]['data'] = $data;
                } elseif (!empty($value->Plan_level_one) && !empty($value->Plan_level_two)) {
                    if ($dataReturn[$value->Plan_level_one]['data']['min'] < ($data['margin-left'] + $data['width']) or $dataReturn[$value->Plan_level_one]['data']['min'] == 0) {
                        $dataReturn[$value->Plan_level_one]['data']['min'] = ($data['margin-left'] + $data['width']);
                    }
                    if (($dataReturn[$value->Plan_level_one][$value->Plan_level_two]['data']['minMargin'] > $data['margin-left']) or $dataReturn[$value->Plan_level_one][$value->Plan_level_two]['data']['minMargin'] == 0) {
                        $dataReturn[$value->Plan_level_one][$value->Plan_level_two]['data']['minMargin'] = $data['margin-left'];
                    }
                    if ($dataReturn[$value->Plan_level_one][$value->Plan_level_two]['data']['min'] < ($data['margin-left'] + $data['width']) or $dataReturn[$value->Plan_level_one][$value->Plan_level_two]['data']['min'] == 0) {
                        $dataReturn[$value->Plan_level_one][$value->Plan_level_two]['data']['min'] = ($data['margin-left'] + $data['width']);
                    }
                    $dataReturn[$value->Plan_level_one][$value->Plan_level_two][$value->Plan_id]['data'] = $data;
                } elseif (!empty($value->Plan_level_one)) {
                    if (($dataReturn[$value->Plan_level_one]['data']['minMargin'] > $data['margin-left']) or $dataReturn[$value->Plan_level_one]['data']['minMargin'] == 0) {
                        $dataReturn[$value->Plan_level_one]['data']['minMargin'] = $data['margin-left'];
                    }
                    if ($dataReturn[$value->Plan_level_one]['data']['min'] < ($data['margin-left'] + $data['width']) or $dataReturn[$value->Plan_level_one]['data']['min'] == 0) {
                        $dataReturn[$value->Plan_level_one]['data']['min'] = ($data['margin-left'] + $data['width']);
                    }
                    $dataReturn[$value->Plan_level_one][$value->Plan_id]['data'] = $data;
                }
            }
        }
        return $dataReturn;
    }

	public static function GetPlanByProject(){
		Yii::app()->session;
		if(!empty(Yii::app()->session['Project_id'])){
			$criteriaPlan = new CDbCriteria;
			$criteriaPlan->compare('Plan_remove', 'N', FALSE);
			$criteriaPlan->compare('Project_id', Yii::app()->session['Project_id'], FALSE);
			$criteriaPlan->order=('Plan_level_one,Plan_level_two,Plan_level_three,Plan_level');
			$dataPlan=Plan::model()->findAll($criteriaPlan);
			$dataReturn=array();
			foreach($dataPlan as $key=>$value){
				if(empty($value->Plan_level_one)&&empty($value->Plan_level_two)&&empty($value->Plan_level_three)){
					$dataReturn[$value->Plan_id]['data']=$value;
                }elseif(!empty($value->Plan_level_one) && !empty($value->Plan_level_two) && !empty($value->Plan_level_three)){
                    $dataReturn[$value->Plan_level_one][$value->Plan_level_two][$value->Plan_level_three][$value->Plan_id]['data']=$value;
				}elseif(!empty($value->Plan_level_one) && !empty($value->Plan_level_two)){
					$dataReturn[$value->Plan_level_one][$value->Plan_level_two][$value->Plan_id]['data']=$value;
				}elseif(!empty($value->Plan_level_one)){
					$dataReturn[$value->Plan_level_one][$value->Plan_id]['data']=$value;
				}
			}
			return $dataReturn;
		}else{
			return null;
		}
	}

    public static function SumPlanThree(){
        $sumPrice = Yii::app()->db->createCommand('select id_product,sum(product_count) as sumProduct from bill_productin_detail where ' . $string . ' group by id_product')->queryAll();
    }

    public static function SumPlan($Plan_id,$type){
        $sumThree=0;
        $sumHour=0;
        $dataPlan=Plan::model()->with('planStaff')->findAll('t.Plan_id=? or t.Plan_level_one=? or t.Plan_level_two=? or t.Plan_level_three=?',array($Plan_id,$Plan_id,$Plan_id,$Plan_id));
        foreach($dataPlan as $key=>$value){
            if($value->Plan_remove=='N' && $type=='three') {
                    $sumThree += $value->Plan_hour;
                    if(!empty($value->planStaff)){
                        foreach($value->planStaff as $SPkey=>$SPvalue){
                            $sumHour+=$SPvalue->Plan_staff_hour;
                        }
                    }
            }elseif($value->Plan_remove=='N' && $type=='two') {
                if (!empty($value->Plan_level_three)) {
                    $sumThree += $value->Plan_hour;
                    if(!empty($value->planStaff)){
                        foreach($value->planStaff as $SPkey=>$SPvalue){
                            $sumHour+=$SPvalue->Plan_staff_hour;
                        }
                    }
                }elseif(!empty($value->Plan_level_two) && empty($value->Plan_level_three)) {
                    $dataPlan=Plan::model()->findAll('Plan_level_three=? && Plan_remove="N"',array($value->Plan_id));
                    if(empty($dataPlan)) {
                        $sumThree += $value->Plan_hour;
                        if(!empty($value->planStaff)){
                            foreach($value->planStaff as $SPkey=>$SPvalue){
                                $sumHour+=$SPvalue->Plan_staff_hour;
                            }
                        }
                    }
                }
            }elseif($value->Plan_remove=='N' && $type=='one') {
                if (!empty($value->Plan_level_three)) {
                    $sumThree += $value->Plan_hour;
                    if(!empty($value->planStaff)){
                        foreach($value->planStaff as $SPkey=>$SPvalue){
                            $sumHour+=$SPvalue->Plan_staff_hour;
                        }
                    }
                }elseif(!empty($value->Plan_level_two) && empty($value->Plan_level_three)) {
                    $dataPlan = Plan::model()->findAll('Plan_level_three=? && Plan_remove="N"', array($value->Plan_id));
                    if (empty($dataPlan)) {
                        $sumThree += $value->Plan_hour;
                        if(!empty($value->planStaff)){
                            foreach($value->planStaff as $SPkey=>$SPvalue){
                                $sumHour+=$SPvalue->Plan_staff_hour;
                            }
                        }
                    }
                }elseif(!empty($value->Plan_level_one) && empty($value->Plan_level_two) && empty($value->Plan_level_three)){
                    $dataPlan = Plan::model()->findAll('Plan_level_two=? && Plan_level_three is null && Plan_remove="N"', array($value->Plan_id));
                    if (empty($dataPlan)) {
                        $sumThree += $value->Plan_hour;
                        if(!empty($value->planStaff)){
                            foreach($value->planStaff as $SPkey=>$SPvalue){
                                $sumHour+=$SPvalue->Plan_staff_hour;
                            }
                        }
                    }
                }
            }elseif($value->Plan_remove=='N' && $type=='plan') {
                if (!empty($value->Plan_level_three)) {
                    $sumThree += $value->Plan_hour;
                    if(!empty($value->planStaff)){
                        foreach($value->planStaff as $SPkey=>$SPvalue){
                            $sumHour+=$SPvalue->Plan_staff_hour;
                        }
                    }
                }elseif(!empty($value->Plan_level_two) && empty($value->Plan_level_three)) {
                    $dataPlan = Plan::model()->findAll('Plan_level_three=? && Plan_remove="N"', array($value->Plan_id));
                    if (empty($dataPlan)) {
                        $sumThree += $value->Plan_hour;
                        if(!empty($value->planStaff)){
                            foreach($value->planStaff as $SPkey=>$SPvalue){
                                $sumHour+=$SPvalue->Plan_staff_hour;
                            }
                        }
                    }
                }elseif(!empty($value->Plan_level_one) && empty($value->Plan_level_two) && empty($value->Plan_level_three)){
                    $dataPlan = Plan::model()->findAll('Plan_level_two=? && Plan_level_three is null && Plan_remove="N"', array($value->Plan_id));
                    if (empty($dataPlan)) {
                        $sumThree += $value->Plan_hour;
                        if(!empty($value->planStaff)){
                            foreach($value->planStaff as $SPkey=>$SPvalue){
                                $sumHour+=$SPvalue->Plan_staff_hour;
                            }
                        }
                    }
                }elseif(empty($value->Plan_level_one) && empty($value->Plan_level_two) && empty($value->Plan_level_three)){
                    $dataPlan = Plan::model()->findAll('Plan_level_one=? && Plan_level_two is null && Plan_level_three is null && Plan_remove="N"', array($value->Plan_id));
                    if (empty($dataPlan)) {
                        $sumThree += $value->Plan_hour;
                        if(!empty($value->planStaff)){
                            foreach($value->planStaff as $SPkey=>$SPvalue){
                                $sumHour+=$SPvalue->Plan_staff_hour;
                            }
                        }
                    }
                }
            }
        }
        return $sumThree.'/'. Workdetail::GetTimeWorkShort($sumHour);
    }
}
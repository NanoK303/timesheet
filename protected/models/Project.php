<?php

Yii::import('application.models._base.BaseProject');

class Project extends BaseProject
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public static function GetNameProject($id){
		$data=self::model()->findByPk($id);
		if(!empty($data)){
			return $data->Project_name;
		}
	}

	public static function GetProjectStaff($Company_id=null){
        if(!empty($Company_id)){
            $dateProject = Project::model()->findAll(array("select" => "Project_id,Project_name,Public", "condition" => "Remove =  'N' and Company_id=".$Company_id, 'order' => 'Project_id ASC'));
        }else {
            $dateProject = Project::model()->findAll(array("select" => "Project_id,Project_name,Public", "condition" => "Remove =  'N'", 'order' => 'Project_id ASC'));
        }
		$dataReturn=Array();
		foreach($dateProject as $key=>$value){
			if(Yii::app()->user->type=='admin'){
				array_push($dataReturn,$value);
			}else if($value->Public=='N' and Groupstaff::CheckGroupProject(Yii::app()->user->id,$value->Project_id)){
				array_push($dataReturn,$value);
			}elseif ($value->Public=='Y'){
				array_push($dataReturn,$value);
			}
		}
		return $dataReturn;
	}
}
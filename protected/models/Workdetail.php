<?php

Yii::import('application.models._base.BaseWorkdetail');

class Workdetail extends BaseWorkdetail
{
	public $StartHour;
	public $StartMinute;
	public $EndHour;
	public $EndMinute;
	public $TypeLeave;
	public $Plan_id;
	public $Plan_level_one;
	public $Plan_level_two;
	public $Plan_level_three;
    public $Plan;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	protected function beforeSave()
	{

		if ($this->RecordDate == null) {
			$this->RecordDate = new CDbExpression('NOW()');
		}

        if($this->UserName_Record==null){
            if(!empty(Yii::app()->user->id)){
                $this->UserName_Record=Yii::app()->user->id;
            }
        }

		if($this->StartHour!==null && $this->StartMinute!==null) {
			$datetime = new DateTime("now", new DateTimeZone('Asia/Bangkok'));
			$datetime->setTime($this->StartHour, $this->StartMinute, 00);
			$this->StartDate = $datetime->format('Y-m-d H:i:s');
		}else{
			if(!empty($this->StartDate))
				$this->StartDate = date('Y-m-d H:i:s', strtotime($this->StartDate));
		}

		if($this->EndHour!==null && $this->EndMinute!==null) {
			$datetime = new DateTime("now", new DateTimeZone('Asia/Bangkok'));
			$datetime->setTime($this->EndHour, $this->EndMinute, 00);
			$this->EndDate = $datetime->format('Y-m-d H:i:s');
		}else {
			if (!empty($this->EndDate))
				$this->EndDate = date('Y-m-d H:i:s', strtotime($this->EndDate));
		}

		if($this->TypeLeave!==''&&!empty($this->TypeLeave)){
			$this->Detail=$this->TypeLeave.','.$this->Detail;
		}

		if(!empty($this->StartDate) && !empty($this->EndDate))
			$this->Value=(strtotime($this->EndDate)-strtotime($this->StartDate));

        if($this->TypeWork=="request"){
            $day=self::getDateTimeNowDay();
            $month=self::getDateTimeNowMonth();
            $year=self::getDateTimeNowYear();
            $date = new DateTime();
            $day-= 1;
            for ($i=0;$i<=10;$i++){
                if (Holiday::CheckHoliday($year, $month, $day)) {
                    $day-= 1;
                }
            }
            $date->setDate($year, $month, $day);
            $date->setTime($this->StartHour, $this->StartMinute, 00);
            $this->StartDate = $date->format('Y-m-d H:i:s');
            $date->setTime($this->EndHour, $this->EndMinute, 00);
            $this->EndDate = $date->format('Y-m-d H:i:s');
        }

		return parent::beforeSave();
	}

	public static function checkWorkDetailByDate(){
        $day=intval(self::getDateTimeNowDay());
        $month=self::getDateTimeNowMonth();
        $year=self::getDateTimeNowYear();
        $date = new DateTime();
        $day-= 1;
        for ($i=0;$i<=10;$i++){
            if (Holiday::CheckHoliday($year, $month, $day)) {
                $day-= 1;
            }
        }
        $date->setDate($year, $month, $day);
        $criteriawork = new CDbCriteria;
        $criteriawork->compare('Remove', 'N', FALSE);
        $criteriawork->compare('UserName', Yii::app()->user->id, FALSE);
        $criteriawork->addCondition('StartDate >= "' . $date->format('Y-m-d 00:00:00') . '" ');
        $criteriawork->addCondition('EndDate <= "' . $date->format('Y-m-d 23:59:59') . '" ');
        $criteriawork->addCondition("TypeWork != 'request'");
        $dataWorkDetail=self::model()->find($criteriawork);
//        var_dump($date->format('Y-m-d 00:00:00')."::".$date->format('Y-m-d 23:59:59'));
//        exit;
        if(!empty($dataWorkDetail)){
            return true;
        }else{
            return false;
        }
    }

    public static function getNotComplete(){
        $dataReturn=array();
        $dataStaff=Staff::model()->findAll('Position !=? and remove=?',array('admin','N'));
        $day=Workdetail::getDateTimeNowDay();
        $month=Workdetail::getDateTimeNowMonth();
        $year=Workdetail::getDateTimeNowYear();
        $reportStaff=Workdetail::CheckHourWorkStaffNullUser('',$month,$year);
        foreach($dataStaff as $key=>$value){
            for ($i = 1; $i <= Workdetail::checkNumberOfDays($year,$month); $i++) {
                if(!Holiday::CheckHoliday($year,$month,$i) && $i<$day) {
                    if (empty($reportStaff[$value->UserName][$i])){
                       $data=array();
                       $data['Name']=$value->Name;
                       $data['LastName']=$value->LastName;
                       $data['day']=$i;
                       $data['status'] = 'NotComplete';
                       array_push($dataReturn,$data);
                    }else{
                        if($reportStaff[$value->UserName][$i]['UserName']!=$reportStaff[$value->UserName][$i]['UserName_Record']){
                            $data=array();
                            $data['Name']=$value->Name;
                            $data['LastName']=$value->LastName;
                            $data['day']=$i;
                            $data['status'] = 'AdminEdit';
                            array_push($dataReturn,$data);
                        }
                    }
                }
            }
        }
        return $dataReturn;
//        foreach($dataReturn as $key=>$value){
//            var_dump($value);
//            echo '<br>';
//            echo '<br>';
//        }
////        var_dump($dataReturn);
//        exit;
    }

    public static function getStaffWorkPlan($Plan_id){
        $list = Yii::app()->db->createCommand("select staff.Name,staff.LastName,staff.Nickname,sum(plan_staff.Plan_staff_hour) as sumHour
            from plan
            INNER JOIN plan_staff on (plan_staff.Plan_id=plan.Plan_id)
            INNER JOIN staff on (staff.UserName=plan_staff.UserName)
            where plan_staff.Plan_staff_remove='N' and plan.Plan_remove='N' and (plan.Plan_id=".$Plan_id." || plan.Plan_level_one=".$Plan_id." || plan.Plan_level_two=".$Plan_id." || plan.Plan_level_three=".$Plan_id.")
            group by plan_staff.UserName")->queryAll();
        if(!empty($list)){
            $string="ผู้ปฏิบัตงาน";
            foreach($list as $key=>$value){
                $string.="<br>".$value['Name']." ".$value['LastName']."  (".$value['Nickname'].") ใช้ไป ".floatval($value['sumHour']/3600);
            }
            return $string;
        }else{
            return false;
        }
    }

    public static function CheckWorkPlanUser($Project_id,$Plan_id,$year,$month,$day){
        $date = new DateTime();
        $date->setDate($year,$month,$day);
        $list= Yii::app()->db->createCommand('select workdetail.UserName,workdetail.Value
        from workdetail
        INNER JOIN plan_staff on (plan_staff.WorkDetail_id=workdetail.WorkDetail_id)
        where Project_id='.$Project_id.' and plan_staff.Plan_id='.$Plan_id.' and  workdetail.StartDate >= "'. $date->format('Y-m-d 00:00:00') .'" and  workdetail.EndDate<="'. $date->format('Y-m-d 23:59:59') .'" and workdetail.TypeWork="plan"')->queryAll();
        return $list;
    }

    public static function CheckEndDate($UserName){
        $dataPlan=Workdetail::model()->findAll('UserName=? and EndDate is null',array($UserName));
        foreach($dataPlan as $key=>$value){
            if((intval(self::formatDate($value->StartDate, 'n')==self::getDateTimeNowMonth()))){
                if ((intval(self::formatDate($value->StartDate, 'j')) < self::getDateTimeNowDay())) {
                    $dataUpdate = Workdetail::model()->findByPk($value->WorkDetail_id);
                    $datetime = new DateTime($dataUpdate->StartDate);
                    $datetime->modify('+1 hour');
                    $dataUpdate->EndDate = $datetime->format('Y-m-d H:i:s');
                    if ($dataUpdate->validate()) {
                        $dataUpdate->save();
                    }
                }
            }elseif((intval(self::formatDate($value->StartDate, 'n')<self::getDateTimeNowMonth()))){
                $dataUpdate = Workdetail::model()->findByPk($value->WorkDetail_id);
                $datetime = new DateTime($dataUpdate->StartDate);
                $datetime->modify('+1 hour');
                $dataUpdate->EndDate = $datetime->format('Y-m-d H:i:s');
                if ($dataUpdate->validate()) {
                    $dataUpdate->save();
                }
            }
        }
    }

	public static function GetSaveLeave($model){

		if(!empty($model->StartDate) && !empty($model->EndDate)) {
			$model->Value = (strtotime($model->EndDate) - strtotime($model->StartDate));
		}

		$datetime = new DateTime($model->StartDate);
		$day=floor(($model->Value/3600)/24);
		$hour=($model->Value%(3600*24))/3600;
//		echo $day." :: ".$hour;
//		exit;
		if($day>0) {
			for ($i = 1; $i <= $day; $i++) {
				if (!Holiday::CheckHoliday(date('Y', strtotime($datetime->format('Y-m-d H:i:s'))),date('n', strtotime($datetime->format('Y-m-d H:i:s'))),date('j', strtotime($datetime->format('Y-m-d H:i:s'))))) {
					$newWorkDetail = New Workdetail();
					$newWorkDetail->UserName = $model->UserName;
					$newWorkDetail->Detail = $model->Detail;
					$newWorkDetail->TypeWork = $model->TypeWork;
					$newWorkDetail->StartDate = $datetime->format('Y-m-d H:i:s');
					if(!empty($model->TypeLeave)){
						$newWorkDetail->TypeLeave=$model->TypeLeave;
					}
					$datetime->modify('+4 hour');
					$newWorkDetail->EndDate = $datetime->format('Y-m-d H:i:s');
					if ($newWorkDetail->validate()) {
						$newWorkDetail->save();
					}else{
						$errores = $newWorkDetail->getErrors();
						var_dump($errores);
						exit;
					}
					$newWorkDetail = New Workdetail();
					$newWorkDetail->UserName = $model->UserName;
					$newWorkDetail->Detail = $model->Detail;
					$newWorkDetail->TypeWork = $model->TypeWork;
					if(!empty($model->TypeLeave)){
						$newWorkDetail->TypeLeave=$model->TypeLeave;
					}
					$datetime->modify('+1 hour');
					$newWorkDetail->StartDate = $datetime->format('Y-m-d H:i:s');
					$datetime->modify('+5 hour');
					$newWorkDetail->EndDate = $datetime->format('Y-m-d H:i:s');
					if ($newWorkDetail->validate()) {
						$newWorkDetail->save();
					}else{
						$errores = $newWorkDetail->getErrors();
						var_dump($errores);
						exit;
					}
					$datetime->modify('+14 hour');
				} else {
					$datetime->modify('+24 hour');
				}
			}
		}

		if($hour>=4){
			if(!Holiday::CheckHoliday(date('Y', strtotime($datetime->format('Y-m-d H:i:s'))),date('n', strtotime($datetime->format('Y-m-d H:i:s'))),date('j', strtotime($datetime->format('Y-m-d H:i:s'))))) {
				$newWorkDetail = New Workdetail();
				$newWorkDetail->UserName = $model->UserName;
				$newWorkDetail->Detail = $model->Detail;
				$newWorkDetail->TypeWork = $model->TypeWork;
				$newWorkDetail->StartDate = $datetime->format('Y-m-d H:i:s');
				$datetime->modify('+4 hour');
				$newWorkDetail->EndDate = $datetime->format('Y-m-d H:i:s');
				if(!empty($model->TypeLeave)){
					$newWorkDetail->TypeLeave=$model->TypeLeave;
				}
//				echo $newWorkDetail->StartDate;
				if ($newWorkDetail->validate()) {
					if($newWorkDetail->save(false)){
//						echo 'true1';
					}else{
						echo 'false1';
					}
				}else{
					echo '<br>Save1<br>';
					$errores = $newWorkDetail->getErrors();
					var_dump($errores);
					exit;
				}
				$hour = $hour - 5;
				if ($hour > 0) {
					$newWorkDetail = New Workdetail();
					$newWorkDetail->UserName = $model->UserName;
					$newWorkDetail->Detail = $model->Detail;
					$newWorkDetail->TypeWork = $model->TypeWork;
					$datetime->modify('+1 hour');
					$newWorkDetail->StartDate = $datetime->format('Y-m-d H:i:s');
					$datetime->modify('+' . $hour . ' hour');
					$newWorkDetail->EndDate = $datetime->format('Y-m-d H:i:s');
					if(!empty($model->TypeLeave)){
						$newWorkDetail->TypeLeave=$model->TypeLeave;
					}
					if ($newWorkDetail->validate()) {
						if($newWorkDetail->save(false)){
//							echo 'true2';
						}else{
							echo 'false2';
						}
					}else{
						echo 'Save2';
						$errores = $newWorkDetail->getErrors();
						var_dump($errores);
						exit;
					}
				}
			}
		}elseif($hour>1){
			if(!Holiday::CheckHoliday(date('Y', strtotime($datetime->format('Y-m-d H:i:s'))),date('n', strtotime($datetime->format('Y-m-d H:i:s'))),date('j', strtotime($datetime->format('Y-m-d H:i:s'))))) {
				$newWorkDetail = New Workdetail();
				$newWorkDetail->UserName = $model->UserName;
				$newWorkDetail->Detail = $model->Detail;
				$newWorkDetail->TypeWork = $model->TypeWork;
				$newWorkDetail->StartDate = $datetime->format('Y-m-d H:i:s');
				$datetime->modify('+' . $hour . ' hour');
				$newWorkDetail->EndDate = $datetime->format('Y-m-d H:i:s');
				if(!empty($model->TypeLeave)){
					$newWorkDetail->TypeLeave=$model->TypeLeave;
				}
				if ($newWorkDetail->validate()) {
					if($newWorkDetail->save(false)){
//						echo 'true3';
					}else{
						echo 'false3';
					}
				}else{
					$errores = $newWorkDetail->getErrors();
					var_dump($errores);
					exit;
				}
			}
		}
	}

	public static function EditWrokDetail($id){
		$dataUpdate=Workdetail::model()->findByPk($id);
		$dataUpdate->StartHour=date('H', strtotime($dataUpdate->StartDate));
		$dataUpdate->StartMinute=date('i', strtotime($dataUpdate->StartDate));
		$dataUpdate->EndHour=date('H', strtotime($dataUpdate->EndDate));
		$dataUpdate->EndMinute=date('i', strtotime($dataUpdate->EndDate));
		return $dataUpdate;
	}

	public static function SumHourProject($id){
        if(!empty($id)) {
            $sumPrice = Yii::app()->db->createCommand("SELECT SUM(`Value`) AS `sum` FROM `workdetail` WHERE `Remove`='N' and Project_id=" . $id . "")->queryAll();
            if (empty($sumPrice[0]['sum'])) {
                return 0;
            } else {
                return $sumPrice[0]['sum'];
            }
        }else{
            return 0;
        }
	}

	public static function SumHourUserName($UserName){
		$sumPrice = Yii::app()->db->createCommand("SELECT SUM(`Value`) AS `sum` FROM `workdetail` WHERE `Remove`='N' and UserName='".$UserName."'")->queryAll();
		if(empty($sumPrice[0]['sum'])){
			return 0;
		}else{
			return $sumPrice[0]['sum'];
		}
	}

	public static function CheckRecordDate($data){
		$datetime = new DateTime("now", new DateTimeZone('Asia/Bangkok'));
		$startDate=$datetime->format('Y-m-d 00:00:00');
		$endDate=$datetime->format('Y-m-d 23:59:59');

		if(strtotime($startDate)<=strtotime($data) && strtotime($data)<=strtotime($endDate)){
			return true;
		}else{
			return false;
		}
	}

	public static function GetTimeWork($data){
		$string='';
		$house=0;
		$minute=0;
		$day=0;

		if($data>=3600){
			$house=floor($data/3600);
			$data=$data-($house*3600);
		}

		if($data>=60){
			$minute=floor($data/60);
		}

		if($day>0) {
			$house += ($day * 9);
		}

		if($house>0) {
			$string .= $house . ' ชั่วโมง ';
		}

		if($minute>0) {
			$string .= $minute . ' นาที';
		}
		return $string;
	}

	public static function  GetFormatTimeHoliday($data){
		$hour=0;
		$day=0;
		if($data>=3600){
			$day=floor($data/(32400));
			$data=$data-($day*32400);
		}

		if($data>=3600){
			$hour=floor($data/(3600));
		}
		return $day.'.'.$hour;
	}

	public static function GetTimeWorkShort($data){
		$string='';
		$house=0;
		$minute=0;
		$day=0;

		if($data>=3600){
			$house=floor($data/3600);
			$data=$data-($house*3600);
		}

		if($data>=60){
			$minute=floor($data/60);
		}

		if($day>0) {
			$house += ($day * 9);
		}

		if($house>=10) {
			$string .= $house;
		}else{
			$string .= '0'.$house;
		}

		if($minute>0) {
			if($minute>=10){
				$string.=':';
				$string .= $minute;
			}else{
				$string.=':0';
				$string .= $minute;
			}
		}else{
			$string.=':00';
		}
		return $string;
	}

	public static function GetWeekday(){
		$datetime = new DateTime("now", new DateTimeZone('Asia/Bangkok'));
		$dayCount=date('w', strtotime($datetime->format('Y-m-d H:i:s')));
		if($dayCount==1){
			$dataReturn['start']=$datetime->format('Y-m-d 00:00:00');
			$datetime->modify('+5 day');
			$dataReturn['end']=$datetime->format('Y-m-d 23:59:59');
		}elseif($dayCount==2){
			$datetime->modify('-1 day');
			$dataReturn['start']=$datetime->format('Y-m-d 00:00:00');
			$datetime->modify('+5 day');
			$dataReturn['end']=$datetime->format('Y-m-d 23:59:59');
		}elseif($dayCount==3){
			$datetime->modify('-2 day');
			$dataReturn['start']=$datetime->format('Y-m-d 00:00:00');
			$datetime->modify('+6 day');
			$dataReturn['end']=$datetime->format('Y-m-d 23:59:59');
		}elseif($dayCount==4){
			$datetime->modify('-3 day');
			$dataReturn['start']=$datetime->format('Y-m-d 00:00:00');
			$datetime->modify('+6 day');
			$dataReturn['end']=$datetime->format('Y-m-d 23:59:59');
		}elseif($dayCount==5){
			$datetime->modify('-4 day');
			$dataReturn['start']=$datetime->format('Y-m-d 00:00:00');
			$datetime->modify('+6 day');
			$dataReturn['end']=$datetime->format('Y-m-d 23:59:59');
		}elseif($dayCount==6){
			$datetime->modify('-5 day');
			$dataReturn['start']=$datetime->format('Y-m-d 00:00:00');
			$datetime->modify('+6 day');
			$dataReturn['end']=$datetime->format('Y-m-d 23:59:59');
		}elseif($dayCount==7){
			$datetime->modify('-6 day');
			$dataReturn['start']=$datetime->format('Y-m-d 00:00:00');
			$datetime->modify('+6 day');
			$dataReturn['end']=$datetime->format('Y-m-d 23:59:59');
		}
		return $dataReturn;
	}

    public static function GetTextNowMonth(){
        $datetime = new DateTime("now", new DateTimeZone('Asia/Bangkok'));
        $monthNames = array("","มกราคม","กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
        return $monthNames[$datetime->format('n')];
    }

	public static function GetYesterday(){
		$dataReturn=array();
		$datetime = new DateTime("now", new DateTimeZone('Asia/Bangkok'));
		$datetime->modify('-1 day');
		$dataReturn['start']=$datetime->format('Y-m-d 00:00:00');
		$dataReturn['end']=$datetime->format('Y-m-d 23:59:59');
		return $dataReturn;
	}

    public static function getDateTimeNowMonth() {
        $datetime = new DateTime("now", new DateTimeZone('Asia/Bangkok'));
        return $datetime->format('n');
    }

    public static function checkNumberOfDays($Year,$Month){
        $date = new DateTime("now", new DateTimeZone('Asia/Bangkok'));
        $date->setDate($Year,$Month,1);
        return intval($date->format('t'));
//        return intval(date('t', strtotime($date->format('Y-m'))));
    }

    public static function GetMonthSet($selectMonthSearch,$selectYearSearch){
        $datetimenow = new DateTime("now", new DateTimeZone('Asia/Bangkok'));
        $datetimenow->setDate($selectYearSearch,$selectMonthSearch,1);
        $CountDayMonthNow=Workdetail::checkNumberOfDays(intval($datetimenow->format('Y')),intval($datetimenow->format('m')))-1;
        $dataReturn=array();
        $datetime = new DateTime("now", new DateTimeZone('Asia/Bangkok'));
        $datetime->setDate(intval($datetimenow->format('Y')),intval($datetimenow->format('m')),1);
        $dataReturn['start']=$datetime->format('Y-m-d 00:00:00');
        $datetime->modify('+'.$CountDayMonthNow.' day');
        $dataReturn['end']=$datetime->format('Y-m-d 23:59:59');
        return $dataReturn;
    }

    public static function GetMonthNow(){
        $datetimenow = new DateTime("now", new DateTimeZone('Asia/Bangkok'));
        $CountDayMonthNow=Workdetail::checkNumberOfDays(intval($datetimenow->format('Y')),intval($datetimenow->format('m')))-1;
        $dataReturn=array();
        $datetime = new DateTime();
        $datetime->setDate(intval($datetimenow->format('Y')),intval($datetimenow->format('m')),1);
        $dataReturn['start']=$datetime->format('Y-m-d 00:00:00');
        $datetime->modify('+'.$CountDayMonthNow.' day');
        $dataReturn['end']=$datetime->format('Y-m-d 23:59:59');
        return $dataReturn;
    }

    public static function GetYearNow(){
        $datetimenow = new DateTime("now", new DateTimeZone('Asia/Bangkok'));
        $datetime = new DateTime();
        $datetime->setDate(intval($datetimenow->format('Y')),1,1);
        $dataReturn['start']=$datetime->format('Y-m-d 00:00:00');
        $datetime->modify('+1 year');
        $dataReturn['end']=$datetime->format('Y-m-d 00:00:00');
        return $dataReturn;
    }

	public static function GetToDay(){
		$dataReturn=array();
		$datetime = new DateTime("now", new DateTimeZone('Asia/Bangkok'));
		$dataReturn['start']=$datetime->format('Y-m-d 00:00:00');
		$dataReturn['end']=$datetime->format('Y-m-d 23:59:59');
		return $dataReturn;
	}

	public static function getDateTimeNowDay() {
		$datetime = new DateTime("now", new DateTimeZone('Asia/Bangkok'));
		return $datetime->format('j');
	}

	public static function getDateTime() {
		$datetime = new DateTime("now", new DateTimeZone('Asia/Bangkok'));
		return $datetime->format('Y-m-d H:i:s');
	}

	public static function getDateTimeNowYear() {
		$datetime = new DateTime("now", new DateTimeZone('Asia/Bangkok'));
		return $datetime->format('Y');
	}

	public static function formatDate($date, $format) {
    $datetime = new DateTime($date, new DateTimeZone('Asia/Bangkok'));
    return $datetime->format($format);
//		$date = date_create($date);
//		$date2 = date_format($date, $format);
//		return $date2;
}

	public static function GetWorkDetail($criteriasum){
		$dataWorkdetail = Workdetail::model()->findAll($criteriasum);
		$dataRetrn=array();
		$dataRetrn['sum']=0;

		foreach($dataWorkdetail as $key=>$value){
			if(Yii::app()->user->type=='admin' || (Yii::app()->user->type!='admin' && Yii::app()->user->id==$value->UserName)) {
				if(Yii::app()->user->type=='admin'){
					$value->Remove = 'A';
				}elseif ((Yii::app()->user->id == $value->UserName && Workdetail::CheckRecordDate($value->RecordDate))) {
					$value->Remove = 'E';
				}
				$dataRetrn['sum'] += $value->Value;
				$value->UserName = Staff::GetNickName($value->UserName);
				$value->Project_id = Project::GetNameProject($value->Project_id);
				$value->Value = Workdetail::GetTimeWork($value->Value);
			}else{
				$value->Remove = 'Y';
			}
		}

		$dataRetrn['sum']=Workdetail::GetTimeWork($dataRetrn['sum']);
		$dataRetrn['status']='true';
		$dataRetrn['data']=$dataWorkdetail;
		return $dataRetrn;
	}

	public static function GetWorkDetailToDay(){
		$typeSearch='StartDate';
		$dateWeek=Workdetail::GetToDay();
		$criteriawork = new CDbCriteria;
		$criteriawork->compare('Remove', 'N', FALSE);
		if(!empty($typeSearch) && !empty($dateWeek['start']) && !empty($dateWeek['end'])) {
			$criteriawork->addCondition(''. $typeSearch .' >= "' . $dateWeek['start'] . '" ');
			$criteriawork->addCondition(''. $typeSearch .' <= "' . $dateWeek['end'] . '" ');
		}
		$criteriawork->order=('StartDate DESC');
        $criteriawork->limit=30;
		return Workdetail::model()->findAll($criteriawork);
	}

	public static function GetWorkDetailYesterDay(){
		$typeSearch='StartDate';
		$dateWeek=Workdetail::GetYesterday();
		$criteriawork = new CDbCriteria;
		$criteriawork->compare('Remove', 'N', FALSE);
		if(!empty($typeSearch) && !empty($dateWeek['start']) && !empty($dateWeek['end'])) {
			$criteriawork->addCondition(''. $typeSearch .' >= "' . $dateWeek['start'] . '" ');
			$criteriawork->addCondition(''. $typeSearch .' <= "' . $dateWeek['end'] . '" ');
		}
		$criteriawork->order=('StartDate DESC');
        $criteriawork->limit=50;
		return Workdetail::model()->findAll($criteriawork);
	}

	public static function GetWorkDetailWeek(){
		$typeSearch='StartDate';
		$dateWeek=Workdetail::GetWeekday();
		$criteriawork = new CDbCriteria;
		$criteriawork->compare('Remove', 'N', FALSE);
		if(!empty($typeSearch) && !empty($dateWeek['start']) && !empty($dateWeek['end'])) {
				$criteriawork->addCondition(''. $typeSearch .' >= "' . $dateWeek['start'] . '" ');
				$criteriawork->addCondition(''. $typeSearch .' <= "' . $dateWeek['end'] . '" ');
		}
		$criteriawork->order=('StartDate DESC');
        $criteriawork->limit=30;
		return Workdetail::model()->findAll($criteriawork);
	}

    public static function GetTimeAllUserName($year=null,$UserName,$type){
        $sumPrice = Yii::app()->db->createCommand("SELECT SUM(`Value`) AS `sum` FROM `workdetail` WHERE `Remove`='N' and UserName='".$UserName."' and TypeWork='".$type."' and YEAR(StartDate) = ".$year." and YEAR(EndDate) = ".$year)->queryAll();
        if(empty($sumPrice[0]['sum'])){
            return 0;
        }else{
            return $sumPrice[0]['sum'];
        }
    }

    public static function GetTimeAllUserNameBy($year=null,$UserName,$type,$leave){
        $sumPrice = Yii::app()->db->createCommand("SELECT SUM(`Value`) AS `sum` FROM `workdetail` WHERE `Detail` LIKE '".$leave."%' and `Remove`='N' and UserName='".$UserName."' and TypeWork='".$type."' and YEAR(StartDate) = ".$year." and YEAR(EndDate) = ".$year)->queryAll();
        if(empty($sumPrice[0]['sum'])){
            return 0;
        }else{
            return $sumPrice[0]['sum'];
        }
    }

    public static function CheckLeaveAllYear(){
        $dataYear=Workdetail::getDateTimeNowYear();
    }

    public static function CheckReportStaff($month=null,$year=null){
        if(!empty($month)&&!empty($year)){
            $dateWeek=Workdetail::GetMonthSet($month,$year);
        }else {
            $dateWeek = Workdetail::GetMonthNow();
        }
        $dateYear = Workdetail::GetYearNow();
        $type='StartDate';
        $criteriawork = new CDbCriteria;
        $criteriawork->compare('Remove', 'N', FALSE);
        $criteriawork->compare('TypeWork', 'leave', FALSE);

        if(!empty($type) && !empty($dateYear['start']) && !empty($dateYear['end'])) {
            $criteriawork->addCondition(''. $type .' >= "' . $dateYear['start'] . '" ');
            $criteriawork->addCondition(''. $type .' <= "' . $dateYear['end'] . '" ');
        }

        if(Yii::app()->user->type != 'admin' && !empty(Yii::app()->user->id)){
            $criteriawork->compare('UserName', Yii::app()->user->id, FALSE);
        }
        $dataWorkDetail = Workdetail::model()->findAll($criteriawork);
        $dataReturn=array();
        foreach($dataWorkDetail as $key=>$value){
            $data = explode(',',$value->Detail);
            if (empty($dataReturn[$value->UserName][$data[0]])) {
                if($value->StartDate>=$dateWeek['start'] and $value->StartDate<=$dateWeek['end'] ) {
                    $dataReturn[$value->UserName][$data[0]]['month'] = 0.5;
                    $dataReturn[$value->UserName][$data[0]]['year'] = 0.5;
                    $dataReturn[$value->UserName]['sumMonth'] = 0.5;
                }else{
                    $dataReturn[$value->UserName][$data[0]]['year'] = 0.5;
                    $dataReturn[$value->UserName][$data[0]]['month'] = 0;
                    $dataReturn[$value->UserName]['sumMonth'] = 0;
                }
            } else {
                if($value->StartDate>=$dateWeek['start'] and $value->StartDate>=$dateWeek['end'] ) {
                    $dataReturn[$value->UserName][$data[0]]['month'] += 0.5;
                    $dataReturn[$value->UserName]['sumMonth'] += 0.5;
                }
                $dataReturn[$value->UserName][$data[0]]['year'] += 0.5;
            }

            if(!empty($dataReturn[$value->UserName]['sumYear'])) {
                $dataReturn[$value->UserName]['sumYear'] += 0.5;
            }else{
                $dataReturn[$value->UserName]['sumYear'] = 0.5;
            }
        }
        return $dataReturn;
    }

    public static function CheckHourWorkStaffNullUser($status,$month=null,$year=null){
        if(!empty($month)&&!empty($year)){
            $dateWeek=Workdetail::GetMonthSet($month,$year);
        }else {
            $dateWeek = Workdetail::GetMonthNow();
        }

        $type='StartDate';
        $criteriawork = new CDbCriteria;
        $criteriawork->compare('Remove', 'N', FALSE);

        if(!empty($type) && !empty($dateWeek['start']) && !empty($dateWeek['end'])) {
            $criteriawork->addCondition(''. $type .' >= "' . $dateWeek['start'] . '" ');
            $criteriawork->addCondition(''. $type .' <= "' . $dateWeek['end'] . '" ');
        }

//        if(Yii::app()->user->type != 'admin' && Yii::app()->user->type != 'manage' && !empty(Yii::app()->user->id)){
//            $criteriawork->compare('UserName', Yii::app()->user->id, FALSE);
//        }

        if($status=='work'){
            $criteriawork->compare('TypeWork', 'work', FALSE);
            $dataWorkDetail = Workdetail::model()->findAll($criteriawork);
        }elseif($status=='leave'){
            $criteriawork->compare('TypeWork', 'leave', FALSE);
            $dataWorkDetail = Workdetail::model()->findAll($criteriawork);
        }elseif($status=='plan'){
            $criteriawork->compare('TypeWork', 'plan', FALSE);
            $dataWorkDetail = Workdetail::model()->findAll($criteriawork);
        }else{
            $dataWorkDetail = Workdetail::model()->findAll($criteriawork);
        }
        $dataReturn=array();
        $day=0;
        foreach($dataWorkDetail as $key=>$value) {
//            if ((Yii::app()->user->type == 'admin' || Yii::app()->user->type == 'manage') || ((Yii::app()->user->type != 'admin' && Yii::app()->user->type != 'manage') && Yii::app()->user->id == $value->UserName)) {
                if (empty($dataReturn[$value->UserName][(intval(self::formatDate($value[$type], 'j')))])) {
                    $dataReturn[$value->UserName][(intval(self::formatDate($value[$type], 'j')))]['type']=$value->TypeWork;
                    $dataReturn[$value->UserName][(intval(self::formatDate($value[$type], 'j')))]['UserName']=$value->UserName;
                    $dataReturn[$value->UserName][(intval(self::formatDate($value[$type], 'j')))]['UserName_Record']=$value->UserName_Record;
                    if($value->TypeWork=='leave'){
                        $dataReturn[$value->UserName][(intval(self::formatDate($value[$type], 'j')))]['Detail']=$value->Detail;
                        $dataReturn[$value->UserName][(intval(self::formatDate($value[$type], 'j')))]['value'] = 0;
                        $dataReturn[$value->UserName][(intval(self::formatDate($value[$type], 'j')))]['leave'] = $value->Value;
                    }elseif($value->TypeWork==='work' || $value->TypeWork==='plan'){
                        if($value->TypeWork=='plan'){
                            $dataPlanstaff=PlanStaff::model()->with('plan')->find('WorkDetail_id=? and t.UserName=? and t.Plan_staff_remove="N"',array($value['WorkDetail_id'],$value['UserName']));
                            $dataReturn[$value->UserName][(intval(self::formatDate($value[$type], 'j')))]['Detail']=Plan::GetPathPlan($dataPlanstaff->plan->Plan_id);
                        }
                        $dataReturn[$value->UserName][(intval(self::formatDate($value[$type], 'j')))]['value'] = $value->Value;
                        $dataReturn[$value->UserName][(intval(self::formatDate($value[$type], 'j')))]['leave'] = 0;
                    }
                } else {
                    if($value->TypeWork==='work' || $value->TypeWork==='plan') {
                        $dataReturn[$value->UserName][(intval(self::formatDate($value[$type], 'j')))]['value'] += $value->Value;
                    }elseif($value->TypeWork=='leave'){
                        $dataReturn[$value->UserName][(intval(self::formatDate($value[$type], 'j')))]['leave'] += $value->Value;
                    }
                }
//            }
        }

        return $dataReturn;
    }

    public static function CheckLastLeave(){
        $listDate="";
        $endDate="";
        $startDate="";
        $day=0;
        $dataReturn=array();
        $list = Yii::app()->db->createCommand("select DATE(workdetail.StartDate) as dataDate,count(*) as total,Detail from workdetail where Remove='N' and workdetail.TypeWork='leave' and workdetail.UserName='".Yii::app()->user->username."'  GROUP BY DATE(workdetail.StartDate) order by dataDate DESC limit 10")->queryAll();
        foreach($list as $key=>$value){
            if(empty($startDate)) {
                $startDate = $list[(count($list) - 1) - $key]['dataDate'];
                $endDate=$value['dataDate'];
            }else {
                if((strtotime($list[(count($list) - 1) - $key]['dataDate']) - strtotime($listDate))!=86400) {
                    $startDate = $list[(count($list) - 1) - $key]['dataDate'];
                    $day=0;
                }else{
                    $day+=($list[(count($list) - 1) - $key]['total']*0.5);
                }
                $listDate=$list[(count($list) - 1) - $key]['dataDate'];
            }
        }
        $day+=($list[0]['total']*0.5);
        $data = explode(',',$list[0]['Detail']);
        $dataReturn['startDate']=$startDate;
        $dataReturn['endDate']=$endDate;
        $dataReturn['day']=$day;
        $dataReturn['type']=$data[0];
        return $dataReturn;
    }

	public static function CheckHourWorkStaff($status,$month=null,$year=null){
        if(!empty($month)&&!empty($year)){
            $dateWeek=Workdetail::GetMonthSet($month,$year);
        }else {
            $dateWeek = Workdetail::GetMonthNow();
        }

		$type='StartDate';
        $criteriawork = new CDbCriteria;
        $criteriawork->compare('Remove', 'N', FALSE);
        $criteriawork->addCondition("TypeWork != 'request'");

        if(!empty($type) && !empty($dateWeek['start']) && !empty($dateWeek['end'])) {
            $criteriawork->addCondition(''. $type .' >= "' . $dateWeek['start'] . '" ');
            $criteriawork->addCondition(''. $type .' <= "' . $dateWeek['end'] . '" ');
        }

        if(Yii::app()->user->type != 'admin' && Yii::app()->user->type != 'manage' && !empty(Yii::app()->user->id)){
            $criteriawork->compare('UserName', Yii::app()->user->id, FALSE);
        }elseif((Yii::app()->user->type == 'admin' || Yii::app()->user->type == 'manage') && !empty(Yii::app()->user->SubOrganizer_id)){
            $dataStaff=Staff::model()->findAll('SubOrganizer_id=?',array(Yii::app()->user->SubOrganizer_id));
            $string="";
            foreach($dataStaff as $key=>$value){
                if($key==0) {
                    $string .= "UserName = '" . $value->UserName . "'";
                }else{
                    $string .= " or UserName = '" . $value->UserName . "'";
                }
            }
            $criteriawork->addCondition($string);
        }

		if($status=='work'){
            $criteriawork->compare('TypeWork', 'work', FALSE);
			$dataWorkDetail = Workdetail::model()->findAll($criteriawork);
		}elseif($status=='leave'){
            $criteriawork->compare('TypeWork', 'leave', FALSE);
			$dataWorkDetail = Workdetail::model()->findAll($criteriawork);
        }elseif($status=='plan'){
            $criteriawork->compare('TypeWork', 'plan', FALSE);
            $dataWorkDetail = Workdetail::model()->findAll($criteriawork);
		}else{
			$dataWorkDetail = Workdetail::model()->findAll($criteriawork);
		}
		$dataReturn=array();
		$day=0;

		foreach($dataWorkDetail as $key=>$value) {
			if ((Yii::app()->user->type == 'admin' || Yii::app()->user->type == 'manage') || ((Yii::app()->user->type != 'admin' && Yii::app()->user->type != 'manage') && Yii::app()->user->id == $value->UserName)) {
                    if(empty($dataReturn[$value->UserName])){
                        $year=Workdetail::getDateTimeNowYear();
                        $dataReturn[$value->UserName]['totalYearWork']=Workdetail::GetTimeAllUserName($year,$value->UserName,'work');
                        $dataReturn[$value->UserName]['totalYearLeave']=Workdetail::GetTimeAllUserName($year,$value->UserName,'leave');
                        $dataReturn[$value->UserName]['totalYearSick']=Workdetail::GetTimeAllUserNameBy($year,$value->UserName,'leave','ลาป่วย');
                        $dataReturn[$value->UserName]['totalYearOther']=Workdetail::GetTimeAllUserNameBy($year,$value->UserName,'leave','ลาอื่นๆ');
                        $dataReturn[$value->UserName]['totalYearVacation']=Workdetail::GetTimeAllUserNameBy($year,$value->UserName,'leave','ลาพักร้อน');
                        $dataReturn[$value->UserName]['totalYearProduce']=Workdetail::GetTimeAllUserNameBy($year,$value->UserName,'leave','ลาคลอด');
                        $dataReturn[$value->UserName]['totalYearOrdain']=Workdetail::GetTimeAllUserNameBy($year,$value->UserName,'leave','ลาบวช');
                        $dataReturn[$value->UserName]['totalYearMarry']=Workdetail::GetTimeAllUserNameBy($year,$value->UserName,'leave','ลาสมรส');
                        $dataReturn[$value->UserName]['totalYearAbsence']=Workdetail::GetTimeAllUserNameBy($year,$value->UserName,'leave','ขาดงาน');
                    }
			        if (empty($dataReturn[$value->UserName][(intval(self::formatDate($value[$type], 'j')))])) {
						$dataReturn[$value->UserName][(intval(self::formatDate($value[$type], 'j')))]['type']=$value->TypeWork;
                        $dataReturn[$value->UserName][(intval(self::formatDate($value[$type], 'j')))]['UserName']=$value->UserName;
                        $dataReturn[$value->UserName][(intval(self::formatDate($value[$type], 'j')))]['UserName_Record']=$value->UserName_Record;
						if($value->TypeWork=='leave'){
							$dataReturn[$value->UserName][(intval(self::formatDate($value[$type], 'j')))]['Detail']=$value->Detail;
							$dataReturn[$value->UserName][(intval(self::formatDate($value[$type], 'j')))]['value'] = 0;
							$dataReturn[$value->UserName][(intval(self::formatDate($value[$type], 'j')))]['leave'] = $value->Value;
							if(empty($dataReturn[$value->UserName]['totalLeave'])){
                                $dataReturn[$value->UserName]['totalLeave']= $value->Value;
                            }else {
                                $dataReturn[$value->UserName]['totalLeave'] += $value->Value;
                            }
						}elseif($value->TypeWork==='work' || $value->TypeWork==='plan'){
                            if($value->TypeWork=='plan'){
                                $dataPlanstaff=PlanStaff::model()->with('plan')->find('WorkDetail_id=? and t.UserName=? and t.Plan_staff_remove="N"',array($value['WorkDetail_id'],$value['UserName']));
                                if(empty($dataPlanstaff)){
                                    echo $value['WorkDetail_id']." :: ".$value['UserName'];
                                    exit;
                                }
                                $dataReturn[$value->UserName][(intval(self::formatDate($value[$type], 'j')))]['Detail']=Plan::GetPathPlan($dataPlanstaff->plan->Plan_id);
                            }
							$dataReturn[$value->UserName][(intval(self::formatDate($value[$type], 'j')))]['value'] = $value->Value;
                            if(empty( $dataReturn[$value->UserName]['totalValue'])){
                                $dataReturn[$value->UserName]['totalValue'] = $value->Value;
                            }else {
                                $dataReturn[$value->UserName]['totalValue'] += $value->Value;
                            }
							$dataReturn[$value->UserName][(intval(self::formatDate($value[$type], 'j')))]['leave'] = 0;
						}
					} else {
						if($value->TypeWork==='work' || $value->TypeWork==='plan') {
							$dataReturn[$value->UserName][(intval(self::formatDate($value[$type], 'j')))]['value'] += $value->Value;
                            if(empty( $dataReturn[$value->UserName]['totalValue'])){
                                $dataReturn[$value->UserName]['totalValue'] = $value->Value;
                            }else {
                                $dataReturn[$value->UserName]['totalValue'] += $value->Value;
                            }
						}elseif($value->TypeWork=='leave'){
							$dataReturn[$value->UserName][(intval(self::formatDate($value[$type], 'j')))]['leave'] += $value->Value;
                            if(empty($dataReturn[$value->UserName]['totalLeave'])){
                                $dataReturn[$value->UserName]['totalLeave']= $value->Value;
                            }else {
                                $dataReturn[$value->UserName]['totalLeave'] += $value->Value;
                            }
						}
					}
			}
		}
		return $dataReturn;
	}


}
<?php

Yii::import('application.models._base.BaseHoliday');

class Holiday extends BaseHoliday
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public static function CheckHoliday($year,$month,$day){
		$status=false;

		$date = new DateTime("now", new DateTimeZone('Asia/Bangkok'));
		$date->setDate($year,$month,$day);
//		$status=date('w', strtotime($date->format('Y-m-d H:i:s')));

		if($date->format('w')=='6' || $date->format('w')=='0' ){
			$status=true;
		}else{
			$holiday=Holiday::model()->find('holiday_year=? and holiday_month=? and holiday_day=? and holiday_remove=? ',array($year,$month,$day,'N'));
			if(!empty($holiday)){
				$status=true;
			}else {
				$status=false;
			}
		}
		return $status;
	}
}
<?php

Yii::import('application.models._base.BaseLogPlan');

class LogPlan extends BaseLogPlan
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    protected function beforeSave()
    {
        if ($this->LogPlan_date == null) {
            $this->LogPlan_date = new CDbExpression('NOW()');
        }

        if ($this->UserName == null) {
            $this->UserName = Yii::app()->user->id;
        }

        return parent::beforeSave();
    }

}
<?php

Yii::import('application.models._base.BaseResponsible');

class Responsible extends BaseResponsible
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public static function CheckResponsible($username,$day,$month,$year){
        $dataResponsible=Responsible::model()->find('UserName=? && responsible_day=? && responsible_month=? && responsible_year=? && responsible_remove=?',array($username,$day,$month,$year,'N'));
        if(!empty($dataResponsible)){
            $dataStaff=Staff::model()->findByPk($dataResponsible->responsible_user_name);
            return $dataStaff->Name.' '.$dataStaff->LastName.' รับผิดชอบค่าปรับให้';
        }
    }

    public static function getAll($month=null,$year=null){
        if(empty($month)) {
            $month = Workdetail::getDateTimeNowMonth();
        }
        if(empty($year)) {
            $year = Workdetail::getDateTimeNowYear();
        }
        return Responsible::model()->with('responsibleUserName')->findAll('responsible_month=? && responsible_year=? && responsible_remove=?',array($month,$year,'N'));
    }
}
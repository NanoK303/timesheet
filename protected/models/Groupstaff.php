<?php

Yii::import('application.models._base.BaseGroupstaff');

class Groupstaff extends BaseGroupstaff
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public static function CheckGroupProject($UserName,$Project_id){
		$dataGroup=Groupstaff::model()->find('UserName=? and Project_id=? and Remove=?',array($UserName,$Project_id,'N'));
		if(!empty($dataGroup)){
			return true;
		}else{
			return false;
		}
	}

    public static function GetGroupStaffByProjectId($id){
        return Groupstaff::model()->with('userName')->findAll(array('order'=>'t.Position ASC', 'condition'=>'Project_id='.$id.' and t.Remove="N"'));
    }
}
<?php

Yii::import('application.models._base.BasePlanStaff');

class PlanStaff extends BasePlanStaff
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public static function SumPlan($Plan_id,$type){
        $sumThree=0;
        $dataPlan=Plan::model()->with('planStaff')->findAll('Plan_id=? or Plan_level_one=? or Plan_level_two=? or Plan_level_three=?',array($Plan_id,$Plan_id,$Plan_id,$Plan_id));
        foreach($dataPlan as $key=>$value){
            if($value->Plan_remove=='N' && $type=='three') {
                $sumThree += $value->Plan_hour;
            }elseif($value->Plan_remove=='N' && $type=='two') {
                if (!empty($value->Plan_level_three)) {
                    $sumThree += $value->Plan_hour;
                }elseif(!empty($value->Plan_level_two) && empty($value->Plan_level_three)) {
                    $dataPlan=Plan::model()->findAll('Plan_level_three=? && Plan_remove="N"',array($value->Plan_id));
                    if(empty($dataPlan)) {
                        $sumThree += $value->Plan_hour;
                    }
                }
            }elseif($value->Plan_remove=='N' && $type=='one') {
                if (!empty($value->Plan_level_three)) {
                    $sumThree += $value->Plan_hour;
                }elseif(!empty($value->Plan_level_two) && empty($value->Plan_level_three)) {
                    $dataPlan = Plan::model()->findAll('Plan_level_three=? && Plan_remove="N"', array($value->Plan_id));
                    if (empty($dataPlan)) {
                        $sumThree += $value->Plan_hour;
                    }
                }elseif(!empty($value->Plan_level_one) && empty($value->Plan_level_two) && empty($value->Plan_level_three)){
                    $dataPlan = Plan::model()->findAll('Plan_level_two=? && Plan_level_three is null && Plan_remove="N"', array($value->Plan_id));
                    if (empty($dataPlan)) {
                        $sumThree += $value->Plan_hour;
                    }
                }
            }elseif($value->Plan_remove=='N' && $type=='plan') {
                if (!empty($value->Plan_level_three)) {
                    $sumThree += $value->Plan_hour;
                }elseif(!empty($value->Plan_level_two) && empty($value->Plan_level_three)) {
                    $dataPlan = Plan::model()->findAll('Plan_level_three=? && Plan_remove="N"', array($value->Plan_id));
                    if (empty($dataPlan)) {
                        $sumThree += $value->Plan_hour;
                    }
                }elseif(!empty($value->Plan_level_one) && empty($value->Plan_level_two) && empty($value->Plan_level_three)){
                    $dataPlan = Plan::model()->findAll('Plan_level_two=? && Plan_level_three is null && Plan_remove="N"', array($value->Plan_id));
                    if (empty($dataPlan)) {
                        $sumThree += $value->Plan_hour;
                    }
                }elseif(empty($value->Plan_level_one) && empty($value->Plan_level_two) && empty($value->Plan_level_three)){
                    $dataPlan = Plan::model()->findAll('Plan_level_one=? && Plan_level_two is null && Plan_level_three is null && Plan_remove="N"', array($value->Plan_id));
                    if (empty($dataPlan)) {
                        $sumThree += $value->Plan_hour;
                    }
                }
            }
        }
        return $sumThree;
    }
}
<?php

/**
 * This is the model base class for the table "plan".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Plan".
 *
 * Columns in table "plan" available as properties of the model,
 * followed by relations of table "plan" available as properties of the model.
 *
 * @property integer $Plan_id
 * @property string $UserName
 * @property integer $Project_id
 * @property integer $Plan_level_one
 * @property integer $Plan_level_two
 * @property integer $Plan_level_three
 * @property string $Plan_title
 * @property string $Plan_detail
 * @property integer $Plan_hour
 * @property string $Plan_start_date
 * @property string $Plan_end_date
 * @property string $Plan_create_date
 * @property string $Plan_status
 * @property string $Plan_remove
 *
 * @property Plan $planLevelOne
 * @property Plan[] $plans
 * @property Plan $planLevelTwo
 * @property Plan[] $plans1
 * @property Plan $planLevelThree
 * @property Plan[] $plans2
 * @property Project $project
 * @property Staff $userName
 * @property PlanStaff[] $planStaff
 */
abstract class BasePlan extends GxActiveRecord {

    public $LogPlan_detail;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'plan';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'Plan|Plans', $n);
	}

	public static function representingColumn() {
		return 'UserName';
	}

	public function rules() {
		return array(
			array('Project_id,Plan_title,Plan_start_date,Plan_end_date', 'required'),
			array('Project_id, Plan_level_one, Plan_level_two, Plan_level_three, Plan_hour', 'numerical', 'integerOnly'=>true),
			array('UserName', 'length', 'max'=>50),
			array('Plan_title', 'length', 'max'=>250),
			array('Plan_status', 'length', 'max'=>10),
			array('Plan_remove', 'length', 'max'=>1),
			array('Plan_detail, Plan_start_date, Plan_end_date, Plan_create_date', 'safe'),
			array('LogPlan_detail,UserName, Project_id, Plan_level_one, Plan_level_two, Plan_level_three, Plan_title, Plan_detail, Plan_hour, Plan_start_date, Plan_end_date, Plan_create_date, Plan_status, Plan_remove', 'default', 'setOnEmpty' => true, 'value' => null),
			array('LogPlan_detail,Plan_id, UserName, Project_id, Plan_level_one, Plan_level_two, Plan_level_three, Plan_title, Plan_detail, Plan_hour, Plan_start_date, Plan_end_date, Plan_create_date, Plan_status, Plan_remove', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
            'logPlans' => array(self::HAS_MANY, 'LogPlan', 'Plan_id'),
			'planLevelOne' => array(self::BELONGS_TO, 'Plan', 'Plan_level_one'),
			'plans' => array(self::HAS_MANY, 'Plan', 'Plan_level_one'),
			'planLevelTwo' => array(self::BELONGS_TO, 'Plan', 'Plan_level_two'),
			'plans1' => array(self::HAS_MANY, 'Plan', 'Plan_level_two'),
			'planLevelThree' => array(self::BELONGS_TO, 'Plan', 'Plan_level_three'),
			'plans2' => array(self::HAS_MANY, 'Plan', 'Plan_level_three'),
			'project' => array(self::BELONGS_TO, 'Project', 'Project_id'),
			'userName' => array(self::BELONGS_TO, 'Staff', 'UserName'),
            'staff' => array(self::MANY_MANY, 'Staff', 'plan_assign(Plan_id, UserName)'),
			'planStaff' => array(self::HAS_MANY, 'PlanStaff', 'Plan_id'),
		);
	}

    public function pivotModels() {
        return array(
            'staff' => 'PlanAssign',
        );
    }

	public function attributeLabels() {
		return array(
			'Plan_id' => Yii::t('app', 'รหัสแผน'),
			'UserName' => Yii::t('app', 'ผู้สร้าง'),
			'Project_id' => Yii::t('app', 'โครงการ'),
			'Plan_level_one' => Yii::t('app', 'แผนหลัก'),
			'Plan_level_two' => Yii::t('app', 'แผนกรอง'),
			'Plan_level_three' => Yii::t('app', 'แผนงาน'),
			'Plan_title' => Yii::t('app', 'หัวข้อ'),
			'Plan_detail' => Yii::t('app', 'รายละเอียด'),
			'Plan_hour' => Yii::t('app', 'ชั่วโมง'),
			'Plan_start_date' => Yii::t('app', 'เริ่ม'),
			'Plan_end_date' => Yii::t('app', 'ถึง'),
			'Plan_create_date' => Yii::t('app', 'วันสร้าง'),
			'Plan_status' => Yii::t('app', 'สถานะ'),
			'Plan_remove' => Yii::t('app', 'ลบ'),
			'planLevelOne' => Yii::t('app', 'Plan'),
			'plans' => Yii::t('app', 'Plan'),
			'planLevelTwo' => Yii::t('app', 'Plan'),
			'plans1' => Yii::t('app', 'Plan'),
			'planLevelThree' => Yii::t('app', 'Plan'),
			'plans2' =>Yii::t('app', 'Plan'),
			'project' => Yii::t('app', 'Plan'),
			'userName' => Yii::t('app', 'Plan'),
			'planStaff' => Yii::t('app', 'Plan'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('รหัสแผน', $this->Plan_id);
		$criteria->compare('ชื่อผู้สร้าง', $this->UserName);
		$criteria->compare('รหัสโครงการ', $this->Project_id);
		$criteria->compare('แผนหลัก', $this->Plan_level_one);
		$criteria->compare('แผนรอง', $this->Plan_level_two);
		$criteria->compare('แผนรอง', $this->Plan_level_three);
		$criteria->compare('หัวข้อ', $this->Plan_title, true);
		$criteria->compare('รายละเอียด', $this->Plan_detail, true);
		$criteria->compare('ชั่วโมง', $this->Plan_hour);
		$criteria->compare('เริ่ม', $this->Plan_start_date, true);
		$criteria->compare('ถึง', $this->Plan_end_date, true);
		$criteria->compare('วันสร้าง', $this->Plan_create_date, true);
		$criteria->compare('สถานะ', $this->Plan_status, true);
		$criteria->compare('ลบ', $this->Plan_remove, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

    function afterValidate() {

        if(!empty($this->Plan_id) && $this->LogPlan_detail==''){
            $this->addError('LogPlan_detail', 'กรุณากรอกสาเหตการแก้ไข');
        }

        if(!empty($this->Plan_end_date) and !empty($this->Plan_start_date)) {
            if (strtotime($this->Plan_end_date) < strtotime($this->Plan_start_date)) {
                $this->addError('Plan_start_date', 'วันที่เริ่มควรน้อย กว่าวันที่เสร็จ');
            }
        }

        if(!empty($this->Plan_level_one)&&!empty($this->Plan_level_two)&&!empty($this->Plan_level_three)){
            $datPlan=Plan::model()->findByPk($this->Plan_level_three);
            if(!empty($datPlan)){
                if(strtotime($datPlan->Plan_start_date)>strtotime($this->Plan_start_date)){
                    $this->addError('Plan_start_date', 'กรุณาเลือกหลังวันที่ '. date("d/m/Y",strtotime($datPlan->Plan_start_date)));
                }elseif(strtotime($this->Plan_start_date)>strtotime($this->Plan_end_date)){
                    $this->addError('Plan_end_date', 'กรุณาเลือกก่อนวันที่ '.date("d/m/Y",strtotime($this->Plan_start_date)));
                }
            }
        }elseif(!empty($this->Plan_level_one)&&!empty($this->Plan_level_two)&&empty($this->Plan_level_three)){
            $datPlan=Plan::model()->findByPk($this->Plan_level_two);
            if(!empty($datPlan)){
                if(strtotime($datPlan->Plan_start_date)>strtotime($this->Plan_start_date)){
                    $this->addError('Plan_start_date', 'กรุณาเลือกหลังวันที่ '. date("d/m/Y",strtotime($datPlan->Plan_start_date)));
                }elseif(strtotime($this->Plan_start_date)>strtotime($this->Plan_end_date)){
                    $this->addError('Plan_end_date', 'กรุณาเลือกก่อนวันที่ '.date("d/m/Y",strtotime($this->Plan_start_date)));
                }
            }
        }elseif(!empty($this->Plan_level_one)&&empty($this->Plan_level_two)&&empty($this->Plan_level_three)){
            $datPlan=Plan::model()->findByPk($this->Plan_level_one);
            if(!empty($datPlan)){
                if(strtotime($datPlan->Plan_start_date)>strtotime($this->Plan_start_date)) {
                    $this->addError('Plan_start_date', 'กรุณาเลือกหลังวันที่ ' . date("d/m/Y",strtotime($datPlan->Plan_start_date)));
                }elseif(strtotime($this->Plan_start_date)>strtotime($this->Plan_end_date)){
                    $this->addError('Plan_end_date', 'กรุณาเลือกก่อนวันที่ '.date("d/m/Y",strtotime($this->Plan_start_date)));
                }
            }
        }

        return parent::afterValidate();
    }
}
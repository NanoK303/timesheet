<?php

/**
 * This is the model base class for the table "sub_organizer".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "SubOrganizer".
 *
 * Columns in table "sub_organizer" available as properties of the model,
 * followed by relations of table "sub_organizer" available as properties of the model.
 *
 * @property integer $SubOrganizer_id
 * @property integer $Company_id
 * @property string $NameOrganizer
 * @property string $Remove
 *
 * @property Staff[] $staff
 * @property Company $company
 */
abstract class BaseSubOrganizer extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'sub_organizer';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'SubOrganizer|SubOrganizers', $n);
	}

	public static function representingColumn() {
		return 'NameOrganizer';
	}

	public function rules() {
		return array(
            array('Company_id,NameOrganizer', 'required'),
			array('Company_id', 'numerical', 'integerOnly'=>true),
			array('NameOrganizer', 'length', 'max'=>250),
			array('Remove', 'length', 'max'=>1),
			array('Company_id, NameOrganizer, Remove', 'default', 'setOnEmpty' => true, 'value' => null),
			array('SubOrganizer_id, Company_id, NameOrganizer, Remove', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'staff' => array(self::HAS_MANY, 'Staff', 'SubOrganizer_id'),
			'company' => array(self::BELONGS_TO, 'Company', 'Company_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'SubOrganizer_id' => Yii::t('app', 'Sub Organizer'),
			'Company_id' => null,
			'NameOrganizer' => Yii::t('app', 'Name Organizer'),
			'Remove' => Yii::t('app', 'Remove'),
			'staff' => null,
			'company' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('SubOrganizer_id', $this->SubOrganizer_id);
		$criteria->compare('Company_id', $this->Company_id);
		$criteria->compare('NameOrganizer', $this->NameOrganizer, true);
		$criteria->compare('Remove', $this->Remove, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}
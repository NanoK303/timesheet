<?php

Yii::import('application.models._base.BaseStaff');

class Staff extends BaseStaff
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function beforeSave() {

		if ($this->getScenario() == 'Edit' || $this->getScenario() == 'Register') {
			$this->Password = md5($this->Password);
		}

		return parent::beforeSave();
	}

	public static function GetNickName($username){
		$staff=Staff::model()->findByPk($username);
		if(!empty($staff)){
			return $staff->Name.' '.$staff->LastName.' (' .$staff->Nickname.')';
		}else{
			return 'ไม่มีข้อมูล';
		}
	}

	public static function GetNickNameShot($username){
		$staff=Staff::model()->findByPk($username);
		if(!empty($staff)){
			return $staff->Nickname;
		}else{
			return 'ไม่มีข้อมูล';
		}
	}

	public static function CheckUsername($Username){
		$dataStaff=Staff::model()->findByPk($Username);
		if(!empty($dataStaff)){
			return true;
		}else{
			return false;
		}
	}

    public static function CheckEmail($Email){
        $dataStaff=Staff::model()->find('Email=?',array($Email));
        if(!empty($dataStaff)){
            return true;
        }else{
            return false;
        }
    }


}
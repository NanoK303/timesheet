<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
//	public function authenticate()
//	{
//		$users=array(
//			// username => password
//			'demo'=>'demo',
//			'admin'=>'admin',
//		);
//		if(!isset($users[$this->username]))
//			$this->errorCode=self::ERROR_USERNAME_INVALID;
//		elseif($users[$this->username]!==$this->password)
//			$this->errorCode=self::ERROR_PASSWORD_INVALID;
//		else
//			$this->errorCode=self::ERROR_NONE;
//		return !$this->errorCode;
//	}

	public function authenticate()
	{
		$Member=Staff::model()->find('UserName = ? && remove=?', array($this->username, "N"));
		if(!empty($Member)){
			if($Member->Password===md5($this->password)){
				$now = date("Y-m-d H:i:s");
				$this->setState('id', $Member->UserName);
				$this->setState('username', $Member->UserName);
				$this->setState('type', $Member->Position);
                $this->setState('SubOrganizer_id', $Member->SubOrganizer_id);
//				$this->setState('Project_id', null);
//				$this->setState('img',$Member->member_img);

				$session = new CHttpSession;
				$session->open();
				$session['session_key'] = md5($now . $Member->UserName);
				$this->errorCode = self::ERROR_NONE;
			}else{
				$this->errorCode=self::ERROR_PASSWORD_INVALID;
			}
		}else{
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		}
		return !$this->errorCode;
	}

}
<?php
class PlanController extends Controller
{
    public function init() {
        if (Yii::app()->user->isGuest) {
            $this->redirect('/site/login');
        }
    }

    public function actionPlan() {
        $Project_id = Yii::app()->getRequest()->getParam('id', null);

        $list= Yii::app()->db->createCommand("select plan.Plan_id,plan.Plan_title,plan.Plan_status
from plan_assign
INNER JOIN plan ON plan.Plan_id=plan_assign.Plan_id
where plan_assign.UserName='".Yii::app()->user->username."' and plan.Project_id=".$Project_id)->queryAll();

        if(!empty($list)) {
            echo '<option value="">' . Yii::t('main', 'กรุณาเลือก') . '</option>';
            foreach ($list as $k => $v){
                if($v['Plan_status']=='success'){
                    echo '<option value="'.$v['Plan_id'].'">(ปิด) '.$v['Plan_title'].'</option>';
                }else {
                    echo '<option value="'.$v['Plan_id'].'">'.$v['Plan_title'].'</option>';
                }
            }
        }else{
//            echo '<option value="">' . Yii::t('main', 'ไม่มีข้อมูล') . '</option>';
        }
    }

    public function actionPlanOne() {
        $Project_id = Yii::app()->getRequest()->getParam('id', null);
        if(!empty($Project_id)){
            $model = Plan::model()->findAll('Project_id=? and Plan_remove=? and Plan_level_one is null and Plan_level_two is null and Plan_level_three is null',array($Project_id,'N'));
        }
        if(!empty($model)) {
            echo '<option value="">' . Yii::t('main', 'กรุณาเลือก') . '</option>';
            foreach ($model as $k => $v){
                if($v->Plan_status=='success'){
                    echo "<option value='$v->Plan_id'>(ปิด) $v->Plan_title</option>";
                }else {
                    echo "<option value='$v->Plan_id'>$v->Plan_title</option>";
                }
            }
        }else{
//            echo '<option value="">' . Yii::t('main', 'ไม่มีข้อมูล') . '</option>';
        }
    }

    public function actionPlanTwo() {
        $Plan_level_one = Yii::app()->getRequest()->getParam('id', null);
        if(!empty($Plan_level_one)){
            $model = Plan::model()->findAll('Plan_level_one=? and Plan_remove=? and Plan_level_two is null',array($Plan_level_one,'N'));
        }
        if(!empty($model)) {
            echo '<option value="">' . Yii::t('main', 'กรุณาเลือก') . '</option>';
            foreach ($model as $k => $v) {
                if($v->Plan_status=='success'){
                    echo "<option value='$v->Plan_id'>(ปิด) $v->Plan_title</option>";
                }else {
                    echo "<option value='$v->Plan_id'>$v->Plan_title</option>";
                }
            }
        }else{
//            echo '<option value="">' . Yii::t('main', 'ไม่มีข้อมูล') . '</option>';
        }
    }

    public function actionPlanThree() {
        $Plan_level_two = Yii::app()->getRequest()->getParam('id', null);
        if(!empty($Plan_level_two)){
            $model = Plan::model()->findAll('Plan_level_two=? and Plan_remove=? and Plan_level_three is null',array($Plan_level_two,'N'));
        }
        if(!empty($model)) {
            echo '<option value="">' . Yii::t('main', 'กรุณาเลือก') . '</option>';
            foreach ($model as $k => $v) {
                if($v->Plan_status=='success'){
                    echo "<option value='$v->Plan_id'>(ปิด) $v->Plan_title</option>";
                }else {
                    echo "<option value='$v->Plan_id'>$v->Plan_title</option>";
                }
            }
        }else{
//            echo '<option value="">' . Yii::t('main', 'ไม่มีข้อมูล') . '</option>';
        }
    }

    public function actionPlanFour() {
        $Plan_level_three = Yii::app()->getRequest()->getParam('id', null);
        if(!empty($Plan_level_three)){
            $model = Plan::model()->findAll('Plan_level_three=? and Plan_remove=?',array($Plan_level_three,'N'));
        }
        if(!empty($model)) {
            echo '<option value="">' . Yii::t('main', 'กรุณาเลือก') . '</option>';
            foreach ($model as $k => $v) {
                if($v->Plan_status=='success'){
                    echo "<option value='$v->Plan_id'>(ปิด) $v->Plan_title</option>";
                }else {
                    echo "<option value='$v->Plan_id'>$v->Plan_title</option>";
                }
            }
        }else{
//            echo '<option value="">' . Yii::t('main', 'ไม่มีข้อมูล') . '</option>';
        }
    }

    public function actionSuccess(){
        Yii::app()->session;
        $id = Yii::app()->request->getParam('id', null);
        if(!empty($id)){
            $dataUpdate=Plan::model()->findByPk($id);
            if(empty($dataUpdate->Plan_level_one)&&empty($dataUpdate->Plan_level_two)&&empty($dataUpdate->Plan_level_three)) {
                Plan::model()->updateAll(array('Plan_status' => 'success'), 'Plan_level_one=' . $id);
            }elseif(!empty($dataUpdate->Plan_level_one)&&!empty($dataUpdate->Plan_level_two)&&empty($dataUpdate->Plan_level_three)){
                Plan::model()->updateAll(array('Plan_status'=>'success'),'Plan_level_three='.$id);
            }elseif(!empty($dataUpdate->Plan_level_one)&&empty($dataUpdate->Plan_level_two)&&empty($dataUpdate->Plan_level_three)){
                Plan::model()->updateAll(array('Plan_status'=>'success'),'Plan_level_two='.$id);
            }

            Plan::model()->updateByPk($id,array('Plan_status'=>'success','Plan_create_date' => new CDbExpression('NOW()')));

            $NewLogPlan=new LogPlan();
            $NewLogPlan->Plan_id=$id;
            $NewLogPlan->LogPlan_type = 'success';
            if(!empty(Yii::app()->session['Project_id'])){
                $NewLogPlan->Project_id=Yii::app()->session['Project_id'];
            }
            if($NewLogPlan->save(false)) {
                Yii::app()->user->setFlash('success', "ปิดงานสำเร็จ สำเร็จ!");
                $this->redirect('/plan/manage');
            }
        }
    }


    public function actionDelete(){
        Yii::app()->session;
        $id = Yii::app()->request->getParam('id', null);
        if(!empty($id)){
            $dataUpdate=Plan::model()->findByPk($id);
            if(empty($dataUpdate->Plan_level_one)&&empty($dataUpdate->Plan_level_two)&&empty($dataUpdate->Plan_level_three)) {
                Plan::model()->updateAll(array('Plan_remove' => 'Y'), 'Plan_level_one=' . $id);
            }elseif(!empty($dataUpdate->Plan_level_one)&&!empty($dataUpdate->Plan_level_two)&&empty($dataUpdate->Plan_level_three)){
                    Plan::model()->updateAll(array('Plan_remove'=>'Y'),'Plan_level_three='.$id);
            }elseif(!empty($dataUpdate->Plan_level_one)&&empty($dataUpdate->Plan_level_two)&&empty($dataUpdate->Plan_level_three)){
                Plan::model()->updateAll(array('Plan_remove'=>'Y'),'Plan_level_two='.$id);
            }

            Plan::model()->updateByPk($id,array('Plan_remove'=>'Y'));

            $NewLogPlan=new LogPlan();
            $NewLogPlan->Plan_id=$id;
            $NewLogPlan->LogPlan_type = 'delete';
            if(!empty(Yii::app()->session['Project_id'])){
                $NewLogPlan->Project_id=Yii::app()->session['Project_id'];
            }
            if($NewLogPlan->save(false)) {
                Yii::app()->user->setFlash('success', "ลบแผนงานสำเร็จ สำเร็จ!");
                $this->redirect('/plan/manage');
            }
        }
    }

    public function actionChangeLevel(){
        $id = Yii::app()->request->getParam('id', null);
        $type = Yii::app()->request->getParam('type', null);

        $idBefore=0;
        $idAfter=0;
        $status=false;

        $dataRetrn=array();
        $dataPlan=Plan::model()->findByPk($id);

        if(!empty($dataPlan->Plan_level_three)){
            $list = Yii::app()->db->createCommand("select * from plan where plan.Plan_level_three=".$dataPlan->Plan_level_three." and Plan_remove='N' order by Plan_level asc")->queryAll();
        }elseif(!empty($dataPlan->Plan_level_two)){
            $list = Yii::app()->db->createCommand("select * from plan where plan.Plan_level_two=".$dataPlan->Plan_level_two." and Plan_level_three is null and Plan_remove='N' order by Plan_level asc")->queryAll();
        }elseif(!empty($dataPlan->Plan_level_one)){
            $list = Yii::app()->db->createCommand("select * from plan where plan.Plan_level_one=".$dataPlan->Plan_level_one." and Plan_level_two is null and Plan_level_three is null and Plan_remove='N' order by Plan_level asc")->queryAll();
        }elseif(!empty($dataPlan->Project_id)){
            $list = Yii::app()->db->createCommand("select * from plan where plan.Project_id=".$dataPlan->Project_id." and Plan_level_one is null and Plan_level_two is null and Plan_level_three is null and Plan_remove='N' order by Plan_level asc")->queryAll();
        }

        foreach($list as $key=>$value){
            if($value['Plan_id']==$dataPlan->Plan_id){
                $status=true;
            }
            if(!$status) {
                $idBefore = $value['Plan_id'];
            }
            if($status){
                $idAfter=$value['Plan_id'];
            }
        }

        if(!empty($dataPlan)){
            if($type=='up'){
                if(!empty($idBefore)){
                    $planBefore=Plan::model()->findByPk($idBefore);
                    $planBefore->Plan_level=$planBefore->Plan_level+1;
                    $dataPlan->Plan_level=$dataPlan->Plan_level-1;
                    if($planBefore->save(false)){
                        if($dataPlan->save(false)) {
                            $dataRetrn['status'] = 'true';
                        }else{
                            $dataRetrn['status']='false';
                        }
                    }else{
                        $dataRetrn['status']='false';
                    }
                }else{
                    $dataRetrn['status']='idBefore null';
                }
            }elseif($type=='down'){
                if(!empty($idAfter)){
                    $planAfter=Plan::model()->findByPk($idAfter);
                    $planAfter->Plan_level=$planAfter->Plan_level-1;
                    $dataPlan->Plan_level=$dataPlan->Plan_level+1;
                    if($planAfter->save(false)){
                        if($dataPlan->save(false)) {
                            $dataRetrn['status'] = 'true';
                        }else{
                            $dataRetrn['status']='false';
                        }
                    }else{
                        $dataRetrn['status']='false';
                    }
                }else{
                    $dataRetrn['status']='idAfter null';
                }
            }

        }else{
            $dataRetrn['status']='false';
        }

        echo CJSON::encode($dataRetrn);
    }

    public function actionGetPlanStaff(){
        $Plan_id = Yii::app()->request->getParam('Plan_id', null);
        $string="";
//        $dataPlanAssign=PlanAssign::model()->with('userName','plan')->findAll('Plan_id=? && Remove=?',array($Plan_id,'N'));
        $dataPlanAssign=Yii::app()->db->createCommand("select plan.Plan_id,plan.Plan_title,staff.UserName,staff.Name,staff.LastName
from plan_assign
INNER JOIN plan on (plan.Plan_id=plan_assign.Plan_id)
INNER JOIN staff on (staff.UserName=plan_assign.UserName)
where plan_assign.Plan_id=".$Plan_id." and plan_assign.Remove='N'")->queryAll();
        if(!empty($dataPlanAssign)) {
            foreach ($dataPlanAssign as $key => $value) {
                $string .= "<tr><th>" . ($key + 1) . "</th><td>" . $value['Plan_title'] . "</td><td>" . $value['Name'] . " " . $value['LastName'] . "</td><td><a href='/plan/Remove/UserName/" . $value['UserName'] . "/Plan_id/" . $value['Plan_id'] . "' class='btn btn-danger'>ลบ</a></td></tr>";
            }
        }
        echo CJSON::encode($string);
    }

    public function actionRemove(){
        $Plan_id = Yii::app()->request->getParam('Plan_id', null);
        $UserName = Yii::app()->request->getParam('UserName', null);
        $dataPlanAssign=PlanAssign::model()->find('Plan_id=? and UserName=?',array($Plan_id,$UserName));
        if(!empty($dataPlanAssign)){
            $dataPlanAssign->Remove='Y';
            if($dataPlanAssign->save(false)){
                Yii::app()->user->setFlash('success', "ลบพนักงาน ออกจากแผน สำเร็จ");
                $this->redirect('/plan/manage');
            }
        }
    }

    public function actionTest(){
        $Project_id = Yii::app()->request->getParam('Project_id', 23);
        $dateCalendar=new DateTime('Now');
        $dateCalendar->setDate(2015,1,1);
        $firstPlan=Plan::model()->find("Project_id=? and Plan_remove='N' order by Plan_start_date asc",array($Project_id));
        $dateNow=new DateTime($firstPlan->Plan_start_date);
        $dataPlan=Plan::GetAllPlanByProjectId($Project_id);
        $this->render('test',array('dataPlan'=>$dataPlan,'dateCalendar'=>$dateCalendar,'dateNow'=>$dateNow));
    }

    public function actionUpdatePlan(){
        $Plan_id = Yii::app()->request->getParam('Plan_id', null);
        $Type = Yii::app()->request->getParam('Type', null);
        $day = Yii::app()->request->getParam('day', null);
        $dataPlan=Plan::model()->findByPk($Plan_id);
        $StartDate=new DateTime($dataPlan->Plan_start_date);
        $StartEnd=new DateTime($dataPlan->Plan_end_date);
        if($Type=='TabWork'){
            if($day<0){
                $StartDate->modify($day.' day');
                $StartEnd->modify($day.'day');
            }else{
                $StartDate->modify('+'.$day.' day');
                $StartEnd->modify('+'.$day.'day');
            }
        }elseif($Type=='forward'){
            if($day<0){
                $StartEnd->modify($day.'day');
            }else {
                $StartEnd->modify('+' . $day . 'day');
            }
        }elseif($Type=='backward'){
            if($day<0){
                $StartDate->modify($day.' day');
            }else{
                $StartDate->modify('+'.$day.' day');
            }
        }
        $dataPlan->Plan_start_date=$StartDate->format('Y-m-d H:i:s');
        $dataPlan->Plan_end_date=$StartEnd->format('Y-m-d H:i:s');

        if(!empty($dataPlan->Plan_level_one)){
            $dataPlanOne=Plan::model()->findByPk($dataPlan->Plan_level_one);
            $StartDateOne=new DateTime($dataPlanOne->Plan_start_date);
            $StartEndOne=new DateTime($dataPlanOne->Plan_end_date);

            if($StartDate<$StartDateOne)
                $dataPlanOne->Plan_start_date=$StartDate->format('Y-m-d H:i:s');

            if($StartEnd>$StartEndOne)
                $dataPlanOne->Plan_end_date=$StartEnd->format('Y-m-d H:i:s');

//            echo $dataPlanOne->Plan_start_date." :: ".$dataPlanOne->Plan_end_date;

            if($dataPlanOne->save(false)){
                $NewLogPlan=new LogPlan();
                $NewLogPlan->Plan_id=$dataPlanOne->Plan_id;
//                $NewLogPlan->LogPlan_detail=$dataPlanOne->detail;
                $NewLogPlan->Project_id=$dataPlanOne->Project_id;
                $NewLogPlan->LogPlan_type = 'edit';
                if($NewLogPlan->save(false)) {
                    if (!empty($dataPlanOne->Plan_level_three)) {
                        Plan::model()->updateByPk($dataPlanOne->Plan_level_three, array('Plan_hour' => Plan::sumHourPlanId($dataPlanOne->Plan_level_three,'Plan_level_three')));
                    }
                    if (!empty($dataPlanOne->Plan_level_two)) {
                        Plan::model()->updateByPk($dataPlanOne->Plan_level_two, array('Plan_hour' => Plan::sumHourPlanId($dataPlanOne->Plan_level_two,'Plan_level_two')));
                    }
                    if (!empty($dataPlanOne->Plan_level_one)) {
                        Plan::model()->updateByPk($dataPlanOne->Plan_level_one, array('Plan_hour' => Plan::sumHourPlanId($dataPlanOne->Plan_level_one,'Plan_level_one')));
                    }
                }
                echo 'true';
            }
        }

        if(!empty($dataPlan->Plan_level_two)){
            $dataPlanOne=Plan::model()->findByPk($dataPlan->Plan_level_two);
            $StartDateOne=new DateTime($dataPlanOne->Plan_start_date);
            $StartEndOne=new DateTime($dataPlanOne->Plan_end_date);

            if($StartDate<$StartDateOne)
                $dataPlanOne->Plan_start_date=$StartDate->format('Y-m-d H:i:s');

            if($StartEnd>$StartEndOne)
                $dataPlanOne->Plan_end_date=$StartEnd->format('Y-m-d H:i:s');

//            echo $dataPlanOne->Plan_start_date." :: ".$dataPlanOne->Plan_end_date;

            if($dataPlanOne->save(false)){
                $NewLogPlan=new LogPlan();
                $NewLogPlan->Plan_id=$dataPlanOne->Plan_id;
//                $NewLogPlan->LogPlan_detail=$dataPlanOne->detail;
                $NewLogPlan->Project_id=$dataPlanOne->Project_id;
                $NewLogPlan->LogPlan_type = 'edit';
                if($NewLogPlan->save(false)) {
                    if (!empty($dataPlanOne->Plan_level_three)) {
                        Plan::model()->updateByPk($dataPlanOne->Plan_level_three, array('Plan_hour' => Plan::sumHourPlanId($dataPlanOne->Plan_level_three,'Plan_level_three')));
                    }
                    if (!empty($dataPlanOne->Plan_level_two)) {
                        Plan::model()->updateByPk($dataPlanOne->Plan_level_two, array('Plan_hour' => Plan::sumHourPlanId($dataPlanOne->Plan_level_two,'Plan_level_two')));
                    }
                    if (!empty($dataPlanOne->Plan_level_one)) {
                        Plan::model()->updateByPk($dataPlanOne->Plan_level_one, array('Plan_hour' => Plan::sumHourPlanId($dataPlanOne->Plan_level_one,'Plan_level_one')));
                    }
                }
                echo 'true';
            }
        }

        if(!empty($dataPlan->Plan_level_three)){
            $dataPlanOne=Plan::model()->findByPk($dataPlan->Plan_level_three);
            $StartDateOne=new DateTime($dataPlanOne->Plan_start_date);
            $StartEndOne=new DateTime($dataPlanOne->Plan_end_date);

            if($StartDate<$StartDateOne)
                $dataPlanOne->Plan_start_date=$StartDate->format('Y-m-d H:i:s');

            if($StartEnd>$StartEndOne)
                $dataPlanOne->Plan_end_date=$StartEnd->format('Y-m-d H:i:s');

//            echo $dataPlanOne->Plan_start_date." :: ".$dataPlanOne->Plan_end_date;

            if($dataPlanOne->save(false)){
                $NewLogPlan=new LogPlan();
                $NewLogPlan->Plan_id=$dataPlanOne->Plan_id;
//                $NewLogPlan->LogPlan_detail=$dataPlanOne->detail;
                $NewLogPlan->Project_id=$dataPlanOne->Project_id;
                $NewLogPlan->LogPlan_type = 'edit';
                if($NewLogPlan->save(false)) {
                    if (!empty($dataPlanOne->Plan_level_three)) {
                        Plan::model()->updateByPk($dataPlanOne->Plan_level_three, array('Plan_hour' => Plan::sumHourPlanId($dataPlanOne->Plan_level_three,'Plan_level_three')));
                    }
                    if (!empty($dataPlanOne->Plan_level_two)) {
                        Plan::model()->updateByPk($dataPlanOne->Plan_level_two, array('Plan_hour' => Plan::sumHourPlanId($dataPlanOne->Plan_level_two,'Plan_level_two')));
                    }
                    if (!empty($dataPlanOne->Plan_level_one)) {
                        Plan::model()->updateByPk($dataPlanOne->Plan_level_one, array('Plan_hour' => Plan::sumHourPlanId($dataPlanOne->Plan_level_one,'Plan_level_one')));
                    }
                }
                echo 'true';
            }
        }

        if($dataPlan->save(false)){
            echo 'true';
        }
    }

    public function actionManage(){
        Yii::app()->session;
        $id = Yii::app()->request->getParam('id', null);
        $Project_id = Yii::app()->request->getParam('Project_id', null);
        $dataProjectSelect=null;
        $dataLogPlan=null;
        $PlanAssign=new PlanAssign();
        $firstPlan=array();
        $dateNow=null;
        $dataPlan=null;
        if(!empty($Project_id)) {
           Yii::app()->session['Project_id'] = $Project_id;
        }

        if(!empty($id)){
            $model=Plan::model()->findByPk($id);
            if(!empty($model->Plan_start_date)){
                $model->Plan_start_date=date("Y-m-d",strtotime($model->Plan_start_date));
            }
            if(!empty($model->Plan_end_date)){
                $model->Plan_end_date=date("Y-m-d",strtotime($model->Plan_end_date));
            }
        }else {
            $model = new Plan();
            if(!empty(Yii::app()->session['Project_id'])) {
                $model->Project_id = Yii::app()->session['Project_id'];
            }
        }

        if(isset($_POST['ajax']) && $_POST['ajax']==='formAddPlanStaff')
        {
            echo CActiveForm::validate($PlanAssign);
            Yii::app()->end();
        }


        if(isset($_POST['PlanAssign'])) {
            $PlanAssign->attributes = $_POST['PlanAssign'];
            if($PlanAssign->validate()) {
                $dataPlanAssign=PlanAssign::model()->find('Plan_id=? and UserName=?',array($PlanAssign->Plan_id,$PlanAssign->UserName));
                if(!empty($dataPlanAssign)){
                    if($dataPlanAssign->Remove=='Y')
                        $dataPlanAssign->Remove='N';
                    if($dataPlanAssign->save(false)){
                        Yii::app()->user->setFlash('success', "เพิ่มพนักงานเข้าแผน สำเร็จ");
                    }
                }else {
                    if ($PlanAssign->save(false)) {
                        Yii::app()->user->setFlash('success', "เพิ่มพนักงานเข้าแผน สำเร็จ");
                    }
                }
            }else{
                $errores = $PlanAssign->getErrors();
                var_dump($errores);
                exit;
            }
        }

        if(isset($_POST['ajax']) && $_POST['ajax']==='formAddPlan')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if(isset($_POST['Plan']))
        {
            $model->attributes=$_POST['Plan'];

            if (isset($_POST['Plan']['detail'])) {
                $model->detail = $_POST['Plan']['detail'];
            }
            // validate user input and redirect to the previous page if valid
            if($model->validate()) {
                if($model->save(false)) {

                    if(empty($model->Plan_level)){
                        Plan::maxLevel($model->Plan_id);
                    }

                    $NewLogPlan=new LogPlan();
                    $NewLogPlan->Plan_id=$model->Plan_id;
                    $NewLogPlan->LogPlan_detail=$model->detail;
                    $NewLogPlan->Project_id=$model->Project_id;
                    if($model->detail!==''){
                        $NewLogPlan->LogPlan_type = 'edit';
                    }else {
                        $NewLogPlan->LogPlan_type = 'create';
                    }
                    if($NewLogPlan->save(false)) {
                        if (!empty($model->Plan_level_three)) {
                            Plan::model()->updateByPk($model->Plan_level_three, array('Plan_hour' => Plan::sumHourPlanId($model->Plan_level_three,'Plan_level_three')));
                        }
                        if (!empty($model->Plan_level_two)) {
                            Plan::model()->updateByPk($model->Plan_level_two, array('Plan_hour' => Plan::sumHourPlanId($model->Plan_level_two,'Plan_level_two')));
                        }
                        if (!empty($model->Plan_level_one)) {
                            Plan::model()->updateByPk($model->Plan_level_one, array('Plan_hour' => Plan::sumHourPlanId($model->Plan_level_one,'Plan_level_one')));
                        }

                        if($model->detail!==''){
                            Yii::app()->user->setFlash('success', "แก้ไขแผนสำเร็จ สำเร็จ!");
                        }else {
                            Yii::app()->user->setFlash('success', "เพิ่มแผนสำเร็จ สำเร็จ!");
                        }
                        $this->redirect('/plan/manage');
                    }
                }
            }else{
                $errores = $model->getErrors();
                var_dump($errores);
                exit;
            }
        }

        if(!empty(Yii::app()->session['Project_id'])){
            $dataPlanSelect=Plan::model()->findAll('Project_id=? and Plan_remove=? and Plan_level_one is null',array(Yii::app()->session['Project_id'],'N'));
            $dataPlanOne=Plan::model()->findAll('Project_id=? and Plan_remove=? and Plan_level_one IS NOT NULL and Plan_level_two is null and Plan_level_three is null',array(Yii::app()->session['Project_id'],'N'));
            $dataPlanTwo=Plan::model()->findAll('Project_id=? and Plan_remove=? and Plan_level_two IS NOT NULL',array(Yii::app()->session['Project_id'],'N'));
            $dataProjectSelect=Project::model()->findByPk(Yii::app()->session['Project_id']);
//            =========log Plan==========
            $criteria=new CDbCriteria();
            $criteria->condition="Project_id = '".Yii::app()->session['Project_id']."'";
            $criteria->order='LogPlan_date DESC';
            $dataLogPlan=LogPlan::model()->findAll($criteria);
//            =========end log Plan==========
            $criteriaPlan = new CDbCriteria;
            $criteriaPlan->condition="Project_id = '".Yii::app()->session['Project_id']."' and Plan_remove='N'";
            $criteriaPlan->order=('Plan_start_date asc');
            $firstPlan=Plan::model()->find($criteriaPlan);
            $dateCalendar=new DateTime('Now');
            $dateNow = new DateTime('Now');
            if(!empty($firstPlan)) {
                $dateCalendar->setDate(date_format(date_create($firstPlan->Plan_start_date), 'Y'), 1, 1);
                $firstPlan=Plan::model()->find("Project_id=? and Plan_remove='N' order by Plan_start_date asc",array(Yii::app()->session['Project_id']));
                $dateNow = new DateTime($firstPlan->Plan_start_date);
            }
            $dataPlanView=Plan::GetAllPlanByProjectId(Yii::app()->session['Project_id']);
        }else{
            echo 'กรุณาเลือกโครงการ';
            exit;
        }
        $dataProject=Project::model()->findAll('Remove=?',array('N'));
        $dataPlan=Plan::GetPlanByProject();
        $dataGroupStaff=Groupstaff::model()->find('UserName=? and Project_id=? and Remove=?',array(Yii::app()->user->username,$Project_id,'N'));

//        $dataPlanView=Plan::GetAllPlanByProjectId($Project_id);
        $this->render('manage',array('dataPlanView'=>$dataPlanView,'dateCalendar'=>$dateCalendar,'dateNow'=>$dateNow,'dataGroupStaff'=>$dataGroupStaff,'PlanAssign'=>$PlanAssign,'dataLogPlan'=>$dataLogPlan,'dataProjectSelect'=>$dataProjectSelect,'model'=>$model,'dataProject'=>$dataProject,'dataPlan'=>$dataPlan,'dataPlanSelect'=>$dataPlanSelect,'dataPlanTwo'=>$dataPlanTwo,'dataPlanOne'=>$dataPlanOne));
    }
}
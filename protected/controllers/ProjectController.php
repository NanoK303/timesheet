<?php
class ProjectController extends Controller
{
    public function init() {
        if (Yii::app()->user->isGuest) {
                $this->redirect('/site/login');
        }
    }

    public function actionReport(){
        $Project_id = Yii::app()->request->getParam('Project_id', null);
        $monthNow = Yii::app()->request->getParam('month', null);
        $yearNow = Yii::app()->request->getParam('year', null);
        if(empty($monthNow)&&empty($yearNow)){
            $day=Workdetail::getDateTimeNowDay();
            $monthNow=Workdetail::getDateTimeNowMonth();
            $yearNow=Workdetail::getDateTimeNowYear();

            $dataProjectSelect=Project::model()->findByPk($Project_id);
            $dataPlan=Plan::GetPlanByProjectId($Project_id,$monthNow,$yearNow);
            $monthNames = array("","มกราคม","กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
            $this->render('report',array('day'=>$day,'monthNow'=>$monthNow,'yearNow'=>$yearNow,'monthNames'=>$monthNames,'dataProjectSelect'=>$dataProjectSelect,'dataPlan'=>$dataPlan,'Project_id'=>$Project_id));
        }else{
            $getMonthNow=Workdetail::getDateTimeNowMonth();
            $getYearNow=Workdetail::getDateTimeNowYear();
            if($getMonthNow==$monthNow && $getYearNow==$yearNow){
                $day=Workdetail::getDateTimeNowDay();
            }else{
                $day=Workdetail::checkNumberOfDays($yearNow,$monthNow);
            }
            $dataProjectSelect=Project::model()->findByPk($Project_id);
            $dataPlan=Plan::GetPlanByProjectId($Project_id,$monthNow,$yearNow);
            $monthNames = array("","มกราคม","กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
//            $this->render('report',array('day'=>$day,'monthNow'=>$monthNow,'yearNow'=>$yearNow,'monthNames'=>$monthNames,'dataProjectSelect'=>$dataProjectSelect,'dataPlan'=>$dataPlan));
            echo $this->renderPartial('getReport',array('day'=>$day,'monthNow'=>$monthNow,'yearNow'=>$yearNow,'monthNames'=>$monthNames,'dataProjectSelect'=>$dataProjectSelect,'dataPlan'=>$dataPlan,'Project_id'=>$Project_id),true);
        }
    }

    public function actionManage(){
        $project=new Project();
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'formAddProduct') {
            echo CActiveForm::validate($project);
            Yii::app()->end();
        }

        if(isset($_POST['Project'])){
            // collects user input data
            $project->attributes = $_POST['Project'];
            // validates user input and redirect to previous page if validated

            if ($project->validate()) {
                if ($project->save(false)) {
                    Yii::app()->user->setFlash('success', "เพิ่มข้อมูลโครงการ สำเร็จ!");
                    $this->redirect('/project/manage');
                }
            }
        }if(isset($_POST['Position'])){
            $dataGroupStaff=Groupstaff::model()->findByPk($_POST['Position']['groupstaff_id']);
            if(!empty($dataGroupStaff)){
                $dataGroupStaff->Position=$_POST['Position']['Position'];
                if($dataGroupStaff->save(false)){
                    Yii::app()->user->setFlash('success', "เปลิ่ยนตำเหน่ง สำเร็จ!");
                    $this->redirect('/project/manage');
                }
            }
        }

        if(!empty(Yii::app()->user->SubOrganizer_id)){
            $criteriawork = new CDbCriteria;
            $criteriawork->compare('Remove', 'N', FALSE);
            $criteriawork->addCondition("Position != 'admin'");
            $dataStaff=Staff::model()->findAll('SubOrganizer_id=?',array(Yii::app()->user->SubOrganizer_id));
            $string="";
            foreach($dataStaff as $key=>$value){
                if($key==0) {
                    $string .= "UserName = '" . $value->UserName . "'";
                }else{
                    $string .= " or UserName = '" . $value->UserName . "'";
                }
            }
            $criteriawork->addCondition($string);
            $dateStaff=Staff::model()->findAll($criteriawork);
        }else {
            $dateStaff = Staff::model()->findAll('Position!="admin" and remove=?', array('N'));
        }
        if(!empty(Yii::app()->user->SubOrganizer_id)) {
            $dataSubOrganizer = SubOrganizer::model()->findByPk(Yii::app()->user->SubOrganizer_id);
            $dataProject = Yii::app()->db->createCommand('select project.MaxTime,project.Remove,project.Public,project.Project_id,project.Project_name,count(*) as count
            from project
            left JOIN groupstaff on groupstaff.Project_id=project.Project_id
            where project.Remove="N" and project.Company_id='.$dataSubOrganizer->Company_id.'
            group by project.Project_id
            order by count DESC,project.Project_id DESC')->queryAll();
            $company=Company::model()->findAll(array("condition" => "Remove =  'N' and Company_id=".$dataSubOrganizer->Company_id,'order' => 'Company_id ASC'));
        }else {
            $dataProject = Yii::app()->db->createCommand('select project.MaxTime,project.Remove,project.Public,project.Project_id,project.Project_name,count(*) as count
            from project
            left JOIN groupstaff on groupstaff.Project_id=project.Project_id
            where project.Remove="N"
            group by project.Project_id
            order by count DESC,project.Project_id DESC')->queryAll();
            $company=Company::model()->findAll(array('order' => 'Company_id ASC'));
        }
        $this->render('manage', array('company'=>$company,'dateStaff'=>$dateStaff,'project'=>$project,'dataProject'=>$dataProject));
    }


    public function actionDeleteProject(){
        $Project_id = Yii::app()->request->getParam('ProjectId', null);
        $type = Yii::app()->request->getParam('type', null);
        $dataRetrn=array();
        $dataProject=Project::model()->findByPk($Project_id);
        $dataProject->Remove=$type;
        if($dataProject->save(false)){
            $dataRetrn['status']='true';
        }else{
            $dataRetrn['status']='false';
        }
        if($type=='Y') {
            Yii::app()->user->setFlash('error', "ลบโครงการ สำเร็จ!");
        }elseif($type=='N'){
            Yii::app()->user->setFlash('success', "เปิดโครงการ สำเร็จ!");
        }elseif($type=='C'){
            Yii::app()->user->setFlash('error', "ปิดโครงการ สำเร็จ!");
        }
        echo CJSON::encode($dataRetrn);
    }

    public function actionEditProject(){
        $Project_id = Yii::app()->request->getParam('ProjectId', null);
        $name = Yii::app()->request->getParam('name', null);
        $MaxTime = Yii::app()->request->getParam('MaxTime', null);
        $dataRetrn=array();
        $dataProject=Project::model()->findByPk($Project_id);

        if(!empty($MaxTime)) {
            $dataProject->MaxTime =$MaxTime;
        }

        if(!empty($name)){
            $dataProject->Project_name=$name;
        }

        if($dataProject->save(false)){
            $dataRetrn['status']='true';
        }else{
            $dataRetrn['status']='false';
        }

        echo CJSON::encode($dataRetrn);
    }

    public function actionAddHourProject(){
        $Project_id = Yii::app()->request->getParam('ProjectId', null);
        $value = Yii::app()->request->getParam('value', null);
        $dataRetrn=array();
        $dataProject=Project::model()->findByPk($Project_id);
        $dataProject->MaxTime+=$value;
        if($dataProject->save(false)){
            $dataRetrn['status']='true';
        }else{
            $dataRetrn['status']='false';
        }

        echo CJSON::encode($dataRetrn);
    }

    public function actionAddStaffProject(){
        $Project_id = Yii::app()->request->getParam('ProjectId', null);
        $UserName = Yii::app()->request->getParam('UserName', null);
        $dataRetrn=array();
        $newGroupProject=Groupstaff::model()->find('UserName=? and Project_id=?',array($UserName,$Project_id));
        if(!empty($newGroupProject)){
            $newGroupProject->Remove='N';
        }else {
            $newGroupProject = new Groupstaff();
            $newGroupProject->Project_id = $Project_id;
            $newGroupProject->UserName = $UserName;
        }
        if($newGroupProject->save(false)){
            $dataRetrn['status']='true';
            $dataRetrn['data']=Staff::GetNickName($UserName);
        }else{
            $dataRetrn['status']='false';
        }
        echo CJSON::encode($dataRetrn);
    }

    public function actionRemoveStaffProject(){
        $Project_id = Yii::app()->request->getParam('ProjectId', null);
        $UserName = Yii::app()->request->getParam('UserName', null);
        $dataRetrn=array();
        $dataGroupStaff=Groupstaff::model()->find('UserName=? and Project_id=?',array($UserName,$Project_id));

        if(!empty($dataGroupStaff)){
            $dataGroupStaff->Remove='Y';
            if($dataGroupStaff->save(false)){
                $dataRetrn['status']='true';
                $dataRetrn['data']=$dataGroupStaff->groupstaff_id;
            }else{
                $dataRetrn['status']='false';
            }
        }
        echo CJSON::encode($dataRetrn);
    }

    public function actionChangePublic(){
        $Project_id = Yii::app()->request->getParam('ProjectId', null);
        $dataRetrn=array();
        $dataProject=Project::model()->findByPk($Project_id);
        if(!empty($dataProject)){
            if($dataProject->Public=='Y'){
                $dataProject->Public='N';
            }elseif ($dataProject->Public=='N'){
                $dataProject->Public='Y';
            }
            if($dataProject->save(false)){
                $dataRetrn['status']='true';
            }else{
                $dataRetrn['status']='false';
            }
        }
        echo CJSON::encode($dataRetrn);
    }

}
<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	public function actionChangeTime(){
//		Yii::import('application.extensions.Carbon-master.src.Carbon.Carbon');
//		echo dirname(__FILE__).'../extensions/Carbon/Carbon.php';
//		exit;
//		var_dump(is_file('D:\projectX3\timesheet\protected\extensions\Carbon\src\Carbon\Carbon.php'));
//exit;

//	use Carbon\Carbon;

//		$carbon = new Carbon();
//		echo Carbon::now();
//		$carbon = new Carbon('first day of January 2008', 'America/Vancouver');
//		echo get_class($carbon);

		$dataWork=Workdetail::model()->findAll();
		foreach($dataWork as $key=>$value){
			$dataUpdate=Workdetail::model()->findByPk($value->WorkDetail_id);

			if($dataUpdate->save(false)){
				echo '<br>'.$value->WorkDetail_id.'true';
			}else{
				echo '<br>'.$value->WorkDetail_id.'false';
			}
		}
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		if (!empty(Yii::app()->user->id)) {

            $id = Yii::app()->request->getParam('id', null);
            $type = Yii::app()->request->getParam('type', null);
            $dataSelectPlan = array();
            $dataSelectPlanOne = array();
            $dataSelectPlanTwo = array();
            $dataSelectPlanThree = array();
            $dataRequest=array();

            if (!empty($id)) {
                $model = Workdetail::EditWrokDetail($id);
                if (!empty($model)) {
                    if (yii::app()->user->type != 'admin' && ($model->UserName != Yii::app()->user->id)) {
                        $model = new Workdetail();
                        $model->UserName = Yii::app()->user->id;
                        $model->TypeWork = 'work';
                    } elseif (yii::app()->user->type == 'admin') {
                        $dataDetail = explode(',', $model->Detail);
                        if (count($dataDetail) > 1) {
                            if ($dataDetail[0] == 'ขาดงาน' || $dataDetail[0] == 'ลาป่วย' || $dataDetail[0] == 'ลากิจอื่นๆ' || $dataDetail[0] == 'ลาคลอด' || $dataDetail[0] == 'ลาบวช' || $dataDetail[0] == 'ลาสมรส' || $dataDetail[0] == 'ลาพักร้อน' || $dataDetail[0] == 'ขออนุมัติ') {
                                $model->Detail = '';
                                foreach ($dataDetail as $key => $value) {
                                    if ($key > 0) {
                                        $model->Detail .= $value;
                                    }
                                }
                                $model->TypeLeave = $dataDetail[0];
                            }
                        }
                    }

                    if (!empty($model->TypeWork) && $model->TypeWork == 'plan') {
                        $dataPlanStaff = PlanStaff::model()->with('plan')->find('WorkDetail_id=?', array($id));;
                        if (!empty($dataPlanStaff)) {
                            if (empty($dataPlanStaff->plan->Plan_level_three) && empty($dataPlanStaff->plan->Plan_level_two) && empty($dataPlanStaff->plan->Plan_level_one)) {
                                $model->Plan_id = $dataPlanStaff->plan->Plan_id;
                                $model->Plan_level_one = '';
                                $dataSelectPlanOne = Plan::model()->findAll('Plan_level_one=? and Plan_remove=? and Plan_level_two is null', array($dataPlanStaff->plan->Plan_id, 'N'));
                                $dataSelectPlan = Plan::model()->findAll('Project_id=? and Plan_remove=? and Plan_level_one is null and Plan_level_two is null and Plan_level_three is null', array($dataPlanStaff->plan->Project_id, 'N'));
                            } elseif (!empty($dataPlanStaff->plan->Plan_level_three) && !empty($dataPlanStaff->plan->Plan_level_two) && !empty($dataPlanStaff->plan->Plan_level_one)) {
                                $model->Plan_level_three = $dataPlanStaff->plan->Plan_id;
                                $model->Plan_level_two = $dataPlanStaff->plan->Plan_level_three;
                                $model->Plan_level_one = $dataPlanStaff->plan->Plan_level_two;
                                $model->Plan_id = $dataPlanStaff->plan->Plan_level_one;
                                $dataSelectPlanThree = Plan::model()->findAll('Plan_level_three=? and Plan_remove=?', array($dataPlanStaff->plan->Plan_level_three, 'N'));
                                $dataSelectPlanTwo = Plan::model()->findAll('Plan_level_two=? and Plan_remove=? and Plan_level_three is null', array($dataPlanStaff->plan->Plan_level_two, 'N'));
                                $dataSelectPlanOne = Plan::model()->findAll('Plan_level_one=? and Plan_remove=? and Plan_level_two is null', array($dataPlanStaff->plan->Plan_level_one, 'N'));
                                $dataSelectPlan = Plan::model()->findAll('Project_id=? and Plan_remove=? and Plan_level_one is null and Plan_level_two is null and Plan_level_three is null', array($dataPlanStaff->plan->Project_id, 'N'));
                            } elseif (!empty($dataPlanStaff->plan->Plan_level_two) && !empty($dataPlanStaff->plan->Plan_level_one)) {
                                $model->Plan_level_two = $dataPlanStaff->plan->Plan_id;
                                $model->Plan_level_one = $dataPlanStaff->plan->Plan_level_two;
                                $model->Plan_id = $dataPlanStaff->plan->Plan_level_one;
                                $dataSelectPlanThree = Plan::model()->findAll('Plan_level_three=? and Plan_remove=?', array($dataPlanStaff->plan->Plan_id, 'N'));
                                $dataSelectPlanTwo = Plan::model()->findAll('Plan_level_two=? and Plan_remove=? and Plan_level_three is null', array($dataPlanStaff->plan->Plan_level_two, 'N'));
                                $dataSelectPlanOne = Plan::model()->findAll('Plan_level_one=? and Plan_remove=? and Plan_level_two is null', array($dataPlanStaff->plan->Plan_level_one, 'N'));
                                $dataSelectPlan = Plan::model()->findAll('Project_id=? and Plan_remove=? and Plan_level_one is null and Plan_level_two is null and Plan_level_three is null', array($dataPlanStaff->plan->Project_id, 'N'));
                            } elseif (!empty($dataPlanStaff->plan->Plan_level_one)) {
                                $model->Plan_level_one = $dataPlanStaff->plan->Plan_id;
                                $model->Plan_id = $dataPlanStaff->plan->Plan_level_one;
                                $dataSelectPlanTwo = Plan::model()->findAll('Plan_level_two=? and Plan_remove=? and Plan_level_three is null', array($dataPlanStaff->plan->Plan_id, 'N'));
                                $dataSelectPlanOne = Plan::model()->findAll('Plan_level_one=? and Project_id=? and Plan_remove=? and Plan_level_two is null and Plan_level_three is null', array($dataPlanStaff->plan->Plan_level_one, $dataPlanStaff->plan->Project_id, 'N'));
                                $dataSelectPlan = Plan::model()->findAll('Project_id=? and Plan_remove=? and Plan_level_one is null and Plan_level_two is null and Plan_level_three is null', array($dataPlanStaff->plan->Project_id, 'N'));
                            }
                        }
                    }

                    if (!empty($model->Project_id)) {
                        $dataSelectPlan= Yii::app()->db->createCommand("select plan.Plan_id,plan.Plan_title,plan.Plan_status
                            from plan_assign
                            INNER JOIN plan ON plan.Plan_id=plan_assign.Plan_id
                            where plan_assign.UserName='".Yii::app()->user->username."' and plan.Project_id=".$model->Project_id)->queryAll();
//                        $dataSelectPlan = Plan::model()->findAll('Project_id=? and Plan_remove=? and Plan_level_one IS NULL and Plan_level_two is null and Plan_level_three is null', array($model->Project_id, 'N'));
                    }
                    if (!empty($model->Plan_id)) {
                        $dataSelectPlanOne = Plan::model()->findAll('Project_id=? and Plan_level_one=? and Plan_remove=? and Plan_level_two is null and Plan_level_three is null ', array($model->Project_id, $model->Plan_id, 'N'));
                    }
                    if (!empty($model->Plan_level_one)){
                        $dataSelectPlanTwo = Plan::model()->findAll('Project_id=? and Plan_level_one=? and Plan_level_two=? and  Plan_remove=? and Plan_level_three is null', array($model->Project_id, $model->Plan_id,$model->Plan_level_one, 'N'));
                    }
                    if (!empty($model->Plan_level_two)){
                        $dataSelectPlanThree=Plan::model()->findAll('Project_id=? and Plan_level_one=? and Plan_level_two=? and Plan_level_three=? and Plan_remove=?',array($model->Project_id, $model->Plan_id, $model->Plan_level_one,$model->Plan_level_two,'N'));
                    }
                } else {
                    $model = new Workdetail();
                    $model->UserName = Yii::app()->user->id;
                    $model->TypeWork = 'work';
                }
            } else {
                $model = new Workdetail();
                $model->UserName = Yii::app()->user->id;
                $model->TypeWork = 'work';
            }

            $model->scenario = 'Add';
            $project = new Project();
            $staff = Staff::model()->findByPk(Yii::app()->user->id);
            $staff->Password = '';
            $staff->BeforeUserName = $staff->UserName;
            $staff->BeforeEmail = $staff->Email;
            $staff->scenario = 'Edit';
//            $staff = Staff::model()->findByPk(Yii::app()->user->id);
//            $staff->Password = '';
//            $staff->BeforeUserName = $staff->UserName;
//            $staff->scenario = 'Edit';

            if (isset($_POST['ajax']) && $_POST['ajax'] === 'modelformworkdetail') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            if (isset($_POST['ajax']) && $_POST['ajax'] === 'formworkdetail') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            if (isset($_POST['ajax']) && $_POST['ajax'] === 'formRegister') {
                echo CActiveForm::validate($staff);
                Yii::app()->end();
            }

            if (isset($_POST['Workdetail'])) {
                // collects user input data
                if (!empty($_POST['Workdetail']['WorkDetail_id'])) {
                    $model = Workdetail::model()->findByPk($_POST['Workdetail']['WorkDetail_id']);
                }

                $model->attributes = $_POST['Workdetail'];

                if (isset($_POST['Workdetail']['Plan_id'])) {
                    $model->Plan_id = $_POST['Workdetail']['Plan_id'];
                }

                if (isset($_POST['Workdetail']['Plan_level_one'])) {
                    $model->Plan_level_one = $_POST['Workdetail']['Plan_level_one'];
                }

                if (isset($_POST['Workdetail']['Plan_level_two'])) {
                    $model->Plan_level_two = $_POST['Workdetail']['Plan_level_two'];
                }

                if (isset($_POST['Workdetail']['Plan_level_three'])) {
                    $model->Plan_level_three = $_POST['Workdetail']['Plan_level_three'];
                }

                if (isset($_POST['Workdetail']['StartMinute'])) {
                    $model->StartMinute = $_POST['Workdetail']['StartMinute'];
                }

                if (isset($_POST['Workdetail']['StartHour'])) {
                    $model->StartHour = $_POST['Workdetail']['StartHour'];
                }

                if (isset($_POST['Workdetail']['TypeLeave'])) {
                    $model->TypeLeave = $_POST['Workdetail']['TypeLeave'];
                }

                if (isset($_POST['Workdetail']['EndHour']) && $_POST['Workdetail']['EndHour'] != '00') {
                    $model->EndHour = $_POST['Workdetail']['EndHour'];
                    if (isset($_POST['Workdetail']['EndMinute'])) {
                        $model->EndMinute = $_POST['Workdetail']['EndMinute'];
                    }
                }

                if (!empty($_POST['Workdetail']['Plan_level_three']) || !empty($_POST['Workdetail']['Plan_level_two']) || !empty($_POST['Workdetail']['Plan_level_one']) || !empty($_POST['Workdetail']['Plan_id'])) {
                    $model->TypeWork = 'plan';
                }

                if ($model->TypeWork == 'leave' && ((strtotime($model->EndDate) - strtotime($model->StartDate) >= 36000))) {
                    $model->GetSaveLeave($model);
                    Yii::app()->user->setFlash('success', "เพิ่มวันลา สำเร็จ!");
                    $this->redirect(Yii::app()->user->returnUrl);
                } elseif ($model->validate()) {

                    // validates user input and redirect to previous page if validated
//					$model->UserName = Yii::app()->user->id;
                    if ($model->save(false)) {
                        $newPlanStaff = PlanStaff::model()->find('WorkDetail_id=? and UserName=?', array($model->WorkDetail_id, Yii::app()->user->id));

                        if (!empty($_POST['Workdetail']['Plan'])) {
                            if (!empty($newPlanStaff)) {
                                $newPlanStaff->Plan_id = $_POST['Workdetail']['Plan'];
                            } else {
                                $newPlanStaff = New PlanStaff();
                                $newPlanStaff->WorkDetail_id = $model->WorkDetail_id;
                                $newPlanStaff->Plan_id = $_POST['Workdetail']['Plan'];
                                $newPlanStaff->UserName =$model->UserName;
                            }
                            if(!empty($model->EndDate)&&!empty($model->StartDate)) {
                                $newPlanStaff->Plan_staff_hour=(strtotime($model->EndDate) - strtotime($model->StartDate));
                            }
                            if ($newPlanStaff->validate()) {
                                $newPlanStaff->save();
                            } else {
                                $errores = $newPlanStaff->getErrors();
                                var_dump($errores);
                                exit;
                            }

                        } elseif (!empty($_POST['Workdetail']['Plan_level_three'])) {
                            if (!empty($newPlanStaff)) {
                                $newPlanStaff->Plan_id = $_POST['Workdetail']['Plan_level_three'];
                            } else {
                                $newPlanStaff = New PlanStaff();
                                $newPlanStaff->WorkDetail_id = $model->WorkDetail_id;
                                $newPlanStaff->Plan_id = $_POST['Workdetail']['Plan_level_three'];
                                $newPlanStaff->UserName =$model->UserName;
                            }
                            if(!empty($model->EndDate)&&!empty($model->StartDate)) {
                                $newPlanStaff->Plan_staff_hour=(strtotime($model->EndDate) - strtotime($model->StartDate));
                            }
                            if ($newPlanStaff->validate()) {
                                $newPlanStaff->save();
                            } else {
                                $errores = $newPlanStaff->getErrors();
                                var_dump($errores);
                                exit;
                            }

                        } elseif (!empty($_POST['Workdetail']['Plan_level_two'])) {
                            if (!empty($newPlanStaff)) {
                                $newPlanStaff->Plan_id = $_POST['Workdetail']['Plan_level_two'];
                            } else {
                                $newPlanStaff = New PlanStaff();
                                $newPlanStaff->WorkDetail_id = $model->WorkDetail_id;
                                $newPlanStaff->Plan_id = $_POST['Workdetail']['Plan_level_two'];
                                $newPlanStaff->UserName =$model->UserName;
                            }
                            if(!empty($model->EndDate)&&!empty($model->StartDate)) {
                                $newPlanStaff->Plan_staff_hour=(strtotime($model->EndDate) - strtotime($model->StartDate));
                            }
                            if ($newPlanStaff->validate()) {
                                $newPlanStaff->save();
                            } else {
                                $errores = $newPlanStaff->getErrors();
                                var_dump($errores);
                                exit;
                            }
                        } elseif (!empty($_POST['Workdetail']['Plan_level_one'])) {
                            if (!empty($newPlanStaff)) {
                                $newPlanStaff->Plan_id = $_POST['Workdetail']['Plan_level_one'];
                            } else {
                                $newPlanStaff = New PlanStaff();
                                $newPlanStaff->WorkDetail_id = $model->WorkDetail_id;
                                $newPlanStaff->Plan_id = $_POST['Workdetail']['Plan_level_one'];
                                $newPlanStaff->UserName =$model->UserName;
                            }
                            if(!empty($model->EndDate)&&!empty($model->StartDate)) {
                                $newPlanStaff->Plan_staff_hour=(strtotime($model->EndDate) - strtotime($model->StartDate));
                            }
                            if ($newPlanStaff->validate()) {
                                $newPlanStaff->save();
                            } else {
                                $errores = $newPlanStaff->getErrors();
                                var_dump($errores);
                                exit;
                            }
                        } elseif (!empty($_POST['Workdetail']['Plan_id'])) {
                            if (!empty($newPlanStaff)) {
                                $newPlanStaff->Plan_id = $_POST['Workdetail']['Plan_id'];
                            } else {
                                $newPlanStaff = New PlanStaff();
                                $newPlanStaff = New PlanStaff();
                                $newPlanStaff->WorkDetail_id = $model->WorkDetail_id;
                                $newPlanStaff->Plan_id = $_POST['Workdetail']['Plan_id'];
                                $newPlanStaff->UserName =$model->UserName;
                            }
                            if(!empty($model->EndDate)&&!empty($model->StartDate)) {
                                $newPlanStaff->Plan_staff_hour=(strtotime($model->EndDate) - strtotime($model->StartDate));
                            }
                            if ($newPlanStaff->validate()) {
                                $newPlanStaff->save();
                            } else {
                                $errores = $newPlanStaff->getErrors();
                                var_dump($errores);
                                exit;
                            }
                        }

                        if (!empty($_POST['Workdetail']['WorkDetail_id'])) {
                            Yii::app()->user->setFlash('success', "แก้ไขข้อมูลงาน สำเร็จ!");
                            $this->redirect(Yii::app()->user->returnUrl);
                        }elseif (!empty($_POST['Workdetail']['TypeWork'])and$_POST['Workdetail']['TypeWork']=="request"){
                            Yii::app()->user->setFlash('success', "ส่งคำขอสำเร็จ สำเร็จ!");
                            $this->redirect(Yii::app()->user->returnUrl);
                        } else {
                            Yii::app()->user->setFlash('success', "เพิ่มงานใหม่ สำเร็จ!");
                            $this->redirect(Yii::app()->user->returnUrl);
                        }
                    }
                }
//                else{
//                    var_dump($model);
//                    $errores = $model->getErrors();
//                    var_dump($errores);
//                    exit;
//                }
            } elseif (isset($_POST['Staff'])) {
                // collects user input data
                $staff->attributes = $_POST['Staff'];
                // validates user input and redirect to previous page if validated
                if ($staff->validate()) {
                    if ($staff->save(false)) {
                        Workdetail::model()->updateAll(array('UserName' => $staff->UserName), 'UserName="' . $staff->BeforeUserName . '"');
                        Groupstaff::model()->updateAll(array('UserName' => $staff->UserName), 'UserName="' . $staff->BeforeUserName . '"');
                        PlanStaff::model()->updateAll(array('UserName' => $staff->UserName), 'UserName="' . $staff->BeforeUserName . '"');
                        Yii::app()->user->id = $staff->UserName;
                        Yii::app()->user->username = $staff->UserName;
                        Yii::app()->user->setFlash('success', "แก้ไขข้อมูลของคุณสำเร็จ สำเร็จ!");
                        $this->redirect(Yii::app()->user->returnUrl);
                    }else{
                        Yii::app()->user->setFlash('error', "ไม่สามารถ แก้ไขข้อมูล");
                        $this->redirect(Yii::app()->user->returnUrl);
                    }
                }else{
                    $errores = $staff->getErrors();
                    var_dump($errores);
                    exit;
                }
            }

            if (Yii::app()->user->type == 'admin') {
                if (!empty($type) && $type == 'Week') {
                    $dataWork = Workdetail::GetWorkDetailWeek();
                } elseif (!empty($type) && $type == 'ToDay') {
                    $dataWork = Workdetail::GetWorkDetailToDay();
                } elseif (!empty($type) && $type == 'YesterDay') {
                    $dataWork = Workdetail::GetWorkDetailYesterDay();
                } else {
                    $dataWork = Workdetail::GetWorkDetailYesterDay();
                }
            } else {
                $criteriawork = new CDbCriteria;
                $criteriawork->compare('Remove', 'N', FALSE);
                $criteriawork->addCondition('UserName = "' . Yii::app()->user->id . '"');
//                $criteriawork->addCondition("TypeWork != 'request'");
                $criteriawork->order = ('StartDate DESC');
                $criteriawork->limit=30;
                $dataWork = Workdetail::model()->findAll($criteriawork);
            }

            if(!empty(Yii::app()->user->SubOrganizer_id)){
                $dataSubOrganizer=SubOrganizer::model()->findByPk(Yii::app()->user->SubOrganizer_id);
                $dataProject = Project::GetProjectStaff($dataSubOrganizer->Company_id);
            }else {
                $dataProject = Project::GetProjectStaff();
            }

            if (Yii::app()->user->type == 'admin' || Yii::app()->user->type == 'manage' ) {
                if(!empty(Yii::app()->user->SubOrganizer_id)){
                    $criteriawork = new CDbCriteria;
                    $criteriawork->compare('Remove', 'N', FALSE);
                    $criteriawork->addCondition("Position != 'admin'");
                    $dataStaff=Staff::model()->findAll('SubOrganizer_id=?',array(Yii::app()->user->SubOrganizer_id));
                    $string="";
                    foreach($dataStaff as $key=>$value){
                        if($key==0) {
                            $string .= "UserName = '" . $value->UserName . "'";
                        }else{
                            $string .= " or UserName = '" . $value->UserName . "'";
                        }
                    }
                    $criteriawork->addCondition($string);
                    $criteriawork->order='SubOrganizer_id asc';
                    $dataStaff = Staff::model()->findAll($criteriawork);
                }else {
                    $dataStaff = Staff::model()->findAll(array('order'=>'SubOrganizer_id asc', 'condition'=>"remove='N' && Position!='admin'"));
                }
            } else {
                $dataStaff = Staff::model()->findAll(array('order'=>'SubOrganizer_id asc', 'condition'=>"remove='N' && UserName='".Yii::app()->user->id."'"));
            }
            if (Yii::app()->user->type == 'admin') {
                $dataRequest = Workdetail::model()->findAll(array('condition' => "TypeWork='request' and Remove='N'"));
            }
			$reportStaff=Workdetail::CheckHourWorkStaff('');

//            $reportStaff=null;
			$reportStaffWork=null;
			$reportStaffLeave=null;
            $reportStaffPlan=null;


//			$reportStaffWork=Workdetail::CheckHourWorkStaff('work');
//			$reportStaffLeave=Workdetail::CheckHourWorkStaff('leave');
//          $reportStaffPlan=Workdetail::CheckHourWorkStaff('plan');

			$this->render('index', array('dataRequest'=>$dataRequest,'staff'=>$staff,'model' => $model, 'dataWork' => $dataWork,'dataStaff'=>$dataStaff,'dataProject'=>$dataProject,'dataSelectPlan'=>$dataSelectPlan,'dataSelectPlanOne'=>$dataSelectPlanOne,'dataSelectPlanTwo'=>$dataSelectPlanTwo,'dataSelectPlanThree'=>$dataSelectPlanThree,'reportStaff'=>$reportStaff,'reportStaffLeave'=>$reportStaffLeave,'reportStaffWork'=>$reportStaffWork,'reportStaffPlan'=>$reportStaffPlan));
		}else{
			$this->redirect('/site/login');
		}
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login()) {
                Workdetail::CheckEndDate($model->username);
                $newLog = new Log();
                $newLog->UserName = $model->username;
                $newLog->detail = 'Log In';
                if ($newLog->validate()) {
                    $newLog->save();
                }
                $this->redirect(Yii::app()->user->returnUrl);
            }
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
        $newLog=new Log();
        $newLog->UserName=Yii::app()->user->id;
        $newLog->detail='Log out';
        if($newLog->validate()){
            $newLog->save();
        }
		Yii::app()->user->logout();
		Yii::app()->session->destroy();
		$this->redirect(Yii::app()->homeUrl);
	}

	public function actionRegister(){
		$model=new Staff();
		$model->scenario = 'Register';
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'formRegister') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		if(isset($_POST['Staff']))
		{
			// collects user input data
			$model->attributes=$_POST['Staff'];
			// validates user input and redirect to previous page if validated
			if($model->validate()) {
				if($model->save(false)){
					$this->redirect('/site/index');
				}
			}
		}
		$this->render('register',array('model'=>$model));
	}

	public function actionCloseWork(){
		$id = Yii::app()->request->getParam('id', null);
		if(!empty($id)){
			$dataUpdate=Workdetail::model()->findByPk($id);
			$dataUpdate->EndDate=Workdetail::getDateTime();
			if($dataUpdate->save(false)){
				Yii::app()->user->setFlash('success', "ปิดงาน สำเร็จ!");
				$this->redirect('/site/index');
			}
		}
	}

	public function actionDeleteWorkDetail(){
		$WorkDetail_id = Yii::app()->request->getParam('WorkDetail_id', null);
		$dataWorkDetail=Workdetail::model()->findByPk($WorkDetail_id);
		$dataPlanStaff=PlanStaff::model()->find('WorkDetail_id=?',array($WorkDetail_id));
		$dataRetrn=array();
		if(!empty($dataWorkDetail)){
			$dataWorkDetail->Remove='Y';
			if($dataWorkDetail->save(false)){
			    if(!empty($dataPlanStaff)){
			        $dataPlanStaff->Plan_staff_remove="Y";
			        if($dataPlanStaff->save(false)){
			            $dataRetrn['status'] = 'true';
                    }
                }else {
                    $dataRetrn['status'] = 'true';
                }
			}else{
				$dataRetrn['status']='false';
			}
			echo CJSON::encode($dataRetrn);
		}
	}

    public function actionGetHoliday(){
        $datetime = new DateTime("now", new DateTimeZone('Asia/Bangkok'));
        $dataRetrn=array();

        if($datetime->format('W')==0){

        }else{
            $datetime->modify('-1 day');
        }

        $dataRetrn['day']=$datetime->format('d');
        $dataRetrn['month']=$datetime->format('m');
        $dataRetrn['year']=$datetime->format('Y');


        echo CJSON::encode($dataRetrn);
    }

	public function actionGetWorkDetailPicker(){
		$selectMonthSearch = Yii::app()->request->getParam('selectMonthSearch', null);
        $selectYearSearch = Yii::app()->request->getParam('selectYearSearch', null);
        $selectDay = Yii::app()->request->getParam('selectDay', null);
        $UserName = Yii::app()->request->getParam('UserName', null);

		$date = new DateTime();
		$date->setDate($selectYearSearch, $selectMonthSearch,  $selectDay);

		$datePicker_start =$date->format('Y-m-d 00:00:00');
		$datePicker_end =$date->format('Y-m-d 23:59:59');
		$typeSearch='StartDate';

		$criteriasum = new CDbCriteria;
		$criteriasum->compare('Remove', 'N', FALSE);

		if(!empty($typeSearch) && !empty($datePicker_start) && !empty($datePicker_end)) {
			if (isset($datePicker_start) && !empty($datePicker_start)) {
				$criteriasum->addCondition(''. $typeSearch .' >= "' . $datePicker_start . '" ');
			}
			if (isset($datePicker_end) && !empty($datePicker_end)) {
				$criteriasum->addCondition(''. $typeSearch .' <= "' . $datePicker_end . '" ');
			}
		}

		if (isset($UserName) && !empty($UserName) && $UserName != '') {
			$criteriasum->addCondition('UserName = "' . $UserName . '"');
		}

		echo CJSON::encode(Workdetail::GetWorkDetail($criteriasum));
	}

    public function actionGetViewTable(){
        $type = Yii::app()->request->getParam('type', null);
        $selectMonthSearch = Yii::app()->request->getParam('selectMonthSearch', null);
        $selectYearSearch = Yii::app()->request->getParam('selectYearSearch', null);
        if (Yii::app()->user->type == 'admin' || Yii::app()->user->type == 'manage' ) {
            if(!empty(Yii::app()->user->SubOrganizer_id)){
                $criteriawork = new CDbCriteria;
                $criteriawork->compare('Remove', 'N', FALSE);
                $criteriawork->addCondition("Position != 'admin'");
                $dataStaff=Staff::model()->findAll('SubOrganizer_id=?',array(Yii::app()->user->SubOrganizer_id));
                $string="";
                foreach($dataStaff as $key=>$value){
                    if($key==0) {
                        $string .= "UserName = '" . $value->UserName . "'";
                    }else{
                        $string .= " or UserName = '" . $value->UserName . "'";
                    }
                }
                $criteriawork->addCondition($string);
                $criteriawork->order='SubOrganizer_id asc';
                $dataStaff = Staff::model()->findAll($criteriawork);
            }else {
                $dataStaff = Staff::model()->findAll(array('order'=>'SubOrganizer_id asc', 'condition'=>"remove='N' && Position!='admin'"));
            }
        } else {
            $dataStaff = Staff::model()->findAll(array('order'=>'SubOrganizer_id asc', 'condition'=>"remove='N' && UserName='".Yii::app()->user->id."'"));
        }
        switch ($type) {
            case 'all':
                $reportStaff=Workdetail::CheckHourWorkStaff('',$selectMonthSearch,$selectYearSearch);
                $this->render('gettableall',array('dataStaff'=>$dataStaff,'reportStaff'=>$reportStaff,'year'=>$selectYearSearch,'month'=>$selectMonthSearch));
                break;
            case 'leave':
                $reportStaffLeave=Workdetail::CheckHourWorkStaff('leave',$selectMonthSearch,$selectYearSearch);
                $this->render('gettableleave', array('dataStaff'=>$dataStaff,'reportStaffLeave' => $reportStaffLeave,'year'=>$selectYearSearch,'month'=>$selectMonthSearch));
                break;
            case 'work':
                $reportStaffWork=Workdetail::CheckHourWorkStaff('work',$selectMonthSearch,$selectYearSearch);
                $this->render('gettablework', array('dataStaff'=>$dataStaff,'reportStaffWork' => $reportStaffWork,'year'=>$selectYearSearch,'month'=>$selectMonthSearch));
                break;
            case 'plan':
                $reportStaffPlan=Workdetail::CheckHourWorkStaff('plan',$selectMonthSearch,$selectYearSearch);
                $this->render('gettableplan', array('dataStaff'=>$dataStaff,'reportStaffPlan' => $reportStaffPlan,'year'=>$selectYearSearch,'month'=>$selectMonthSearch));
                break;
            case 'report':
//                $checkLastLeave=Workdetail::CheckLastLeave();
                $checkLastLeave=Yii::app()->db->createCommand("select DATE(workdetail.StartDate) as dataDate,count(*) as total,Detail from workdetail where YEAR(StartDate) = YEAR(CURRENT_TIMESTAMP) and Remove='N' and workdetail.TypeWork='leave' and workdetail.UserName='".Yii::app()->user->username."'  GROUP BY DATE(workdetail.StartDate) order by dataDate DESC limit 20")->queryAll();
                $reportStaffReport=Workdetail::CheckReportStaff($selectMonthSearch,$selectYearSearch);
                $this->render('gettablereport',array('checkLastLeave'=>$checkLastLeave,'dataStaff'=>$dataStaff,'reportStaffReport'=>$reportStaffReport,'year'=>$selectYearSearch,'month'=>$selectMonthSearch));
                break;
            default:
        }
//        $reportStaff=Workdetail::CheckHourWorkStaff('',$selectMonthSearch,$selectYearSearch);
//        $dataRetrn['all']=$this->renderPartial('gettableall',array('dataStaff'=>$dataStaff,'reportStaff'=>$reportStaff,'year'=>$selectYearSearch,'month'=>$selectMonthSearch), true);
//        $reportStaffLeave=Workdetail::CheckHourWorkStaff('leave',$selectMonthSearch,$selectYearSearch);
//        $dataRetrn['leave']=$this->renderPartial('gettableleave', array('dataStaff'=>$dataStaff,'reportStaffLeave' => $reportStaffLeave,'year'=>$selectYearSearch,'month'=>$selectMonthSearch),true);
//        $reportStaffPlan=Workdetail::CheckHourWorkStaff('plan',$selectMonthSearch,$selectYearSearch);
//        $dataRetrn['plan']=$this->renderPartial('gettableplan', array('dataStaff'=>$dataStaff,'reportStaffPlan' => $reportStaffPlan,'year'=>$selectYearSearch,'month'=>$selectMonthSearch));
//        $reportStaffWork=Workdetail::CheckHourWorkStaff('work',$selectMonthSearch,$selectYearSearch);
//        $dataRetrn['work']=$this->renderPartial('gettablework', array('dataStaff'=>$dataStaff,'reportStaffWork' => $reportStaffWork,'year'=>$selectYearSearch,'month'=>$selectMonthSearch),true);
//        echo CJSON::encode($dataRetrn);
    }



	public function actionGetWorkDetail(){
		$Project_id = Yii::app()->request->getParam('Project_id', null);
		$UserName = Yii::app()->request->getParam('UserName', null);
		$datePicker_start = Yii::app()->request->getParam('datepicker_start', null);
		$datePicker_end = Yii::app()->request->getParam('datepicker_end', null);
		$typeSearch=Yii::app()->request->getParam('typeSearch',null);
		$type=Yii::app()->request->getParam('type',null);

        $newDateStart=new DateTime($datePicker_start);
        $newDateEnd=new DateTime($datePicker_end);
        $datePicker_start=$newDateStart->format('Y-m-d');
        $datePicker_end=$newDateEnd->format('Y-m-d');

		$criteriasum = new CDbCriteria;
		$criteriasum->compare('Remove', 'N', FALSE);

		if($type=='YesterDay'){
			$dateWeek=Workdetail::GetYesterday();
			if(!empty($typeSearch) && !empty($dateWeek['start']) && !empty($dateWeek['end'])) {
				$criteriasum->addCondition(''. $typeSearch .' >= "' . $dateWeek['start'] . '" ');
				$criteriasum->addCondition(''. $typeSearch .' <= "' . $dateWeek['end'] . '" ');
			}
		}elseif($type=='ToDay'){
			$dateWeek=Workdetail::GetToDay();
			if(!empty($typeSearch) && !empty($dateWeek['start']) && !empty($dateWeek['end'])) {
				$criteriasum->addCondition(''. $typeSearch .' >= "' . $dateWeek['start'] . '" ');
				$criteriasum->addCondition(''. $typeSearch .' <= "' . $dateWeek['end'] . '" ');
			}
		}elseif($type=='Week'){
			$dateWeek=Workdetail::GetWeekday();
			if(!empty($typeSearch) && !empty($dateWeek['start']) && !empty($dateWeek['end'])) {
				$criteriasum->addCondition(''. $typeSearch .' >= "' . $dateWeek['start'] . '" ');
				$criteriasum->addCondition(''. $typeSearch .' <= "' . $dateWeek['end'] . '" ');
			}
		}else if(!empty($typeSearch) && !empty($datePicker_start) && !empty($datePicker_end)) {
			if (isset($datePicker_start) && !empty($datePicker_start)) {
				$criteriasum->addCondition(''. $typeSearch .' >= "' . $datePicker_start . '" ');
			}
			if (isset($datePicker_end) && !empty($datePicker_end)) {
				$criteriasum->addCondition(''. $typeSearch .' <= "' . $datePicker_end . '" ');
			}
		}


		if (isset($UserName) && !empty($UserName) && $UserName != '') {
			$criteriasum->addCondition('UserName = "' . $UserName . '"');
		}

		if (isset($Project_id) && !empty($Project_id) && $Project_id != '') {
			$criteriasum->addCondition('Project_id = "' . $Project_id . '"');
		}

		$criteriasum->order=('StartDate DESC');

		echo CJSON::encode(Workdetail::GetWorkDetail($criteriasum));

	}

    public function actionSandEmail() {
        $data=Workdetail::getNotComplete();
        $dataResponsible=Responsible::getAll();
//        $this->render('mail',array('data'=>$data,'dataResponsible'=>$dataResponsible));
        $dataStaff=Staff::model()->findAll('Position=? || Position=?',array('admin','manage'));
        foreach ($dataStaff as $key => $value) {
            if($value->remove=='N' && !empty($value->Email)) {
                Yii::import('application.extensions.yii-mail.YiiMailMessage');
                $message = new YiiMailMessage; //เรียกใช้ YiiMailMessage ผ่าน $message
                $message->view = 'mail';
                $message->setBody(array('data' => $data,'dataResponsible'=>$dataResponsible), 'text/html'); //ใส่ข้อความลงให้เมลที่เราจะส่ง
                $message->subject = 'แจ้งเตือน จากระบบ TimeSheet'; //หัวข้อของ Email ที่จะส่ง
                $message->addTo($value->Email); //ใส่ email ปลายทาง
                $message->from = 'sootipong303@gmail.com'; //ใส่ Email ผู้ส่ง
                Yii::app()->mail->send($message); //ส่ง Email
            }
        }
    }

    public function actionViewSendMail(){
        $data=Workdetail::getNotComplete();
        $dataResponsible=Responsible::getAll();
        $this->render('mail',array('data'=>$data,'dataResponsible'=>$dataResponsible));
    }

    public function actionConfirmWorkDetail(){
        $id = Yii::app()->request->getParam('id', null);
        Workdetail::model()->updateByPk($id, array("TypeWork"=>'work'));
        Yii::app()->user->setFlash('success', "ยืนยันข้อมูล สำเร็จ!");
        $this->redirect('/site/index');
    }
    public function actionConfirmWorkDetailAll(){
        $dataRequest = Workdetail::model()->findAll(array('condition'=>"TypeWork='request'"));
        foreach ($dataRequest as $key=>$value){
            Workdetail::model()->updateByPk($value->WorkDetail_id, array("TypeWork"=>'work'));
        }
        Yii::app()->user->setFlash('success', "ยืนยันข้อมูลทั้งหมด สำเร็จ!");
        $this->redirect('/site/index');
    }
}
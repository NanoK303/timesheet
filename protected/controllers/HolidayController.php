<?php
class HolidayController extends Controller
{
    public function init()
    {
        if (Yii::app()->user->isGuest) {
            $this->redirect('/site/login');
        }
    }

    public function actionDelete(){
        $id = Yii::app()->request->getParam('id', null);
        if(!empty($id)){
            Holiday::model()->updateByPk($id,array('holiday_remove'=>'Y'));
            $this->redirect('/holiday/manage');
        }
    }

    public function actionManage(){
        $id = Yii::app()->request->getParam('id', null);

        if(!empty($id)){
            $model=Holiday::model()->findByPk($id);
        }else {
            $model = new Holiday();
        }

        if(isset($_POST['ajax']) && $_POST['ajax']==='formAddHoliday')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if(isset($_POST['Holiday']))
        {
            $model->attributes=$_POST['Holiday'];
            // validate user input and redirect to the previous page if valid
            if($model->validate()) {
                if($model->save(false)) {
                    $this->redirect('/holiday/manage');
                }
            }
        }

        $dataHoliday=Holiday::model()->findAll('holiday_remove=?  ORDER BY holiday_id DESC',array('N'));
        $this->render('manage',array('model'=>$model,'dataHoliday'=>$dataHoliday));
    }
}
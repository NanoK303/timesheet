<?php
class ResponsibleController extends Controller{

    public function actionManage(){

        if(isset($_POST['search'])){
            $criteria = new CDbCriteria;
            if($_POST['search']['day']){
                $criteria->addCondition('responsible_day = "' . $_POST['search']['day'] . '" ');
            }
            if($_POST['search']['month']){
                $criteria->addCondition('responsible_month = "' . $_POST['search']['month'] . '" ');
            }
            if($_POST['search']['year']){
                $criteria->addCondition('responsible_year = "' . $_POST['search']['year'] . '" ');
            }
            $criteria->addCondition("responsible_remove ='N'");
            $dataResponsible = Responsible::model()->with('responsibleUserName', 'userName')->findAll($criteria);
        }else {
            $dataResponsible = Responsible::model()->with('responsibleUserName', 'userName')->findAll('responsible_remove=?',array('N'));
        }
        $this->render('manage',array('dataResponsible'=>$dataResponsible));
    }

    public function actionRemoveResponsible(){
        $id = Yii::app()->request->getParam('id', null);
        Responsible::model()->updateByPk($id,array('responsible_remove'=>'Y'));
        $this->redirect('/responsible/manage');
    }

    public function actionAddResponsible(){
        $user = Yii::app()->request->getParam('user', null);
        $day = Yii::app()->request->getParam('day', null);
        $month = Yii::app()->request->getParam('month', null);
        $year = Yii::app()->request->getParam('year', null);
        if(!empty($user)&&!empty($day)&&!empty($month)&&!empty($year)){
            $newResponsible=new Responsible();
            $newResponsible->responsible_day=$day;
            $newResponsible->responsible_month=$month;
            $newResponsible->responsible_year=$year;
            $newResponsible->UserName=$user;
            $newResponsible->responsible_user_name=Yii::app()->user->id;
            if($newResponsible->save(false)){
                $this->redirect('/site/index');
            }
        }

    }

}
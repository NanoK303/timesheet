<?php
class StaffController extends Controller
{
    public function actionRemoveSubOrganizer(){
        $UserName = Yii::app()->request->getParam('UserName', null);
        $dataStaff=Staff::model()->findByPk($UserName);
        if(!empty($dataStaff)){
            $dataStaff->SubOrganizer_id=null;
            if($dataStaff->save(false)){
                Yii::app()->user->setFlash('success', "ลบพนักงานลงกลุ่มงาน สำเร็จ!");
                $this->redirect('/staff/manageOrganizer');
            }
        }
    }

    public function actionRemoveOrganizer(){
        $id = Yii::app()->request->getParam('id', null);
        $dataOrganizer=SubOrganizer::model()->findByPk($id);
        if(!empty($dataOrganizer)){
            $dataOrganizer->Remove='Y';
            if($dataOrganizer->save(false)){
                Yii::app()->user->setFlash('success', "ลบกลุ่มงาน สำเร็จ!");
                $this->redirect('/staff/manageOrganizer');
            }
        }

    }

    public function actionAddStaffToOrganizer(){
        if($_POST['add']){
            if(!empty($_POST['add']['SubOrganizer_id']) and !empty($_POST['add']['UserName'])){
                $dataStaff=Staff::model()->findByPk($_POST['add']['UserName']);
                if(!empty($dataStaff)){
                    $dataStaff->SubOrganizer_id=$_POST['add']['SubOrganizer_id'];
                    if($dataStaff->save(false)){
                        Yii::app()->user->setFlash('success', "เพิ่มพนักงานลงกลุ่มงาน สำเร็จ!");
                        $this->redirect('/staff/manageOrganizer');
                    }
                }
            }
        }
    }

    public function actionManageOrganizer(){
        $id = Yii::app()->request->getParam('id', null);

        if(!empty($id)){
            $organizer=SubOrganizer::model()->findByPk($id);
        }else {
            $organizer = new SubOrganizer();
        }

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'formOrganizer') {
            echo CActiveForm::validate($organizer);
            Yii::app()->end();
        }

        if (isset($_POST['SubOrganizer'])) {
            // collects user input data
            $organizer->attributes = $_POST['SubOrganizer'];
            // validates user input and redirect to previous page if validated
            if ($organizer->validate()) {
                if ($organizer->save(false)) {
                    if (!empty($id)) {
                        Yii::app()->user->setFlash('success', "แก้ไขข้อมูลกลุ่มงาน สำเร็จ!");
                    }else {
                        Yii::app()->user->setFlash('success', "เพิ่มกลุ่มงาน สำเร็จ!");
                    }
                    $this->redirect('/staff/manageOrganizer');
                }
            }
        }
        $dataOrganizer=SubOrganizer::model()->with('company','staff')->findAll();
        $dateStaff=Staff::model()->findAll('remove=?',array('N'));
        $this->render('organizer',array('dateStaff'=>$dateStaff,'organizer'=>$organizer,'dataOrganizer'=>$dataOrganizer));
    }

    public function actionDeleteStaff(){
        if (!empty(Yii::app()->user->id)) {
            if(Yii::app()->user->type=='admin') {
                $id = Yii::app()->request->getParam('username', null);
                $staff=Staff::model()->findByPk($id);
                $staff->remove='Y';
                if ($staff->validate()) {
                    if ($staff->save(false)) {
                        Yii::app()->user->setFlash('success', "ลบข้อมูลพนักงานใหม่ สำเร็จ!");
                        $this->redirect('/staff/managestaff');
                    }
                }
            }else{
                $this->redirect('/site/index');
            }
        }else{
            $this->redirect('/site/login');
        }
    }

    public function actionManageStaff(){
        if (!empty(Yii::app()->user->id)) {
            if(Yii::app()->user->type=='admin') {
                $id = Yii::app()->request->getParam('username', null);
                if (!empty($id)) {
                    $staff = Staff::model()->findByPk($id);
                    $staff->Password = '';
                    $staff->BeforeUserName = $staff->UserName;
                    $staff->BeforeEmail = $staff->Email;
                    $staff->scenario = 'Edit';
                } else {
                    $staff = new Staff();
                    $staff->scenario = 'Register';
                }

                if (isset($_POST['ajax']) && $_POST['ajax'] === 'formRegister') {
                    echo CActiveForm::validate($staff);
                    Yii::app()->end();
                }

                if (isset($_POST['Staff'])) {
                    // collects user input data
                    $staff->attributes = $_POST['Staff'];
                    // validates user input and redirect to previous page if validated
                    if ($staff->validate()) {
                        if ($staff->save(false)) {
                            if (!empty($id)) {
                                Workdetail::model()->updateAll(array('UserName'=>$staff->UserName),'UserName="'.$staff->BeforeUserName.'"');
                                Groupstaff::model()->updateAll(array('UserName'=>$staff->UserName),'UserName="'.$staff->BeforeUserName.'"');
                                PlanStaff::model()->updateAll(array('UserName'=>$staff->UserName),'UserName="'.$staff->BeforeUserName.'"');
                                Yii::app()->user->setFlash('success', "แก้ไขข้อมูลพนักงานใหม่ สำเร็จ!");
                            } else {
                                Yii::app()->user->setFlash('success', "เพิ่มพนักงานใหม่ สำเร็จ!");
                            }
                            $this->redirect('/staff/managestaff');
                        }
                    }
                }


                $dataStaff = Staff::model()->findAll('remove=?', array('N'));
                $this->render('managestaff', array('staff' => $staff, 'dataStaff' => $dataStaff));
            }else{
                $this->redirect('/site/index');
            }
        }else{
            $this->redirect('/site/login');
        }
    }
}
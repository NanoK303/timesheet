<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>
<div class="row" style="">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'formAddHoliday',
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => true,
        ),
        'htmlOptions' => array(
            'class' => 'form form-inline right',
            'style'=>'margin-bottom: 20px;'
        ),
    ));
    echo $form->hiddenField($model, 'holiday_id');
    ?>
    <div class="form-group">
<!--        <label for="exampleInputName2">บริษัท</label>-->
        <div class="form">
            <?php echo $form->error($model,'holiday_day'); ?>
        </div>
        <?php
        echo $form->labelEx($model,'holiday_day',array('style'=>'text-align: left;'));
        echo $form->dropDownList($model,'holiday_day', array('1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8','9'=>'9','10'=>'10','11'=>'11','12'=>'12','13'=>'13','14'=>'14','15'=>'15','16'=>'16','17'=>'17','18'=>'18','19'=>'19','20'=>'20','21'=>'21','22'=>'22','23'=>'23','24'=>'24','25'=>'25','26'=>'26','27'=>'27','28'=>'28','29'=>'29','30'=>'30','31'=>'31'), array('empty'=>'เลือกวัน','class' => 'form-control'))
        ?>

    </div>
    <div class="form-group">
<!--        <label for="exampleInputName2">ชื่อ</label>-->
        <div class="form">
            <?php echo $form->error($model, 'holiday_month'); ?>
        </div>
        <?php
//        echo $form->labelEx($model,'holiday_month',array('style'=>'text-align: left;'));
        echo $form->dropDownList($model,'holiday_month', array("1"=>"มกราคม","2"=>"กุมภาพันธ์", "3"=>"มีนาคม", "4"=>"เมษายน", "5"=>"พฤษภาคม", "6"=>"มิถุนายน", "7"=>"กรกฎาคม", "8"=>"สิงหาคม", "9"=>"กันยายน", "10"=>"ตุลาคม", "11"=>"พฤศจิกายน", "12"=>"ธันวาคม"), array('empty'=>'เลือกเดือน','class' => 'form-control')) ?>

    </div>
    <div class="form-group">
<!--        <label for="exampleInputEmail2">ชั่วโมง</label>-->
        <div class="form">
            <?php echo $form->error($model, 'holiday_year'); ?>
        </div>
        <?php
//        echo $form->labelEx($model,'holiday_year',array('style'=>'text-align: left;'));
        echo $form->dropDownList($model,'holiday_year', array('2013' => '2013','2014' => '2014','2015' => '2015','2016' => '2016','2017' => '2017','2018' => '2018','2019' => '2019','2020' => '2020'), array('empty'=>'กรุณาปี','class' => 'form-control')); ?>

    </div>
    <div class="form-group">
<!--        <label for="exampleInputEmail2">ประเภท</label>-->
        <div class="form">
            <?php echo $form->error($model, 'holiday_detail'); ?>
        </div>
        <?php
        echo $form->labelEx($model,'holiday_detail',array('style'=>'text-align: left;'));
        echo $form->textfield($model, 'holiday_detail', array('class' => 'form-control','style'=>'width:300px;')); ?>
    </div>
    <div class="form-group">
        <?php
        if(!empty($model->holiday_id)) {
            echo CHtml::submitButton(Yii::t('main', 'แก้ไข'), array('id' => 'addform', 'class' => 'btn btn-warning','style'=>'float: left'));
            ?>
            <a style="float: left;margin-left: 5px;" href="/holiday/manage" class="btn btn-default">กลับ</a>
        <?php
        }else{
            echo CHtml::submitButton(Yii::t('main', 'เพิ่ม'), array('id' => 'addform', 'class' => 'btn btn-primary'));
        }
        ?>
    </div>
    <?php $this->endWidget(); ?>
    <table class="table table-bordered">
    <thead>
    <tr>
        <th>#</th>
        <th>วันที่</th>
        <th>รายละเอียด</th>
        <th>จัดการ</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($dataHoliday as $key=>$value){ ?>
    <tr>
        <th scope="row"><?php echo $value->holiday_id ?></th>
        <td><?php echo $value->holiday_day.'/'.$value->holiday_month.'/'.$value->holiday_year ?></td>
        <td><?php echo $value->holiday_detail ?></td>
        <td>
            <a href="/holiday/manage/<?php echo $value->holiday_id ?>" class="btn btn-primary">แก้ไข</a>
            <a href="/holiday/delete/<?php echo $value->holiday_id ?>" onclick="return confirm('ยืนยันการลบข้อมูล วันหยุดนี้ ?')" class="btn btn-danger ">ลบ</a>
        </td>
    </tr>
   <?php } ?>
    </tbody>
</table>
</div>

<?php
$this->layout = false;
$day=Workdetail::getDateTimeNowDay();
$monthNow=Workdetail::getDateTimeNowMonth();
$yearNow=Workdetail::getDateTimeNowYear();
$monthNames = array("","มกราคม","กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
if($month==$monthNow && $year==$yearNow){
    $day=Workdetail::getDateTimeNowDay();
}else{
    $day=Workdetail::checkNumberOfDays($year,$month);
//    echo $day;
}


?>
<!--<table class="table table-bordered" style="border-top: solid 1px #fff;">-->
<thead>
<tr>
    <td rowspan="2" style="text-align: center;">วันที่</td>
    <td colspan="<?php echo Workdetail::checkNumberOfDays($year,$month) ?>" style="text-align: center; border-bottom-color:#080808;border-width:1px; ">
        <div style="float: left; width: 40%;font-weight: bold;font-size: 28px;">ตารางสรุปเวลางาน</div>
        <div style="float: left; width: 30%;"></div>
    </td>
    <td colspan="4" style="border-bottom-color:#080808;border-width: 1px;" ><div style="text-align: center;font-weight: bold:font-size:28px;">รวม</div></td>
</tr>
</thead>
<tbody>
<tr>
    <td></td>
    <?php for ($i = 1; $i <= Workdetail::checkNumberOfDays($year,$month); $i++) { ?>
        <td style="border-bottom-color: #080808;"><?php echo $i; ?></td>
    <?php } ?>
    <td style="border-bottom-color: #080808;">รวม</td>
    <td style="border-bottom-color: #080808;">ทั้งหมด</td>
    <td style="border-bottom-color: #080808;" >ลา(<?php echo $month?>)</td>
    <td style="border-bottom-color: #080808;text-align: center;padding: 3px;" >
        <select id="select-leave" class="form-control input-sm" style="width: 60px;padding: 0px;">
            <option value="leave-1">ทั้งหมด</option>
            <option value="leave-2">ป่วย</option>
            <option value="leave-3">อื่น</option>
            <option value="leave-4">พักร้อน</option>
            <option value="leave-5">ลาคลอด</option>
            <option value="leave-6">ลาบวช</option>
            <option value="leave-7">ลาสมรส</option>
            <option value="leave-8">ขาดงาน</option>
        </select>
    </td>
</tr>
<?php if(!empty($reportStaff)){
$sum1=0;
$sum2=0;
$sum3=0;
$sum4=0;
$sum5=0;
$sum6=0;
$sum7=0;
$sum8=0;
$sum9=0;
$sum10=0;
$sum11=0;
?>
<!--				--><?php //foreach($reportStaff as $mainKey=> $mainValue){
//					if(!empty($mainValue)){
//						?>
    <tbody class="tbody_Month" style="border: solid 1px #000;"  id="table-view-all">
    <?php foreach($dataStaff as $key=>$value){ ?>
        <!--							--><?php //if(!empty($mainValue[$value->UserName])){
        $sum=0;
        $sumleave=0;

        ?>
        <tr id="UserNameNotEmpty" >
            <td style="padding: 0px;border-right-color: #080808;"><span data-toggle="tooltip" data-placement="top" title="<?php echo Staff::GetNickName($value->UserName); ?>"><?php echo $value->Name ?></span> </td>
            <?php for ($i = 1; $i <= Workdetail::checkNumberOfDays($year,$month); $i++) { ?>
                <td  id="<?php echo (empty($reportStaff[$value->UserName][$i])) ? 'td_View_Report_Empty' : 'td_View_Report_NotEmpty' ?>" day="<?php echo $i ?>" UserName="<?php echo $value->UserName ?>" style="padding: 0px;border-color: #080808;text-align: center;" class="<?php
                if ((empty($reportStaff[$value->UserName][$i]))){
                    if(Holiday::CheckHoliday($year,$month,$i)) {
                        echo 'active';
                    }
                }else{
                    if(Holiday::CheckHoliday($year,$month,$i)) {
                        echo 'success';
                    }elseif(!empty($reportStaff[$value->UserName][$i]['leave'])){
                        echo 'info';
                    }elseif($reportStaff[$value->UserName][$i]['type']=='plan'){
                        echo 'info';
                    }elseif($reportStaff[$value->UserName][$i]['type']=='leave' || $reportStaff[$value->UserName][$i]['value']==32400 ){
                        echo 'warning';
                    }elseif ($reportStaff[$value->UserName][$i]['value']>32400){
                        echo 'success';
                    }else{
                        echo 'danger';
                    }
                }
                ?>">
                    <?php
                    if(!empty($reportStaff[$value->UserName][$i]['leave'])){
                        $sumleave+=$reportStaff[$value->UserName][$i]['leave'];
                    }

                    if ((!empty($reportStaff[$value->UserName][$i]))){
                        if($reportStaff[$value->UserName][$i]['type']=='work' || $reportStaff[$value->UserName][$i]['type']=='plan') {
                            $sum += $reportStaff[$value->UserName][$i]['value'];
                            if(!empty($reportStaff[$value->UserName][$i]['leave']) && !empty($reportStaff[$value->UserName][$i]['value'])){
                                echo "<span data-toggle='tooltip' data-placement='top' title='ลา ". floor($reportStaff[$value->UserName][$i]['leave']/3600) ." ชั่วโมง' >";
                            }elseif($reportStaff[$value->UserName][$i]['type']=='plan'){
                                echo "<span data-toggle='tooltip' data-placement='top' title='".$reportStaff[$value->UserName][$i]['Detail']."'>";
                            }else {
                                echo "<span>";
                            }
                            echo Workdetail::GetTimeWorkShort($reportStaff[$value->UserName][$i]['value']);
                            echo "</sapn>";
                        }elseif($reportStaff[$value->UserName][$i]['type']=='leave' && empty($reportStaff[$value->UserName][$i]['value'])){
                            if(!empty($reportStaff[$value->UserName][$i]['Detail'])) {
                                $data = explode(',', $reportStaff[$value->UserName][$i]['Detail']);
                                if(count($data)>1){
                                    if($data[0]=='ขาดงาน'){ ?>
                                        <span data-toggle="tooltip" data-placement="top" title="<?php echo $data[1] ?>" class="glyphicon glyphicon-minus" aria-hidden="true"></span>
                                    <?php }elseif($data[0]=='ลาป่วย'){ ?>
                                        <i data-toggle="tooltip" data-placement="top" title="<?php echo $data[1] ?>" class="fa fa-bed"></i>
                                    <?php }elseif($data[0]=='ลาอื่นๆ'){?>
                                        <span data-toggle="tooltip" data-placement="top" title="<?php echo $data[1] ?>" class="glyphicon glyphicon-briefcase" aria-hidden="true"></span>
                                    <?php }elseif($data[0]=='ลาคลอด'){?>
                                        <i data-toggle="tooltip" data-placement="top" title="<?php echo $data[1] ?>" class="fa fa-child"></i>
                                    <?php }elseif($data[0]=='ลาบวช'){?>
                                        <i data-toggle="tooltip" data-placement="top" title="<?php echo $data[1] ?>" class="fa fa-street-view"></i>
                                    <?php }elseif($data[0]=='ลาสมรส'){?>
                                        <i data-toggle="tooltip" data-placement="top" title="<?php echo $data[1] ?>" class="fa fa-venus-mars"></i>
                                    <?php }elseif($data[0]=='ลาพักร้อน'){?>
                                        <i data-toggle="tooltip" data-placement="top" title="<?php echo $data[1] ?>" class="fa fa-ship"></i>
                                    <?php }elseif($data[0]=='ขออนุมัติ'){?>
                                        <i data-toggle="tooltip" data-placement="top" title="<?php echo $data[1] ?>" class="fa fa-gavel"></i>
                                    <?php }
                                }else{
                                    ?>
                                    <span data-toggle="tooltip" data-placement="top" title="<?php echo $reportStaff[$value->UserName][$i]['Detail'] ?>" class="glyphicon glyphicon-home" aria-hidden="true"></span>
                                <?php }}
                        }elseif(!empty($reportStaff[$value->UserName][$i]['value'])){
                            echo "<span data-toggle='tooltip' data-placement='top' title='ลา ". floor($reportStaff[$value->UserName][$i]['leave']/3600) ." ชั่วโมง' >";
                            echo Workdetail::GetTimeWorkShort($reportStaff[$value->UserName][$i]['value']);
                            echo "</sapn>";
                        }
                    }else{
                        if(!Holiday::CheckHoliday($year,$month,$i) && ($i<=$day)) {
                            if(Responsible::CheckResponsible($value->UserName,$i,$month,$year)){ ?>
                                <span data-toggle="tooltip" data-placement="top" title="<?php echo Responsible::CheckResponsible($value->UserName,$i,$month,$year) ?>" class='glyphicon glyphicon-credit-card' aria-hidden='true'></span>
                            <?php }else {
                                if (Yii::app()->user->type == 'admin') { ?>
                                    <a href="/responsible/addresponsible/user/<?php echo $value->UserName ?>/day/<?php echo $i ?>/month/<?php echo $month?>/year/<?php echo $year; ?>"
                                       style="padding: 0px 8px" class="btn btn-default"
                                       onclick="return confirm('ยืนยันการรับผิดชอบ ค่าใช่จ่าย ของ <?php echo $value->Name ?> <?php echo $value->LastName ?> ในวันที่ <?php echo $i ?> เดือน <?php echo $monthNames[$month] ?> ปี <?php echo $year ?>  ')"><span
                                            class='glyphicon glyphicon-ban-circle'
                                            aria-hidden='true'></span></a>
                                <?php } else { ?>
                                    <span class='glyphicon glyphicon-ban-circle' aria-hidden='true'></span>
                                <?php }
                            }
                        }
                    }
                    ?>
                </td>
            <?php } ?>
            <td class="success" style="padding: 0px;text-align: center;border-color: #080808;"><?php echo Workdetail::GetTimeWorkShort($sum) ?></td>
            <td class="success" style="padding: 0px;text-align: center;border-color: #080808;font-weight: bold;"><?php echo Workdetail::GetTimeWorkShort(Workdetail::GetTimeAllUserName($year,$value->UserName,'work')) ?></td>
            <td class="warning" style="padding: 0px;text-align: center;border-color: #080808;"><?php echo Workdetail::GetFormatTimeHoliday($sumleave) ?></td>
            <td class="warning" style="padding: 0px;text-align: center;border-color: #080808;font-weight: bold;">
                <span class="leave-view leave-1"><?php echo Workdetail::GetFormatTimeHoliday(Workdetail::GetTimeAllUserName($year,$value->UserName,'leave')) ?></span>
                <span class="leave-view leave-2 hide"><?php echo Workdetail::GetFormatTimeHoliday(Workdetail::GetTimeAllUserNameBy($year,$value->UserName,'leave','ลาป่วย')) ?></span>
                <span class="leave-view leave-3 hide"><?php echo Workdetail::GetFormatTimeHoliday(Workdetail::GetTimeAllUserNameBy($year,$value->UserName,'leave','ลาอื่นๆ')) ?></span>
                <span class="leave-view leave-4 hide"><?php echo Workdetail::GetFormatTimeHoliday(Workdetail::GetTimeAllUserNameBy($year,$value->UserName,'leave','ลาพักร้อน')) ?></span>
                <span class="leave-view leave-5 hide"><?php echo Workdetail::GetFormatTimeHoliday(Workdetail::GetTimeAllUserNameBy($year,$value->UserName,'leave','ลาคลอด')) ?></span>
                <span class="leave-view leave-6 hide"><?php echo Workdetail::GetFormatTimeHoliday(Workdetail::GetTimeAllUserNameBy($year,$value->UserName,'leave','ลาบวช')) ?></span>
                <span class="leave-view leave-7 hide"><?php echo Workdetail::GetFormatTimeHoliday(Workdetail::GetTimeAllUserNameBy($year,$value->UserName,'leave','ลาสมรส')) ?></span>
                <span class="leave-view leave-8 hide"><?php echo Workdetail::GetFormatTimeHoliday(Workdetail::GetTimeAllUserNameBy($year,$value->UserName,'leave','ขาดงาน')) ?></span>
            </td>
            <?php
            $sum1+=$sum;
            $sum2+=Workdetail::GetTimeAllUserName($year,$value->UserName,'work');
            $sum3+=$sumleave;
            $sum4+=Workdetail::GetTimeAllUserNameBy($year,$value->UserName,'leave','ลาป่วย');
            $sum5+=Workdetail::GetTimeAllUserNameBy($year,$value->UserName,'leave','ลาอื่นๆ');
            $sum6+=Workdetail::GetTimeAllUserNameBy($year,$value->UserName,'leave','ลาพักร้อน');
            $sum7+=Workdetail::GetTimeAllUserNameBy($year,$value->UserName,'leave','ลาคลอด');
            $sum8+=Workdetail::GetTimeAllUserNameBy($year,$value->UserName,'leave','ลาบวช');
            $sum9+=Workdetail::GetTimeAllUserNameBy($year,$value->UserName,'leave','ลาสมรส');
            $sum11+=Workdetail::GetTimeAllUserNameBy($year,$value->UserName,'leave','ขาดงาน');
            $sum10+=Workdetail::GetTimeAllUserName($year,$value->UserName,'leave');
            ?>
        </tr>
    <?php }?>
    <tr>
        <td style="padding: 0px;text-align: center;border-color: #080808;font-weight: bold;" class="active"  colspan="<?php echo Workdetail::checkNumberOfDays($year,$month)+1 ?>" >รวม</td>
        <td style="padding: 0px;text-align: center;border-color: #080808;font-weight: bold;"><?php echo Workdetail::GetTimeWorkShort($sum1); ?></td>
        <td style="padding: 0px;text-align: center;border-color: #080808;font-weight: bold;"><?php echo Workdetail::GetTimeWorkShort($sum2); ?></td>
        <td style="padding: 0px;text-align: center;border-color: #080808;font-weight: bold;"><?php echo Workdetail::GetFormatTimeHoliday($sum3); ?></td>
        <td style="padding: 0px;text-align: center;border-color: #080808;font-weight: bold;">
            <span class="leave-view leave-1"><?php echo Workdetail::GetFormatTimeHoliday($sum10); ?></span>
            <span class="leave-view leave-2 hide"><?php echo Workdetail::GetFormatTimeHoliday($sum4); ?></span>
            <span class="leave-view leave-3 hide"><?php echo Workdetail::GetFormatTimeHoliday($sum5); ?></span>
            <span class="leave-view leave-4 hide"><?php echo Workdetail::GetFormatTimeHoliday($sum6); ?></span>
            <span class="leave-view leave-5 hide"><?php echo Workdetail::GetFormatTimeHoliday($sum7); ?></span>
            <span class="leave-view leave-6 hide"><?php echo Workdetail::GetFormatTimeHoliday($sum8); ?></span>
            <span class="leave-view leave-7 hide"><?php echo Workdetail::GetFormatTimeHoliday($sum9); ?></span>
            <span class="leave-view leave-8 hide"><?php echo Workdetail::GetFormatTimeHoliday($sum11); ?></span>
        </td>
    </tr>
    </tbody>
        <?php } ?>
<?php //}} ?>
<!--</table>-->
<?php
$this->layout = false;
$day=Workdetail::getDateTimeNowDay();
?>
<!--<table class="table table-bordered" style="border-top: solid 1px #fff;">-->
    <thead>
    <tr>
        <td rowspan="2" style="text-align: center;">วันที่</td>
        <td colspan="<?php echo Workdetail::checkNumberOfDays($year,$month) ?>" style="text-align: center;">
            <div style="float: left; width: 40%;font-weight: bold;font-size: 28px;">ตารางสรุปเวลางาน</div>
        </td>
        <td colspan="2" style="border-bottom-color:#080808;border-width: 1px;" ><div style="text-align: center;font-weight: bold:font-size:28px;">รวม</div></td>
    </tr>
    <tr>
        <?php for ($i = 1; $i <= Workdetail::checkNumberOfDays($year,$month); $i++) { ?>
            <td style="border-bottom-color: #080808;"><?php echo $i; ?></td>
        <?php } ?>
        <td style="border-bottom-color: #080808;">รวม</td>
        <td style="border-bottom-color: #080808;">ทั้งหมด</td>
        <!--					<td style="border-bottom-color: #080808;">ลา</td>-->
    </tr>
    </thead>
    <?php if(!empty($reportStaffWork)){ ?>
<!--    --><?php //foreach($reportStaffWork as $mainKey=> $mainValue){
//        if(!empty($mainValue)){
//            ?>
            <tbody class="tbody_Month" style="border: solid 1px #000;"  >
            <?php foreach($dataStaff as $key=>$value){ ?>
                <!--							--><?php //if(!empty($mainValue[$value->UserName])){
                $sum=0;
                $sumleave=0;
                ?>
                <tr>
                    <td style="padding: 0px;border-right-color: #080808;"><span data-toggle="tooltip" data-placement="top" title="<?php echo Staff::GetNickName($value->UserName); ?>"><?php echo $value->Name ?></span> </td>
                    <?php for ($i = 1; $i <= Workdetail::checkNumberOfDays($year,$month); $i++) { ?>
                        <td  id="<?php echo (empty($reportStaffWork[$value->UserName][$i])) ? 'td_View_Report_Empty' : 'td_View_Report_NotEmpty' ?>" day="<?php echo $i ?>" UserName="<?php echo $value->UserName ?>" style="padding: 0px;border-color: #080808;text-align: center;" class="<?php
                        if ((empty($reportStaffWork[$value->UserName][$i]))){
                            if(!Holiday::CheckHoliday($year,$month,$i)) {
                                echo 'active';
                            }
                        }else{
                            if(!empty($reportStaffWork[$value->UserName][$i]['leave'])){
                                echo 'warning';
                            }elseif($reportStaffWork[$value->UserName][$i]['type']=='plan'){
                                echo 'info';
                            }elseif($reportStaffWork[$value->UserName][$i]['type']=='leave'){
                                echo 'warning';
                            }elseif ($reportStaffWork[$value->UserName][$i]['value']>=32400){
                                echo 'success';
                            }else{
                                echo 'danger';
                            }
                        }
                        ?>">
                            <?php
                            if(!empty($reportStaffWork[$value->UserName][$i]['leave'])){
                                $sumleave+=$reportStaffWork[$value->UserName][$i]['leave'];
                            }

                            if ((!empty($reportStaffWork[$value->UserName][$i]))){
                                if($reportStaffWork[$value->UserName][$i]['type']=='work' || $reportStaffWork[$value->UserName][$i]['type']=='plan') {
                                    $sum += $reportStaffWork[$value->UserName][$i]['value'];
                                    if(!empty($reportStaffWork[$value->UserName][$i]['leave']) && !empty($reportStaffWork[$value->UserName][$i]['value'])){
                                        echo "<span data-toggle='tooltip' data-placement='top' title='ลา ". floor($reportStaffWork[$value->UserName][$i]['leave']/3600) ." ชั่วโมง' >";
                                    }elseif($reportStaffWork[$value->UserName][$i]['type']=='plan'){
                                        echo "<span data-toggle='tooltip' data-placement='top' title='".$reportStaffWork[$value->UserName][$i]['Detail']."'>";
                                    }else {
                                        echo "<span>";
                                    }
                                    echo Workdetail::GetTimeWorkShort($reportStaffWork[$value->UserName][$i]['value']);
                                    echo "</sapn>";
                                }elseif($reportStaffWork[$value->UserName][$i]['type']=='leave'){
                                    if(!empty($reportStaffWork[$value->UserName][$i]['Detail'])) {
                                        $data = explode(',', $reportStaffWork[$value->UserName][$i]['Detail']);
                                        if(count($data)>1){
                                            if($data[0]=='ขาดงาน'){ ?>
                                                <span data-toggle="tooltip" data-placement="top" title="<?php echo $data[1] ?>" class="glyphicon glyphicon-minus" aria-hidden="true"></span>
                                            <?php }elseif($data[0]=='ลาป่วย'){ ?>
                                                <i data-toggle="tooltip" data-placement="top" title="<?php echo $data[1] ?>" class="fa fa-bed"></i>
                                            <?php }elseif($data[0]=='ลาอื่นๆ'){?>
															<span data-toggle="tooltip" data-placement="top" title="<?php echo $data[1] ?>" class="glyphicon glyphicon-briefcase aria-hidden="true"></span>
														<?php }elseif($data[0]=='ลาคลอด'){?>
                                                <i data-toggle="tooltip" data-placement="top" title="<?php echo $data[1] ?>" class="fa fa-child"></i>
                                            <?php }elseif($data[0]=='ลาบวช'){?>
                                                <i data-toggle="tooltip" data-placement="top" title="<?php echo $data[1] ?>" class="fa fa-street-view"></i>
                                            <?php }elseif($data[0]=='ลาสมรส'){?>
                                                <i data-toggle="tooltip" data-placement="top" title="<?php echo $data[1] ?>" class="fa fa-venus-mars"></i>
                                            <?php }elseif($data[0]=='ลาพักร้อน'){?>
                                                <i data-toggle="tooltip" data-placement="top" title="<?php echo $data[1] ?>" class="fa fa-ship"></i>
                                            <?php }elseif($data[0]=='ขออนุมัติ'){?>
                                                <i data-toggle="tooltip" data-placement="top" title="<?php echo $data[1] ?>" class="fa fa-gavel"></i>
                                            <?php }
                                        }else{
                                            ?>
                                            <span data-toggle="tooltip" data-placement="top" title="<?php echo $reportStaffWork[$value->UserName][$i]['Detail'] ?>" class="glyphicon glyphicon-home" aria-hidden="true"></span>
                                        <?php }}}
                            }
                            ?>
                        </td>
                    <?php } ?>
                    <td class="success" style="padding: 0px;text-align: center;border-color: #080808;"><?php echo Workdetail::GetTimeWorkShort($sum) ?></td>
                    <td class="success" style="padding: 0px;text-align: center;border-color: #080808;font-weight: bold;"><?php echo Workdetail::GetTimeWorkShort(Workdetail::GetTimeAllUserName($value->UserName,'work')) ?></td>
                    <!--								<td class="danger" style="padding: 0px;text-align: center;border-color: #080808;">--><?php //echo Workdetail::GetFormatTimeHoliday($sumleave) ?><!--</td>-->
                    <!--							--><?php //} ?>
                </tr>
            <?php }?>
            </tbody>
<!--        --><?php //}} ?>
<?php } ?>
<!--</table>-->
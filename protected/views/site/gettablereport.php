<?php
$this->layout = false;
$day=Workdetail::getDateTimeNowDay();
$sum1=0;$sum2=0;$sum3=0;$sum4=0;$sum5=0;$sum6=0;$sum7=0;$sum8=0;
?>
<!--<table class="table table-bordered" style="border-top: solid 1px #fff;">-->
    <thead>
    <tr>
        <td rowspan="2" style="text-align: center;">วันที่</td>
        <td colspan="8" style="text-align: center;">
            <div style="float: left; width: 40%;font-weight: bold;font-size: 28px;">ตารางสรุปการลา</div>
        </td>
        <td rowspan="2"  style="border-bottom-color:#080808;border-width: 1px;" ><div style="text-align: center;font-weight: bold:font-size:28px;">รวม</div></td>
    </tr>
    <tr>
        <td style="border-bottom-color: #080808; text-align: center;font-weight: bold;">ขาดงาน<br>เดือน/ปี</td>
        <td style="border-bottom-color: #080808; text-align: center;font-weight: bold;">ลาป่วย<br>เดือน/ปี</td>
        <td style="border-bottom-color: #080808; text-align: center;font-weight: bold;">ลาอื่นๆ<br>เดือน/ปี</td>
        <td style="border-bottom-color: #080808; text-align: center;font-weight: bold;">ลาคลอด<br>เดือน/ปี</td>
        <td style="border-bottom-color: #080808; text-align: center;font-weight: bold;">ลาบวช<br>เดือน/ปี</td>
        <td style="border-bottom-color: #080808; text-align: center;font-weight: bold;">ลาสมรส<br>เดือน/ปี</td>
        <td style="border-bottom-color: #080808; text-align: center;font-weight: bold;">ลาพักร้อน<br>เดือน/ปี</td>
        <td style="border-bottom-color: #080808; text-align: center;font-weight: bold;">ขออนุมัติ<br>เดือน/ปี</td>
<!--        <td style="border-bottom-color: #080808;">รวม</td>-->
    </tr>
    </thead>
    <?php foreach($dataStaff as $key=>$value){
//            if(!empty($reportStaffReport[$value->UserName]['ขาดงาน']))
//                $sum1+=$reportStaffReport[$value->UserName]['ขาดงาน'];
//            if(!empty($reportStaffReport[$value->UserName]['ลาป่วย']))
//                $sum2+=$reportStaffReport[$value->UserName]['ลาป่วย'];
//            if(!empty($reportStaffReport[$value->UserName]['ลาอื่นๆ']))
//                $sum3+=$reportStaffReport[$value->UserName]['ลาอื่นๆ'];
//            if(!empty($reportStaffReport[$value->UserName]['ลาคลอด']))
//                $sum4+=$reportStaffReport[$value->UserName]['ลาคลอด'];
//            if(!empty($reportStaffReport[$value->UserName]['ลาบวช']))
//                $sum5+=$reportStaffReport[$value->UserName]['ลาบวช'];
//            if(!empty($reportStaffReport[$value->UserName]['ลาสมรส']))
//                $sum6+=$reportStaffReport[$value->UserName]['ลาสมรส'];
//            if(!empty($reportStaffReport[$value->UserName]['ลาพักร้อน']))
//                $sum7+=$reportStaffReport[$value->UserName]['ลาพักร้อน'];
//            if(!empty($reportStaffReport[$value->UserName]['ขออนุมัต']))
//                $sum8+=$reportStaffReport[$value->UserName]['ขออนุมัต'];
        ?>
        <tr>
            <td style="padding: 0px;border-right-color: #080808;" ><?php echo $value->Name ?></td>
            <td style="padding: 0px;border-color: #080808;text-align: center;" >
                <?php echo(!empty($reportStaffReport[$value->UserName]['ขาดงาน']['month'])?$reportStaffReport[$value->UserName]['ขาดงาน']['month']:0)?>/<span style="font-weight: bolder;"></span><?php echo(!empty($reportStaffReport[$value->UserName]['ขาดงาน']['year'])?$reportStaffReport[$value->UserName]['ขาดงาน']['year']:0)?></span>
            </td>
            <td style="padding: 0px;border-color: #080808;text-align: center;">
                <?php echo(!empty($reportStaffReport[$value->UserName]['ลาป่วย']['month'])?$reportStaffReport[$value->UserName]['ลาป่วย']['month']:0)?>/<span style="font-weight: bolder;"><?php echo(!empty($reportStaffReport[$value->UserName]['ลาป่วย']['year'])?$reportStaffReport[$value->UserName]['ลาป่วย']['year']:0)?></span>
            </td>
            <td style="padding: 0px;border-color: #080808;text-align: center;">
                <?php echo(!empty($reportStaffReport[$value->UserName]['ลาอื่นๆ']['month'])?$reportStaffReport[$value->UserName]['ลาอื่นๆ']['month']:0)?>/<span style="font-weight: bolder;"><?php echo(!empty($reportStaffReport[$value->UserName]['ลาอื่นๆ']['year'])?$reportStaffReport[$value->UserName]['ลาอื่นๆ']['year']:0)?></span>
            </td>
            <td style="padding: 0px;border-color: #080808;text-align: center;">
                <?php echo(!empty($reportStaffReport[$value->UserName]['ลาคลอด']['month'])?$reportStaffReport[$value->UserName]['ลาคลอด']['month']:0)?>/<span style="font-weight: bolder;"><?php echo(!empty($reportStaffReport[$value->UserName]['ลาคลอด']['year'])?$reportStaffReport[$value->UserName]['ลาคลอด']['year']:0)?></span>
            </td>
            <td style="padding: 0px;border-color: #080808;text-align: center;">
                <?php echo(!empty($reportStaffReport[$value->UserName]['ลาบวช']['month'])?$reportStaffReport[$value->UserName]['ลาบวช']['month']:0)?>/<span style="font-weight: bolder;"><?php echo(!empty($reportStaffReport[$value->UserName]['ลาบวช']['year'])?$reportStaffReport[$value->UserName]['ลาบวช']['year']:0)?></span>
            </td>
            <td style="padding: 0px;border-color: #080808;text-align: center;">
                <?php echo(!empty($reportStaffReport[$value->UserName]['ลาสมรส']['month'])?$reportStaffReport[$value->UserName]['ลาสมรส']['month']:0)?>/<span style="font-weight: bolder;"><?php echo(!empty($reportStaffReport[$value->UserName]['ลาสมรส']['year'])?$reportStaffReport[$value->UserName]['ลาสมรส']['year']:0)?></span>
            </td>
            <td style="padding: 0px;border-color: #080808;text-align: center;">
                <?php echo(!empty($reportStaffReport[$value->UserName]['ลาพักร้อน']['month'])?$reportStaffReport[$value->UserName]['ลาพักร้อน']['month']:0)?>/<span style="font-weight: bolder;"><?php echo(!empty($reportStaffReport[$value->UserName]['ลาพักร้อน']['year'])?$reportStaffReport[$value->UserName]['ลาพักร้อน']['year']:0)?></span>
            </td>
            <td style="padding: 0px;border-color: #080808;text-align: center;">
                <?php echo(!empty($reportStaffReport[$value->UserName]['ขออนุมัต']['month'])?$reportStaffReport[$value->UserName]['ขออนุมัต']['month']:0)?>/<span style="font-weight: bolder;"><?php echo(!empty($reportStaffReport[$value->UserName]['ขออนุมัต']['year'])?$reportStaffReport[$value->UserName]['ขออนุมัต']['year']:0)?></span>
            </td>
            <td class="active" style="padding: 0px;border-color: #080808;text-align: center;font-weight: bold;">
                <?php echo(!empty($reportStaffReport[$value->UserName]['sumMonth'])?$reportStaffReport[$value->UserName]['sumMonth']:0)?>/<span style="font-weight: bolder;"><?php echo(!empty($reportStaffReport[$value->UserName]['sumYear'])?$reportStaffReport[$value->UserName]['sumYear']:0)?></span>
            </td>
        </tr>
    <?php } ?>
        <tr>
            <td colspan="9">
                <?php foreach($checkLastLeave as $key=>$value){
                    $data = explode(',',$value['Detail']);
                    ?>
                    <span style="font-weight: bolder;">วันที่ <?php echo $value['dataDate']; ?> <?php echo $data[0]; ?> <?php echo (($value['total']==2)?'1 วัน':'คลึ่งวัน')?> สาเหตุ: <?php echo $data[1]; ?></span></br>
                <?php } ?>
            </td>
        </tr>
<!--    <tr>-->
<!--        <td class="active" style="padding: 0px;border-color: #080808;text-align: center;font-weight: bold;">รวม</td>-->
<!--        <td class="active" style="padding: 0px;border-color: #080808;text-align: center;font-weight: bold;">--><?php //echo $sum1; ?><!--</td>-->
<!--        <td class="active" style="padding: 0px;border-color: #080808;text-align: center;font-weight: bold;">--><?php //echo $sum2; ?><!--</td>-->
<!--        <td class="active" style="padding: 0px;border-color: #080808;text-align: center;font-weight: bold;">--><?php //echo $sum3; ?><!--</td>-->
<!--        <td class="active" style="padding: 0px;border-color: #080808;text-align: center;font-weight: bold;">--><?php //echo $sum4; ?><!--</td>-->
<!--        <td class="active" style="padding: 0px;border-color: #080808;text-align: center;font-weight: bold;">--><?php //echo $sum5; ?><!--</td>-->
<!--        <td class="active" style="padding: 0px;border-color: #080808;text-align: center;font-weight: bold;">--><?php //echo $sum6; ?><!--</td>-->
<!--        <td class="active" style="padding: 0px;border-color: #080808;text-align: center;font-weight: bold;">--><?php //echo $sum7; ?><!--</td>-->
<!--        <td class="active" style="padding: 0px;border-color: #080808;text-align: center;font-weight: bold;">--><?php //echo $sum8; ?><!--</td>-->
<!--        <td class="active" style="padding: 0px;border-color: #080808;text-align: center;font-weight: bold;"></td>-->
<!--    </tr>-->
<!--</table>-->

<?php
$this->layout='column1';
?>
<div class="row" style="font-weight:bold;font-size: 28px; margin: 0px;padding: 0px;">
    สมัครสมาชิก
</div>
<div class="row">
<div class="form" style="margin-left: 25%;width: 50%;">
    <?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'formRegister',
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
    'enableAjaxValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
//        'validateOnChange' => true,
//        'validateOnType' => true,
    ),
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    ),
));
?>
    <div class="form-group">
        <label for="inputPassword3" class="col-sm-4 control-label">ชื่อ</label>
        <div class="col-sm-8">
            <?php echo $form->textField($model, 'Name', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'Name'); ?>
        </div>
    </div>

    <div class="form-group">
        <label for="inputPassword3" class="col-sm-4 control-label">นามสกุล</label>
        <div class="col-sm-8">
            <?php echo $form->textField($model, 'LastName', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'LastName'); ?>
        </div>
    </div>

        <div class="form-group">
            <label for="inputPassword3" class="col-sm-4 control-label">ชื่อเล่น</label>
            <div class="col-sm-8">
                <?php echo $form->textField($model, 'Nickname', array('class' => 'form-control')); ?>
                <?php echo $form->error($model, 'Nickname'); ?>
            </div>
        </div>

    <div class="form-group">
        <label for="inputPassword3" class="col-sm-4 control-label">ชื่อเข้าระบบ</label>
        <div class="col-sm-8">
            <?php echo $form->textField($model, 'UserName', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'UserName'); ?>
        </div>
    </div>

    <div class="form-group">
        <label for="inputPassword3" class="col-sm-4 control-label">รหัสผ่าน</label>
        <div class="col-sm-8">
            <?php echo $form->passwordField($model, 'Password', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'Password'); ?>
        </div>
    </div>

    <div class="form-group">
        <label for="inputPassword3" class="col-sm-4 control-label">ยืนยันรหัสผ่าน</label>
        <div class="col-sm-8">
            <?php echo $form->passwordField($model, 'Password2', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'Password2'); ?>
        </div>
    </div>

    <div class="form-group">
        <label for="inputPassword3" class="col-sm-4 control-label">อีเมล์</label>
        <div class="col-sm-8">
            <?php echo $form->textField($model, 'Email', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'Email'); ?>
        </div>
    </div>

    <div class="form-group">
        <label for="inputPassword3" class="col-sm-4 control-label">เบอร์โทร</label>
        <div class="col-sm-8">
            <?php echo $form->textField($model, 'Phone', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'Phone'); ?>
        </div>
    </div>

    <div class="form-group">
        <label for="inputPassword3" class="col-sm-4 control-label">ที่อยู่</label>
        <div class="col-sm-8">
            <?php echo $form->textArea($model, 'Address', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'Address'); ?>
        </div>
    </div>

    <div class="form-group">
        <label for="inputPassword3" class="col-sm-4 control-label">ตำแหน่ง</label>
        <div class="col-sm-8">
            <?php echo $form->dropDownList($model,'Position', array('programmer'=>'โปรแกรมเมอร์','design'=>'ออกแบบ','engineer'=>'วิศวกร','foreman'=>'โฟร์แมน','architect'=>'สถาปนิก'), array('empty'=>'เลือกตำแหน่ง','class' => 'form-control')) ?>
            <?php echo $form->error($model, 'Position'); ?>
        </div>
    </div>

        <div class="form-group">
            <div class="col-sm-12">
                <div style="float: right;">
                <?php echo CHtml::submitButton(Yii::t('main', 'สมัคร'), array('id' => 'addform', 'class' => 'btn btn-primary')); ?>
                </div>
            </div>
        </div>
<?php $this->endWidget(); ?>
</div>
</div>
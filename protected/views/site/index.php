﻿<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
$dateNow=date_create(Yii::app()->db->createCommand("select now()")->queryScalar());
$Hour=intval($dateNow->format('H'));
$day=Workdetail::getDateTimeNowDay();
$month=Workdetail::getDateTimeNowMonth();
$year=Workdetail::getDateTimeNowYear();
$monthNames = array("","มกราคม","กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");

?>

<style>
	#td_View_Report_NotEmpty,#td_View_Report_Empty{
		cursor: pointer;
	}

    #UserNameNotEmpty:hover{
        /*color: #000000;*/
        background-color: rgba(0, 255, 0, .2) !important;
    }

    #td_View_Report_NotEmpty:hover{
		color: #000000;
		background-color: rgba(0, 255, 0, 0.50);

		/*font-weight: bold;*/
	}
		/*background-color: #cccccc;*/
	#td_View_Report_Empty:hover{
		background-color: rgba(0, 200, 0, .5);
	}
    tbody>tr>td>img{
        margin-left: 50%;
        width: 5%;
        opacity: 0.6;
    }
</style>

<!--**********************************start ตารางสรุปเวลางาน-->
<div class="row">
	<ul class="nav nav-tabs" role="tablist" id="myTab" style="">
		<li role="presentation" class="active" type="all" ><a type="all" href="#home" aria-controls="home" role="tab" data-toggle="tab">รวม</a></li>
		<li role="presentation" type="leave" ><a type="leave" href="#profile"  aria-controls="profile" role="tab" data-toggle="tab">วันลา</a></li>
		<li role="presentation" type="work" ><a type="work" href="#messages" aria-controls="messages" role="tab" data-toggle="tab">ทำงาน</a></li>
        <li role="presentation" type="plan"><a type="plan" href="#plan" aria-controls="plan" role="tab" data-toggle="tab">ตามแผน</a></li>
        <li role="presentation" type="report"><a type="report" href="#report" aria-controls="report" role="tab" data-toggle="tab">สรุป</a></li>
		<li style="width: 50%;float: left;">
			<div style="float: right; width: 50%;font-weight: bold;font-size: 20px;text-align: center;">
				<button style="margin: 0px;float: left;" class="btn btn-info" id="btn_Back" selectYear="<?php echo $year?>" selectMonth="<?php echo $month; ?>" sum="<?php echo count($reportStaff) ?>"> << </button>
				เดือน <span id="nameMonth"><?php echo $monthNames[$month].' / '.$year; ?></span>
				<button style="margin: 0px;float: right;" class="btn btn-info" id="btn_Next" selectYear="<?php echo $year?>" selectMonth="<?php echo $month; ?>" sum="<?php echo count($reportStaff) ?>"> >> </button>
			</div>
		</li>
		<li style="width: 20%;float: left;">
			<div class="form-inline" style="width: 100%;float: right;padding-left: 15%; ">
				<div class="form-group">
					<select class="form-control" id="selectMonthSearch">
						<?php
						foreach($monthNames as $key=>$value){
							if(!empty($value)){	?>
								<option <?php echo ($month==$key)?'selected':''; ?> value="<?php echo $key ?>"><?php echo $value ?></option>
							<?php }} ?>
					</select>
				</div>
				<div class="form-group">
					<select class="form-control" id="selectYearSearch">
						<?php
						for ($i = 2010; $i <= 2020; $i++) { ?>
							<option <?php echo ($year==$i)?'selected':''; ?> value="<?php echo $i ?>"><?php echo $i ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
		</li>
<!--		<li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>-->
	</ul>

	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="home">
			<table class="table table-bordered" style="border-top: solid 1px #fff;" id="table-view-all">
				<thead>
				<tr>
					<td rowspan="2" style="text-align: center;">วันที่</td>
					<td colspan="<?php echo Workdetail::checkNumberOfDays($year,$month) ?>" style="text-align: center; border-bottom-color:#080808;border-width:1px; ">
						<div style="float: left; width: 40%;font-weight: bold;font-size: 28px;">ตารางสรุปเวลางาน</div>
						<div style="float: left; width: 30%;"></div>
					</td>
                    <td colspan="4" style="border-bottom-color:#080808;border-width: 1px;" ><div style="text-align: center;font-weight: bold:font-size:28px;">รวม</div></td>
				</tr>
				</thead>
                <tbody>
                    <tr>
                        <td></td>
                        <?php for ($i = 1; $i <= Workdetail::checkNumberOfDays($year,$month); $i++) { ?>
                            <td style="border-bottom-color: #080808;"><?php echo $i; ?></td>
                        <?php } ?>
                        <td style="border-bottom-color: #080808;">รวม</td>
                        <td style="border-bottom-color: #080808;">ทั้งหมด</td>
                        <td style="border-bottom-color: #080808;" >ลา(<?php echo $month?>)</td>
                        <td style="border-bottom-color: #080808;text-align: center;padding: 3px;" >
                            <select id="select-leave" class="form-control input-sm" style="width: 60px;padding: 0px;">
                                <option value="leave-1">ทั้งหมด</option>
                                <option value="leave-2">ป่วย</option>
                                <option value="leave-3">อื่น</option>
                                <option value="leave-4">พักร้อน</option>
                                <option value="leave-5">ลาคลอด</option>
                                <option value="leave-6">ลาบวช</option>
                                <option value="leave-7">ลาสมรส
                                <option value="leave-8">ขาดงาน</option>
                            </select>
                        </td>
                    </tr>
                    <?php if(!empty($reportStaff)){
                    $sum1=0;
                    $sum2=0;
                    $sum3=0;
                    $sum4=0;
                    $sum5=0;
                    $sum6=0;
                    $sum7=0;
                    $sum8=0;
                    $sum9=0;
                    $sum10=0;
                    $sum11=0;
                    ?>
<!--				--><?php //foreach($reportStaff as $mainKey=> $mainValue){
//					if(!empty($mainValue)){
//						?>
						<tbody class="tbody_Month" style="border: solid 1px #000;"  id="table-view-all">
						<?php foreach($dataStaff as $key=>$value){ ?>
<!--							--><?php //if(!empty($mainValue[$value->UserName])){
								$sum=0;
								$sumleave=0;

								?>
								<tr id="UserNameNotEmpty" >
								<td style="padding: 0px;border-right-color: #080808;"><span data-toggle="tooltip" data-placement="top" title="<?php echo Staff::GetNickName($value->UserName); ?>"><?php echo $value->Name ?></span> </td>
								<?php for ($i = 1; $i <= Workdetail::checkNumberOfDays($year,$month); $i++) { ?>
									<td  id="<?php echo (empty($reportStaff[$value->UserName][$i])) ? 'td_View_Report_Empty' : 'td_View_Report_NotEmpty' ?>" day="<?php echo $i ?>" UserName="<?php echo $value->UserName ?>" style="padding: 0px;border-color: #080808;text-align: center;" class="<?php
									if ((empty($reportStaff[$value->UserName][$i]))){
										if(Holiday::CheckHoliday($year,$month,$i)) {
											echo 'active';
										}
									}else{
                                        if(Holiday::CheckHoliday($year,$month,$i)) {
                                            echo 'success';
                                        }elseif(!empty($reportStaff[$value->UserName][$i]['leave'])){
											echo 'info';
                                        }elseif($reportStaff[$value->UserName][$i]['type']=='plan'){
                                            echo 'info';
										}elseif($reportStaff[$value->UserName][$i]['type']=='leave' || (!empty($reportStaff[$value->UserName][$i]['value']) && $reportStaff[$value->UserName][$i]['value']==32400)){
											echo 'warning';
										}elseif ((!empty($reportStaff[$value->UserName][$i]['value']) && $reportStaff[$value->UserName][$i]['value']>32400)){
											echo 'success';
										}else{
											echo 'danger';
										}
									}
									?>">
										<?php
//										if(!empty($reportStaff[$value->UserName][$i]['leave'])){
////											$sumleave+=$reportStaff[$value->UserName][$i]['leave'];
//										}

										if ((!empty($reportStaff[$value->UserName][$i]))){
											if($reportStaff[$value->UserName][$i]['type']=='work' || $reportStaff[$value->UserName][$i]['type']=='plan') {
//												$sum += $reportStaff[$value->UserName][$i]['value'];
												if(!empty($reportStaff[$value->UserName][$i]['leave']) && !empty($reportStaff[$value->UserName][$i]['value'])){
                                                    echo "<span data-toggle='tooltip' data-placement='top' title='ลา' ". floor($reportStaff[$value->UserName][$i]['leave']/3600) ." ชั่วโมง' >";
                                                }elseif($reportStaff[$value->UserName][$i]['type']=='plan'){
                                                    echo "<span data-toggle='tooltip' data-placement='top' title='".$reportStaff[$value->UserName][$i]['Detail']."'>";
												}else {
													echo "<span>";
												}
												echo Workdetail::GetTimeWorkShort($reportStaff[$value->UserName][$i]['value']);
												echo "</sapn>";
											}elseif($reportStaff[$value->UserName][$i]['type']=='leave' && empty($reportStaff[$value->UserName][$i]['value'])){
												if(!empty($reportStaff[$value->UserName][$i]['Detail'])) {
													$data = explode(',', $reportStaff[$value->UserName][$i]['Detail']);
													if(count($data)>1){
														if($data[0]=='ขาดงาน'){ ?>
															<span data-toggle="tooltip" data-placement="top" title="<?php echo $data[1] ?>" class="glyphicon glyphicon-minus" aria-hidden="true"></span>
														<?php }elseif($data[0]=='ลาป่วย'){ ?>
															<i data-toggle="tooltip" data-placement="top" title="<?php echo $data[1] ?>" class="fa fa-bed"></i>
														<?php }elseif($data[0]=='ลาอื่นๆ'){?>
															<span data-toggle="tooltip" data-placement="top" title="<?php echo $data[1] ?>" class="glyphicon glyphicon-briefcase" aria-hidden="true"></span>
														<?php }elseif($data[0]=='ลาคลอด'){?>
															<i data-toggle="tooltip" data-placement="top" title="<?php echo $data[1] ?>" class="fa fa-child"></i>
														<?php }elseif($data[0]=='ลาบวช'){?>
															<i data-toggle="tooltip" data-placement="top" title="<?php echo $data[1] ?>" class="fa fa-street-view"></i>
														<?php }elseif($data[0]=='ลาสมรส'){?>
															<i data-toggle="tooltip" data-placement="top" title="<?php echo $data[1] ?>" class="fa fa-venus-mars"></i>
														<?php }elseif($data[0]=='ลาพักร้อน'){?>
															<i data-toggle="tooltip" data-placement="top" title="<?php echo $data[1] ?>" class="fa fa-ship"></i>
														<?php }elseif($data[0]=='ขออนุมัติ'){?>
															<i data-toggle="tooltip" data-placement="top" title="<?php echo $data[1] ?>" class="fa fa-gavel"></i>
														<?php }
													}else{
														?>
														<span data-toggle="tooltip" data-placement="top" title="<?php echo $reportStaff[$value->UserName][$i]['Detail'] ?>" class="glyphicon glyphicon-home" aria-hidden="true"></span>
													<?php }}
                                            }elseif(!empty($reportStaff[$value->UserName][$i]['value'])){
                                                echo "<span data-toggle='tooltip' data-placement='top' title='ลา ". floor($reportStaff[$value->UserName][$i]['leave']/3600) ." ชั่วโมง' >";
                                                echo Workdetail::GetTimeWorkShort($reportStaff[$value->UserName][$i]['value']);
                                                echo "</sapn>";
                                            }
										}else{
                                            if(!Holiday::CheckHoliday($year,$month,$i) && ($i<=$day)) {
                                                if(Responsible::CheckResponsible($value->UserName,$i,$month,$year)){ ?>
                                                    <span data-toggle="tooltip" data-placement="top" title="<?php echo Responsible::CheckResponsible($value->UserName,$i,$month,$year) ?>" class='glyphicon glyphicon-credit-card' aria-hidden='true'></span>
                                                <?php }else {
                                                    if (Yii::app()->user->type == 'admin') { ?>
                                                        <a href="/responsible/addresponsible/user/<?php echo $value->UserName ?>/day/<?php echo $i ?>/month/<?php echo $month?>/year/<?php echo $year; ?>"
                                                           style="padding: 0px 8px" class="btn btn-default"
                                                           onclick="return confirm('ยืนยันการรับผิดชอบ ค่าใช่จ่าย ของ <?php echo $value->Name ?> <?php echo $value->LastName ?> ในวันที่ <?php echo $i ?> เดือน <?php echo $monthNames[$month] ?> ปี <?php echo $year ?>  ')"><span
                                                                class='glyphicon glyphicon-ban-circle'
                                                                aria-hidden='true'></span></a>
                                                    <?php } else { ?>
                                                        <span class='glyphicon glyphicon-ban-circle'
                                                              aria-hidden='true'></span>
                                                    <?php }
                                                }
                                            }
                                        }
										?>
									</td>
								<?php } ?>
								<td class="success" style="padding: 0px;text-align: center;border-color: #080808;"><?php echo Workdetail::GetTimeWorkShort((!empty($reportStaff[$value->UserName]['totalValue'])?$reportStaff[$value->UserName]['totalValue']:0)); ?></td>
                                <td class="success" style="padding: 0px;text-align: center;border-color: #080808;font-weight: bold;"><?php echo Workdetail::GetTimeWorkShort((!empty($reportStaff[$value->UserName]['totalYearWork'])?$reportStaff[$value->UserName]['totalYearWork']:0)) ?></td>
                                <td class="warning" style="padding: 0px;text-align: center;border-color: #080808;"><?php echo Workdetail::GetFormatTimeHoliday((!empty($reportStaff[$value->UserName]['totalLeave'])?$reportStaff[$value->UserName]['totalLeave']:0)); ?></td>
                                <td class="warning" style="padding: 0px;text-align: center;border-color: #080808;font-weight: bold;">
                                    <span class="leave-view leave-1"><?php echo Workdetail::GetFormatTimeHoliday((!empty($reportStaff[$value->UserName]['totalYearLeave'])?$reportStaff[$value->UserName]['totalYearLeave']:0)) ?></span>
                                    <span class="leave-view leave-2 hide"><?php echo Workdetail::GetFormatTimeHoliday((!empty($reportStaff[$value->UserName]['totalYearSick'])?$reportStaff[$value->UserName]['totalYearSick']:0)) ?></span>
                                    <span class="leave-view leave-3 hide"><?php echo Workdetail::GetFormatTimeHoliday((!empty($reportStaff[$value->UserName]['totalYearOther'])?$reportStaff[$value->UserName]['totalYearOther']:0)) ?></span>
                                    <span class="leave-view leave-4 hide"><?php echo Workdetail::GetFormatTimeHoliday((!empty($reportStaff[$value->UserName]['totalYearVacation'])?$reportStaff[$value->UserName]['totalYearVacation']:0)) ?></span>
                                    <span class="leave-view leave-5 hide"><?php echo Workdetail::GetFormatTimeHoliday((!empty($reportStaff[$value->UserName]['totalYearProduce'])?$reportStaff[$value->UserName]['totalYearProduce']:0)) ?></span>
                                    <span class="leave-view leave-6 hide"><?php echo Workdetail::GetFormatTimeHoliday((!empty($reportStaff[$value->UserName]['totalYearOrdain'])?$reportStaff[$value->UserName]['totalYearOrdain']:0)) ?></span>
                                    <span class="leave-view leave-7 hide"><?php echo Workdetail::GetFormatTimeHoliday((!empty($reportStaff[$value->UserName]['totalYearMarry'])?$reportStaff[$value->UserName]['totalYearMarry']:0)) ?></span>
                                    <span class="leave-view leave-8 hide"><?php echo Workdetail::GetFormatTimeHoliday((!empty($reportStaff[$value->UserName]['totalYearAbsence'])?$reportStaff[$value->UserName]['totalYearAbsence']:0)) ?></span>
                                </td>
                                    <?php
                                    $sum1+=(!empty($reportStaff[$value->UserName]['totalValue'])?$reportStaff[$value->UserName]['totalValue']:0);
                                    $sum2+=(!empty($reportStaff[$value->UserName]['totalYearWork'])?$reportStaff[$value->UserName]['totalYearWork']:0);
                                    $sum3+=(!empty($reportStaff[$value->UserName]['totalLeave'])?$reportStaff[$value->UserName]['totalLeave']:0);
                                    $sum4+=(!empty($reportStaff[$value->UserName]['totalYearSick'])?$reportStaff[$value->UserName]['totalYearSick']:0);
                                    $sum5+=(!empty($reportStaff[$value->UserName]['totalYearOther'])?$reportStaff[$value->UserName]['totalYearOther']:0);
                                    $sum6+=(!empty($reportStaff[$value->UserName]['totalYearVacation'])?$reportStaff[$value->UserName]['totalYearVacation']:0);
                                    $sum7+=(!empty($reportStaff[$value->UserName]['totalYearProduce'])?$reportStaff[$value->UserName]['totalYearProduce']:0);
                                    $sum8+=(!empty($reportStaff[$value->UserName]['totalYearOrdain'])?$reportStaff[$value->UserName]['totalYearOrdain']:0);
                                    $sum9+=(!empty($reportStaff[$value->UserName]['totalYearMarry'])?$reportStaff[$value->UserName]['totalYearMarry']:0);
                                    $sum11+=(!empty($reportStaff[$value->UserName]['totalYearAbsence'])?$reportStaff[$value->UserName]['totalYearAbsence']:0);
                                    $sum10+=(!empty($reportStaff[$value->UserName]['totalLeave'])?$reportStaff[$value->UserName]['totalLeave']:0)
//                                    ?>
							</tr>
						<?php }?>
                <tr>
                    <td style="padding: 0px;text-align: center;border-color: #080808;font-weight: bold;" class="active"  colspan="<?php echo Workdetail::checkNumberOfDays($year,$month)+1 ?>" >รวม</td>
                    <td id="sum1" style="padding: 0px;text-align: center;border-color: #080808;font-weight: bold;"><?php echo Workdetail::GetTimeWorkShort($sum1); ?></td>
                    <td id="sum2" style="padding: 0px;text-align: center;border-color: #080808;font-weight: bold;"><?php echo Workdetail::GetTimeWorkShort($sum2); ?></td>
                    <td id="sum3" style="padding: 0px;text-align: center;border-color: #080808;font-weight: bold;"><?php echo Workdetail::GetFormatTimeHoliday($sum3); ?></td>
                    <td style="padding: 0px;text-align: center;border-color: #080808;font-weight: bold;">
                        <span class="leave-view leave-1"><?php echo Workdetail::GetFormatTimeHoliday($sum10); ?></span>
                        <span class="leave-view leave-2 hide"><?php echo Workdetail::GetFormatTimeHoliday($sum4); ?></span>
                        <span class="leave-view leave-3 hide"><?php echo Workdetail::GetFormatTimeHoliday($sum5); ?></span>
                        <span class="leave-view leave-4 hide"><?php echo Workdetail::GetFormatTimeHoliday($sum6); ?></span>
                        <span class="leave-view leave-5 hide"><?php echo Workdetail::GetFormatTimeHoliday($sum7); ?></span>
                        <span class="leave-view leave-6 hide"><?php echo Workdetail::GetFormatTimeHoliday($sum8); ?></span>
                        <span class="leave-view leave-7 hide"><?php echo Workdetail::GetFormatTimeHoliday($sum9); ?></span>
                        <span class="leave-view leave-8 hide"><?php echo Workdetail::GetFormatTimeHoliday($sum11); ?></span>
                    </td>
                </tr>
						</tbody>
					<?php } ?>
<!--                    --><?php //}} ?>
			</table>
		</div>
		<div role="tabpanel" class="tab-pane" id="profile">
            <table  id="table-view-leave" class="table table-bordered" style="border-top: solid 1px #fff;">

            </table>
		</div>
		<div role="tabpanel" class="tab-pane" id="messages">
            <table id="table-view-work" class="table table-bordered" style="border-top: solid 1px #fff;">

            </table>
		</div>
        <div role="tabpanel" class="tab-pane" id="plan">
            <table id="table-view-plan" class="table table-bordered" style="border-top: solid 1px #fff;">

            </table>
        </div>
        <div role="tabpanel" class="tab-pane" id="report">
<!--            Report*********-->
            <table id="table-view-report" class="table table-bordered" style="border-top: solid 1px #fff;">

            </table>
        </div>
<!--		<div role="tabpanel" class="tab-pane" id="settings">...settings</div>-->
	</div>
</div>
<!--**********************************End ตารางสรุปเวลางาน-->
<div class="row" style="margin-bottom: 5px; ">
	<div style="float:right;width: 100%;border: solid 1px #cccccc; border-radius: 5px;padding: 5px;">
		<div class="form-inline" style="padding-bottom:5px;width: 100%;float: left; ">
		<div class="form-group">
			<label for="exampleInputName2">งาน</label>
			<select class="form-control" id="Project_id">
				<option value="">ทั้งหมด</option>
				<?php
				foreach($dataProject as $key=>$value){ ?>
				<option value="<?php echo $value->Project_id ?>"><?php echo $value->Project_name ?></option>
				<?php }	?>
			</select>
		</div>

		<?php if(Yii::app()->user->type=='admin'){ ?>
		<div class="form-group">
			<label for="exampleInputName2">พนักงาน</label>
			<select class="form-control" id="UserName" style="width: 180px;">
				<option value="">ทั้งหมด</option>
				<?php
//				$dateStaff=Staff::model()->findAll(array('order' => 'UserName ASC'));
				foreach($dataStaff as $key=>$value){ ?>
					<option value="<?php echo $value->UserName ?>"><?php echo $value->Name ?> <?php echo $value->LastName ?>(<?php echo $value->Nickname ?>)</option>
				<?php }
				?>
			</select>
		</div>
		<?php } ?>
<!--		</div>-->
<!--		<div class="form-inline" style="padding-bottom:5px;width: 100%;float: left; ">-->
			<div class="form-group" style="display: none">
				<label for="exampleInputName2">ช่วงเวลา</label>
				<select class="form-control" id="typeSearch" >
					<option value="StartDate">เริ่มทำงาน</option>
					<option value="EndDate">เสร็จงาน</option>
					<option value="RecordDate">เวลาบันทึก</option>
				</select>
			</div>

		<div class="form-group">
			<label for="exampleInputName2">เริ่ม</label>
			<input type="text" class="form-control" id="datepicker_start" />
		</div>

		<div class="form-group">
			<label for="exampleInputName2">ถึง</label>
			<input type="text" class="form-control" id="datepicker_end" />
		</div>

<!--		<div class="form-inline" style="float: right;padding-bottom:5px; ">-->
		<div  class="form-group">
			<a  id="btn-search" class="btn btn-info">ค้นหา</a>
			<?php if(Yii::app()->user->type=='admin'){ ?>
			<div class="btn-group btn-group-sm" role="group" aria-label="...">
<!--				<a href="/site/index/type/YesterDay"  id="btn-search" class="btn btn-success">เมื่อวาน</a>-->
<!--				<a href="/site/index/type/ToDay" id="btn-search" class="btn btn-success">วันนี้</a>-->
<!--				<a href="/site/index/type/Week" id="btn-search" class="btn btn-success">สัปดาห์นี้</a>-->
				<a id="btn-search" type="YesterDay" class="btn btn-success">เมื่อวาน</a>
				<a id="btn-search" type="ToDay" class="btn btn-success">วันนี้</a>
				<a id="btn-search" type="Week" class="btn btn-success">สัปดาห์นี้</a>
			</div>
			<?php } ?>
		</div>
		</div>
<!--			</div>-->
		</div>
</div>
<?php if(!empty($dataRequest) and Yii::app()->user->type == 'admin'){ ?>
<div class="row">
	<div style="float:left;width: 70%;font-weight: bold;font-size: 28px;">
		รายการส่งคำขอทั้งหมด
	</div>
	<div style="float:left;width: 30%;font-weight: bold;">
		<a href="/site/confirmWorkDetailAll" onclick="return confirm('ยืนยันการอุมัติคำขอทั้งหมด')" style="float: right;" type="button" class="btn btn-danger">
			ยืนยันทั้งหมด (<?php echo count($dataRequest) ?>)
		</a>
	</div>
</div>

<div class="row">
	<table class="table table-bordered">
		<thead>
		<tr>
			<th>#</th>
			<th style="text-align: center;">เริ่มเวลา</th>
			<th style="text-align: center;">เสร็จเวลา</th>
			<th style="text-align: center; width: 10%;">ประเภท</th>
			<th style="text-align: center; width: 10%;">ชื่อพนักงาน</th>
			<th style="text-align: center;">รายละเอียด</th>
			<th style="text-align: center;">ใช้เวลา รวม</th>
            <th style="text-align: center;">ยืนยันคำขอ</th>
			<!--        <th style="text-align: center;">ใช้เวลา รวม<br>(<span id="sumHour">--><?php //echo Workdetail::GetTimeWork(Workdetail::SumHourUserName(Yii::app()->user->id)) ?><!--</span>)</th>-->
		</tr>
		</thead>
		<tbody>
			<?php foreach($dataRequest as $key=>$value){ ?>
				<tr>
					<td><?php echo $value->RecordDate; ?></td>
					<td><?php echo $value->StartDate; ?></td>
					<td><?php echo $value->EndDate; ?></td>
					<td>ส่งคำขอ</td>
					<td><?php echo Staff::GetNickName($value->UserName);?></td>
					<td><?php echo $value->Detail; ?></td>
<!--                    <td>--><?php //echo Workdetail::GetTimeWork($value->Value); ?><!--</td>-->
                    <td style="text-align: center;">
						<a onclick="return confirm('ยืนยันการอุมัติคำขอ')" href="/site/confirmWorkDetail?id=<?php echo $value->WorkDetail_id ?>" class="btn btn-warning">ยืนยันคำขอ</a>
						<a id="btnDeleteWorkDetail" WorkDetail_id="<?php echo $value->WorkDetail_id?>" class="btn btn-group btn-danger" data-toggle="tooltip" data-placement="top" title="ลบคำขอนี้">
							<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
						</a>
					</td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>
<?php } ?>


<div class="row">
	<div style="float:left;width: 70%;font-weight: bold;font-size: 28px;">
		รายการงานทั้งหมด
	</div>
	<div style="float:left;width: 30%;font-weight: bold;">
		<button style="float: right;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
			เพิ่มงาน
		</button>
	</div>
</div>

<div class="row">
<table class="table table-bordered">
	<thead>
	<tr>
		<th>#</th>
		<th style="text-align: center;">เริ่มเวลา</th>
		<th style="text-align: center;">เสร็จเวลา</th>
		<th style="text-align: center; width: 10%;">งาน</th>
		<th style="text-align: center; width: 10%;">ชื่อพนักงาน</th>
		<th style="text-align: center;">รายละเอียด</th>
        <th style="text-align: center;">ใช้เวลา รวม</th>
<!--        <th style="text-align: center;">ใช้เวลา รวม<br>(<span id="sumHour">--><?php //echo Workdetail::GetTimeWork(Workdetail::SumHourUserName(Yii::app()->user->id)) ?><!--</span>)</th>-->
	</tr>
	</thead>
	<tbody id="TableView">
	<?php
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'formworkdetail',
		'htmlOptions' => array('enctype' => 'multipart/form-data'),
		'enableAjaxValidation' => true,
		'clientOptions' => array(
			'validateOnSubmit' => true,
			'validateOnChange' => true,
			'validateOnType' => true,
		),
		'htmlOptions' => array(
			'class' => 'form-horizontal',
		),
	));
			echo $form->hiddenField($model, 'WorkDetail_id');
	?>
	<tr>
		<td style="width: 3%;text-align: center;color: red;font-size: 18px;">*</td>
		<td style="width: 12%;text-align: center;" colspan="1">
			<?php
			if(Yii::app()->user->type=='admin'){
                Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
                $this->widget('CJuiDateTimePicker',array(
                    'language'=>'',
                    'model'=>$model,                                // Model object
                    'attribute'=>'StartDate', // Attribute name//'minDate'=>date('Y-m-d'),  'hourMin' => (int)date('h')
                    'mode'=>'datetime',                     // Use "time","date" or "datetime" (default)
//                    'options'=>array(),                         // jquery plugin options
                    'htmlOptions'=>array('readonly'=>true, 'class' => 'form-control'), // HTML options
                    'options'=>array(
                        'minDate' => null,      // minimum date
                        'maxDate' => null,      // maximum date
                    ),
                ));

//				Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
//				$this->widget('CJuiDateTimePicker', array(
//					'language' => '',
//					'model' => $model,                                // Model object
//					'attribute' => 'StartDate', // Attribute name
//					'mode' => 'datetime',                     // Use "time","date" or "datetime" (default)
//					'options' => array(
//                        'maxDate' => 'today',
//                        'minDate' => 'today',
//                    ),                     // jquery plugin options
//					'htmlOptions' => array('readonly' => true, 'class' => 'form-control') // HTML options
//				));
			}else {
//				echo $form->labelEx($model,'StartHour',array('style'=>'text-align: center;float:left;width:50%;padding-top: 6px;'));
				echo $form->dropDownList($model,'StartHour', array('00'=>'00','01'=>'01','02'=>'02','03'=>'03','04'=>'04','05'=>'05','06'=>'06','07'=>'07','08'=>'08','09'=>'09','10'=>'10','11'=>'11','12'=>'12','13'=>'13','14'=>'14','15'=>'15','16'=>'16','17'=>'17','18'=>'18','19'=>'19','20'=>'20','21'=>'21','22'=>'22','23'=>'23'), array('class' => 'form-control','style'=>'padding:2px;width:50%;float: left;'));
				?>
			<?php
//				echo $form->labelEx($model,'StartMinute',array('style'=>'text-align: center;float:left;width:50%;padding-top: 6px;'));
				echo $form->dropDownList($model,'StartMinute', array('00'=>'00','01'=>'01','02'=>'02','03'=>'03','04'=>'04','05'=>'05','06'=>'06','07'=>'07','08'=>'08','09'=>'09','10'=>'10','11'=>'11','12'=>'12','13'=>'13','14'=>'14','15'=>'15','16'=>'16','17'=>'17','18'=>'18','19'=>'19','20'=>'20','20'=>'20','21'=>'21','22'=>'22','23'=>'23','24'=>'24','25'=>'25','26'=>'26','27'=>'27','28'=>'28','29'=>'29','30'=>'30','31'=>'31','32'=>'32','33'=>'33','34'=>'34','35'=>'35','36'=>'36','37'=>'37','38'=>'38','39'=>'39','40'=>'40','41'=>'41','42'=>'42','43'=>'43','44'=>'44','45'=>'45','46'=>'46','47'=>'47','48'=>'48','49'=>'49','50'=>'50','51'=>'51','52'=>'52','53'=>'53','54'=>'54','55'=>'55','56'=>'56','57'=>'57','58'=>'58','59'=>'59'), array('class' => 'form-control','style'=>'padding:2px;width:50%;float: left;'));
				?>
				<div class="form">
					<?php echo $form->error($model, 'StartMinute'); ?>
				</div>
				<div class="form">
					<?php echo $form->error($model, 'StartHour'); ?>
				</div>
			<?php
			}?>
			<div class="form">
			<?php echo $form->error($model, 'StartDate'); ?>
			</div>
		</td>
		<td style="width: 12%;text-align: center;">
			<?php
			if(Yii::app()->user->type=='admin'){
				Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
				$this->widget('CJuiDateTimePicker', array(
					'language' => '',
					'model' => $model,                                // Model object
					'attribute' => 'EndDate', // Attribute name
					'mode' => 'datetime',                     // Use "time","date" or "datetime" (default)
					'options' => array(),                       // jquery plugin options
//									'dateFormat' => 'yy-mm-dd H:mm:ss',
					'htmlOptions' => array('readonly' => true, 'class' => 'form-control') // HTML options
				));
			}elseif(Yii::app()->user->type!='admin'){
//				echo $form->labelEx($model,'StartHour',array('style'=>'text-align: center;float:left;width:50%;padding-top: 6px;'));
				echo $form->dropDownList($model,'EndHour', array('00'=>'00','01'=>'01','02'=>'02','03'=>'03','04'=>'04','05'=>'05','06'=>'06','07'=>'07','08'=>'08','09'=>'09','10'=>'10','11'=>'11','12'=>'12','13'=>'13','14'=>'14','15'=>'15','16'=>'16','17'=>'17','18'=>'18','19'=>'19','20'=>'20','21'=>'21','22'=>'22','23'=>'23'), array('class' => 'form-control','style'=>'padding:2px;width:50%;float: left;'));
			?>
			<?php
//				echo $form->labelEx($model,'StartMinute',array('style'=>'text-align: center;float:left;width:50%;padding-top: 6px;'));
				echo $form->dropDownList($model,'EndMinute', array('00'=>'00','01'=>'01','02'=>'02','03'=>'03','04'=>'04','05'=>'05','06'=>'06','07'=>'07','08'=>'08','09'=>'09','10'=>'10','11'=>'11','12'=>'12','13'=>'13','14'=>'14','15'=>'15','16'=>'16','17'=>'17','18'=>'18','19'=>'19','20'=>'20','20'=>'20','21'=>'21','22'=>'22','23'=>'23','24'=>'24','25'=>'25','26'=>'26','27'=>'27','28'=>'28','29'=>'29','30'=>'30','31'=>'31','32'=>'32','33'=>'33','34'=>'34','35'=>'35','36'=>'36','37'=>'37','38'=>'38','39'=>'39','40'=>'40','41'=>'41','42'=>'42','43'=>'43','44'=>'44','45'=>'45','46'=>'46','47'=>'47','48'=>'48','49'=>'49','50'=>'50','51'=>'51','52'=>'52','53'=>'53','54'=>'54','55'=>'55','56'=>'56','57'=>'57','58'=>'58','59'=>'59'), array('class' => 'form-control','style'=>'padding:2px;width:50%;float: left;'));
			?>
			<div class="form">
				<?php echo $form->error($model, 'EndMinute'); ?>
			</div>
			<div class="form">
				<?php echo $form->error($model, 'EndHour'); ?>
			</div>
			<?php
			}elseif(Yii::app()->user->type!='admin' && !empty($model->EndDate)) {
				Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
				$this->widget('CJuiDateTimePicker', array(
					'language' => '',
					'model' => $model,                                // Model object
					'attribute' => 'EndDate', // Attribute name
					'mode' => 'datetime',                     // Use "time","date" or "datetime" (default)
					'options' => array(
						'maxDate' => 'today',
						'minDate' => 'today'
					),                       // jquery plugin options
//									'dateFormat' => 'yy-mm-dd H:mm:ss',
					'htmlOptions' => array('readonly' => true, 'class' => 'form-control') // HTML options
				));
			}
			?>
			<div class="form">
			<?php echo $form->error($model, 'EndDate'); ?>
				</div>
		</td>
		<td colspan="2" style="width: 10%;text-align: center;">

			<?php
			if(Yii::app()->user->type=='admin'){
			echo $form->labelEx($model,'TypeWork',array('style'=>'text-align: left;float:left;width:40%;'));
			echo $form->dropDownList($model,'TypeWork', array('work'=>'ชั่วโมงทำงาน','leave'=>'ลางาน','plan'=>'แผน'), array('empty'=>'กรุณาเลือก','class' => 'form-control','style'=>'width:60%;'))
			?>
			<div class="form">
				<?php echo $form->error($model, 'TypeWork'); }?>
			</div>

			<?php
			if(Yii::app()->user->type=='admin' && $model->TypeLeave!=''){
				echo $form->labelEx($model, 'TypeLeave', array('style' => 'text-align: left;float:left;width:40%;'));
				echo $form->dropDownList($model, 'TypeLeave', array('ขาดงาน' => 'ขาดงาน','ลาพักร้อน' => 'ลาพักร้อน', 'ลาป่วย' => 'ลาป่วย', 'ลาอื่นๆ' => 'ลาอื่นๆ', 'ลาคลอด' => 'ลาคลอด', 'ลาบวช' => 'ลาบวช', 'ลาสมรส' => 'ลาสมรส', 'ขออนุมัติ' => 'ขออนุมัติ'), array('empty' => 'กรุณาเลือก', 'class' => 'form-control', 'style' => 'width:60%;'));
			}else {
				echo $form->labelEx($model, 'TypeLeave', array('style' => 'text-align: left;float:left;width:40%;display:none;'));
				echo $form->dropDownList($model, 'TypeLeave', array('ขาดงาน' => 'ขาดงาน','ลาพักร้อน' => 'ลาพักร้อน', 'ลาป่วย' => 'ลาป่วย', 'ลาอื่นๆ' => 'ลาอื่นๆ', 'ลาคลอด' => 'ลาคลอด', 'ลาบวช' => 'ลาบวช', 'ลาสมรส' => 'ลาสมรส', 'ขออนุมัติ' => 'ขออนุมัติ'), array('empty' => 'กรุณาเลือก', 'class' => 'form-control', 'style' => 'width:60%;display:none;'));
			}
			?>
			<div class="form">
				<?php echo $form->error($model, 'TypeLeave');?>
			</div>

			<?php
			if(Yii::app()->user->type=='admin' && $model->TypeLeave!='') {
				echo $form->labelEx($model, 'Project_id', array('style' => 'text-align: left;float:left;width:40%;display:none;'));
				echo $form->dropDownList($model,'Project_id', CHtml::listData($dataProject, 'Project_id', 'Project_name'), array('empty'=>'กรุณาเลือกงาน','class' => 'form-control','style'=>'width:60%;display:none;'));
			}else {
				if(Yii::app()->user->type=='admin') {
					echo $form->labelEx($model, 'Project_id', array('style' => 'text-align: left;float:left;width:40%;'));
					echo $form->dropDownList($model,'Project_id', CHtml::listData($dataProject, 'Project_id', 'Project_name'), array(
                        'empty'=>'กรุณาเลือกงาน',
                        'class' => 'form-control',
                        'style'=>'width:60%;',
                        'ajax' => array(
                            'type' => 'POST',
                            'data' => 'js:"id="+$(\'#Workdetail_Project_id\').val()',
                            'url' => CController::createUrl('/plan/PlanOne'),
                            'update' => '#Workdetail_Plan_id',
                            'success' => 'function(data){
                                if(data!=""){
                                    $(\'#Workdetail_Plan_id\').show();
                                    $(\'[for="Workdetail_Plan_id"]\').show();
                                    $(\'#Workdetail_Plan_id\').html(data);
                                    $(\'#Workdetail_Plan_level_one\').val("");
                                    $(\'#Workdetail_Plan_level_two\').val("");
                                    $(\'#Workdetail_Plan_level_three\').val("");
                                }else{
                                    $(\'#Workdetail_Plan_id\').hide();
                                    $(\'[for="Workdetail_Plan_id"]\').hide();
                                    $(\'#Workdetail_Plan_id\').val("");
                                    $(\'#Workdetail_Plan_level_one\').hide();
                                    $(\'[for="Workdetail_Plan_level_one"]\').hide();
                                    $(\'#Workdetail_Plan_level_one\').val("");
                                    $(\'#Workdetail_Plan_level_two\').hide();
                                    $(\'[for="Workdetail_Plan_level_two"]\').hide();
                                    $(\'#Workdetail_Plan_level_two\').val("");
                                    $(\'#Workdetail_Plan_level_three\').hide();
                                    $(\'[for="Workdetail_Plan_level_three"]\').hide();
                                    $(\'#Workdetail_Plan_level_three\').val("");
                                }
                            }',
                        )));
				}else {

                    echo $form->dropDownList($model, 'Project_id', CHtml::listData($dataProject, 'Project_id', 'Project_name'), array(
                        'empty' => 'กรุณาเลือกงาน',
                        'class' => 'form-control',
                        'style' => 'width:100%;',
                        'ajax' => array(
                            'type' => 'POST',
                            'data' => 'js:"id="+$(\'#Workdetail_Project_id\').val()',
                            'url' => CController::createUrl('/plan/Plan'),
                            'update' => '#Workdetail_Plan',
                            'success' => 'function(data){
                                if(data!=""){
                                    $(\'#Workdetail_Plan\').show();
                                    $(\'#Workdetail_Plan\').html(data);
                                }else{
                                    $(\'#Workdetail_Plan\').hide();
                                }
                            }',
                        ),
                    ));

//                    echo $form->dropDownList($model, 'Project_id', CHtml::listData($dataProject, 'Project_id', 'Project_name'), array(
//                        'empty' => 'กรุณาเลือกงาน',
//                        'class' => 'form-control',
//                        'style' => 'width:100%;',
//                        'ajax' => array(
//                            'type' => 'POST',
//                            'data' => 'js:"id="+$(\'#Workdetail_Project_id\').val()',
//                            'url' => CController::createUrl('/plan/PlanOne'),
//                            'update' => '#Workdetail_Plan_id',
//                            'success' => 'function(data){
//                                if(data!=""){
//                                    $(\'#Workdetail_Plan_id\').show();
//                                    $(\'[for="Workdetail_Plan_id"]\').show();
//                                    $(\'#Workdetail_Plan_id\').html(data);
//                                    $(\'#Workdetail_Plan_level_one\').val("");
//                                    $(\'#Workdetail_Plan_level_two\').val("");
//                                    $(\'#Workdetail_Plan_level_three\').val("");
//                                }else{
//                                    $(\'#Workdetail_Plan_id\').hide();
//                                    $(\'#Workdetail_Plan_id\').val("");
//                                    $(\'#Workdetail_Plan_level_one\').hide();
//                                    $(\'[for="Workdetail_Plan_level_one"]\').hide();
//                                    $(\'#Workdetail_Plan_level_one\').val("");
//                                    $(\'#Workdetail_Plan_level_two\').hide();
//                                    $(\'[for="Workdetail_Plan_level_two"]\').hide();
//                                    $(\'#Workdetail_Plan_level_two\').val("");
//                                    $(\'#Workdetail_Plan_level_three\').hide();
//                                    $(\'[for="Workdetail_Plan_level_three"]\').hide();
//                                    $(\'#Workdetail_Plan_level_three\').val("");
//                                }
//                            }',
//                        ),
//                    ));
                }
                    if(!empty($model->Project_id)) {
                        $stylePlan = 'width:100%;display:block;';
                    }else{
                        $stylePlan = 'width:100%;display:none;';
                    }
                    echo $form->dropDownList($model, 'Plan', CHtml::listData($dataSelectPlan, 'Plan_id', 'Plan_title'), array(
                        'class' => 'form-control',
                        'style' => $stylePlan,
                        'onchange' => 'if($(\'#Workdetail_Plan option:selected\').text()!=="กรุณาเลือก"){
                            $(\'#Workdetail_Detail\').val($(\'#Workdetail_Plan option:selected\').text());
                        }else{
                          $(\'#Workdetail_Detail\').val("");
                        }',
                    ));

                    $stylePlanId='width:100%;display:none;';
                    if(!empty($model->Plan_id)){
                        if(Yii::app()->user->type=='admin'){
                            echo $form->labelEx($model, 'Plan_id', array('style' => 'text-align: left;float:left;width:40%;'));
                            $stylePlanId = 'width:100%;width:60%;';
                        }else {
                            $stylePlanId = 'width:100%';
                        }
                    }

				}

			?>
			<div class="form">
			<?php echo $form->error($model, 'Project_id'); ?>
			</div>

			<?php
			if(Yii::app()->user->type=='admin'){
			echo $form->labelEx($model,'UserName',array('style'=>'text-align: left;float:left;width:40%;'));
			echo $form->dropDownList($model,'UserName', CHtml::listData($dataStaff, 'UserName', 'Nickname'), array('empty'=>'กรุณาเลือก','class' => 'form-control','style'=>'width:60%;'))
			?>
			<div class="form">
				<?php echo $form->error($model, 'UserName'); }?>
			</div>


		</td>
		<td style="width: 45%;">
			<?php echo $form->textArea($model, 'Detail', array('class' => 'form-control')); ?>
			<div class="form">
			<?php echo $form->error($model, 'Detail'); ?>
				</div>
		</td>
		<td style="width: 12%;text-align: center;" >
			<?php
			if(!empty($model->WorkDetail_id)) {
				echo CHtml::submitButton(Yii::t('main', 'แก้ไข'), array('id' => 'addform', 'class' => 'btn btn-warning','style'=>'float: left'));
				?>
				<a style="float: left;margin-left: 5px;" href="/site/index" class="btn btn-default">กลับ</a>
			<?php }else{ ?>
				<?php if(!Workdetail::checkWorkDetailByDate() and Yii::app()->user->type!=='admin' and ($Hour<=21)){ ?>
                    <label class="radio-inline left">
                      <input type="radio" name="Workdetail[TypeWork]" id="inlineRadio1" value="work" checked> ส่งเวลา
                    </label>
                    <div style="clear: both;"></div>
                    <label class="radio-inline left">
                      <input type="radio" name="Workdetail[TypeWork]" id="inlineRadio2" value="request"> ส่งคำขอ
                    </label>
            <?php }
				echo CHtml::submitButton(Yii::t('main', 'เพิ่ม'), array('class' => 'btn btn-primary'));
			}
			?>
		</td>
	</tr>
	<?php $this->endWidget(); ?>
    <?php if(!empty($dataWork)){
    	$countdataWork=1;
    	?>
	<?php foreach($dataWork as $key=>$value){
		if(Yii::app()->user->type=='admin' || (Yii::app()->user->type!='admin' && Yii::app()->user->id==$value->UserName)){
		?>
	<tr class="view_data <?php echo (($value->TypeWork=="request")?'warning':'') ?>">
		<td style="width: 3%;text-align: center;">
			<?php echo (($value->TypeWork=="request")?'<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>':$countdataWork++) ?>
		</td>
		<td style="width: 10%;text-align: center;"><?php echo $value->StartDate ?></td>
		<td style="width: 10%;text-align: center;"><?php echo $value->EndDate ?></td>
		<?php
		$dataDetail=explode(',',$value->Detail);
		$Detail = '';
		if(count($dataDetail)>1){
			if( $dataDetail[0] == 'ขาดงาน' || $dataDetail[0] == 'ลาป่วย' || $dataDetail[0] == 'ลากิจอื่นๆ' || $dataDetail[0] == 'ลาคลอด' || $dataDetail[0] == 'ลาบวช' || $dataDetail[0] == 'ลาสมรส' || $dataDetail[0] == 'ลาพักร้อน' || $dataDetail[0] =="ขออนุมัติ") {
				$Detail = '';
				foreach ($dataDetail as $dataDetailkey => $dataDetailvalue) {
					if ($key > 0) {
						$Detail .= $dataDetailvalue;
					}
				}
			?>
			<td style="width: 10%;text-align: center;"><?php echo $dataDetail[0]; ?></td>
			<td style="width: 10%"><?php echo Staff::GetNickName($value->UserName); ?></td>
			<td style="width: 45%;" id="td_detail_<?php echo $value->WorkDetail_id ?>"><?php echo $Detail; ?></td>
		<?php }else{?>
				<td style="width: 10%;text-align: center;"><?php echo Project::GetNameProject($value->Project_id) ?></td>
				<td style="width: 10%"><?php echo Staff::GetNickName($value->UserName)?></td>
				<td style="width: 45%;" id="td_detail_<?php echo $value->WorkDetail_id ?>"><?php echo $value->Detail ?></td>
		<?php } ?>
		<?php }else{ ?>
			<td style="width: 10%;text-align: center;"><?php echo Project::GetNameProject($value->Project_id) ?></td>
			<td style="width: 10%"><?php echo Staff::GetNickName($value->UserName)?></td>
			<td style="width: 45%;" id="td_detail_<?php echo $value->WorkDetail_id ?>"><?php echo $value->Detail ?></td>
		<?php } ?>

		<td style="width: 12%;text-align: center;">
			<?php echo Workdetail::GetTimeWork($value->Value); ?>
			<div class="btn-group btn-group-xs" aria-label="Default button group" role="group" style="margin: 5px;">

			<?php if(empty($value->EndDate)){ ?>
				<a href="/site/closework/<?php echo $value->WorkDetail_id ?>" class="btn btn-success" onclick="return confirm('คุณเเน่ใจที่จะปิดงานนี้ให้เวลา <?php echo Workdetail::getDateTime() ?>')" data-toggle="tooltip" data-placement="top" title="ปิดงานนี้" ><span class="glyphicon glyphicon-off" aria-hidden="true"></span></a>
			<?php }	?>

			<?php if(Yii::app()->user->type=='admin' || (Yii::app()->user->id==$value->UserName && Workdetail::CheckRecordDate($value->StartDate) && $value->TypeWork!=='leave')){ ?>
					<a id="btnDeleteWorkDetail" WorkDetail_id="<?php echo $value->WorkDetail_id?>" class="btn btn-group btn-danger" data-toggle="tooltip" data-placement="top" title="ลบงานนี้">
						<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
					</a>
					<a href="/site/index/<?php echo $value->WorkDetail_id ?>" class="btn btn-group btn-success " data-toggle="tooltip" data-placement="top" title="แก้ไขข้อมูล" >
						<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
					</a>
			<?php }elseif($value->TypeWork=='request'){ ?>
					<a id="btnDeleteWorkDetail" WorkDetail_id="<?php echo $value->WorkDetail_id?>" class="btn btn-group btn-danger" data-toggle="tooltip" data-placement="top" title="ลบคำขอนี้">
						<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
					</a>
			<?php }	?>
				<?php if($value->TypeWork!=='leave'){ ?>
					<button class="btn btn-warning" id="btn-Clone-Work" EndMinute="<?php echo date("i",(strtotime($value->EndDate))) ?>" EndHour="<?php echo date("H",(strtotime($value->EndDate))) ?>" StartHour="<?php echo date("H",(strtotime($value->StartDate)));?>" StartMinute="<?php echo date("i",(strtotime($value->StartDate)));?>" Project_id="<?php echo $value->Project_id ?>"  WorkDetail_id="<?php echo $value->WorkDetail_id; ?>" data-toggle="tooltip" data-placement="top" title="สร้างงานใหม่" ><i class="fa fa-files-o"></i></button>
				<?php }	?>
			</div>
		</td>
	</tr>
	<?php }}} ?>
	</tbody>
</table>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="">
	<div class="modal-dialog">
		<div class="modal-content">
				<?php
				$form = $this->beginWidget('CActiveForm', array(
					'id' => 'modelformworkdetail',
					'htmlOptions' => array('enctype' => 'multipart/form-data'),
					'enableAjaxValidation' => true,
					'clientOptions' => array(
						'validateOnSubmit' => true,
						'validateOnChange' => true,
						'validateOnType' => true,
					),
					'htmlOptions' => array(
						'class' => 'form-horizontal',
					),
				));
				echo $form->hiddenField($model, 'WorkDetail_id');
				?>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">จัดการ พนักงาน</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<?php
						if(Yii::app()->user->type=='admin') {
							echo $form->labelEx($model, 'StartDate', array('class' => 'col-sm-3 control-label'));
						}else{
							echo $form->labelEx($model, 'StartHour', array('class' => 'col-sm-3 control-label'));
						}
						?>
						<div class="col-sm-3" >
							<?php
							if(Yii::app()->user->type=='admin'){
								Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
								$this->widget('CJuiDateTimePicker', array(
									'language' => '',
									'model' => $model,                                // Model object
									'attribute' => 'StartDate', // Attribute name
									'mode' => 'datetime',                     // Use "time","date" or "datetime" (default)
									'options' => array(),                     // jquery plugin options
//				'dateFormat' => 'yy-mm-dd H:mm:ss',
									'htmlOptions' => array('readonly' => true, 'class' => 'form-control') // HTML options
								));
							}else {
//								echo $form->labelEx($model,'StartHour',array('style'=>'text-align: center;float:left;width:50%;padding-top: 6px;'));
								echo $form->dropDownList($model,'StartHour', array('00'=>'00','01'=>'01','02'=>'02','03'=>'03','04'=>'04','05'=>'05','06'=>'06','07'=>'07','08'=>'08','09'=>'09','10'=>'10','11'=>'11','12'=>'12','13'=>'13','14'=>'14','15'=>'15','16'=>'16','17'=>'17','18'=>'18','19'=>'19','20'=>'20','21'=>'21','22'=>'22','23'=>'23'), array('class' => 'form-control','style'=>'padding:2px;width:50%;float: left;'));
							}?>
							<div class="form">
								<?php echo $form->error($model, 'StartDate'); ?>
							</div>
						</div>
						<?php
						if(Yii::app()->user->type=='admin') {
							echo $form->labelEx($model, 'datetime', array('class' => 'col-sm-2 control-label'));
						}else{
							echo $form->labelEx($model, 'StartMinute', array('class' => 'col-sm-2 control-label'));
						}
						?>
						<div class="col-sm-3" >
						<?php
						if(Yii::app()->user->type=='admin'){
							Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
							$this->widget('CJuiDateTimePicker', array(
								'language' => '',
								'model' => $model,                                // Model object
								'attribute' => 'EndDate', // Attribute name
								'mode' => 'datetime',                     // Use "time","date" or "datetime" (default)
								'options' => array(),                       // jquery plugin options
//									'dateFormat' => 'yy-mm-dd H:mm:ss',
								'htmlOptions' => array('readonly' => true, 'class' => 'form-control') // HTML options
							));
						}	elseif(Yii::app()->user->type!='admin'){
//							echo $form->labelEx($model,'StartMinute',array('style'=>'text-align: center;float:left;width:50%;padding-top: 6px;'));
							echo $form->dropDownList($model,'StartMinute', array('00'=>'00','01'=>'01','02'=>'02','03'=>'03','04'=>'04','05'=>'05','06'=>'06','07'=>'07','08'=>'08','09'=>'09','10'=>'10','11'=>'11','12'=>'12','13'=>'13','14'=>'14','15'=>'15','16'=>'16','17'=>'17','18'=>'18','19'=>'19','20'=>'20','20'=>'20','21'=>'21','22'=>'22','23'=>'23','24'=>'24','25'=>'25','26'=>'26','27'=>'27','28'=>'28','29'=>'29','30'=>'30','31'=>'31','32'=>'32','33'=>'33','34'=>'34','35'=>'35','36'=>'36','37'=>'37','38'=>'38','39'=>'39','40'=>'40','41'=>'41','42'=>'42','43'=>'43','44'=>'44','45'=>'45','46'=>'46','47'=>'47','48'=>'48','49'=>'49','50'=>'50','51'=>'51','52'=>'52','53'=>'53','54'=>'54','55'=>'55','56'=>'56','57'=>'57','58'=>'58','59'=>'59'), array('class' => 'form-control','style'=>'padding:2px;width:50%;float: left;'));
						}
						?>
						</div>
					</div>

					<div class="form-group">
						<?php
						if(Yii::app()->user->type=='admin') {
							echo $form->labelEx($model, 'EndDate', array('class' => 'col-sm-3 control-label'));
						}else{
							echo $form->labelEx($model, 'EndHour', array('class' => 'col-sm-3 control-label'));
						}
						?>
						<div class="col-sm-3" >
							<?php
							if(Yii::app()->user->type=='admin'){
								Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
								$this->widget('CJuiDateTimePicker', array(
									'language' => '',
									'model' => $model,                                // Model object
									'attribute' => 'EndDate', // Attribute name
									'mode' => 'datetime',                     // Use "time","date" or "datetime" (default)
									'options' => array(),                     // jquery plugin options
//				'dateFormat' => 'yy-mm-dd H:mm:ss',
									'htmlOptions' => array('readonly' => true, 'class' => 'form-control') // HTML options
								));
							}else {
//								echo $form->labelEx($model,'StartHour',array('style'=>'text-align: center;float:left;width:50%;padding-top: 6px;'));
								echo $form->dropDownList($model,'EndHour', array('00'=>'00','01'=>'01','02'=>'02','03'=>'03','04'=>'04','05'=>'05','06'=>'06','07'=>'07','08'=>'08','09'=>'09','10'=>'10','11'=>'11','12'=>'12','13'=>'13','14'=>'14','15'=>'15','16'=>'16','17'=>'17','18'=>'18','19'=>'19','20'=>'20','21'=>'21','22'=>'22','23'=>'23'), array('class' => 'form-control','style'=>'padding:2px;width:50%;float: left;'));
							}?>
							<div class="form">
								<?php echo $form->error($model, 'EndDate'); ?>
							</div>
						</div>
						<?php
						if(Yii::app()->user->type=='admin') {
							echo $form->labelEx($model, 'datetime', array('class' => 'col-sm-2 control-label'));
						}else{
							echo $form->labelEx($model, 'EndMinute', array('class' => 'col-sm-2 control-label'));
						}
						?>
						<div class="col-sm-3" >
							<?php
							if(Yii::app()->user->type=='admin'){
								Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
								$this->widget('CJuiDateTimePicker', array(
									'language' => '',
									'model' => $model,                                // Model object
									'attribute' => 'EndDate', // Attribute name
									'mode' => 'datetime',                     // Use "time","date" or "datetime" (default)
									'options' => array(),                       // jquery plugin options
//									'dateFormat' => 'yy-mm-dd H:mm:ss',
									'htmlOptions' => array('readonly' => true, 'class' => 'form-control') // HTML options
								));
							}	elseif(Yii::app()->user->type!='admin'){
//							echo $form->labelEx($model,'StartMinute',array('style'=>'text-align: center;float:left;width:50%;padding-top: 6px;'));
								echo $form->dropDownList($model,'EndMinute', array('00'=>'00','01'=>'01','02'=>'02','03'=>'03','04'=>'04','05'=>'05','06'=>'06','07'=>'07','08'=>'08','09'=>'09','10'=>'10','11'=>'11','12'=>'12','13'=>'13','14'=>'14','15'=>'15','16'=>'16','17'=>'17','18'=>'18','19'=>'19','20'=>'20','20'=>'20','21'=>'21','22'=>'22','23'=>'23','24'=>'24','25'=>'25','26'=>'26','27'=>'27','28'=>'28','29'=>'29','30'=>'30','31'=>'31','32'=>'32','33'=>'33','34'=>'34','35'=>'35','36'=>'36','37'=>'37','38'=>'38','39'=>'39','40'=>'40','41'=>'41','42'=>'42','43'=>'43','44'=>'44','45'=>'45','46'=>'46','47'=>'47','48'=>'48','49'=>'49','50'=>'50','51'=>'51','52'=>'52','53'=>'53','54'=>'54','55'=>'55','56'=>'56','57'=>'57','58'=>'58','59'=>'59'), array('class' => 'form-control','style'=>'padding:2px;width:50%;float: left;'));
							}
							?>
						</div>
					</div>

					<div class="form-group">
						<?php
						if(Yii::app()->user->type=='admin') {
								echo $form->labelEx($model, 'TypeWork', array('class' => 'col-sm-1 control-label'));
						?>
						<div class="col-sm-3">
							<?php
							if(Yii::app()->user->type=='admin'){
//							echo $form->labelEx($model,'TypeWork',array('style'=>'text-align: left;float:left;width:40%;'));
							echo $form->dropDownList($model,'TypeWork', array('work'=>'ชั่วโมงทำงาน','leave'=>'ลางาน'), array('empty'=>'กรุณาเลือก','class' => 'form-control'))
							?>
							<div class="form">
								<?php echo $form->error($model, 'TypeWork'); }?>
							</div>
						</div>
						<?php } ?>

						<?php
						echo $form->labelEx($model, 'Project_id', array('class' => 'col-sm-2 control-label'));
						?>

							<?php
							if(Yii::app()->user->type=='admin') { ?>
								<div class="col-sm-3">
							<?php
//								echo $form->labelEx($model, 'Project_id', array('style' => 'text-align: left;float:left;width:40%;'));
								echo $form->dropDownList($model,'Project_id', CHtml::listData($dataProject, 'Project_id', 'Project_name'), array('empty'=>'กรุณาเลือกงาน','class' => 'form-control'));
							}else { ?>
								<div class="col-sm-7">
							<?php
								echo $form->dropDownList($model, 'Project_id', CHtml::listData($dataProject, 'Project_id', 'Project_name'), array('empty' => 'กรุณาเลือกงาน', 'class' => 'form-control'));
							}
							?>
							<div class="form">
								<?php echo $form->error($model, 'Project_id'); ?>
							</div>
						</div>
						<?php
						if(Yii::app()->user->type=='admin') {
							echo $form->labelEx($model, 'UserName', array('class' => 'col-sm-1 control-label'));

						?>
						<div class="col-sm-3">
							<?php
							if(Yii::app()->user->type=='admin'){
//							echo $form->labelEx($model,'UserName',array('style'=>'text-align: left;float:left;width:40%;'));
							echo $form->dropDownList($model,'UserName', CHtml::listData($dataStaff, 'UserName', 'Nickname'), array('empty'=>'กรุณาเลือก','class' => 'form-control'))
							?>
							<div class="form">
								<?php echo $form->error($model, 'UserName'); }?>
							</div>
						</div>
						<?php } ?>
					</div>

					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">รายละเอียด</label>
						<div class="col-sm-10">
							<?php
							echo $form->textArea($model, 'Detail', array('class' => 'col-sm-10 form-control'));
							?>
							<div class="form">
								<?php echo $form->error($model, 'Detail'); ?>
							</div>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<?php
					if(!empty($model->WorkDetail_id)) {
						echo CHtml::submitButton(Yii::t('main', 'แก้ไข'), array('id' => 'addform', 'class' => 'btn btn-warning'));
						?>
<!--						<a style="float: left;margin-left: 5px;" href="/site/index" class="btn btn-default"><span class="glyphicon glyphicon-off" aria-hidden="true"></span> ปิด</a>-->
					<?php
					}else{
						echo CHtml::submitButton(Yii::t('main', 'เพิ่ม'), array('id' => 'addform', 'class' => 'btn btn-primary'));
					}
					?>
					<button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-off" aria-hidden="true"></span> ปิด</button>
				</div>
				<?php $this->endWidget(); ?>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="modal fade" tabindex="-1" role="dialog" id="myModalEditStaff" aria-labelledby="myModalEditStaff" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<?php
				$formRegister = $this->beginWidget('CActiveForm', array(
					'id' => 'formRegister',
					'htmlOptions' => array('enctype' => 'multipart/form-data'),
					'enableAjaxValidation' => true,
					'clientOptions' => array(
						'validateOnSubmit' => true,
					),
					'htmlOptions' => array(
						'class' => 'form-horizontal',
					),
				));
//				                echo $staff->scenario;
				?>
				<div class="modal-header">
					<!--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
					<h4 class="modal-title" id="myModalLabel">จัดการข้อมูล พนักงาน</h4>
				</div>
				<div class="modal-body">
					<div class="form" style="margin-left: 5%;">

						<div class="form-group">
							<label for="inputPassword3" class="col-sm-3 control-label">ชื่อ</label>
							<div class="col-sm-9">
								<?php echo $formRegister->textField($staff, 'Name', array('class' => 'form-control')); ?>
								<?php echo $formRegister->error($staff, 'Name'); ?>
							</div>
						</div>

						<div class="form-group">
							<label for="inputPassword3" class="col-sm-3 control-label">นามสกุล</label>
							<div class="col-sm-9">
								<?php echo $formRegister->textField($staff, 'LastName', array('class' => 'form-control')); ?>
								<?php echo $formRegister->error($staff, 'LastName'); ?>
							</div>
						</div>

						<div class="form-group">
							<label for="inputPassword3" class="col-sm-3 control-label">ชื่อเล่น</label>
							<div class="col-sm-9">
								<?php echo $formRegister->textField($staff, 'Nickname', array('class' => 'form-control')); ?>
								<?php echo $formRegister->error($staff, 'Nickname'); ?>
							</div>
						</div>

						<div class="form-group">
							<label for="inputPassword3" class="col-sm-3 control-label">ชื่อเข้าระบบ</label>
							<div class="col-sm-9">
								<?php echo $formRegister->textField($staff, 'UserName', array('class' => 'form-control')); ?>
								<?php echo $formRegister->error($staff, 'UserName'); ?>
							</div>
						</div>

						<div class="form-group">
							<label for="inputPassword3" class="col-sm-3 control-label">รหัสผ่าน</label>
							<div class="col-sm-9">
								<?php echo $formRegister->passwordField($staff, 'Password', array('class' => 'form-control')); ?>
								<?php echo $formRegister->error($staff, 'Password'); ?>
							</div>
						</div>

						<div class="form-group">
							<label for="inputPassword3" class="col-sm-3 control-label">ยืนยันรหัสผ่าน</label>
							<div class="col-sm-9">
								<?php echo $formRegister->passwordField($staff, 'Password2', array('class' => 'form-control')); ?>
								<?php echo $formRegister->error($staff, 'Password2'); ?>
							</div>
						</div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">อีเมล์</label>
                            <div class="col-sm-9">
                                <?php echo $formRegister->textField($staff, 'Email', array('class' => 'form-control')); ?>
                                <?php echo $formRegister->error($staff, 'Email'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">เบอร์โทร</label>
                            <div class="col-sm-9">
                                <?php echo $formRegister->textField($staff, 'Phone', array('class' => 'form-control')); ?>
                                <?php echo $formRegister->error($staff, 'Phone'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">ที่อยู่</label>
                            <div class="col-sm-9">
                                <?php echo $formRegister->textArea($staff, 'Address', array('class' => 'form-control')); ?>
                                <?php echo $formRegister->error($staff, 'Address'); ?>
                            </div>
                        </div>

						<div class="form-group">
							<label for="inputPassword3" class="col-sm-3 control-label">ตำแหน่ง</label>
							<div class="col-sm-9">
								<?php echo $formRegister->dropDownList($staff,'Position', array('programmer'=>'โปรแกรมเมอร์','design'=>'ออกแบบ'), array('empty'=>'เลือกตำแหน่ง','class' => 'form-control')) ?>
								<?php echo $formRegister->error($staff, 'Position'); ?>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<a href="/site/managestaff" class="btn btn-default" data-dismiss="modal">Close</a>
					<?php echo CHtml::submitButton(Yii::t('main', 'แก้ไข'), array('id' => 'addFormStaff', 'class' => 'btn btn-primary')); ?>
					<!--					<button type="button" class="btn btn-primary">Save changes</button>-->
				</div>
				<?php $this->endWidget(); ?>
			</div>
		</div>
	</div>
</div>
<?php
$this->layout=false;
/**
 * Created by PhpStorm.
 * User: sootipong
 * Date: 8/14/2015
 * Time: 15:59
 */
?>
<!--<link rel="stylesheet" type="text/css" href="--><?php //echo Yii::app()->request->baseUrl; ?><!--/css/screen.css" media="screen, projection">-->
<!--<link rel="stylesheet" type="text/css" href="--><?php //echo Yii::app()->request->baseUrl; ?><!--/css/print.css" media="print">-->
<!--<link rel="stylesheet" type="text/css" href="--><?php //echo Yii::app()->request->baseUrl; ?><!--/css/ie.css" media="screen, projection">-->
<!--<link rel="stylesheet" type="text/css" href="--><?php //echo Yii::app()->request->baseUrl; ?><!--/css/main.css">-->
<!--<link rel="stylesheet" type="text/css" href="--><?php //echo Yii::app()->request->baseUrl; ?><!--/css/form.css">-->
<h3>รายการ การไม่ลงเวลา ในระบบ TimeSheet </h3>
<h3>เดือน <?php echo Workdetail::GetTextNowMonth() ?></h3>
<span style="font-weight: bolder;font-size: 16px;">ไม่ลง TimeSheet</span>
<table>
    <thead>
    <tr>
        <td>#</td>
        <td>วันที่</td>
        <td>ชื่อพนักงาน</td>
        <td>สถานนะ</td>
    </tr>
    </thead>
    <tbody>
    <?php foreach($data as $key=>$value){ ?>
        <?php if($value['status']=='NotComplete'){ ?>
            <tr>
                <td style="border-bottom: solid 1px;"><?php echo $key+1 ?></td>
                <td style="border-bottom: solid 1px;text-align: center;"><?php echo $value['day'] ?></td>
                <td style="border-bottom: solid 1px;"><?php echo $value['Name']?> <?php echo $value['LastName'] ?></td>
                <td style="border-bottom: solid 1px;">
                    <?php
                    if($value['status']=='AdminEdit'){
                        echo 'เพิ่มโดย admin';
                    }elseif($value['status']=='NotComplete'){
                        echo 'ไม่ลงข้อมูล';
                    }
                    ?>
                </td>
            </tr>
        <?php }} ?>
    </tbody>
</table>
<span style="font-weight: bolder;font-size: 16px;">ค่าปรับ</span>
<table>
    <thead>
    <tr>
        <td>#</td>
        <td>วันที่</td>
        <td>ชื่อพนักงาน</td>
        <td>บาท</td>
    </tr>
    </thead>
    <tbody>
    <?php $number=1;$sum=0;$beForName='ศรัณย์';$beForLastName='ลายกลาง'; ?>
    <?php foreach($data as $key=>$value){ ?>
        <?php if($value['status']=='NotComplete'){ ?>
            <?php if($beForName!=$value['Name'] && $beForLastName!=$value['LastName']){ ?>
                <tr>
                    <td style="border-bottom: solid 1px;"><?php echo $number ?></td>
                    <td style="border-bottom: solid 1px;text-align: center;"><?php echo $value['day'] ?></td>
                    <td style="border-bottom: solid 1px;"><?php echo $beForName ?> <?php echo $beForLastName ?></td>
                    <td style="border-bottom: solid 1px;"><?php echo $sum; ?></td>
                </tr>
            <?php  $number++;$beForName=$value['Name'];$beForLastName=$value['LastName'];$sum=0; } ?>
            <?php $sum+=50; ?>
        <?php }} ?>
                <tr>
                    <td style="border-bottom: solid 1px;"><?php echo $number ?></td>
                    <td style="border-bottom: solid 1px;text-align: center;"><?php echo $value['day'] ?></td>
                    <td style="border-bottom: solid 1px;"><?php echo $beForName ?> <?php echo $beForLastName ?></td>
                    <td style="border-bottom: solid 1px;"><?php echo $sum; ?></td>
                </tr>
    </tbody>
</table>
<span style="font-weight: bolder;font-size: 16px;">รับผิดชอบค่าปรับเทน</span>
<table>
    <thead>
    <tr>
        <td>#</td>
        <td>ชื่อพนักงาน</td>
        <td>บาท</td>
    </tr>
    </thead>
    <tbody>
    <?php $number=1;$sum=0;$beForName='ศรัณย์';$beForLastName='ลายกลาง'; ?>
    <?php foreach($dataResponsible as $key=>$value) { ?>
        <?php if ($key == 0) {
            $beForName = $value->responsibleUserName->Name;
            $beForLastName = $value->responsibleUserName->LastName;
            }
        ?>
            <?php if($beForName!=$value->responsibleUserName->Name && $beForLastName!=$value->responsibleUserName->LastName){ ?>
                <tr>
                    <td style="border-bottom: solid 1px;"><?php echo $number ?></td>
                    <td style="border-bottom: solid 1px;"><?php echo $beForName ?> <?php echo $beForLastName ?></td>
                    <td style="border-bottom: solid 1px;"><?php echo $sum; ?></td>
                </tr>
                <?php  $number++;$beForName=$value->responsibleUserName->Name;$beForLastName=$value->responsibleUserName->LastName;$sum=0; } ?>
            <?php $sum+=50; ?>
        <?php } ?>
    <tr>
        <td style="border-bottom: solid 1px;"><?php echo $number ?></td>
        <td style="border-bottom: solid 1px;"><?php echo $beForName ?> <?php echo $beForLastName ?></td>
        <td style="border-bottom: solid 1px;"><?php echo $sum; ?></td>
    </tr>
    </tbody>
</table>

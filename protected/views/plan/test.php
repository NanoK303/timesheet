<?php
/**
 * Created by PhpStorm.
 * User: sootipong
 * Date: 8/31/2015
 * Time: 11:27
 */
$this->pageTitle=Yii::app()->name;
$monthNames = array("","มกราคม","กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
?>

<script>
    $(document).ready(function() {
        var monthScrollLeft = [0,0,1240,2360,3600,4800,6040,7240,8480,9720,10920,12160,13360,14600];
//        console.log(monthScrollLeft[$('input#month').val()]);
        $("div#Box-Calendar").scrollLeft(((monthScrollLeft[$('input#month').val()])+($('input#day').val()*40)-40));
    })
</script>
<input id="day" type="hidden" value="<?php echo $dateNow->format('j') ?>">
<input id="month" type="hidden" value="<?php echo $dateNow->format('n') ?>">
<div class="row">
    <div class="col-lg-12" style="border: solid 1px;padding: 0px;">
        <div style="float:left;width: 20%;">
            <div style="width:100%;text-align: center;padding: 16px;">กิจกรรม</div>
            <?php foreach($dataPlan as $key=>$value){ ?>
                <?php if($key!='data'){ ?>
                        <?php foreach($value as $Rkey=>$Rvalue){ ?>
                            <?php if($Rkey=='data'){ ?>
                                <div style="padding-left:10px;width:100%;border-top: solid 1px;border-right: solid 1px;"><?php echo $Rvalue['Plan_title']?></div>
                            <?php }else{ ?>
                                <?php foreach($Rvalue as $Tkey=>$Tvalue){ ?>
                                    <?php if($Tkey=='data'){ ?>
                                        <div style="padding-left:20px;width:100%;border-top: solid 1px;border-right: solid 1px;"><?php echo $Tvalue['Plan_title']?></div>
                                    <?php }else{ ?>
                                        <?php foreach($Tvalue as $Ykey=>$Yvalue){ ?>
                                            <?php if($Ykey=='data'){ ?>
                                                <div style="padding-left:30px;width:100%;border-top: solid 1px;border-right: solid 1px;"><?php echo $Yvalue['Plan_title']?></div>
                                            <?php }else{ ?>
                                                <?php foreach($Yvalue as $Ukey=>$Uvalue){ ?>
                                                    <?php if($Ukey=='data'){ ?>
                                                        <div style="padding-left:40px;width:100%;border-top: solid 1px;border-right: solid 1px;"><?php echo $Uvalue['Plan_title']?></div>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                <?php } ?>
            <?php } ?>
            <div style="width:100%;border-top: solid 1px;"></div>
        </div>
        <div id="Box-Calendar" style="float:left;width: 80%;overflow: auto;">
            <div style="width:14600px;">
                <?php for($month=0;$month<12;$month++){ ?>
                    <div style="float: left;display: block;">
                    <div style="width:<?php echo (intval($dateCalendar->format('t'))*40) ?>px;padding:5px;border-right:solid 1px;border-left:solid 1px;text-align: center;">เดือน <?php echo $monthNames[$dateCalendar->format('n')]?></div>
                    <div style="">
                        <?php for($i=1;$i<=intval($dateCalendar->format('t'));$i++){?>
                            <?php if($i==1){ ?>
                                <div style="background-color:#DDDDDD;border-left: solid 1px;border-top:solid 1px;border-bottom:solid 1px;text-align: center;float:left;width: 40px;"><?php echo $i; ?></div>
                            <?php }elseif($i==intval($dateCalendar->format('t'))){ ?>
                                <div style="background-color:#DDDDDD;border-right: solid 1px;border-top:solid 1px;border-bottom:solid 1px;text-align: center;float:left;width: 40px;"><?php echo $i; ?></div>
                            <?php }else{ ?>
                                <div style="background-color:#DDDDDD;border-left:solid 1px#FFFFFF; border-top:solid 1px;border-bottom:solid 1px;text-align: center;float:left;width: 40px;"><?php echo $i; ?></div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                    </div>
                    <?php $dateCalendar->modify("+1 month"); ?>
                <?php } ?>

                <div style="clear: both;"></div>

                <?php foreach($dataPlan as $key=>$value){ ?>
                    <?php if($key!='data'){ ?>
                        <?php foreach($value as $Rkey=>$Rvalue){ ?>
                            <?php if($Rkey=='data'){ ?>
                                <div style="text-align: center;">
                                    <div minMargin="<?php echo $Rvalue['minMargin']; ?>" min="<?php echo $Rvalue['min'] ?>" id="Plan_id_<?php echo $Rvalue['Plan_id'] ?>" style="height: 21px;background-color:#d9edf7;text-align: center;float:left;margin-left:<?php echo $Rvalue['margin-left']?>px;width: <?php echo $Rvalue['width']?>px;">
                                        <div id="backward" Plan_id="<?php echo $Rvalue['Plan_id'] ?>" Plan_level_one="<?php echo $Rvalue['Plan_level_one'] ?>" Plan_level_two="<?php echo $Rvalue['Plan_level_two'] ?>" Plan_level_three="<?php echo $Rvalue['Plan_level_three'] ?>" style="height:100%;cursor: col-resize;float:left;width: 10%;text-align: left;background-color: #8699a4;"><span class="glyphicon glyphicon-step-backward" aria-hidden="true"></span></div>
                                        <div id="TabWorkBox" Plan_id="<?php echo $Rvalue['Plan_id'] ?>" Plan_level_one="<?php echo $Rvalue['Plan_level_one'] ?>" Plan_level_two="<?php echo $Rvalue['Plan_level_two'] ?>" Plan_level_three="<?php echo $Rvalue['Plan_level_three'] ?>" style="height:100%;float:left;width: 80%;"><span style="cursor: pointer;" id="TabWork" class="glyphicon glyphicon-resize-horizontal" aria-hidden="true"></span></div>
                                        <div id="forward" Plan_id="<?php echo $Rvalue['Plan_id'] ?>" Plan_level_one="<?php echo $Rvalue['Plan_level_one'] ?>" Plan_level_two="<?php echo $Rvalue['Plan_level_two'] ?>" Plan_level_three="<?php echo $Rvalue['Plan_level_three'] ?>" style="height:100%;cursor: col-resize;float:left;width: 10%;text-align: right;background-color: #8699a4;"><span class="glyphicon glyphicon-step-forward" aria-hidden="true"></span></div>
                                    </div>
                                </div>
                                <div style="clear: both;"></div>
                            <?php }else{ ?>
                                <?php foreach($Rvalue as $Tkey=>$Tvalue){ ?>
                                    <?php if($Tkey=='data'){ ?>
                                        <div style="text-align: center;">
                                            <div minMargin="<?php echo $Tvalue['minMargin']; ?>" min="<?php echo $Tvalue['min'] ?>" id="Plan_id_<?php echo $Tvalue['Plan_id'] ?>" style="height: 21px;background-color:#d9edf7;text-align: center;float:left;margin-left:<?php echo $Tvalue['margin-left']?>px;width: <?php echo $Tvalue['width']?>px;">
                                                <div id="backward" Plan_id="<?php echo $Tvalue['Plan_id'] ?>" Plan_level_one="<?php echo $Tvalue['Plan_level_one'] ?>" Plan_level_two="<?php echo $Tvalue['Plan_level_two'] ?>" Plan_level_three="<?php echo $Tvalue['Plan_level_three'] ?>" style="height:100%;cursor: col-resize;float:left;width: 10%;text-align: left;background-color: #8699a4;"><span class="glyphicon glyphicon-step-backward" aria-hidden="true"></span></div>
                                                <div id="TabWorkBox" Plan_id="<?php echo $Tvalue['Plan_id'] ?>" Plan_level_one="<?php echo $Tvalue['Plan_level_one'] ?>" Plan_level_two="<?php echo $Tvalue['Plan_level_two'] ?>" Plan_level_three="<?php echo $Tvalue['Plan_level_three'] ?>" style="height:100%;float:left;width: 80%;"><span style="cursor: pointer;" id="TabWork" class="glyphicon glyphicon-resize-horizontal" aria-hidden="true"></span></div>
                                                <div id="forward" Plan_id="<?php echo $Tvalue['Plan_id'] ?>" Plan_level_one="<?php echo $Tvalue['Plan_level_one'] ?>" Plan_level_two="<?php echo $Tvalue['Plan_level_two'] ?>" Plan_level_three="<?php echo $Tvalue['Plan_level_three'] ?>" style="height:100%;cursor: col-resize;float:left;width: 10%;text-align: right;background-color: #8699a4;"><span class="glyphicon glyphicon-step-forward" aria-hidden="true"></span></div>
                                            </div>
                                        </div>
                                        <div style="clear: both;"></div>
                                    <?php }else{ ?>
                                        <?php foreach($Tvalue as $Ykey=>$Yvalue){ ?>
                                            <?php if($Ykey=='data'){ ?>
                                                <div style="text-align: center;">
                                                    <div minMargin="<?php echo $Yvalue['minMargin']; ?>" min="<?php echo $Yvalue['min'] ?>" id="Plan_id_<?php echo $Yvalue['Plan_id'] ?>" style="height: 21px;background-color:#d9edf7;text-align: center;float:left;margin-left:<?php echo $Yvalue['margin-left']?>px;width: <?php echo $Yvalue['width']?>px;">
                                                        <div id="backward" Plan_id="<?php echo $Yvalue['Plan_id'] ?>" Plan_level_one="<?php echo $Yvalue['Plan_level_one'] ?>" Plan_level_two="<?php echo $Yvalue['Plan_level_two'] ?>" Plan_level_three="<?php echo $Yvalue['Plan_level_three'] ?>" style="height:100%;cursor: col-resize;float:left;width: 10%;text-align: left;background-color: #8699a4;"><span class="glyphicon glyphicon-step-backward" aria-hidden="true"></span></div>
                                                        <div id="TabWorkBox" Plan_id="<?php echo $Yvalue['Plan_id'] ?>" Plan_level_one="<?php echo $Yvalue['Plan_level_one'] ?>" Plan_level_two="<?php echo $Yvalue['Plan_level_two'] ?>" Plan_level_three="<?php echo $Yvalue['Plan_level_three'] ?>" style="height:100%;float:left;width: 80%;"><span style="cursor: pointer;" id="TabWork" class="glyphicon glyphicon-resize-horizontal" aria-hidden="true"></span></div>
                                                        <div id="forward" Plan_id="<?php echo $Yvalue['Plan_id'] ?>" Plan_level_one="<?php echo $Yvalue['Plan_level_one'] ?>" Plan_level_two="<?php echo $Yvalue['Plan_level_two'] ?>" Plan_level_three="<?php echo $Yvalue['Plan_level_three'] ?>" style="height:100%;cursor: col-resize;float:left;width: 10%;text-align: right;background-color: #8699a4;"><span class="glyphicon glyphicon-step-forward" aria-hidden="true"></span></div>
                                                    </div>
                                                </div>
                                                <div style="clear: both;"></div>
                                            <?php }else{ ?>
                                                <?php foreach($Yvalue as $Ukey=>$Uvalue){ ?>
                                                    <?php if($Ukey=='data'){ ?>
                                                        <div style="text-align: center;">
                                                            <div minMargin="<?php echo $Uvalue['minMargin']; ?>" min="<?php echo $Uvalue['min'] ?>" id="Plan_id_<?php echo $Uvalue['Plan_id'] ?>" style="height: 21px;background-color:#d9edf7;text-align: center;float:left;margin-left:<?php echo $Uvalue['margin-left']?>px;width: <?php echo $Uvalue['width']?>px;">
                                                                <div id="backward" Plan_id="<?php echo $Uvalue['Plan_id'] ?>" Plan_level_one="<?php echo $Uvalue['Plan_level_one'] ?>" Plan_level_two="<?php echo $Uvalue['Plan_level_two'] ?>" Plan_level_three="<?php echo $Uvalue['Plan_level_three'] ?>" style="height:100%;cursor: col-resize;float:left;width: 10%;text-align: left;background-color: #8699a4;"><span class="glyphicon glyphicon-step-backward" aria-hidden="true"></span></div>
                                                                <div id="TabWorkBox" Plan_id="<?php echo $Uvalue['Plan_id'] ?>" Plan_level_one="<?php echo $Uvalue['Plan_level_one'] ?>" Plan_level_two="<?php echo $Uvalue['Plan_level_two'] ?>" Plan_level_three="<?php echo $Uvalue['Plan_level_three'] ?>" style="height:100%;float:left;width: 80%;"><span style="cursor: pointer;" id="TabWork" class="glyphicon glyphicon-resize-horizontal" aria-hidden="true"></span></div>
                                                                <div id="forward" Plan_id="<?php echo $Uvalue['Plan_id'] ?>" Plan_level_one="<?php echo $Uvalue['Plan_level_one'] ?>" Plan_level_two="<?php echo $Uvalue['Plan_level_two'] ?>" Plan_level_three="<?php echo $Uvalue['Plan_level_three'] ?>" style="height:100%;cursor: col-resize;float:left;width: 10%;text-align: right;background-color: #8699a4;"><span class="glyphicon glyphicon-step-forward" aria-hidden="true"></span></div>
                                                            </div>
                                                        </div>
                                                        <div style="clear: both;"></div>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<div class="modal-backdrop" id="loading" style="display: none;z-index: 1800;background-color:#fff;opacity: 0.9;" >
    <div style="margin-top: 10%;margin-left: 45%;opacity: 0.8;" >
        <img style="width:150px;height: 150px;" src="/images/loading.gif">
    </div>
</div>

<script>
    $(document).ready(function(){
        var minMargin=0,min=0,startX= 0,x= 0,mx= 0,day= 0,width= 0,margin= 0,backward= 0,TabWorkBox= 0,forward= 0,Plan_id= 0,Plan_level_one= 0,Plan_level_two= 0,Plan_level_three= 0,Plan_level_one_width= 0,Plan_level_one_margin=0,Plan_level_two_width= 0,Plan_level_two_margin=0,Plan_level_three_width= 0,Plan_level_three_margin=0;
        var main,tabwork,thisbox;

        function SetMouseDown(type){
            if(type=='TabWorkBox') {
                main.find('#backward').hide();
                main.find('#forward').hide();
                main.find('#TabWorkBox').css({width:'100%'});
            }else if(type=='backward'){
                main.find('#backward').css({width:'100%'});
                main.find('#forward').hide();
                main.find('#TabWorkBox').hide();
            }else if(type=='forward'){
                main.find('#backward').hide();
                main.find('#forward').css({width:'100%'});
                main.find('#TabWorkBox').hide();
            }
        }

        function SetMouseUp(type){
//            if(type=='TabWorkBox') {
                main.find('#backward').show();
                main.find('#backward').css({width:'10%'});
                main.find('#forward').show();
                main.find('#forward').css({width:'10%'});
                main.find('#TabWorkBox').show();
                main.find('#TabWorkBox').css({width:'80%'});
//            }else if(type=='backward'){
//                main.find('#backward').css({width: (backward) + 'px'});
//                main.find('#forward').show();
//                main.find('#TabWorkBox').show();
//            }else if(type=='forward'){
//                main.find('#backward').show();
//                main.find('#forward').css({width: (forward) + 'px'});
//                main.find('#TabWorkBox').show();
//            }
        }

        $(document).on('mousedown', "#TabWork", function(event) {
//            console.log('#TabWork')
            Plan_id=$(this).parent().attr('Plan_id');
            Plan_level_one=$(this).parent().attr('Plan_level_one');
            Plan_level_two=$(this).parent().attr('Plan_level_two');
            Plan_level_three=$(this).parent().attr('Plan_level_three');

            if(typeof(Plan_level_one)!="undefined"){
                Plan_level_one_width=$('div#Plan_id_'+Plan_level_one).css('width');
                Plan_level_one_margin=$('div#Plan_id_'+Plan_level_one).css('margin-left');
            }
            if(typeof(Plan_level_two)!="undefined"){
                Plan_level_two_width=$('div#Plan_id_'+Plan_level_two).css('width');
                Plan_level_two_margin=$('div#Plan_id_'+Plan_level_two).css('margin-left');
            }
            if(typeof(Plan_level_three)!="undefined"){
                Plan_level_three_width=$('div#Plan_id_'+Plan_level_three).css('width');
                Plan_level_three_margin=$('div#Plan_id_'+Plan_level_three).css('margin-left');
            }

            startX = event.pageX-x;
            main=$(this).parent().parent();

            backward=parseInt(main.find('#backward').css('width'));
            TabWorkBox=parseInt(main.find('#TabWorkBox').css('width'));
            forward=parseInt(main.find('#forward').css('width'));

            minMargin=parseInt(main.attr('minMargin'))-40;
            min=parseInt(main.attr('min'));
            margin=parseInt(main.css('margin-left'));
            width=parseInt(main.css('width'));
            event.preventDefault();
            $(document).on('mousemove', mouseMoveMarginWork);
            $(document).on('mouseup', mouseUpMarginWork);
        })

        function mouseMoveMarginWork(event){
            SetMouseDown('TabWorkBox');
            mx = event.pageX - startX;
//            console.log('mouseMoveMarginWork:: '+mx);
            if((margin + mx)>=0){
                main.css({
                    'margin-left': (margin + mx) + 'px'
                });
            }
        }

        function mouseUpMarginWork(event){
//            console.log('mouseUpMarginWork');
            console.log('mouseUpMarginWork:: '+mx);
            if(mx>0) {
                day = Math.ceil(mx / 40);
            }else{
                day = Math.floor(mx / 40);
            }
//            console.log(day*40);
            if((margin + (day * 40))>=0){
                main.css({
                    'margin-left': (margin + (day * 40)) + 'px'
                });
                savePlan(Plan_id,'TabWork',day);
            }else{
                console.log('else');
            }
            SetMouseUp('TabWorkBox');
            $(document).off('mousemove', mouseMoveMarginWork);
            $(document).off('mouseup', mouseUpMarginWork);

            if(Plan_level_one!='' && Plan_level_two!='' && Plan_level_three!=''){
                if((margin+(day * 40))<(parseInt(Plan_level_one_margin))){
                    $('div#Plan_id_'+Plan_level_one).css({
                        'margin-left': (margin + (day * 40)) + 'px',
                        'width': (parseInt(Plan_level_one_width) + (parseInt(Plan_level_one_margin)-(margin+(day * 40)))) + 'px'
                    });
                }else if((margin+(width + (day * 40)))>(parseInt(Plan_level_one_width)+parseInt(Plan_level_one_margin))){
                    $('div#Plan_id_'+Plan_level_one).css({
                        width: (parseInt(Plan_level_one_width)+((margin+(width + (day * 40)))-(parseInt(Plan_level_one_width)+parseInt(Plan_level_one_margin))))+ 'px'
                    });
                }
                if((margin+(day * 40))<(parseInt(Plan_level_two_margin))){
                    $('div#Plan_id_'+Plan_level_two).css({
                        'margin-left': (margin + (day * 40)) + 'px',
                        'width': (parseInt(Plan_level_two_width) + (parseInt(Plan_level_two_margin)-(margin+(day * 40)))) + 'px'
                    });
                }else if((margin+(width + (day * 40)))>(parseInt(Plan_level_two_width)+parseInt(Plan_level_two_margin))){
                    $('div#Plan_id_'+Plan_level_two).css({
                        width: (parseInt(Plan_level_two_width)+((margin+(width + (day * 40)))-(parseInt(Plan_level_two_width)+parseInt(Plan_level_two_margin))))+ 'px'
                    });
                }
                if((margin+(day * 40))<(parseInt(Plan_level_three_margin))){
                    $('div#Plan_id_'+Plan_level_three).css({
                        'margin-left': (margin + (day * 40)) + 'px',
                        'width': (parseInt(Plan_level_three_width) + (parseInt(Plan_level_three_margin)-(margin+(day * 40)))) + 'px'
                    });
                }else if((margin+(width + (day * 40)))>(parseInt(Plan_level_three_width)+parseInt(Plan_level_three_margin))){
                    $('div#Plan_id_'+Plan_level_one).css({
                        width: (parseInt(Plan_level_three_width)+((margin+(width + (day * 40)))-(parseInt(Plan_level_three_width)+parseInt(Plan_level_three_margin))))+ 'px'
                    });
                }
            }else if(Plan_level_one!='' && Plan_level_two!=''){
                if((margin+(day * 40))<(parseInt(Plan_level_one_margin))){
                    $('div#Plan_id_'+Plan_level_one).css({
                        'margin-left': (margin + (day * 40)) + 'px',
                        'width': (parseInt(Plan_level_one_width) + (parseInt(Plan_level_one_margin)-(margin+(day * 40)))) + 'px'
                    });
                }else if((margin+(width + (day * 40)))>(parseInt(Plan_level_one_width)+parseInt(Plan_level_one_margin))){
                    $('div#Plan_id_'+Plan_level_one).css({
                        width: (parseInt(Plan_level_one_width)+((margin+(width + (day * 40)))-(parseInt(Plan_level_one_width)+parseInt(Plan_level_one_margin))))+ 'px'
                    });
                }
                if((margin+(day * 40))<(parseInt(Plan_level_two_margin))){
                    $('div#Plan_id_'+Plan_level_two).css({
                        'margin-left': (margin + (day * 40)) + 'px',
                        'width': (parseInt(Plan_level_two_width) + (parseInt(Plan_level_two_margin)-(margin+(day * 40)))) + 'px'
                    });
                }else if((margin+(width + (day * 40)))>(parseInt(Plan_level_two_width)+parseInt(Plan_level_two_margin))){
                    $('div#Plan_id_'+Plan_level_two).css({
                        width: (parseInt(Plan_level_two_width)+((margin+(width + (day * 40)))-(parseInt(Plan_level_two_width)+parseInt(Plan_level_two_margin))))+ 'px'
                    });
                }
            }else if(Plan_level_one!=''){
//                console.log('Plan_level_one');
                if((margin+(day * 40))<(parseInt(Plan_level_one_margin))){
                    $('div#Plan_id_'+Plan_level_one).css({
                        'margin-left': (margin + (day * 40)) + 'px',
                        'width': (parseInt(Plan_level_one_width) + (parseInt(Plan_level_one_margin)-(margin+(day * 40)))) + 'px'
                    });
                }else if((margin+(width + (day * 40)))>(parseInt(Plan_level_one_width)+parseInt(Plan_level_one_margin))){
                    $('div#Plan_id_'+Plan_level_one).css({
                        width: (parseInt(Plan_level_one_width)+((margin+(width + (day * 40)))-(parseInt(Plan_level_one_width)+parseInt(Plan_level_one_margin))))+ 'px'
                    });
                }
            }

        }

        $(document).on('mousedown', "#forward", function(event) {
            Plan_id=$(this).attr('Plan_id');
            Plan_level_one=$(this).attr('Plan_level_one');
            Plan_level_two=$(this).attr('Plan_level_two');
            Plan_level_three=$(this).attr('Plan_level_three');

            if(typeof(Plan_level_one)!="undefined"){
                Plan_level_one_width=$('div#Plan_id_'+Plan_level_one).css('width');
                Plan_level_one_margin=$('div#Plan_id_'+Plan_level_one).css('margin-left');
            }
            if(typeof(Plan_level_two)!="undefined"){
                Plan_level_two_width=$('div#Plan_id_'+Plan_level_two).css('width');
                Plan_level_two_margin=$('div#Plan_id_'+Plan_level_two).css('margin-left');
            }
            if(typeof(Plan_level_three)!="undefined"){
                Plan_level_three_width=$('div#Plan_id_'+Plan_level_three).css('width');
                Plan_level_three_margin=$('div#Plan_id_'+Plan_level_three).css('margin-left');
            }

            startX = event.pageX-x;
            main=$(this).parent();

            backward=parseInt(main.find('#backward').css('width'));
            TabWorkBox=parseInt(main.find('#TabWorkBox').css('width'));
            forward=parseInt(main.find('#forward').css('width'));

            tabwork=$(this).parent().find('#TabWorkBox');
            minMargin=parseInt(main.attr('minMargin'));
            min=parseInt(main.attr('min'));
            width=parseInt(main.css('width'));
            margin=parseInt(main.css('margin-left'));
            event.preventDefault();
            $(document).on('mousemove', mouseMove);
            $(document).on('mouseup', mouseUp);
        })

        function mouseMove(event) {
            SetMouseDown('forward');
            mx = event.pageX - startX;
            if ((width + mx) > 40 && ((margin+(width+mx))>=(min))){
                main.css({
                    width: (width + mx) + 'px'
                });
                tabwork.css({
                    width: ((width + mx)-26) + 'px'
                })
            }
        }

        function mouseUp(event){
//            console.log('mouseUp');
            day=Math.ceil(mx/40);
            if ((width + (day * 40)) > 40 && ((margin+(width+(day * 40)))>=min)) {
//                console.log('if');
                main.css({
                    width: (width + (day * 40)) + 'px'
                });
                tabwork.css({
                    width: ((width + (day * 40))-26) + 'px'
                })
                savePlan(Plan_id,'forward',day);
            }else{
//                console.log('else');
                main.css({
                    width: (width +day) + 'px'
                });
                tabwork.css({
                    width: ((width +day)-26) + 'px'
                })
            }
            SetMouseUp('forward');
            $(document).off('mousemove', mouseMove);
            $(document).off('mouseup', mouseUp);

            if(Plan_level_one!='' && Plan_level_two!='' && Plan_level_three!=''){
                if((margin+(width + (day * 40)))>(parseInt(Plan_level_one_width)+parseInt(Plan_level_one_margin))){
                    $('div#Plan_id_'+Plan_level_one).css({
                        width: (parseInt(Plan_level_one_width)+((margin+(width + (day * 40)))-(parseInt(Plan_level_one_width)+parseInt(Plan_level_one_margin))))+ 'px'
                    });
                }
                if((margin+(width + (day * 40)))>(parseInt(Plan_level_two_width)+parseInt(Plan_level_two_margin))){
                    $('div#Plan_id_'+Plan_level_two).css({
                        width: (parseInt(Plan_level_two_width)+((margin+(width + (day * 40)))-(parseInt(Plan_level_two_width)+parseInt(Plan_level_two_margin))))+ 'px'
                    });
                }
                if((margin+(width + (day * 40)))>(parseInt(Plan_level_three_width)+parseInt(Plan_level_three_margin))){
                    $('div#Plan_id_'+Plan_level_three).css({
                        width: (parseInt(Plan_level_three_width)+((margin+(width + (day * 40)))-(parseInt(Plan_level_three_width)+parseInt(Plan_level_three_margin))))+ 'px'
                    });
                }
            }else if(Plan_level_one!='' && Plan_level_two!=''){
                if((margin+(width + (day * 40)))>(parseInt(Plan_level_one_width)+parseInt(Plan_level_one_margin))){
                    $('div#Plan_id_'+Plan_level_one).css({
                        width: (parseInt(Plan_level_one_width)+((margin+(width + (day * 40)))-(parseInt(Plan_level_one_width)+parseInt(Plan_level_one_margin))))+ 'px'
                    });
                }
                if((margin+(width + (day * 40)))>(parseInt(Plan_level_two_width)+parseInt(Plan_level_two_margin))){
                    $('div#Plan_id_'+Plan_level_two).css({
                        width: (parseInt(Plan_level_two_width)+((margin+(width + (day * 40)))-(parseInt(Plan_level_two_width)+parseInt(Plan_level_two_margin))))+ 'px'
                    });
                }
            }else if(Plan_level_one!=''){
                if((margin+(width + (day * 40)))>(parseInt(Plan_level_one_width)+parseInt(Plan_level_one_margin))){
                    $('div#Plan_id_'+Plan_level_one).css({
                        width: (parseInt(Plan_level_one_width)+((margin+(width + (day * 40)))-(parseInt(Plan_level_one_width)+parseInt(Plan_level_one_margin))))+ 'px'
                    });
                }
            }

        }

        $(document).on('mousedown', "#backward", function(event) {
//            console.log('mousedownBackward')
            Plan_id=$(this).attr('Plan_id');
            Plan_level_one=$(this).attr('Plan_level_one');
            Plan_level_two=$(this).attr('Plan_level_two');
            Plan_level_three=$(this).attr('Plan_level_three');

            if(typeof(Plan_level_one)!="undefined"){
                Plan_level_one_width=$('div#Plan_id_'+Plan_level_one).css('width');
                Plan_level_one_margin=$('div#Plan_id_'+Plan_level_one).css('margin-left');
            }
            if(typeof(Plan_level_two)!="undefined"){
                Plan_level_two_width=$('div#Plan_id_'+Plan_level_two).css('width');
                Plan_level_two_margin=$('div#Plan_id_'+Plan_level_two).css('margin-left');
            }
            if(typeof(Plan_level_three)!="undefined"){
                Plan_level_three_width=$('div#Plan_id_'+Plan_level_three).css('width');
                Plan_level_three_margin=$('div#Plan_id_'+Plan_level_three).css('margin-left');
            }

            startX = event.pageX-x;
            main=$(this).parent();

            backward=parseInt(main.find('#backward').css('width'));
            TabWorkBox=parseInt(main.find('#TabWorkBox').css('width'));
            forward=parseInt(main.find('#forward').css('width'));

            tabwork=$(this).parent().find('#TabWorkBox');
            minMargin=parseInt(main.attr('minMargin'));
            margin=parseInt(main.css('margin-left'));
            width=parseInt(main.css('width'));
            event.preventDefault();
            $(document).on('mousemove', mouseMoveMargin);
            $(document).on('mouseup', mouseUpMargin);
        })

        function mouseMoveMargin(event){
//            console.log('mouseMoveMargin');
            SetMouseDown('backward');
            mx = event.pageX - startX;
//            console.log(width-(margin+mx));
            if((margin + mx)>=0 && (width - mx)>40) {
                main.css({
                    'margin-left': (margin + mx) + 'px',
                    'width': (width - mx) + 'px'
                });
                tabwork.css({
                    width: ((width - mx)-26) + 'px'
                })
            }else{
                console.log('else');
            }
        }

        function mouseUpMargin(event){
//            console.log('mouseUpMargin')
            day=Math.floor(mx/40);
            if((margin + (day * 40))>=0 && (width - (day * 40))>40) {
                main.css({
                    'margin-left': (margin + (day * 40)) + 'px',
                    'width': (width - (day * 40)) + 'px'
                });
                tabwork.css({
                    width: ((width - (day * 40))-26) + 'px'
                })
                savePlan(Plan_id,'backward',day);
            }else{
                main.css({
                    'margin-left': (margin + day) + 'px',
                    'width': (width - day) + 'px'
                });
                tabwork.css({
                    width: ((width - day)-26) + 'px'
                })
            }
            SetMouseUp('backward');
            $(document).off('mousemove', mouseMoveMargin);
            $(document).off('mouseup', mouseUpMargin);

            if(Plan_level_one!='' && Plan_level_two!='' && Plan_level_three!=''){
                if((margin+(day * 40))<(parseInt(Plan_level_one_margin))){
                    $('div#Plan_id_'+Plan_level_one).css({
                        'margin-left': (margin + (day * 40)) + 'px',
                        'width': (parseInt(Plan_level_one_width) + (parseInt(Plan_level_one_margin)-(margin+(day * 40)))) + 'px'
                    });
                }
                if((margin+(day * 40))<(parseInt(Plan_level_two_margin))){
                    $('div#Plan_id_'+Plan_level_two).css({
                        'margin-left': (margin + (day * 40)) + 'px',
                        'width': (parseInt(Plan_level_two_width) + (parseInt(Plan_level_two_margin)-(margin+(day * 40)))) + 'px'
                    });
                }
                if((margin+(day * 40))<(parseInt(Plan_level_three_margin))){
                    $('div#Plan_id_'+Plan_level_three).css({
                        'margin-left': (margin + (day * 40)) + 'px',
                        'width': (parseInt(Plan_level_three_width) + (parseInt(Plan_level_three_margin)-(margin+(day * 40)))) + 'px'
                    });
                }
            }else if(Plan_level_one!='' && Plan_level_two!=''){
                if((margin+(day * 40))<(parseInt(Plan_level_one_margin))){
                    $('div#Plan_id_'+Plan_level_one).css({
                        'margin-left': (margin + (day * 40)) + 'px',
                        'width': (parseInt(Plan_level_one_width) + (parseInt(Plan_level_one_margin)-(margin+(day * 40)))) + 'px'
                    });
                }
                if((margin+(day * 40))<(parseInt(Plan_level_two_margin))){
                    $('div#Plan_id_'+Plan_level_two).css({
                        'margin-left': (margin + (day * 40)) + 'px',
                        'width': (parseInt(Plan_level_two_width) + (parseInt(Plan_level_two_margin)-(margin+(day * 40)))) + 'px'
                    });
                }
            }else if(Plan_level_one!=''){
                if((margin+(day * 40))<(parseInt(Plan_level_one_margin))){
                    $('div#Plan_id_'+Plan_level_one).css({
                        'margin-left': (margin + (day * 40)) + 'px',
                        'width': (parseInt(Plan_level_one_width) + (parseInt(Plan_level_one_margin)-(margin+(day * 40)))) + 'px'
                    });
                }
            }

        }

        function savePlan(Plan_id,Type,day){
            $('div#loading').show();
            $.post("/plan/updatePlan", {
                Plan_id:Plan_id,
                Type:Type,
                day:day
            }, function (datalist) {
                $('div#loading').hide();
            })
        }
    });
</script>
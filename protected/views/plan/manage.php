<?php
$this->pageTitle=Yii::app()->name;
$monthNames = array("","มกราคม","กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
?>
<div class="row">
    <ol class="breadcrumb">
        <li><a href="/project/manage">จัดโครงการ</a></li>
        <li class="active">จัดการแผนงาน <?php echo $dataProjectSelect->Project_name ?></li>
    </ol>
</div>
<div class="row">
    <h3>จัดการแผน <?php echo $dataProjectSelect->Project_name ?></h3>
</div>
<?php if(Yii::app()->user->type=='admin' or (!empty($dataGroupStaff) and $dataGroupStaff->Position=='leader')){?>
<div class="row">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'formAddPlan',
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => true,
        ),
        'htmlOptions' => array(
            'class' => 'form form-horizontal',
            'style'=>'margin-bottom: 20px;'
        ),
    ));
    echo $form->hiddenField($model, 'Plan_id');
    echo $form->hiddenField($model, 'Project_id');
    ?>

    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label"><?php echo $form->labelEx($model,'Plan_level_one',array('style'=>'text-align: left;')); ?></label>
        <div class="col-sm-2">
            <?php echo $form->error($model, 'Plan_level_one',array('style'=>'color:#FF0000;')); ?>
            <?php echo $form->dropDownList($model,'Plan_level_one', CHtml::listData($dataPlanSelect, 'Plan_id', 'Plan_title'), array(
                'empty'=>'เลือกแผนหลัก',
                'class' => 'form-control',
//                'style'=>'width:150px;',
                'ajax' => array(
                    'type' => 'POST',
                    'data' => 'js:"id="+$(\'#Plan_Plan_level_one\').val()',
                    'url' => CController::createUrl('/plan/PlanTwo'),
                    'update' => '#Plan_Plan_level_two',
                ),
            )) ?>
        </div>
        <label for="inputEmail3" class="col-sm-2 control-label"><?php echo $form->labelEx($model,'Plan_level_two',array('style'=>'text-align: left;')); ?></label>
        <div class="col-sm-2">
            <?php echo $form->error($model, 'Plan_level_two',array('style'=>'color:#FF0000;')); ?>
            <?php echo $form->dropDownList($model,'Plan_level_two',CHtml::listData($dataPlanOne, 'Plan_id', 'Plan_title'), array(
                'empty'=>'เลือกกิจกรรม',
                'class' => 'form-control',
//                'style'=>'width:150px;',
                'ajax' => array(
                    'type' => 'POST',
                    'data' => 'js:"id="+$(\'#Plan_Plan_level_two\').val()',
                    'url' => CController::createUrl('/plan/PlanThree'),
                    'update' => '#Plan_Plan_level_three',
                ),
            )) ?>
        </div>
        <label for="inputEmail3" class="col-sm-2 control-label"><?php echo $form->labelEx($model,'Plan_level_three',array('style'=>'text-align: left;')); ?></label>
        <div class="col-sm-2">
            <?php echo $form->error($model, 'Plan_level_Three',array('style'=>'color:#FF0000;')); ?>
            <?php echo $form->dropDownList($model,'Plan_level_three',CHtml::listData($dataPlanTwo, 'Plan_id', 'Plan_title'), array(
                'empty'=>'เลือกกิจกรรม',
                'class' => 'form-control'
//                'style'=>'width:150px;'
            )) ?>
        </div>
    </div>

    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label"><?php echo $form->labelEx($model,'Plan_start_date',array('style'=>'text-align: left;')); ?></label>
        <div class="col-sm-2">
            <?php echo $form->textfield($model,'Plan_start_date',array('class' => 'form-control','style'=>'','placeholder'=>'')) ?>
<!--            --><?php
//            Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
//            $this->widget('CJuiDateTimePicker', array(
//                'language' => '',
//                'model' => $model,                                // Model object
//                'attribute' => 'Plan_start_date', // Attribute name
//                'mode' => 'date',                     // Use "time","date" or "datetime" (default)
//                'options' => array(
//                    'dateFormat' => 'yy-mm-dd',
//                    'beforeShowDay'=>'js:function(date) {
//                        var unavailableDates = ["9-3-2014", "14-3-2014", "15-3-2014"];
//                        var enableDay = ["3-3-2014", "10-3-2014", "17-3-2014"];
//                        var weekend = [0, 6];
//                                dmy = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
//                                if ($.inArray(dmy, unavailableDates) > -1) {
//                                        return [false, "", "Unavailable"];
//                                }
//                                if ($.inArray(dmy, enableDay) > -1) {
//                                        return [true, ""];
//                                }
//                                if ($.inArray(date.getDay(), weekend) > -1) {
//                                        return [false, "", "Unavailable"];
//                                }
//                                return [true, ""];
//                }'
//                ),                       // jquery plugin options
//                'htmlOptions' => array('readonly' => true, 'class' => 'form-control','style'=>'') // HTML options
//            ));
//            ?>
            <?php echo $form->error($model, 'Plan_start_date',array('style'=>'color:#FF0000;')); ?>
        </div>
        <label for="inputEmail3" class="col-sm-2 control-label"><?php echo $form->labelEx($model,'Plan_end_date',array('style'=>'text-align: left;')); ?></label>
        <div class="col-sm-2">
            <?php echo $form->textfield($model,'Plan_end_date',array('class' => 'form-control','style'=>'','placeholder'=>'')) ?>
<!--            --><?php
//            Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
//            $this->widget('CJuiDateTimePicker', array(
//                'language' => '',
//                'model' => $model,                                // Model object
//                'attribute' => 'Plan_end_date', // Attribute name
//                'mode' => 'date',                     // Use "time","date" or "datetime" (default)
//                'options' => array(
//                    'dateFormat' => 'yy-mm-dd',
//                    'beforeShowDay'=>'js:function(date) {
//                        var unavailableDates = ["9-3-2014", "14-3-2014", "15-3-2014"];
//                        var enableDay = ["3-3-2014", "10-3-2014", "17-3-2014"];
//                        var weekend = [0, 6];
//                                dmy = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
//                                if ($.inArray(dmy, unavailableDates) > -1) {
//                                        return [false, "", "Unavailable"];
//                                }
//                                if ($.inArray(dmy, enableDay) > -1) {
//                                        return [true, ""];
//                                }
//                                if ($.inArray(date.getDay(), weekend) > -1) {
//                                        return [false, "", "Unavailable"];
//                                }
//                                return [true, ""];
//                }'
//                ),                       // jquery plugin options
//                'htmlOptions' => array('readonly' => true, 'class' => 'form-control','style'=>'') // HTML options
//            ));
//            ?>
            <?php echo $form->error($model, 'Plan_end_date',array('style'=>'color:#FF0000;')); ?>
        </div>

        <?php if(Plan::checkSubPlan($model->Plan_id)){ ?>
            <label for="inputEmail3" class="col-sm-2 control-label"><?php echo $form->labelEx($model,'Plan_hour',array('style'=>'text-align: left;')); ?></label>
            <div class="col-sm-2">
                <?php echo $form->textfield($model,'Plan_hour',array('disabled'=>'disabled','class' => 'form-control','style'=>'','placeholder'=>'ชั่วโมง')) ?>
                <?php echo $form->error($model, 'Plan_hour',array('style'=>'color:#FF0000;')); ?>
            </div>
        <?php }else{ ?>
            <label for="inputEmail3" class="col-sm-2 control-label"><?php echo $form->labelEx($model,'Plan_hour',array('style'=>'text-align: left;')); ?></label>
            <div class="col-sm-2">
                <?php echo $form->textfield($model,'Plan_hour',array('class' => 'form-control','style'=>'','placeholder'=>'ชั่วโมง')) ?>
                <?php echo $form->error($model, 'Plan_hour',array('style'=>'color:#FF0000;')); ?>
            </div>
        <?php } ?>
    </div>

    <div class="form-group" style="">
        <label for="inputEmail3" class="col-sm-2 control-label"><?php echo $form->labelEx($model,'Plan_title',array('style'=>'text-align: left;')); ?></label>
        <div class="col-sm-4">
            <?php echo $form->textfield($model,'Plan_title',array('class' => 'form-control','style'=>'width:100%;','placeholder'=>'หัวข้อ')) ?>
            <?php echo $form->error($model, 'Plan_title',array('style'=>'color:#FF0000;')); ?>
        </div>
        <?php if(!empty($model->Plan_id)) { ?>
            <label for="inputEmail3" class="col-sm-2 control-label">รายละเอียดการแก้ไข</label>
            <div class="col-sm-4">
                <?php echo $form->textfield($model,'LogPlan_detail',array('class' => 'form-control','style'=>'','placeholder'=>'รายละเอียดการแก้ไข')) ?>
                <?php echo $form->error($model, 'LogPlan_detail',array('style'=>'color:#FF0000;')); ?>
            </div>
        <?php } ?>
    </div>

    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">รายละเอียดแผนงาน</label>
        <div class="col-sm-6">
            <?php echo $form->textArea($model,'Plan_detail',array('class' => 'form-control','style'=>'height: 100px;','placeholder'=>'รายละเอียดแผน')) ?>
            <?php echo $form->error($model, 'Plan_detail',array('style'=>'color:#FF0000;')); ?>
        </div>
        <div class="col-sm-4">
            <?php
            if(!empty($model->Plan_id)) {
                echo CHtml::submitButton(Yii::t('main', 'แก้ไข'), array('id' => 'addform', 'class' => 'btn btn-warning','style'=>'float: left'));
                ?>
                <a style="float: left;margin-left: 5px;" href="/plan/manage" class="btn btn-default">กลับ</a>
            <?php
            }else{
                echo CHtml::submitButton(Yii::t('main', 'เพิ่ม'), array('id' => 'addform', 'class' => 'btn btn-primary'));
            }
            ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<?php } ?>

<div style="" class="row">
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">รายการ จัดการแผน</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>วันที่</th>
                            <th>รายละเอียด</th>
                            <th>ผู้จัดการ</th>
                            <th>ประเภท</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($dataLogPlan as $key=>$value){ ?>
                        <tr class="LogPlan" id="LogPlan_<?php echo $value->Plan_id; ?>">
                            <td><?php echo $value->LogPlan_date ?></td>
                            <td><?php echo $value->LogPlan_detail ?></td>
                            <td><?php echo $value->UserName ?></td>
                            <td><?php echo $value->LogPlan_type ?></td>
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
<!--                    <button type="button" class="btn btn-primary">Save changes</button>-->
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    th.ui-datepicker-week-end,
    td.ui-datepicker-week-end {
        display: none;
    }
</style>

<script>

    $( "#Plan_Plan_start_date" ).datepicker({
        dateFormat: 'yy-mm-dd',
//        beforeShowDay: $.datepicker.noWeekends
    });
    $( "#Plan_Plan_end_date" ).datepicker({
        dateFormat: 'yy-mm-dd'
//        beforeShowDay: $.datepicker.noWeekends
    });


    $(document).ready(function(){
        $(document).on('click',"#BtnAddStaff",function(){
            var Plan_id=$(this).attr('Plan_id');
            $.post("/Plan/getPlanStaff", {
                Plan_id:Plan_id
            }, function (datalist) {
                datalist = jQuery.parseJSON(datalist);
                $('input#PlanAssign_Plan_id').val(Plan_id);
                $('select#PlanAssign_UserName').val('');
                $('tbody#ListPlanStaff').html(datalist);
            })
        })

        $(document).on('click', ".glyphicon-download", function() {
            $.post("/Plan/ChangeLevel", {
                id:$(this).attr('id'),
                type:'down'
            }, function (datalist) {
                datalist = jQuery.parseJSON(datalist);
                if (datalist['status'] == 'true') {
                    location.reload();
                } else {
                    alert("ไม่สามารถ ย้ายตำแหน่งแผนงานได้");
                }
            })
        })

        $(document).on('click', ".glyphicon-upload", function() {
            $.post("/Plan/ChangeLevel", {
                id:$(this).attr('id'),
                type:'up'
        }, function (datalist) {
            datalist = jQuery.parseJSON(datalist);
            if (datalist['status'] == 'true') {
                location.reload();
            } else {
                alert("ไม่สามารถ ย้ายตำแหน่งแผนงานได้");
            }
        })
    })
    })
</script>

<div class="row">
    <div class="col-md-6" style="font-size: 20px;border-bottom: solid 2px;padding-top: 10px;">
        วางแผนการการทำงาน
    </div>
    <div class="col-md-6">
        <a href="/project/report/Project_id/<?php echo $dataProjectSelect->Project_id ?>" target="_blank" style="float: right;" type="button" class="btn btn-default">
            รายงาน โครงการ
        </a>
        <button style="float: right;margin-right: 5px;" id="viewLogPlan" Plan_id="all" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
            รายงานการแก้ไข
        </button>
    </div>
</div>

<style>
    #plan_work:hover{
        background-color: rgba(240, 70, 0, .3) !important;
    }
</style>
<!--===============View Plan Move ============================================================================================-->
<div class="row" style="padding: 10px 0px;">
    <div class="col-md-12">
        <script>
            $(document).ready(function() {
                var monthScrollLeft = [0,0,1240,2360,3600,4800,6040,7240,8480,9720,10920,12160,13360,14600];
//        console.log(monthScrollLeft[$('input#month').val()]);
                $("div#Box-Calendar").scrollLeft(((monthScrollLeft[$('input#month').val()])+($('input#day').val()*40)-40));
            })
        </script>
        <input id="day" type="hidden" value="<?php echo $dateNow->format('j') ?>">
        <input id="month" type="hidden" value="<?php echo $dateNow->format('n') ?>">
        <div class="row">
            <div class="col-lg-12" style="border: solid 1px;padding: 0px;">
                <div style="float:left;width: 20%;">
                    <div style="width:100%;text-align: center;padding: 16px;">กิจกรรม</div>
                    <?php foreach($dataPlanView as $key=>$value){ ?>
                        <?php if($key!='data'){ ?>
                            <?php foreach($value as $Rkey=>$Rvalue){ ?>
                                <?php if($Rkey=='data'){ ?>
                                    <div data-toggle="tooltip" data-placement="top" title="<?php echo $Rvalue['Plan_title']?>" style="height: 21px;overflow:hidden;padding-left:10px;width:100%;border-top: solid 1px;border-right: solid 1px;"><?php echo $Rvalue['Plan_title']?></div>
                                <?php }else{ ?>
                                    <?php foreach($Rvalue as $Tkey=>$Tvalue){ ?>
                                        <?php if($Tkey=='data'){ ?>
                                            <div data-toggle="tooltip" data-placement="top" title="<?php echo $Tvalue['Plan_title']?>" style="height: 21px;overflow:hidden;padding-left:20px;width:100%;border-top: solid 1px;border-right: solid 1px;"><?php echo $Tvalue['Plan_title']?></div>
                                        <?php }else{ ?>
                                            <?php foreach($Tvalue as $Ykey=>$Yvalue){ ?>
                                                <?php if($Ykey=='data'){ ?>
                                                    <div data-toggle="tooltip" data-placement="top" title="<?php echo $Yvalue['Plan_title']?>" style="height: 21px;overflow:hidden;padding-left:30px;width:100%;border-top: solid 1px;border-right: solid 1px;"><?php echo $Yvalue['Plan_title']?></div>
                                                <?php }else{ ?>
                                                    <?php foreach($Yvalue as $Ukey=>$Uvalue){ ?>
                                                        <?php if($Ukey=='data'){ ?>
                                                            <div data-toggle="tooltip" data-placement="top" title="<?php echo $Uvalue['Plan_title']?>" style="height: 21px;overflow:hidden;padding-left:40px;width:100%;border-top: solid 1px;border-right: solid 1px;"><?php echo $Uvalue['Plan_title']?></div>
                                                        <?php } ?>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                    <div style="width:100%;border-top: solid 1px;"></div>
                </div>
                <div id="Box-Calendar" style="float:left;width: 80%;overflow: auto;">
                    <div style="width:30000px;">
                        <?php for($month=0;$month<24;$month++){ ?>
                            <div style="float: left;display: block;">
                                <div style="width:<?php echo (intval($dateCalendar->format('t'))*40) ?>px;padding:5px;border-right:solid 1px;border-left:solid 1px;text-align: center;">เดือน <?php echo $monthNames[$dateCalendar->format('n')]?> ปี <?php echo $dateCalendar->format('Y')?></div>
                                <div style="">
                                    <?php for($i=1;$i<=intval($dateCalendar->format('t'));$i++){?>
                                        <?php if($i==1){ ?>
                                            <div style="background-color:#DDDDDD;border-left: solid 1px;border-top:solid 1px;border-bottom:solid 1px;text-align: center;float:left;width: 40px;"><?php echo $i; ?></div>
                                        <?php }elseif($i==intval($dateCalendar->format('t'))){ ?>
                                            <div style="background-color:#DDDDDD;border-right: solid 1px;border-top:solid 1px;border-bottom:solid 1px;text-align: center;float:left;width: 40px;"><?php echo $i; ?></div>
                                        <?php }else{ ?>
                                            <div style="background-color:#DDDDDD;border-left:solid 1px#FFFFFF; border-top:solid 1px;border-bottom:solid 1px;text-align: center;float:left;width: 40px;"><?php echo $i; ?></div>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php $dateCalendar->modify("+1 month"); ?>
                        <?php } ?>

                        <div style="clear: both;"></div>

                        <?php foreach($dataPlanView as $key=>$value){ ?>
                            <?php if($key!='data'){ ?>
                                <?php foreach($value as $Rkey=>$Rvalue){ ?>
                                    <?php if($Rkey=='data'){ ?>
                                        <div style="text-align: center;">
                                            <div minMargin="<?php echo $Rvalue['minMargin']; ?>" min="<?php echo $Rvalue['min'] ?>" id="Plan_id_<?php echo $Rvalue['Plan_id'] ?>" style="height: 21px;background-color:#d9edf7;text-align: center;float:left;margin-left:<?php echo $Rvalue['margin-left']?>px;width: <?php echo $Rvalue['width']?>px;">
                                                <?php if(Yii::app()->user->type=='admin'){?>
                                                    <div id="backward" Plan_id="<?php echo $Rvalue['Plan_id'] ?>" Plan_level_one="<?php echo $Rvalue['Plan_level_one'] ?>" Plan_level_two="<?php echo $Rvalue['Plan_level_two'] ?>" Plan_level_three="<?php echo $Rvalue['Plan_level_three'] ?>" style="height:100%;cursor: col-resize;float:left;width: 10%;text-align: left;background-color: #8699a4;"><span class="glyphicon glyphicon-step-backward" aria-hidden="true"></span></div>
                                                    <div id="TabWorkBox" Plan_id="<?php echo $Rvalue['Plan_id'] ?>" Plan_level_one="<?php echo $Rvalue['Plan_level_one'] ?>" Plan_level_two="<?php echo $Rvalue['Plan_level_two'] ?>" Plan_level_three="<?php echo $Rvalue['Plan_level_three'] ?>" style="height:100%;float:left;width: 80%;background-color: #006400;"><span style="cursor: pointer;" id="TabWork" class="glyphicon glyphicon-resize-horizontal" aria-hidden="true"></span></div>
                                                    <div id="forward" Plan_id="<?php echo $Rvalue['Plan_id'] ?>" Plan_level_one="<?php echo $Rvalue['Plan_level_one'] ?>" Plan_level_two="<?php echo $Rvalue['Plan_level_two'] ?>" Plan_level_three="<?php echo $Rvalue['Plan_level_three'] ?>" style="height:100%;cursor: col-resize;float:left;width: 10%;text-align: right;background-color: #8699a4;"><span class="glyphicon glyphicon-step-forward" aria-hidden="true"></span></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div style="clear: both;"></div>
                                    <?php }else{ ?>
                                        <?php foreach($Rvalue as $Tkey=>$Tvalue){ ?>
                                            <?php if($Tkey=='data'){ ?>
                                                <div style="text-align: center;">
                                                    <div minMargin="<?php echo $Tvalue['minMargin']; ?>" min="<?php echo $Tvalue['min'] ?>" id="Plan_id_<?php echo $Tvalue['Plan_id'] ?>" style="height: 21px;background-color:#d9edf7;text-align: center;float:left;margin-left:<?php echo $Tvalue['margin-left']?>px;width: <?php echo $Tvalue['width']?>px;">
                                                    <?php if(Yii::app()->user->type=='admin'){?>
                                                        <div id="backward" Plan_id="<?php echo $Tvalue['Plan_id'] ?>" Plan_level_one="<?php echo $Tvalue['Plan_level_one'] ?>" Plan_level_two="<?php echo $Tvalue['Plan_level_two'] ?>" Plan_level_three="<?php echo $Tvalue['Plan_level_three'] ?>" style="height:100%;cursor: col-resize;float:left;width: 10%;text-align: left;background-color: #8699a4;"><span class="glyphicon glyphicon-step-backward" aria-hidden="true"></span></div>
                                                        <div id="TabWorkBox" Plan_id="<?php echo $Tvalue['Plan_id'] ?>" Plan_level_one="<?php echo $Tvalue['Plan_level_one'] ?>" Plan_level_two="<?php echo $Tvalue['Plan_level_two'] ?>" Plan_level_three="<?php echo $Tvalue['Plan_level_three'] ?>" style="height:100%;float:left;width: 80%;background-color: #9ACD32;"><span style="cursor: pointer;" id="TabWork" class="glyphicon glyphicon-resize-horizontal" aria-hidden="true"></span></div>
                                                        <div id="forward" Plan_id="<?php echo $Tvalue['Plan_id'] ?>" Plan_level_one="<?php echo $Tvalue['Plan_level_one'] ?>" Plan_level_two="<?php echo $Tvalue['Plan_level_two'] ?>" Plan_level_three="<?php echo $Tvalue['Plan_level_three'] ?>" style="height:100%;cursor: col-resize;float:left;width: 10%;text-align: right;background-color: #8699a4;"><span class="glyphicon glyphicon-step-forward" aria-hidden="true"></span></div>
                                                    <?php } ?>
                                                    </div>
                                                </div>
                                                <div style="clear: both;"></div>
                                            <?php }else{ ?>
                                                <?php foreach($Tvalue as $Ykey=>$Yvalue){ ?>
                                                    <?php if($Ykey=='data'){ ?>
                                                        <div style="text-align: center;">
                                                            <div minMargin="<?php echo $Yvalue['minMargin']; ?>" min="<?php echo $Yvalue['min'] ?>" id="Plan_id_<?php echo $Yvalue['Plan_id'] ?>" style="height: 21px;background-color:#d9edf7;text-align: center;float:left;margin-left:<?php echo $Yvalue['margin-left']?>px;width: <?php echo $Yvalue['width']?>px;">
                                                            <?php if(Yii::app()->user->type=='admin'){?>
                                                                <div id="backward" Plan_id="<?php echo $Yvalue['Plan_id'] ?>" Plan_level_one="<?php echo $Yvalue['Plan_level_one'] ?>" Plan_level_two="<?php echo $Yvalue['Plan_level_two'] ?>" Plan_level_three="<?php echo $Yvalue['Plan_level_three'] ?>" style="height:100%;cursor: col-resize;float:left;width: 10%;text-align: left;background-color: #8699a4;"><span class="glyphicon glyphicon-step-backward" aria-hidden="true"></span></div>
                                                                <div id="TabWorkBox" Plan_id="<?php echo $Yvalue['Plan_id'] ?>" Plan_level_one="<?php echo $Yvalue['Plan_level_one'] ?>" Plan_level_two="<?php echo $Yvalue['Plan_level_two'] ?>" Plan_level_three="<?php echo $Yvalue['Plan_level_three'] ?>" style="height:100%;float:left;width: 80%;background-color: #6B8E23;"><span style="cursor: pointer;" id="TabWork" class="glyphicon glyphicon-resize-horizontal" aria-hidden="true"></span></div>
                                                                <div id="forward" Plan_id="<?php echo $Yvalue['Plan_id'] ?>" Plan_level_one="<?php echo $Yvalue['Plan_level_one'] ?>" Plan_level_two="<?php echo $Yvalue['Plan_level_two'] ?>" Plan_level_three="<?php echo $Yvalue['Plan_level_three'] ?>" style="height:100%;cursor: col-resize;float:left;width: 10%;text-align: right;background-color: #8699a4;"><span class="glyphicon glyphicon-step-forward" aria-hidden="true"></span></div>
                                                            <?php } ?>
                                                            </div>
                                                        </div>
                                                        <div style="clear: both;"></div>
                                                    <?php }else{ ?>
                                                        <?php foreach($Yvalue as $Ukey=>$Uvalue){ ?>
                                                            <?php if($Ukey=='data'){ ?>
                                                                <div style="text-align: center;">
                                                                    <div minMargin="<?php echo $Uvalue['minMargin']; ?>" min="<?php echo $Uvalue['min'] ?>" id="Plan_id_<?php echo $Uvalue['Plan_id'] ?>" style="height: 21px;background-color:#d9edf7;text-align: center;float:left;margin-left:<?php echo $Uvalue['margin-left']?>px;width: <?php echo $Uvalue['width']?>px;">
                                                                    <?php if(Yii::app()->user->type=='admin'){?>
                                                                        <div id="backward" Plan_id="<?php echo $Uvalue['Plan_id'] ?>" Plan_level_one="<?php echo $Uvalue['Plan_level_one'] ?>" Plan_level_two="<?php echo $Uvalue['Plan_level_two'] ?>" Plan_level_three="<?php echo $Uvalue['Plan_level_three'] ?>" style="height:100%;cursor: col-resize;float:left;width: 10%;text-align: left;background-color: #8699a4;"><span class="glyphicon glyphicon-step-backward" aria-hidden="true"></span></div>
                                                                        <div id="TabWorkBox" Plan_id="<?php echo $Uvalue['Plan_id'] ?>" Plan_level_one="<?php echo $Uvalue['Plan_level_one'] ?>" Plan_level_two="<?php echo $Uvalue['Plan_level_two'] ?>" Plan_level_three="<?php echo $Uvalue['Plan_level_three'] ?>" style="height:100%;float:left;width: 80%;background-color: #556B2F;"><span style="cursor: pointer;" id="TabWork" class="glyphicon glyphicon-resize-horizontal" aria-hidden="true"></span></div>
                                                                        <div id="forward" Plan_id="<?php echo $Uvalue['Plan_id'] ?>" Plan_level_one="<?php echo $Uvalue['Plan_level_one'] ?>" Plan_level_two="<?php echo $Uvalue['Plan_level_two'] ?>" Plan_level_three="<?php echo $Uvalue['Plan_level_three'] ?>" style="height:100%;cursor: col-resize;float:left;width: 10%;text-align: right;background-color: #8699a4;"><span class="glyphicon glyphicon-step-forward" aria-hidden="true"></span></div>
                                                                    <?php } ?>
                                                                    </div>
                                                                </div>
                                                                <div style="clear: both;"></div>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-backdrop" id="loading" style="display: none;z-index: 1800;background-color:#fff;opacity: 0.9;" >
            <div style="margin-top: 10%;margin-left: 45%;opacity: 0.8;" >
                <img style="width:150px;height: 150px;" src="/images/loading.gif">
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        var minMargin=0,min=0,startX= 0,x= 0,mx= 0,day= 0,width= 0,margin= 0,backward= 0,TabWorkBox= 0,forward= 0,Plan_id= 0,Plan_level_one= 0,Plan_level_two= 0,Plan_level_three= 0,Plan_level_one_width= 0,Plan_level_one_margin=0,Plan_level_two_width= 0,Plan_level_two_margin=0,Plan_level_three_width= 0,Plan_level_three_margin=0;
        var main,tabwork,thisbox;

        function SetMouseDown(type){
            if(type=='TabWorkBox') {
                main.find('#backward').hide();
                main.find('#forward').hide();
                main.find('#TabWorkBox').css({width:'100%'});
            }else if(type=='backward'){
                main.find('#backward').css({width:'100%'});
                main.find('#forward').hide();
                main.find('#TabWorkBox').hide();
            }else if(type=='forward'){
                main.find('#backward').hide();
                main.find('#forward').css({width:'100%'});
                main.find('#TabWorkBox').hide();
            }
        }

        function SetMouseUp(type){
//            if(type=='TabWorkBox') {
            main.find('#backward').show();
            main.find('#backward').css({width:'10%'});
            main.find('#forward').show();
            main.find('#forward').css({width:'10%'});
            main.find('#TabWorkBox').show();
            main.find('#TabWorkBox').css({width:'80%'});
//            }else if(type=='backward'){
//                main.find('#backward').css({width: (backward) + 'px'});
//                main.find('#forward').show();
//                main.find('#TabWorkBox').show();
//            }else if(type=='forward'){
//                main.find('#backward').show();
//                main.find('#forward').css({width: (forward) + 'px'});
//                main.find('#TabWorkBox').show();
//            }
        }

        $(document).on('mousedown', "#TabWork", function(event) {
//            console.log('#TabWork')
            Plan_id=$(this).parent().attr('Plan_id');
            Plan_level_one=$(this).parent().attr('Plan_level_one');
            Plan_level_two=$(this).parent().attr('Plan_level_two');
            Plan_level_three=$(this).parent().attr('Plan_level_three');

            if(typeof(Plan_level_one)!="undefined"){
                Plan_level_one_width=$('div#Plan_id_'+Plan_level_one).css('width');
                Plan_level_one_margin=$('div#Plan_id_'+Plan_level_one).css('margin-left');
            }
            if(typeof(Plan_level_two)!="undefined"){
                Plan_level_two_width=$('div#Plan_id_'+Plan_level_two).css('width');
                Plan_level_two_margin=$('div#Plan_id_'+Plan_level_two).css('margin-left');
            }
            if(typeof(Plan_level_three)!="undefined"){
                Plan_level_three_width=$('div#Plan_id_'+Plan_level_three).css('width');
                Plan_level_three_margin=$('div#Plan_id_'+Plan_level_three).css('margin-left');
            }

            startX = event.pageX-x;
            main=$(this).parent().parent();

            backward=parseInt(main.find('#backward').css('width'));
            TabWorkBox=parseInt(main.find('#TabWorkBox').css('width'));
            forward=parseInt(main.find('#forward').css('width'));

            minMargin=parseInt(main.attr('minMargin'))-40;
            min=parseInt(main.attr('min'));
            margin=parseInt(main.css('margin-left'));
            width=parseInt(main.css('width'));
            event.preventDefault();
            $(document).on('mousemove', mouseMoveMarginWork);
            $(document).on('mouseup', mouseUpMarginWork);
        })

        function mouseMoveMarginWork(event){
            SetMouseDown('TabWorkBox');
            mx = event.pageX - startX;
//            console.log('mouseMoveMarginWork:: '+mx);
            if((margin + mx)>=0){
                main.css({
                    'margin-left': (margin + mx) + 'px'
                });
            }
        }

        function mouseUpMarginWork(event){
//            console.log('mouseUpMarginWork');
            console.log('mouseUpMarginWork:: '+mx);
            if(mx>0) {
                day = Math.ceil(mx / 40);
            }else{
                day = Math.floor(mx / 40);
            }
//            console.log(day*40);
            if((margin + (day * 40))>=0){
                main.css({
                    'margin-left': (margin + (day * 40)) + 'px'
                });
                savePlan(Plan_id,'TabWork',day);
            }else{
                console.log('else');
            }
            SetMouseUp('TabWorkBox');
            $(document).off('mousemove', mouseMoveMarginWork);
            $(document).off('mouseup', mouseUpMarginWork);

            if(Plan_level_one!='' && Plan_level_two!='' && Plan_level_three!=''){
                if((margin+(day * 40))<(parseInt(Plan_level_one_margin))){
                    $('div#Plan_id_'+Plan_level_one).css({
                        'margin-left': (margin + (day * 40)) + 'px',
                        'width': (parseInt(Plan_level_one_width) + (parseInt(Plan_level_one_margin)-(margin+(day * 40)))) + 'px'
                    });
                }else if((margin+(width + (day * 40)))>(parseInt(Plan_level_one_width)+parseInt(Plan_level_one_margin))){
                    $('div#Plan_id_'+Plan_level_one).css({
                        width: (parseInt(Plan_level_one_width)+((margin+(width + (day * 40)))-(parseInt(Plan_level_one_width)+parseInt(Plan_level_one_margin))))+ 'px'
                    });
                }
                if((margin+(day * 40))<(parseInt(Plan_level_two_margin))){
                    $('div#Plan_id_'+Plan_level_two).css({
                        'margin-left': (margin + (day * 40)) + 'px',
                        'width': (parseInt(Plan_level_two_width) + (parseInt(Plan_level_two_margin)-(margin+(day * 40)))) + 'px'
                    });
                }else if((margin+(width + (day * 40)))>(parseInt(Plan_level_two_width)+parseInt(Plan_level_two_margin))){
                    $('div#Plan_id_'+Plan_level_two).css({
                        width: (parseInt(Plan_level_two_width)+((margin+(width + (day * 40)))-(parseInt(Plan_level_two_width)+parseInt(Plan_level_two_margin))))+ 'px'
                    });
                }
                if((margin+(day * 40))<(parseInt(Plan_level_three_margin))){
                    $('div#Plan_id_'+Plan_level_three).css({
                        'margin-left': (margin + (day * 40)) + 'px',
                        'width': (parseInt(Plan_level_three_width) + (parseInt(Plan_level_three_margin)-(margin+(day * 40)))) + 'px'
                    });
                }else if((margin+(width + (day * 40)))>(parseInt(Plan_level_three_width)+parseInt(Plan_level_three_margin))){
                    $('div#Plan_id_'+Plan_level_one).css({
                        width: (parseInt(Plan_level_three_width)+((margin+(width + (day * 40)))-(parseInt(Plan_level_three_width)+parseInt(Plan_level_three_margin))))+ 'px'
                    });
                }
            }else if(Plan_level_one!='' && Plan_level_two!=''){
                if((margin+(day * 40))<(parseInt(Plan_level_one_margin))){
                    $('div#Plan_id_'+Plan_level_one).css({
                        'margin-left': (margin + (day * 40)) + 'px',
                        'width': (parseInt(Plan_level_one_width) + (parseInt(Plan_level_one_margin)-(margin+(day * 40)))) + 'px'
                    });
                }else if((margin+(width + (day * 40)))>(parseInt(Plan_level_one_width)+parseInt(Plan_level_one_margin))){
                    $('div#Plan_id_'+Plan_level_one).css({
                        width: (parseInt(Plan_level_one_width)+((margin+(width + (day * 40)))-(parseInt(Plan_level_one_width)+parseInt(Plan_level_one_margin))))+ 'px'
                    });
                }
                if((margin+(day * 40))<(parseInt(Plan_level_two_margin))){
                    $('div#Plan_id_'+Plan_level_two).css({
                        'margin-left': (margin + (day * 40)) + 'px',
                        'width': (parseInt(Plan_level_two_width) + (parseInt(Plan_level_two_margin)-(margin+(day * 40)))) + 'px'
                    });
                }else if((margin+(width + (day * 40)))>(parseInt(Plan_level_two_width)+parseInt(Plan_level_two_margin))){
                    $('div#Plan_id_'+Plan_level_two).css({
                        width: (parseInt(Plan_level_two_width)+((margin+(width + (day * 40)))-(parseInt(Plan_level_two_width)+parseInt(Plan_level_two_margin))))+ 'px'
                    });
                }
            }else if(Plan_level_one!=''){
//                console.log('Plan_level_one');
                if((margin+(day * 40))<(parseInt(Plan_level_one_margin))){
                    $('div#Plan_id_'+Plan_level_one).css({
                        'margin-left': (margin + (day * 40)) + 'px',
                        'width': (parseInt(Plan_level_one_width) + (parseInt(Plan_level_one_margin)-(margin+(day * 40)))) + 'px'
                    });
                }else if((margin+(width + (day * 40)))>(parseInt(Plan_level_one_width)+parseInt(Plan_level_one_margin))){
                    $('div#Plan_id_'+Plan_level_one).css({
                        width: (parseInt(Plan_level_one_width)+((margin+(width + (day * 40)))-(parseInt(Plan_level_one_width)+parseInt(Plan_level_one_margin))))+ 'px'
                    });
                }
            }

        }

        $(document).on('mousedown', "#forward", function(event) {
            Plan_id=$(this).attr('Plan_id');
            Plan_level_one=$(this).attr('Plan_level_one');
            Plan_level_two=$(this).attr('Plan_level_two');
            Plan_level_three=$(this).attr('Plan_level_three');

            if(typeof(Plan_level_one)!="undefined"){
                Plan_level_one_width=$('div#Plan_id_'+Plan_level_one).css('width');
                Plan_level_one_margin=$('div#Plan_id_'+Plan_level_one).css('margin-left');
            }
            if(typeof(Plan_level_two)!="undefined"){
                Plan_level_two_width=$('div#Plan_id_'+Plan_level_two).css('width');
                Plan_level_two_margin=$('div#Plan_id_'+Plan_level_two).css('margin-left');
            }
            if(typeof(Plan_level_three)!="undefined"){
                Plan_level_three_width=$('div#Plan_id_'+Plan_level_three).css('width');
                Plan_level_three_margin=$('div#Plan_id_'+Plan_level_three).css('margin-left');
            }

            startX = event.pageX-x;
            main=$(this).parent();

            backward=parseInt(main.find('#backward').css('width'));
            TabWorkBox=parseInt(main.find('#TabWorkBox').css('width'));
            forward=parseInt(main.find('#forward').css('width'));

            tabwork=$(this).parent().find('#TabWorkBox');
            minMargin=parseInt(main.attr('minMargin'));
            min=parseInt(main.attr('min'));
            width=parseInt(main.css('width'));
            margin=parseInt(main.css('margin-left'));
            event.preventDefault();
            $(document).on('mousemove', mouseMove);
            $(document).on('mouseup', mouseUp);
        })

        function mouseMove(event) {
            SetMouseDown('forward');
            mx = event.pageX - startX;
            if ((width + mx) > 40 && ((margin+(width+mx))>=(min))){
                main.css({
                    width: (width + mx) + 'px'
                });
                tabwork.css({
                    width: ((width + mx)-26) + 'px'
                })
            }
        }

        function mouseUp(event){
//            console.log('mouseUp');
            day=Math.ceil(mx/40);
            if ((width + (day * 40)) > 40 && ((margin+(width+(day * 40)))>=min)) {
//                console.log('if');
                main.css({
                    width: (width + (day * 40)) + 'px'
                });
                tabwork.css({
                    width: ((width + (day * 40))-26) + 'px'
                })
                savePlan(Plan_id,'forward',day);
            }else{
//                console.log('else');
                main.css({
                    width: (width +day) + 'px'
                });
                tabwork.css({
                    width: ((width +day)-26) + 'px'
                })
            }
            SetMouseUp('forward');
            $(document).off('mousemove', mouseMove);
            $(document).off('mouseup', mouseUp);

            if(Plan_level_one!='' && Plan_level_two!='' && Plan_level_three!=''){
                if((margin+(width + (day * 40)))>(parseInt(Plan_level_one_width)+parseInt(Plan_level_one_margin))){
                    $('div#Plan_id_'+Plan_level_one).css({
                        width: (parseInt(Plan_level_one_width)+((margin+(width + (day * 40)))-(parseInt(Plan_level_one_width)+parseInt(Plan_level_one_margin))))+ 'px'
                    });
                }
                if((margin+(width + (day * 40)))>(parseInt(Plan_level_two_width)+parseInt(Plan_level_two_margin))){
                    $('div#Plan_id_'+Plan_level_two).css({
                        width: (parseInt(Plan_level_two_width)+((margin+(width + (day * 40)))-(parseInt(Plan_level_two_width)+parseInt(Plan_level_two_margin))))+ 'px'
                    });
                }
                if((margin+(width + (day * 40)))>(parseInt(Plan_level_three_width)+parseInt(Plan_level_three_margin))){
                    $('div#Plan_id_'+Plan_level_three).css({
                        width: (parseInt(Plan_level_three_width)+((margin+(width + (day * 40)))-(parseInt(Plan_level_three_width)+parseInt(Plan_level_three_margin))))+ 'px'
                    });
                }
            }else if(Plan_level_one!='' && Plan_level_two!=''){
                if((margin+(width + (day * 40)))>(parseInt(Plan_level_one_width)+parseInt(Plan_level_one_margin))){
                    $('div#Plan_id_'+Plan_level_one).css({
                        width: (parseInt(Plan_level_one_width)+((margin+(width + (day * 40)))-(parseInt(Plan_level_one_width)+parseInt(Plan_level_one_margin))))+ 'px'
                    });
                }
                if((margin+(width + (day * 40)))>(parseInt(Plan_level_two_width)+parseInt(Plan_level_two_margin))){
                    $('div#Plan_id_'+Plan_level_two).css({
                        width: (parseInt(Plan_level_two_width)+((margin+(width + (day * 40)))-(parseInt(Plan_level_two_width)+parseInt(Plan_level_two_margin))))+ 'px'
                    });
                }
            }else if(Plan_level_one!=''){
                if((margin+(width + (day * 40)))>(parseInt(Plan_level_one_width)+parseInt(Plan_level_one_margin))){
                    $('div#Plan_id_'+Plan_level_one).css({
                        width: (parseInt(Plan_level_one_width)+((margin+(width + (day * 40)))-(parseInt(Plan_level_one_width)+parseInt(Plan_level_one_margin))))+ 'px'
                    });
                }
            }

        }

        $(document).on('mousedown', "#backward", function(event) {
//            console.log('mousedownBackward')
            Plan_id=$(this).attr('Plan_id');
            Plan_level_one=$(this).attr('Plan_level_one');
            Plan_level_two=$(this).attr('Plan_level_two');
            Plan_level_three=$(this).attr('Plan_level_three');

            if(typeof(Plan_level_one)!="undefined"){
                Plan_level_one_width=$('div#Plan_id_'+Plan_level_one).css('width');
                Plan_level_one_margin=$('div#Plan_id_'+Plan_level_one).css('margin-left');
            }
            if(typeof(Plan_level_two)!="undefined"){
                Plan_level_two_width=$('div#Plan_id_'+Plan_level_two).css('width');
                Plan_level_two_margin=$('div#Plan_id_'+Plan_level_two).css('margin-left');
            }
            if(typeof(Plan_level_three)!="undefined"){
                Plan_level_three_width=$('div#Plan_id_'+Plan_level_three).css('width');
                Plan_level_three_margin=$('div#Plan_id_'+Plan_level_three).css('margin-left');
            }

            startX = event.pageX-x;
            main=$(this).parent();

            backward=parseInt(main.find('#backward').css('width'));
            TabWorkBox=parseInt(main.find('#TabWorkBox').css('width'));
            forward=parseInt(main.find('#forward').css('width'));

            tabwork=$(this).parent().find('#TabWorkBox');
            minMargin=parseInt(main.attr('minMargin'));
            margin=parseInt(main.css('margin-left'));
            width=parseInt(main.css('width'));
            event.preventDefault();
            $(document).on('mousemove', mouseMoveMargin);
            $(document).on('mouseup', mouseUpMargin);
        })

        function mouseMoveMargin(event){
//            console.log('mouseMoveMargin');
            SetMouseDown('backward');
            mx = event.pageX - startX;
//            console.log(width-(margin+mx));
            if((margin + mx)>=0 && (width - mx)>40) {
                main.css({
                    'margin-left': (margin + mx) + 'px',
                    'width': (width - mx) + 'px'
                });
                tabwork.css({
                    width: ((width - mx)-26) + 'px'
                })
            }else{
                console.log('else');
            }
        }

        function mouseUpMargin(event){
//            console.log('mouseUpMargin')
            day=Math.floor(mx/40);
            if((margin + (day * 40))>=0 && (width - (day * 40))>40) {
                main.css({
                    'margin-left': (margin + (day * 40)) + 'px',
                    'width': (width - (day * 40)) + 'px'
                });
                tabwork.css({
                    width: ((width - (day * 40))-26) + 'px'
                })
                savePlan(Plan_id,'backward',day);
            }else{
                main.css({
                    'margin-left': (margin + day) + 'px',
                    'width': (width - day) + 'px'
                });
                tabwork.css({
                    width: ((width - day)-26) + 'px'
                })
            }
            SetMouseUp('backward');
            $(document).off('mousemove', mouseMoveMargin);
            $(document).off('mouseup', mouseUpMargin);

            if(Plan_level_one!='' && Plan_level_two!='' && Plan_level_three!=''){
                if((margin+(day * 40))<(parseInt(Plan_level_one_margin))){
                    $('div#Plan_id_'+Plan_level_one).css({
                        'margin-left': (margin + (day * 40)) + 'px',
                        'width': (parseInt(Plan_level_one_width) + (parseInt(Plan_level_one_margin)-(margin+(day * 40)))) + 'px'
                    });
                }
                if((margin+(day * 40))<(parseInt(Plan_level_two_margin))){
                    $('div#Plan_id_'+Plan_level_two).css({
                        'margin-left': (margin + (day * 40)) + 'px',
                        'width': (parseInt(Plan_level_two_width) + (parseInt(Plan_level_two_margin)-(margin+(day * 40)))) + 'px'
                    });
                }
                if((margin+(day * 40))<(parseInt(Plan_level_three_margin))){
                    $('div#Plan_id_'+Plan_level_three).css({
                        'margin-left': (margin + (day * 40)) + 'px',
                        'width': (parseInt(Plan_level_three_width) + (parseInt(Plan_level_three_margin)-(margin+(day * 40)))) + 'px'
                    });
                }
            }else if(Plan_level_one!='' && Plan_level_two!=''){
                if((margin+(day * 40))<(parseInt(Plan_level_one_margin))){
                    $('div#Plan_id_'+Plan_level_one).css({
                        'margin-left': (margin + (day * 40)) + 'px',
                        'width': (parseInt(Plan_level_one_width) + (parseInt(Plan_level_one_margin)-(margin+(day * 40)))) + 'px'
                    });
                }
                if((margin+(day * 40))<(parseInt(Plan_level_two_margin))){
                    $('div#Plan_id_'+Plan_level_two).css({
                        'margin-left': (margin + (day * 40)) + 'px',
                        'width': (parseInt(Plan_level_two_width) + (parseInt(Plan_level_two_margin)-(margin+(day * 40)))) + 'px'
                    });
                }
            }else if(Plan_level_one!=''){
                if((margin+(day * 40))<(parseInt(Plan_level_one_margin))){
                    $('div#Plan_id_'+Plan_level_one).css({
                        'margin-left': (margin + (day * 40)) + 'px',
                        'width': (parseInt(Plan_level_one_width) + (parseInt(Plan_level_one_margin)-(margin+(day * 40)))) + 'px'
                    });
                }
            }

        }

        function savePlan(Plan_id,Type,day){
            $('div#loading').show();
            $.post("/plan/updatePlan", {
                Plan_id:Plan_id,
                Type:Type,
                day:day
            }, function (datalist) {
                $('div#loading').hide();
            })
        }
    });
</script>
<!--===============End View Plan Move ============================================================================================-->
<div class="row">
    <div class="col-md-6" style="font-size: 20px;border-bottom: solid 2px;padding-top: 10px;">
        จัดการแผนการการทำงาน
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table"  >
        <!--    <thead>-->
            <tr>
                <th style="border-top: solid 1px #FFFFFF;width: 60%;">หัวข้อ</th>
                <th style="border-top: solid 1px #FFFFFF;width: 5%;">ชั่วโมง/วัน</th>
                <th style="border-top: solid 1px #FFFFFF;width: 15%;text-align: center;"><span style="margin-right: 20px;">เริ่ม</span>-<span style="margin-left: 20px;">ถึง</span></th>
                <th style="border-top: solid 1px #FFFFFF;width: 5%;">สถานะ</th>
                <?php if(Yii::app()->user->type=='admin'){?>
                <th style="text-align:center;border-top: solid 1px #FFFFFF;width: 15%;">เครื่องมือ</th>
                <?php } ?>
            </tr>
        <!--    </thead>-->
            <tbody>
            <?php
            $count=0;
            $countOne=0;
            $countTwo=0;
            $countThree=0;
            foreach($dataPlan as $key=>$value) {
                $count++;
                $countOne=0;
                ?>
                    <?php foreach($value as $Lkey=>$Lvalue){ ?>
                        <?php if($Lkey=='data'){ ?>
                            <tr id="plan_work">
                                <td style="border-bottom: solid 2px #cccccc;padding-bottom: 0px;">
                                    <span style="cursor: pointer;" data-toggle="tooltip" data-placement="top" title="<?php echo $value['data']['Plan_detail']; ?>">
                                        <?php
                                        echo $value['data']['Plan_title'];
                                        ?>
                                    </span>
                                    <?php if($count==1 && (count($dataPlan)-1)>1){ ?>
                                        <span id="<?php echo $value['data']['Plan_id']?>" style="cursor: pointer;" class="glyphicon glyphicon-download" aria-hidden="true"></span>
                                    <?php }elseif($count==count($dataPlan) && (count($dataPlan)-1)>1){ ?>
                                        <span id="<?php echo $value['data']['Plan_id']?>" style="cursor: pointer;" class="glyphicon glyphicon-upload" aria-hidden="true"></span>
                                    <?php }elseif((count($dataPlan)-1)>1){ ?>
                                        <span id="<?php echo $value['data']['Plan_id']?>" style="cursor: pointer;" class="glyphicon glyphicon-download" aria-hidden="true"></span>
                                        <span id="<?php echo $value['data']['Plan_id']?>" style="cursor: pointer;" class="glyphicon glyphicon-upload" aria-hidden="true"></span>
                                    <?php } ?>
                                </td>
                                <td style="border-bottom: solid 2px #cccccc;padding: 8px 0 0 0;text-align: left;">
                                    <?php echo ((!empty($value['data']['Plan_hour']))?$value['data']['Plan_hour']:'0') ?>/<?php echo $value['data']['Plan_count_day']; ?>
                                    <?php
                                        if(false!==Workdetail::getStaffWorkPlan($value['data']['Plan_id'])){
                                        echo '<span data-toggle="tooltip" data-placement="top" title="'.Workdetail::getStaffWorkPlan($value['data']['Plan_id']).'" style="padding-left: 8px;padding-top: 2px;" class="glyphicon glyphicon-user" aria-hidden="true"></span>';
                                    }
                                    ?>
                                </td>
                                <td style="border-bottom: solid 2px #cccccc;padding-bottom: 0px;text-align: center;">
                                    <?php echo (!empty($value['data']['Plan_start_date']))?date('Y-m-d', strtotime($value['data']['Plan_start_date'])):'ไม่กำหนด'; ?>
                                    -
                                    <?php echo (!empty($value['data']['Plan_end_date']))? date('Y-m-d', strtotime($value['data']['Plan_end_date'])):'ไม่กำหนด'; ?>
                                </td>
                                <td style="border-bottom: solid 2px #cccccc;padding-bottom: 0px;"><?php echo $value['data']['Plan_status'] ?></td>
                                <?php if(Yii::app()->user->type=='admin' or (!empty($dataGroupStaff) and $dataGroupStaff->Position=='leader')){?>
                                <td style="text-align: right; border-bottom: solid 2px #cccccc;padding: 8px 0 0 8px;">
                                    <a style="border-radius:2px;font-size: 10px;padding: 1px;" class="btn btn-default" id="viewLogPlan" Plan_id="<?php echo $value['data']['Plan_id'] ?>" data-toggle="modal" data-target="#myModal">รายงาน</a>
                                    <a href="/plan/manage/<?php echo $value['data']['Plan_id'] ?>" style="border-radius:2px;font-size: 10px;padding: 1px;" class="btn btn-warning">แก้ไข</a>
                                    <a href="/plan/delete/<?php echo $value['data']['Plan_id'] ?>" onclick="return confirm('ยืนยันการลบข้อมูล แผนงาน **<?php echo $value['data']['Plan_title'] ?>** นี้หรือไม่')" style="border-radius:2px;font-size: 10px;padding: 1px;" class="btn btn-danger">ลบ</a>
                                    <?php if(false==Plan::checkSubPlan($value['data']['Plan_id'])){ ?>
                                        <button Plan_id="<?php echo $value['data']['Plan_id']; ?>" id="BtnAddStaff" data-toggle="modal" data-target="#formAddStaff" style="border-radius:2px;font-size: 10px;padding: 1px;" class="btn btn-info btn-xs">เพิ่มคน</button>
                                    <?php } ?>
                                    <?php if($value['data']['Plan_status']=='plan' and Yii::app()->user->type=='admin'){ ?>
                                    <a href="/plan/success/<?php echo $value['data']['Plan_id'] ?>" onclick="return confirm('ยืนยันการปิด แผนงาน **<?php echo $value['data']['Plan_title'] ?>** นี้หรือไม่')" style="border-radius:2px;font-size: 10px;padding: 1px;" class="btn btn-success">ปิดงาน</a>
                                    <?php } ?>
                                </td>
                                <?php } ?>
                            </tr>
                        <?php }else{
                        $countOne++;
                        $countTwo=0;
                        ?>
                            <?php foreach($Lvalue as $Rkey=>$Rvalue){ ?>
                                <?php if($Rkey=='data'){ ?>
                                    <tr id="plan_work">
                                        <td style="padding: 0 0 0 30px;">
                                            <span style="cursor: pointer;" data-toggle="tooltip" data-placement="top" title="<?php echo $Lvalue['data']['Plan_detail']; ?>">
                                                <?php
                                                echo $Lvalue['data']['Plan_title'];
                                                ?>
                                            </span>
                                            <?php if($countOne==1 && (count($value)-1)>1){ ?>
                                                <span id="<?php echo $Lvalue['data']['Plan_id']?>" style="cursor: pointer;" class="glyphicon glyphicon-download" aria-hidden="true"></span>
                                            <?php }elseif($countOne==count($value)-1 && (count($value)-1)>1){ ?>
                                                <span id="<?php echo $Lvalue['data']['Plan_id']?>" style="cursor: pointer;" class="glyphicon glyphicon-upload" aria-hidden="true"></span>
                                            <?php }elseif ((count($value)-1)>1){ ?>
                                                <span id="<?php echo $Lvalue['data']['Plan_id']?>" style="cursor: pointer;" class="glyphicon glyphicon-download" aria-hidden="true"></span>
                                                <span id="<?php echo $Lvalue['data']['Plan_id']?>"style="cursor: pointer;" class="glyphicon glyphicon-upload" aria-hidden="true"></span>
                                            <?php } ?>
                                        </td>
                                        <td style="text-align: center;padding: 0 0 0 10px;text-align: left;">
                                            <?php
                                            echo ((!empty($Lvalue['data']['Plan_hour']))?$Lvalue['data']['Plan_hour']:'0') ?>/<?php echo $Lvalue['data']['Plan_count_day'];
                                            if(false!==Workdetail::getStaffWorkPlan($Lvalue['data']['Plan_id'])){
                                                echo '<span data-toggle="tooltip" data-placement="top" title="'.Workdetail::getStaffWorkPlan($Lvalue['data']['Plan_id']).'" style="padding-left: 8px;padding-top: 2px;" class="glyphicon glyphicon-user" aria-hidden="true"></span>';
                                            }
                                            ?></td>
                                        <td style="padding: 0 0 0 0px;text-align: left;text-align: center;">
                                            <?php echo (!empty($Lvalue['data']['Plan_start_date']))?date('Y-m-d', strtotime($Lvalue['data']['Plan_start_date'])):'ไม่กำหนด'; ?>
                                            -
                                            <?php echo (!empty($Lvalue['data']['Plan_end_date']))? date('Y-m-d', strtotime($Lvalue['data']['Plan_end_date'])):'ไม่กำหนด'; ?>
                                        </td>
                                        <td style="padding: 0 0 0 8px;"><?php echo $Lvalue['data']['Plan_status'] ?></td>
                                        <?php if(Yii::app()->user->type=='admin' or (!empty($dataGroupStaff) and $dataGroupStaff->Position=='leader')){?>
                                        <td style="padding: 0 0 0 8px;text-align: right;">
                                            <a style="border-radius:2px;font-size: 10px;padding: 1px;" class="btn btn-default" id="viewLogPlan" Plan_id="<?php echo $Lvalue['data']['Plan_id'] ?>" data-toggle="modal" data-target="#myModal">รายงาน</a>
                                            <a href="/plan/manage/<?php echo $Lvalue['data']['Plan_id'] ?>" style="border-radius:2px;font-size: 10px;padding: 1px;" class="btn btn-warning btn-xs">แก้ไข</a>
                                            <a href="/plan/delete/<?php echo $Lvalue['data']['Plan_id'] ?>" onclick="return confirm('ยืนยันการลบข้อมูล แผนงาน **<?php echo $Lvalue['data']['Plan_title'] ?>** นี้หรือไม่')" style="border-radius:2px;font-size: 10px;padding: 1px;" class="btn btn-danger btn-xs">ลบ</a>
                                            <?php if(false==Plan::checkSubPlan($Lvalue['data']['Plan_id'])){ ?>
                                                <button Plan_id="<?php echo $Lvalue['data']['Plan_id']; ?>" id="BtnAddStaff" data-toggle="modal" data-target="#formAddStaff" style="border-radius:2px;font-size: 10px;padding: 1px;" class="btn btn-info btn-xs">เพิ่มคน</button>
                                            <?php } ?>
                                            <?php if($Lvalue['data']['Plan_status']=='plan' and Yii::app()->user->type=='admin'){ ?>
                                            <a href="/plan/success/<?php echo $Lvalue['data']['Plan_id'] ?>" onclick="return confirm('ยืนยันการปิด แผนงาน **<?php echo $Lvalue['data']['Plan_title'] ?>** นี้หรือไม่')" style="border-radius:2px;font-size: 10px;padding: 1px;" class="btn btn-success">ปิดงาน</a>
                                            <?php } ?>
                                        </td>
                                        <?php } ?>
                                    </tr>
                                <?php }else{
                                $countTwo++;
                                $countThree=0;
                                ?>
                                    <?php foreach($Rvalue as $Tkey=>$Tvalue){ ?>
                                        <?php if($Tkey=='data'){ ?>
                                            <tr id="plan_work">
                                                <td style="padding: 0 0 0 50px;">
                                                    <span style="cursor: pointer;" data-toggle="tooltip" data-placement="top" title="<?php echo $Rvalue['data']['Plan_detail']; ?>">
                                                        <?php
                                                        echo $Rvalue['data']['Plan_title'];
                                                        if(false!==Workdetail::getStaffWorkPlan($Rvalue['data']['Plan_id'])){
                                                            echo '<span data-toggle="tooltip" data-placement="top" title="'.Workdetail::getStaffWorkPlan($Rvalue['data']['Plan_id']).'" style="padding-left: 8px;padding-top: 2px;" class="glyphicon glyphicon-user" aria-hidden="true"></span>';
                                                        }
                                                        ?>
                                                    </span>
                                                    <?php if($countTwo==1 && (count($Lvalue)-1)>1){ ?>
                                                        <span id="<?php echo $Rvalue['data']['Plan_id']?>" style="cursor: pointer;" class="glyphicon glyphicon-download" aria-hidden="true"></span>
                                                    <?php }elseif(($countTwo==count($Lvalue)-1) && (count($Lvalue)-1)>1){ ?>
                                                        <span id="<?php echo $Rvalue['data']['Plan_id']?>" style="cursor: pointer;" class="glyphicon glyphicon-upload" aria-hidden="true"></span>
                                                    <?php }elseif((count($Lvalue)-1)>1){ ?>
                                                        <span id="<?php echo $Rvalue['data']['Plan_id']?>" style="cursor: pointer;" class="glyphicon glyphicon-download" aria-hidden="true"></span>
                                                        <span id="<?php echo $Rvalue['data']['Plan_id']?>" style="cursor: pointer;" class="glyphicon glyphicon-upload" aria-hidden="true"></span>
                                                    <?php } ?>
                                                </td>
                                                <td style="text-align: left;padding: 0 0 0 20px;">
                                                    <?php
                                                    echo ((!empty($Rvalue['data']['Plan_hour']))?$Rvalue['data']['Plan_hour']:'0') ?>/<?php echo $Rvalue['data']['Plan_count_day'];
                                                    if(false!==Workdetail::getStaffWorkPlan($Rvalue['data']['Plan_id'])){
                                                        echo '<span data-toggle="tooltip" data-placement="top" title="'.Workdetail::getStaffWorkPlan($Rvalue['data']['Plan_id']).'" style="padding-left: 8px;padding-top: 2px;" class="glyphicon glyphicon-user" aria-hidden="true"></span>';
                                                    }
                                                    ?>
                                                </td>
                                                <td style="padding: 0 0 0 0;text-align: center;">
                                                    <?php echo (!empty($Rvalue['data']['Plan_start_date']))?date('Y-m-d', strtotime($Rvalue['data']['Plan_start_date'])):'ไม่กำหนด'; ?>
                                                    -
                                                    <?php echo (!empty($Rvalue['data']['Plan_end_date']))?date('Y-m-d', strtotime($Rvalue['data']['Plan_end_date'])):'ไม่กำหนด'; ?>
                                                </td>
                                                <td style="padding: 0 0 0 8px;"><?php echo $Rvalue['data']['Plan_status'] ?></td>
                                                <?php if(Yii::app()->user->type=='admin' or (!empty($dataGroupStaff) and $dataGroupStaff->Position=='leader')){?>
                                                <td style="padding: 0 0 0 8px;text-align: right;">
                                                    <a style="border-radius:2px;font-size: 10px;padding: 1px;" class="btn btn-default" id="viewLogPlan" Plan_id="<?php echo $Rvalue['data']['Plan_id'] ?>" data-toggle="modal" data-target="#myModal">รายงาน</a>
                                                    <a href="/plan/manage/<?php echo $Rvalue['data']['Plan_id'] ?>" style="border-radius:2px;font-size: 10px;padding: 1px;" class="btn btn-warning btn-xs">แก้ไข</a>
                                                    <a href="/plan/delete/<?php echo $Rvalue['data']['Plan_id'] ?>" onclick="return confirm('ยืนยันการลบข้อมูล แผนงาน **<?php echo $Rvalue['data']['Plan_title'] ?>** นี้หรือไม่')" style="border-radius:2px;font-size: 10px;padding: 1px;" class="btn btn-danger btn-xs">ลบ</a>
                                                    <?php if(false==Plan::checkSubPlan($Rvalue['data']['Plan_id'])){ ?>
                                                        <button Plan_id="<?php echo $Rvalue['data']['Plan_id']; ?>" id="BtnAddStaff" data-toggle="modal" data-target="#formAddStaff" style="border-radius:2px;font-size: 10px;padding: 1px;" class="btn btn-info btn-xs">เพิ่มคน</button>
                                                    <?php } ?>
                                                    <?php if($Rvalue['data']['Plan_status']=='plan' and Yii::app()->user->type=='admin'){ ?>
                                                    <a href="/plan/success/<?php echo $Rvalue['data']['Plan_id'] ?>" onclick="return confirm('ยืนยันการปิด แผนงาน **<?php echo $Rvalue['data']['Plan_title'] ?>** นี้หรือไม่')" style="border-radius:2px;font-size: 10px;padding: 1px;" class="btn btn-success">ปิดงาน</a>
                                                    <?php } ?>
                                                </td>
                                                <?php } ?>
                                            </tr>
                                        <?php }else{
                                        $countThree++;
                                        ?>
                                            <?php foreach($Tvalue as $Ukey=>$Uvalue){ ?>
                                                <?php if($Ukey=='data'){ ?>
                                                    <tr id="plan_work">
                                                        <td style="padding: 0 0 0 70px;">
                                                            <span style="cursor: pointer;" data-toggle="tooltip" data-placement="top" title="<?php echo $Tvalue['data']['Plan_detail']; ?>">
                                                                <?php
                                                                echo $Tvalue['data']['Plan_title'];
                                                                if(false!==Workdetail::getStaffWorkPlan($Tvalue['data']['Plan_id'])){
                                                                    echo '<span data-toggle="tooltip" data-placement="top" title="'.Workdetail::getStaffWorkPlan($Tvalue['data']['Plan_id']).'" style="padding-left: 8px;padding-top: 2px;" class="glyphicon glyphicon-user" aria-hidden="true"></span>';
                                                                }
                                                                ?>
                                                            </span>
                                                            <?php if($countThree==1 && (count($Rvalue)-1)>1){ ?>
                                                                <span id="<?php echo $Tvalue['data']['Plan_id']?>" style="cursor: pointer;" class="glyphicon glyphicon-download" aria-hidden="true"></span>
                                                            <?php }elseif(($countThree==count($Rvalue)-1) && (count($Rvalue)-1)>1){ ?>
                                                                <span id="<?php echo $Tvalue['data']['Plan_id']?>" style="cursor: pointer;" class="glyphicon glyphicon-upload" aria-hidden="true"></span>
                                                            <?php }elseif((count($Rvalue)-1)>1){ ?>
                                                                <span id="<?php echo $Tvalue['data']['Plan_id']?>" style="cursor: pointer;" class="glyphicon glyphicon-download" aria-hidden="true"></span>
                                                                <span id="<?php echo $Tvalue['data']['Plan_id']?>" style="cursor: pointer;" class="glyphicon glyphicon-upload" aria-hidden="true"></span>
                                                            <?php } ?>
                                                        </td>
                                                        <td style="text-align: left;padding: 0 0 0 30px ;">
                                                            <?php
                                                            echo ((!empty($Tvalue['data']['Plan_hour']))?$Tvalue['data']['Plan_hour']:'0'); ?>/<?php echo $Tvalue['data']['Plan_count_day'];
                                                            if(false!==Workdetail::getStaffWorkPlan($Tvalue['data']['Plan_id'])){
                                                                echo '<span data-toggle="tooltip" data-placement="top" title="'.Workdetail::getStaffWorkPlan($Tvalue['data']['Plan_id']).'" style="padding-left: 8px;padding-top: 2px;" class="glyphicon glyphicon-user" aria-hidden="true"></span>';
                                                            }
                                                            ?>
                                                        </td>
                                                        <td style="padding: 0 0 0 0px;text-align: center;">
                                                            <?php echo (!empty($Tvalue['data']['Plan_start_date']))?date('Y-m-d', strtotime($Tvalue['data']['Plan_start_date'])):'ไม่กำหนด'; ?>
                                                            -
                                                            <?php echo (!empty($Tvalue['data']['Plan_end_date']))?date('Y-m-d', strtotime($Tvalue['data']['Plan_end_date'])):'ไม่กำหนด'; ?>
                                                        </td>
                                                        <td style="padding: 0 0 0 8px;"><?php echo $Tvalue['data']['Plan_status'] ?></td>
                                                        <?php if(Yii::app()->user->type=='admin' or (!empty($dataGroupStaff) and $dataGroupStaff->Position=='leader')){?>
                                                        <td style="padding: 0 0 0 8px;text-align: right;">
                                                            <a style="border-radius:2px;font-size: 10px;padding: 1px;" class="btn btn-default" id="viewLogPlan" Plan_id="<?php echo $Tvalue['data']['Plan_id'] ?>" data-toggle="modal" data-target="#myModal">รายงาน</a>
                                                            <a href="/plan/manage/<?php echo $Tvalue['data']['Plan_id'] ?>" style="border-radius:2px;font-size: 10px;padding: 1px;" class="btn btn-warning btn-xs">แก้ไข</a>
                                                            <a href="/plan/delete/<?php echo $Tvalue['data']['Plan_id'] ?>" onclick="return confirm('ยืนยันการลบ แผนงาน **<?php echo $Tvalue['data']['Plan_title'] ?>** นี้หรือไม่')" style="border-radius:2px;font-size: 10px;padding: 1px;" class="btn btn-danger btn-xs">ลบ</a>
                                                            <?php if(false==Plan::checkSubPlan($Tvalue['data']['Plan_id'])){ ?>
                                                                <button Plan_id="<?php echo $Tvalue['data']['Plan_id']; ?>" id="BtnAddStaff" data-toggle="modal" data-target="#formAddStaff" style="border-radius:2px;font-size: 10px;padding: 1px;" class="btn btn-info btn-xs">เพิ่มคน</button>
                                                            <?php } ?>
                                                            <?php if($Tvalue['data']['Plan_status']=='plan' and Yii::app()->user->type=='admin'){ ?>
                                                            <a href="/plan/success/<?php echo $Tvalue['data']['Plan_id'] ?>" onclick="return confirm('ยืนยันการปิด แผนงาน **<?php echo $Tvalue['data']['Plan_title'] ?>** นี้หรือไม่')" style="border-radius:2px;font-size: 10px;padding: 1px;" class="btn btn-success">ปิดงาน</a>
                                                        <?php } ?>
                                                        </td>
                                                        <?php } ?>
                                                    </tr>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<!-- Modal -->
<div style="" class="modal fade" id="formAddStaff" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">จัดการพนักงานในแผน</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6"></div>
                    <div class="col-md-6">
                        <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'formAddPlanStaff',
                            'htmlOptions' => array('enctype' => 'multipart/form-data'),
                            'enableAjaxValidation' => true,
                            'clientOptions' => array(
                                'validateOnSubmit' => true
//                               'validateOnChange' => true,
//                               'validateOnType' => true,
                            ),
                            'htmlOptions' => array(
                                'class' => 'form form-inline',
                                'style'=>'margin-bottom: 20px;'
                            ),
                        ));
                        echo $form->hiddenField($PlanAssign, 'Plan_id');
                        ?>
                        <div style="" class="form-group">
                            <?php echo $form->error($PlanAssign, 'UserName',array('style'=>'color:#FF0000;')); ?>
                            <?php echo $form->dropDownList($PlanAssign,'UserName',CHtml::listData(Staff::model()->findAllbySql("select staff.UserName,CONCAT(staff.Name,' ',staff.LastName) as Name from groupstaff INNER JOIN staff ON staff.UserName=groupstaff.UserName where Project_id=".$model->Project_id),'UserName', 'Name'), array(
                                'empty'=>'เลือกพนักงาน',
                                'class' => 'form-control'
                            )) ?>

                        </div>
                        <?php echo CHtml::submitButton(Yii::t('main', 'เพิ่ม'), array('id' => 'addform', 'class' => 'btn btn-primary')); ?>
                        <?php $this->endWidget(); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>ชื่อแผน</th>
                                <th>ชื่อนักงาน</th>
                                <th>จัดการ</th>
                            </tr>
                            </thead>
                            <tbody id="ListPlanStaff">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <!--                <button type="button" class="btn btn-primary">Save changes</button>-->
            </div>
        </div>
    </div>
</div>
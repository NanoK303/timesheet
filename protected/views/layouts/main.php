<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print">
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css">

	<?php
//	require 'D:/projectX3/timesheet/protected/extensions/Carbon/src/Carbon/Carbon.php';
	$cs = Yii::app()->clientScript; //respontive-style
	$cs->registerCssFile('/css/bootstrap/css/bootstrap.css');
	$cs->registerCssFile('/css/font-awesome/css/font-awesome.min.css');

	$cs->registerCoreScript('jquery');
	$cs->registerScriptFile('/css/bootstrap/js/bootstrap.min.js');
	$cs->registerScriptFile('/js/index.js');
	$cs->registerScriptFile('/js/jquery-ui-1.9.1.custom/jquery-ui-1.9.1.custom/js/jquery-ui-1.9.1.custom.min.js');
	$cs->registerCssFile('/js/jquery-ui-1.9.1.custom/jquery-ui-1.9.1.custom/css/ui-lightness/jquery-ui-1.9.1.custom.min.css');
//	$cs->registerScriptFile('/js/jQuery-Timepicker-Addon/localization/jquery-ui-timepicker-el.js');
//	$cs->registerScriptFile('/js/jQuery-Timepicker-Addon/jquery-ui-sliderAccess.js');
//	$cs->registerScriptFile('/js/jQuery-Timepicker-Addon/jquery-ui-timepicker-addon.js');
//	$cs->registerCssFile('/js/jQuery-Timepicker-Addon/jquery-ui-timepicker-addon.css');
	?>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
<div class="container" id="page" style="padding: 0px;">

<!--	<div id="header" style="padding: 0px;">-->
<!--		<div id="logo">--><?php //echo CHtml::encode(Yii::app()->name); ?><!--</div>-->
<!--	</div><!-- header -->

	<nav class="navbar navbar-default" role="navigation" style="border-top:solid 0px;border-left:solid 0px;border-right:solid 0px;border-bottom: solid 1px #E7E7E7;">
		<a class="navbar-brand" href="/site/index"><?php echo CHtml::encode(Yii::app()->name); ?></a>
		<ul class="nav navbar-nav navbar-right" style="margin: 0px;">
			<?php if (Yii::app()->user->isGuest) { ?>
				<li style="padding-right: 5px;">
					<!--<button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="left" title="Tooltip on left">Tooltip on left</button>-->
					<a style="float: left;" href="/site/login" >เข้าระบบ</a>
					<a style="float: left;" href="/site/register" >สมัครสมาชิก</a>
				</li>
			<?php } else { ?>
				<?php if(Yii::app()->user->type=='admin'){?>
<!--					<li><a href="/plan/manage" type="button" class="btn btn-default navbar-btn" style="cursor: pointer;padding: 6px;margin-top: 8px;color: #333;margin-left: 2px;">จัดการแผนงาน</a></li>-->
                    <li><a href="/responsible/manage" type="button" class="btn btn-default navbar-btn" style="cursor: pointer;padding: 6px;margin-top: 8px;color: #333;margin-left: 2px;">จัดการรับผิดชอบ</a></li>
                    <li><a href="/holiday/manage" type="button" class="btn btn-default navbar-btn" style="cursor: pointer;padding: 6px;margin-top: 8px;color: #333;margin-left: 2px;">จัดการวันหยุด</a></li>
					<li><a href="/staff/managestaff" type="button" class="btn btn-default"  style="cursor: pointer;padding: 6px;margin-top: 8px;color: #333;margin-left: 2px;">จัดการพนักงาน</a></li>
                    <?php if(empty(Yii::app()->user->SubOrganizer_id)){ ?>
<!--                        <li><a href="/staff/manageOrganizer" type="button" class="btn btn-default"  style="cursor: pointer;padding: 6px;margin-top: 8px;color: #333;margin-left: 2px;">จัดกลุ่มพนักงาน</a></li>-->
                    <?php } ?>
				<?php } ?>
                <li><a href="/project/manage" type="button" class="btn btn-default navbar-btn" style="cursor: pointer;padding: 6px;margin-top: 8px;color: #333;margin-left: 2px;">จัดการโครงการ</a></li>
				<li class="dropdown">
<!--					<img ng-click="changephoto()" style="float: left; height: 40px;width: 40px; margin: 5px 5px 5px 0px;" src="" class="img-thumbnail">-->
					<a style="font-weight: bold;font-size: 16px; float: left;" href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo Yii::app()->user->id ?><b class="caret"></b></a>
					<ul class="dropdown-menu" >
<!--						<li><button data-toggle="modal" data-target="#myModalEditStaff">แก้ไขข้อมูลพนักงาน</button></li>-->
                        <li><a data-toggle="modal" data-target="#myModalEditStaff" ><span class="glyphicon glyphicon-user"></span> แก้ไขข้อมูล</a></li>
						<?php if(Yii::app()->user->type!=='admin'){?>

<!--						--><?php //}else{ ?>
						<li><a ><span class="glyphicon glyphicon-list-alt"></span> รายงานชั่วโมงทำงาน</a></li>
						<?php } ?>
						<li class="divider"></li>
						<li><a href="/site/logout" ><span class="glyphicon glyphicon-off"></span> ออกระบบ</a></li>
					</ul>
				</li>

			<?php } ?>
		</ul>
	</nav>

	<?php
	foreach(Yii::app()->user->getFlashes() as $key => $message) {
		echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
	}
	?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>

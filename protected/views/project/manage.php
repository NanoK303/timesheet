<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>
<div class="row">
    <?php if(Yii::app()->user->type=='admin'){?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'formAddProduct',
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
    'enableAjaxValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'validateOnType' => true,
    ),
    'htmlOptions' => array(
        'class' => 'form form-inline right',
    ),
));
?>
<div class="form-group">
    <label for="exampleInputName2">บริษัท</label>
    <?php
        echo $form->dropDownList($project,'Company_id', CHtml::listData($company, 'Company_id', 'CompanyName'), array('empty'=>'กรุณาเลือก','class' => 'form-control'))
    ?>
    <div class="form">
        <?php echo $form->error($project,'Company_id'); ?>
    </div>
</div>
<div class="form-group">
    <label for="exampleInputName2">ชื่อ</label>
    <?php echo $form->textfield($project, 'Project_name', array('class' => 'form-control')); ?>
    <div class="form">
        <?php echo $form->error($project, 'Project_name'); ?>
    </div>
</div>
<div class="form-group">
    <label for="exampleInputEmail2">ชั่วโมง</label>
    <?php echo $form->textfield($project, 'MaxTime', array('class' => 'form-control','style'=>'width:100px')); ?>
    <div class="form">
        <?php echo $form->error($project, 'MaxTime'); ?>
    </div>
</div>
    <div class="form-group">
        <label for="exampleInputEmail2">ประเภท</label>
            <?php echo $form->dropDownList($project,'Public', array('Y'=>'สาธารณะ','N'=>'ส่วนตัว'), array('empty'=>'เลือกประเภท','class' => 'form-control')) ?>
        <div class="form">
            <?php echo $form->error($project, 'Public'); ?>
        </div>
    </div>
<div class="form-group">
    <?php echo CHtml::submitButton(Yii::t('main', 'เพิ่ม'), array('id' => 'addform', 'class' => 'btn btn-primary')); ?>
</div>
<?php $this->endWidget(); ?>
    <?php } ?>
<div style="clear: both"></div>
<table class="table table-bordered" style="margin-top: 10px;">
    <thead>
    <tr>
        <th>#</th>
        <th>ชื่อโครงการ</th>
        <th>ชั่วโมงทำงาน</th>
        <th>ใช้ไป</th>
        <th>สถานะ</th>
        <th>ประเภท</th>
        <th>ผู้รับผิดชอบ</th>
        <th>จัดการ</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($dataProject as $key=>$value){ ?>
    <?php if(Yii::app()->user->type=='admin' || (Yii::app()->user->type!='admin' && $value['Public']=='Y'  || ($value['Public']=='N' && Groupstaff::CheckGroupProject(Yii::app()->user->id,$value['Project_id'])))){?>
        <tr class="viewProject" id="viewProject_<?php echo $value['Project_id'] ?>">
            <th scope="row"><?php echo $key+1 ?></th>
            <td id="ProjectName_<?php echo $value['Project_id'] ?>"><?php echo $value['Project_name'] ?></td>
            <td id="MaxTime_<?php echo $value['Project_id'] ?>"><?php echo $value['MaxTime'] ?></td>
            <td><?php echo Workdetail::GetTimeWork(Workdetail::SumHourProject($value['Project_id']))?></td>
            <td>
                <?php echo ($value['Remove']=='N')? 'เปิด':'ปิด' ?>
                <?php if(Yii::app()->user->type=='admin'){?>
                <?php if($value['Remove']=='C'){ ?>
                    <button project_id="<?php echo $value['Project_id'] ?>" id="btnSelectOpen" class="btn btn-success btn-xs">เปิด</button>
                <?php }else{ ?>
                    <button project_id="<?php echo $value['Project_id'] ?>" id="btnSelectClose" class="btn btn-danger btn-xs">ปิด</button>
                <?php }?>
                <?php } ?>
            </td>
            <td>
                <?php echo ($value['Public']=='Y')?'สาธารณะ':'ส่วนตัว' ?>
                <?php if(Yii::app()->user->type=='admin'){?>
                <button id="btnChangePublic" project_id="<?php echo $value['Project_id'] ?>" class="btn btn-info btn-xs">เปลิ่ยน</button>
                <?php } ?>
            </td>
            <td id="ListStaff_<?php echo $value['Project_id'] ?>">
<!--                --><?php //echo count(Groupstaff::GetGroupStaffByProjectId($value['Project_id'])); ?>
                <?php if(count(Groupstaff::GetGroupStaffByProjectId($value['Project_id']))>0){ ?>
                     <?php foreach(Groupstaff::GetGroupStaffByProjectId($value['Project_id']) as $Staffkey=>$Staffvalue){ ?>
                            <?php if($Staffvalue->Remove=='N'){ ?>
                                <span id="spanStaffProject" position="<?php echo $Staffvalue->Position ?>" groupstaff_id="<?php echo $Staffvalue->groupstaff_id ?>"  class="label label-success" style="cursor: pointer;padding-right: 17px;">
                                <?php if($Staffvalue->Position=='leader'){ ?>
                                    <span id="spanglyphicon_<?php echo $Staffvalue->groupstaff_id ?>" data-toggle='tooltip' data-placement='top' title='leader' class="glyphicon glyphicon-king" aria-hidden="true"></span>
                                <?php }elseif($Staffvalue->Position=='programmer'){ ?>
                                    <span id="spanglyphicon_<?php echo $Staffvalue->groupstaff_id ?>" data-toggle='tooltip' data-placement='top' title='programmer' class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                <?php }elseif($Staffvalue->Position=='design'){ ?>
                                    <span id="spanglyphicon_<?php echo $Staffvalue->groupstaff_id ?>" data-toggle='tooltip' data-placement='top' title='design' class="glyphicon glyphicon-camera" aria-hidden="true"></span>
                                <?php }elseif($Staffvalue->Position=='content'){ ?>
                                    <span id="spanglyphicon_<?php echo $Staffvalue->groupstaff_id ?>" data-toggle='tooltip' data-placement='top' title='content' class="glyphicon glyphicon-open-file" aria-hidden="true"></span>
                                <?php } ?>
                                  <?php echo Staff::GetNickName($Staffvalue->UserName) ?>
                                <a id="btnRemoveStaffProject" UserName="<?php echo $Staffvalue->UserName ?>" ProjectId="<?php echo $value['Project_id'] ?>" style="z-index: 20;position: absolute;margin-left: 2px;margin-top: 1px;" type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                </a>
                                </span>
                                </br>
                            <?php } ?>
                     <?php } ?>
                <?php } ?>
            </td>
            <td>
                <div id="mainBtn_<?php echo $value['Project_id'] ?>" class="mainBtn" style="display: block;">
                    <a href="/plan/manage/Project_id/<?php echo $value['Project_id'] ?>" id="btnSelectAddHour" class="btn btn-success">จัดการ</a>
                    <a href="/project/report/Project_id/<?php echo $value['Project_id'] ?>" class="btn btn-default">โครงการ</a>
                    <?php if(Yii::app()->user->type=='admin'){?>
                    <button project_id="<?php echo $value['Project_id'] ?>" id="btnSelectAddHour" class="btn btn-info">เพิ่มชั่วโมง</button>
                    <button project_id="<?php echo $value['Project_id'] ?>" id="btnSelectAddStaff" class="btn btn-info" >เพิ่มพนักงาน</button>
                    <button project_id="<?php echo $value['Project_id'] ?>" id="btnSelectEditProject" class="btn btn-primary">แก้ไข</button>
                    <button project_id="<?php echo $value['Project_id'] ?>" id="btnSelectDelete" class="btn btn-danger">ลบ</button>
                    <?php } ?>
                </div>

                <div id="addStaff_<?php echo $value['Project_id'] ?>" class="addStaff" style="display: none;">
                    <div style="float:left;width:50%;">
                        <select id="selectUserName_<?php echo $value['Project_id'] ?>" class="form-control">
                            <option  value="">เลือกพนักงาน</option>
                            <?php
                            foreach($dateStaff as $Staffkey=>$Staffvalue){
                                if(!Groupstaff::CheckGroupProject($Staffvalue['UserName'],$value['Project_id'])){ ?>
                                <option value="<?php echo $Staffvalue['UserName'] ?>"><?php echo $Staffvalue['Name'] ?> <?php echo $Staffvalue['LastName'] ?>(<?php echo $Staffvalue['Nickname'] ?>)</option>
                            <?php }} ?>
                        </select>
                    </div>
                    <div style="float:left;width:50%;padding-left: 5px;">
                        <button class="btn btn-primary" project_id="<?php echo $value['Project_id'] ?>" id="btnAddStaff">เพิ่ม</button>
                        <button class="btn btn-default" project_id="<?php echo $value['Project_id'] ?>" id="btnClose">กลับ</button>
                    </div>
                </div>

                <div id="addHouse_<?php echo $value['Project_id'] ?>" class="addHouse" style="display: none;">
                    <div style="float:left;width:50%;">
                        <input id="inputProductId_<?php echo $value['Project_id'] ?>" type="hidden"  class="form-control" />
                        <input id="inputValue_<?php echo $value['Project_id'] ?>" type="text"  class="form-control" />
                    </div>
                    <div style="float:left;width:50%;padding-left: 5px;">
                        <button class="btn btn-primary" project_id="<?php echo $value['Project_id'] ?>" id="btnAddHour">เพิ่ม</button>
                        <button class="btn btn-default" project_id="<?php echo $value['Project_id'] ?>" id="btnClose">กลับ</button>
                    </div>
                </div>

                <div id="editNameProject_<?php echo $value['Project_id'] ?>" class="editNameProject" style="display: none;">
                    <div style="float:left;width:50%;">
                        <input id="inputEditProductId_<?php echo $value['Project_id'] ?>" type="hidden"  class="form-control" />
                        <input id="inputName_<?php echo $value['Project_id'] ?>" type="text"  class="form-control" />
                    </div>
                    <div style="float:left;width:50%;padding-left: 5px;">
                        <button class="btn btn-primary" project_id="<?php echo $value['Project_id'] ?>" id="btnEditNameProject">เพิ่ม</button>
                        <button class="btn btn-default" project_id="<?php echo $value['Project_id'] ?>" id="btnClose">กลับ</button>
                    </div>
                </div>
            </td>
        </tr>
    <?php }?>
    <?php } ?>
    </tbody>
</table>
</div>

<script>
    $(document).ready(function(){
        $(document).on('click', "span#spanStaffProject", function() {
//            alert('123456');
//            var groupstaff_id=$(this).attr('groupstaff_id');
            $('#myModal').modal('show');
            $('select#select_Position').val($(this).attr('position'))
            $('input#input_groupstaff_id').val($(this).attr('groupstaff_id'));
        })
    })
</script>

<div style="" class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <form method="post" class="form-inline">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">เปลิ่ยนตำแหน่งในโครงการ</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="input_groupstaff_id" name="Position[groupstaff_id]">
                <div class="form-group">
                    <label for="exampleInputName2">ตำแหน่ง:</label>
                    <select id="select_Position" name="Position[Position]" class="form-control">
                        <option value="">เลือก</option>
                        <option value="leader">leader</option>
                        <option value="programmer">programmer</option>
                        <option value="design">design</option>
                        <option value="content">content</option>
                    </select>
                </div>

            </div>
            <div class="modal-footer">
                <a type="button" class="btn btn-default" data-dismiss="modal">ปิด</a>
                <input type="submit" class="btn btn-primary" value="บันทึก" />
            </div>
        </div>
        </form>
    </div>
</div>
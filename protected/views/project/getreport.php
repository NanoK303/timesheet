<?php
$this->layout=false;
?>
<table class="table table-bordered">
    <thead>
    <tr>
        <th style="width: 30%;border-bottom-color: #080808;">ชื่องาน</th>
        <?php for ($i = 1; $i <= Workdetail::checkNumberOfDays($yearNow,$monthNow); $i++) { ?>
            <th style="width:20px;border-bottom-color: #080808;"><?php echo $i; ?></th>
        <?php } ?>
        <th style="border-bottom-color: #080808;text-align: center;">วัน</th>
        <th style="border-bottom-color: #080808;text-align: center;">ชั่วโมงรวม</th>
        <th style="border-bottom-color: #080808;text-align: center;">ชั่วโมง(%)</th>
    </tr>
    </thead>
    <tbody id="table-view">
<?php
foreach($dataPlan as $key=>$value) {
    $sum=0;
    ?>
    <tr id="plan_work">
        <td style="font-weight: bolder;padding: 0px;">
            <span style="cursor: pointer;" class="glyphicon glyphicon-eye-open" aria-hidden="true" level="Plan_level_one"  id="<?php echo $value['data']['Plan_id'] ?>"></span>
            <?php echo $value['data']['Plan_title'] ?>
            <?php
            if(false!==Workdetail::getStaffWorkPlan($value['data']['Plan_id'])){
                echo '<span data-toggle="tooltip" data-placement="top" title="'.Workdetail::getStaffWorkPlan($value['data']['Plan_id']).'" style="padding-left: 8px;padding-top: 2px;" class="glyphicon glyphicon-user" aria-hidden="true"></span>';
            }
            ?>
        </td>
        <?php for ($i = 1; $i <= Workdetail::checkNumberOfDays($yearNow,$monthNow); $i++) { ?>
            <td style="padding: 0px;" class="<?php echo (!empty($value['day'][$i]['status'])?$value['day'][$i]['status']:'') ?>">
                <?php
                if(!empty($value['day'][$i]['icon'])){
                    echo '<span data-toggle="tooltip" data-placement="top" title="'.$value['day'][$i]['status'].'" style="padding-left: 8px;padding-top: 2px;" class="'.$value['day'][$i]['icon'].'" aria-hidden="true"></span>';
                }
                ?>
            </td>
        <?php } ?>
        <td style="text-align: center;padding: 0px;"><span data-toggle="tooltip" data-placement="top" title="เริ่ม <?php echo str_replace(' 00:00:00','',$value['data']['Plan_start_date']) ?> ถึง <?php echo str_replace('00:00:00','',$value['data']['Plan_end_date']) ?>" ><?php echo $value['data']['Plan_count_day'] ?></span></td>
        <td style="text-align: center;padding: 0px;font-weight: bolder;"><?php echo (Plan::CheckWorkPlanUser($value['data']['Plan_id']))."/".$value['data']['Plan_hour'] ?><?php echo ($value['data']['Plan_status']=='success'?'/C':'')?></td>
        <td style="text-align: center;padding: 0px;">
            <?php
            if($value['data']['Plan_status']=='success'){
                echo '100';
            }else {
                if(Plan::CheckWorkPlanUser($value['data']['Plan_id'])>0) {
                    echo ceil(((Plan::CheckWorkPlanUser($value['data']['Plan_id']) / $value['data']['Plan_hour']) * 100));
                }else{
                    echo '0';
                }
            }
            ?>%
        </td>
    </tr>
    <?php foreach($value as $keyS=>$valueS){
        if($keyS!=='data' && $keyS!=='day') {
            $sum=0;
            ?>
            <tr id="plan_work" Plan_level_one="<?php echo $valueS['data']['Plan_level_one']?>">
                <td style="padding: 0px 0px 0px 20px;">
                    <span style="cursor: pointer;" class="glyphicon glyphicon-eye-open" aria-hidden="true" level="Plan_level_two"  id="<?php echo $valueS['data']['Plan_id'] ?>"></span>
                    <?php echo $valueS['data']['Plan_title'] ?>
                    <?php
                    if(false!==Workdetail::getStaffWorkPlan($valueS['data']['Plan_id'])){
                        echo '<span data-toggle="tooltip" data-placement="top" title="'.Workdetail::getStaffWorkPlan($valueS['data']['Plan_id']).'" style="padding-left: 8px;padding-top: 2px;" class="glyphicon glyphicon-user" aria-hidden="true"></span>';
                    }
                    ?>
                </td>
                <?php for ($i = 1; $i <= Workdetail::checkNumberOfDays($yearNow,$monthNow); $i++) { ?>
                    <td style="padding: 0px;" class="<?php echo (!empty($valueS['day'][$i]['status'])?$valueS['day'][$i]['status']:'') ?>">
                        <?php
                        if(!empty($valueS['day'][$i]['icon'])){
                            echo '<span data-toggle="tooltip" data-placement="top" title="'.$valueS['day'][$i]['status'].'" style="padding-left: 8px;padding-top: 2px;" class="'.$valueS['day'][$i]['icon'].'" aria-hidden="true"></span>';
                        }
                        ?>
                    </td>
                <?php } ?>
                <td style="text-align: center;padding: 0px;">
                                <span data-toggle="tooltip" data-placement="top" title="เริ่ม <?php echo str_replace(' 00:00:00','',$valueS['data']['Plan_start_date']) ?> ถึง <?php echo str_replace('00:00:00','',$valueS['data']['Plan_end_date']) ?>" >
                                    <?php echo $valueS['data']['Plan_count_day'] ?>
                                </span>
                </td>
                <td style="text-align: center;padding: 0px;font-weight: bolder;"><?php echo (Plan::CheckWorkPlanUser($valueS['data']['Plan_id']))."/".$valueS['data']['Plan_hour'] ?><?php echo ($valueS['data']['Plan_status']=='success'?'/C':'')?></td>
                <td style="text-align: center;padding: 0px;">
                    <?php
                    if($valueS['data']['Plan_status']=='success'){
                        echo '100';
                    }else {
                        if(Plan::CheckWorkPlanUser($valueS['data']['Plan_id'])>0) {
                            echo ceil((((Plan::CheckWorkPlanUser($valueS['data']['Plan_id'])) / $valueS['data']['Plan_hour']) * 100));
                        }else {
                            echo '0';
                        }
                    }
                    ?>%
                </td>
            </tr>
            <?php foreach($valueS as $keySS=>$valueSS){
                if($keySS!=='data' && $keySS!=='day') {
                    $sum=0;
                    ?>
                    <tr id="plan_work" Plan_level_one="<?php echo $valueS['data']['Plan_level_one']?>" Plan_level_two="<?php echo $valueSS['data']['Plan_level_two']?>">
                        <td style="padding: 0px 0px 0px 40px">
                            <span style="cursor: pointer;" class="glyphicon glyphicon-eye-open" aria-hidden="true" level="Plan_level_three"  id="<?php echo $valueSS['data']['Plan_id'] ?>"></span>
                            <?php echo $valueSS['data']['Plan_title'] ?>
                            <?php
                            if(false!==Workdetail::getStaffWorkPlan($valueSS['data']['Plan_id'])){
                                echo '<span data-toggle="tooltip" data-placement="top" title="'.Workdetail::getStaffWorkPlan($valueSS['data']['Plan_id']).'" style="padding-left: 8px;padding-top: 2px;" class="glyphicon glyphicon-user" aria-hidden="true"></span>';
                            }
                            ?>
                        </td>
                        <?php for ($i = 1; $i <= Workdetail::checkNumberOfDays($yearNow,$monthNow); $i++) { ?>
                            <td style="padding: 0px;" class="<?php echo (!empty($valueSS['day'][$i]['status'])?$valueSS['day'][$i]['status']:'') ?>">
                                <?php
                                if(!empty($valueSS['day'][$i]['icon'])){
                                    echo '<span data-toggle="tooltip" data-placement="top" title="'.$valueSS['day'][$i]['status'].'" style="padding-left: 8px;padding-top: 2px;" class="'.$valueSS['day'][$i]['icon'].'" aria-hidden="true"></span>';
                                }
                                ?>
                            </td>
                        <?php } ?>
                        <td style="text-align: center;padding: 0px;">
                                        <span data-toggle="tooltip" data-placement="top" title="เริ่ม <?php echo str_replace(' 00:00:00','',$valueSS['data']['Plan_start_date']) ?> ถึง <?php echo str_replace('00:00:00','',$valueSS['data']['Plan_end_date']) ?>" >
                                            <?php echo $valueSS['data']['Plan_count_day'] ?>
                                        </span>
                        </td>
                        <td style="padding: 0px;text-align: center;font-weight: bolder;"><?php echo (Plan::CheckWorkPlanUser($valueSS['data']['Plan_id']))."/".$valueSS['data']['Plan_hour'] ?><?php echo ($valueSS['data']['Plan_status']=='success'?'/C':'')?></td>
                        <td style="text-align: center;padding: 0px;">
                            <?php
                            if($valueSS['data']['Plan_status']=='success'){
                                echo '100';
                            }else {
                                if(Plan::CheckWorkPlanUser($valueSS['data']['Plan_id'])>0) {
                                    echo ceil((((Plan::CheckWorkPlanUser($valueSS['data']['Plan_id'])) / $valueSS['data']['Plan_hour']) * 100));
                                }else{
                                    echo '0';
                                }
                            }
                            ?>%
                        </td>
                    </tr>
                    <?php foreach ($valueSS as $keySSS => $valueSSS) {
                        if($keySSS!=='data' && $keySSS!=='day') {   ?>
                            <tr id="plan_work" Plan_level_one="<?php echo $valueS['data']['Plan_level_one']?>" Plan_level_two="<?php echo $valueSS['data']['Plan_level_two']?>" Plan_level_three="<?php echo $valueSSS['data']['Plan_level_three'] ?>">
                                <td style="padding: 0px 0px 0px 70px;">
                                    <?php echo $valueSSS['data']['Plan_title'] ?>
                                    <?php
                                    if(false!==Workdetail::getStaffWorkPlan($valueSSS['data']['Plan_id'])){
                                        echo '<span data-toggle="tooltip" data-placement="top" title="'.Workdetail::getStaffWorkPlan($valueSSS['data']['Plan_id']).'" style="padding-left: 8px;padding-top: 2px;" class="glyphicon glyphicon-user" aria-hidden="true"></span>';
                                    }
                                    ?>
                                </td>
                                <?php for ($i = 1; $i <= Workdetail::checkNumberOfDays($yearNow,$monthNow); $i++){ ?>
                                    <td style="padding: 0px;" class="<?php echo (!empty($valueSSS['day'][$i]['status'])?$valueSSS['day'][$i]['status']:'') ?>">
                                        <?php
                                        if(!empty($valueSSS['day'][$i]['icon'])){
                                            echo '<span data-toggle="tooltip" data-placement="top" title="'.$valueSSS['day'][$i]['status'].'" style="padding-left: 8px;padding-top: 2px;" class="'.$valueSSS['day'][$i]['icon'].'" aria-hidden="true"></span>';
                                        }
                                        ?>
                                    </td>
                                <?php } ?>
                                <td style="text-align: center;padding: 0px;">
                                                <span data-toggle="tooltip" data-placement="top" title="เริ่ม <?php echo str_replace(' 00:00:00','',$valueSSS['data']['Plan_start_date']) ?> ถึง <?php echo str_replace('00:00:00','',$valueSSS['data']['Plan_end_date']) ?>" >
                                                    <?php echo $valueSSS['data']['Plan_count_day'] ?>
                                                </span>
                                </td>
                                <td style="text-align: center;padding: 0px;font-weight: bolder;"><?php echo (Plan::CheckWorkPlanUser($valueSSS['data']['Plan_id']))."/".$valueSSS['data']['Plan_hour']  ?><?php echo ($valueSSS['data']['Plan_status']=='success'?'/C':'')?></td>
                                <td style="text-align: center;padding: 0px;">
                                    <?php
                                    if($valueSSS['data']['Plan_status']=='success'){
                                        echo '100';
                                    }else {
                                        if(Plan::CheckWorkPlanUser($valueSSS['data']['Plan_id'])>0) {
                                            echo ceil((((Plan::CheckWorkPlanUser($valueSSS['data']['Plan_id'])) / $valueSSS['data']['Plan_hour']) * 100));
                                        }else{
                                            echo '0';
                                        }
                                    }
                                    ?>%
                                </td>
                            </tr>
                        <?php }
                    }
                }
            }
        }
    }
}
?>
    </tbody>
</table>

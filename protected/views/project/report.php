<?php
$this->pageTitle=Yii::app()->name;

?>
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/project/manage">จัดโครงการ</a></li>
            <li class="active">รายงาน โครงการ<?php echo $dataProjectSelect->Project_name ?></li>
        </ol>
    </div>
    <div class="row">

    </div>

<script>
    $(document).ready(function(){
        $(document).on('click', ".glyphicon-eye-open", function() {
            $(this).attr('class','glyphicon glyphicon-eye-close');
            $("tr["+$(this).attr('level')+"="+$(this).attr('id')+"]").hide();
        })

        $(document).on('click', ".glyphicon-eye-close", function() {
            $(this).attr('class','glyphicon glyphicon-eye-open');
            $("tr["+$(this).attr('level')+"="+$(this).attr('id')+"]").show();
        })


        $(document).on('click', "#btn_Back_Plan,#btn_Next_Plan", function() {
            var type='';
            var Project_id=$('#Project_id').val();
            var select=$(this).attr('selectMonth');
            var selectYear=$(this).attr('selectYear');
            var monthNames = [ "","มกราคม","กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม" ];
            if($(this).attr('id')=='btn_Back_Plan'){
                if(select==1){
                    selectYear--;
                    select=13;
                }
                $('#btn_Back_Plan').attr('selectMonth',select-1);
                $('#btn_Next_Plan').attr('selectMonth',select-1);
                $('#btn_Back_Plan').attr('selectYear',selectYear);
                $('#btn_Next_Plan').attr('selectYear',selectYear);
                if(typeof(type)!=='undefined'&&typeof(select)!=='undefined'&&typeof(selectYear)!=='undefined'){
                    $('#btn_Back').attr('disabled','disabled');
                    $('#btn_Next').attr('disabled','disabled');
                    $('#selectMonthSearch').attr('disabled','disabled');
                    $('#selectYearSearch').attr('disabled','disabled');
                    $.post("/Project/Report", {
                        Project_id:Project_id,
                        month:select-1,
                        year:selectYear
                    }, function (datalist) {
                        $('#table-view').html(datalist);
                        $('#btn_Back_Plan').removeAttr('disabled');
                        $('#btn_Next_Plan').removeAttr('disabled');
                        $('#selectMonthSearchPlan').removeAttr('disabled');
                        $('#selectYearSearchPlan').removeAttr('disabled');
                    })
                }
                $('#nameMonthPlan').html(monthNames[parseInt(select)-1]+' / '+selectYear);
                $('#selectMonthSearchPlan').val(select-1);
                $('#selectYearSearchPlan').val(selectYear);

            }else if($(this).attr('id')=='btn_Next_Plan'){
                if(select==12){
                    selectYear++;
                    select=0;
                }
                $('#btn_Next_Plan').attr('selectMonth',parseInt(select)+1);
                $('#btn_Back_Plan').attr('selectMonth',parseInt(select)+1);
                $('#btn_Back_Plan').attr('selectYear',selectYear);
                $('#btn_Next_Plan').attr('selectYear',selectYear);
                if(typeof(type)!=='undefined'&&typeof(select)!=='undefined'&&typeof(selectYear)!=='undefined'){
                    $.post("/Project/Report", {
                        Project_id:Project_id,
                        month:parseInt(select)+1,
                        year:selectYear
                    }, function (datalist) {
                        $('#btn_Back_Plan').removeAttr('disabled');
                        $('#btn_Next_Plan').removeAttr('disabled');
                        $('#selectMonthSearchPlan').removeAttr('disabled');
                        $('#selectYearSearchPlan').removeAttr('disabled');
                        $('#table-view').html(datalist);
                    })
                }
                $('#nameMonthPlan').html(monthNames[parseInt(select)+1]+' / '+selectYear);
                $('#selectMonthSearchPlan').val(parseInt(select)+1);
                $('#selectYearSearchPlan').val(selectYear);
            }
        })

        $(document).on('change', "#selectMonthSearchPlan,#selectYearSearchPlan", function() {
            var month=$('#selectMonthSearchPlan').val();
            var year=$('#selectYearSearchPlan').val();
            var Project_id=$('#Project_id').val();
            var monthNames = [ "","มกราคม","กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม" ];
            $('#btn_Next_Plan').attr('selectMonth',month);
            $('#btn_Back_Plan').attr('selectMonth',month);
            $('#btn_Back_Plan').attr('selectYear',year);
            $('#btn_Next_Plan').attr('selectYear',year);
            if(typeof(Project_id)!=='undefined'&&typeof(month)!=='undefined'&&typeof(year)!=='undefined'){
                $('#btn_Back_Plan').attr('disabled','disabled');
                $('#btn_Next_Plan').attr('disabled','disabled');
                $('#selectMonthSearch').attr('disabled','disabled');
                $('#selectYearSearch').attr('disabled','disabled');
                $.post("/Project/Report", {
                    Project_id:Project_id,
                    month:month,
                    year:year
                }, function (datalist) {
                    $('#btn_Back_Plan').removeAttr('disabled');
                    $('#btn_Next_Plan').removeAttr('disabled');
                    $('#selectMonthSearchPlan').removeAttr('disabled');
                    $('#selectYearSearchPlan').removeAttr('disabled');
                    $('#table-view').html(datalist);
                })
            }
            $('#nameMonthPlan').html(monthNames[month]+' / '+year);
            //$('.tbody_Month').hide();
            //$('#tbody_Month_'+year+month).show();
        })

    })
</script>

<style>
    #plan_work:hover{
        background-color: rgba(0, 100, 200, .5);
    }
    td.success{
        background-color: rgba(0, 150, 0, .5) !important;
    }
    td.warning{
        background-color: rgba(255, 70, 0, .3) !important;
    }
    td.danger{
        background-color: rgba(255, 0, 70, .5) !important;
    }
    td.info{
        background-color: rgba(0, 170, 255, .3) !important;
    }
</style>

<div class="row" style="padding-bottom: 10px;">
    <div style="width: 50%;float: left;font-weight: bolder;font-size: 28px;">รายงาน โครงการ <?php echo $dataProjectSelect->Project_name ?></div>
    <div style="width: 50%;float: left;">
        <div style="float: left; width: 50%;font-weight: bold;font-size: 20px;text-align: center;">
            <button style="margin: 0px;float: left;" class="btn btn-info" id="btn_Back_Plan" selectYear="<?php echo $yearNow?>" selectMonth="<?php echo $monthNow; ?>" > << </button>
            เดือน <span id="nameMonthPlan"><?php echo $monthNames[$monthNow].' / '.$yearNow; ?></span>
            <button style="margin: 0px;float: right;" class="btn btn-info" id="btn_Next_Plan" selectYear="<?php echo $yearNow?>" selectMonth="<?php echo $monthNow; ?>" > >> </button>
        </div>
        <div class="form-inline" style="width: 50%;float: left;padding-left: 15%; ">
            <input type="hidden" id="Project_id" value="<?php echo $Project_id; ?>">
            <div class="form-group">
                <select class="form-control" id="selectMonthSearchPlan">
                    <?php
                    foreach($monthNames as $key=>$value){
                        if(!empty($value)){	?>
                            <option <?php echo ($monthNow==$key)?'selected':''; ?> value="<?php echo $key ?>"><?php echo $value ?></option>
                        <?php }} ?>
                </select>
            </div>
            <div class="form-group">
                <select class="form-control" id="selectYearSearchPlan">
                    <?php
                    for ($i = 2010; $i <= 2020; $i++) { ?>
                        <option <?php echo ($yearNow==$i)?'selected':''; ?> value="<?php echo $i ?>"><?php echo $i ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
</div>

<?php //if(Yii::app()->user->type=='admin'){?>
    <div class="row" id="table-view">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th style="width: 30%;border-bottom-color: #080808;">ชื่องาน</th>
                <?php for ($i = 1; $i <= Workdetail::checkNumberOfDays($yearNow,$monthNow); $i++) { ?>
                    <th style="width:20px;border-bottom-color: #080808;"><?php echo $i; ?></th>
                <?php } ?>
                <th style="border-bottom-color: #080808;text-align: center;">วัน</th>
                <th style="border-bottom-color: #080808;text-align: center;">ชั่วโมงรวม</th>
                <th style="border-bottom-color: #080808;text-align: center;">ชั่วโมง(%)</th>
            </tr>
            </thead>
            <tbody>

            <?php
            foreach($dataPlan as $key=>$value) {
                $sum=0;
                ?>
                <tr id="plan_work">
                    <td style="font-weight: bolder;padding: 0px;">
                        <span style="cursor: pointer;" class="glyphicon glyphicon-eye-open" aria-hidden="true" level="Plan_level_one"  id="<?php echo $value['data']['Plan_id'] ?>"></span>
                        <span style="cursor: pointer;" data-toggle="tooltip" data-placement="top" title="<?php echo $value['data']['Plan_detail']; ?>"><?php echo $value['data']['Plan_title'] ?></span>
                        <?php
                        if(false!==Workdetail::getStaffWorkPlan($value['data']['Plan_id'])){
                            echo '<span data-toggle="tooltip" data-placement="top" title="'.Workdetail::getStaffWorkPlan($value['data']['Plan_id']).'" style="padding-left: 8px;padding-top: 2px;" class="glyphicon glyphicon-user" aria-hidden="true"></span>';
                        }
                        ?>
                    </td>
                    <?php for ($i = 1; $i <= Workdetail::checkNumberOfDays($yearNow,$monthNow); $i++) { ?>
                        <td style="padding: 0px;" class="<?php echo (!empty($value['day'][$i]['status'])?$value['day'][$i]['status']:'') ?>">
                        <?php
                            if(!empty($value['day'][$i]['icon'])){
                                echo '<span data-toggle="tooltip" data-placement="top" title="'.$value['day'][$i]['status'].'" style="padding-left: 8px;padding-top: 2px;" class="'.$value['day'][$i]['icon'].'" aria-hidden="true"></span>';
                            }
                        ?>
                        </td>
                    <?php } ?>
                    <td style="text-align: center;padding: 0px;"><span data-toggle="tooltip" data-placement="top" title="เริ่ม <?php echo str_replace(' 00:00:00','',$value['data']['Plan_start_date']) ?> ถึง <?php echo str_replace('00:00:00','',$value['data']['Plan_end_date']) ?>" ><?php echo $value['data']['Plan_count_day'] ?></span></td>
                    <td style="text-align: center;padding: 0px;font-weight: bolder;"><?php echo (Plan::CheckWorkPlanUser($value['data']['Plan_id']))."/".$value['data']['Plan_hour'] ?><?php echo ($value['data']['Plan_status']=='success'?'/C':'')?></td>
                    <td style="text-align: center;padding: 0px;">
                        <?php
                        if($value['data']['Plan_status']=='success'){
                            echo '100';
                        }else {
                            if(Plan::CheckWorkPlanUser($value['data']['Plan_id'])>0 && !empty($value['data']['Plan_hour']) && $value['data']['Plan_hour']>0){
                                echo ceil(((Plan::CheckWorkPlanUser($value['data']['Plan_id']) / $value['data']['Plan_hour']) * 100));
                            }else{
                                echo '0';
                            }
                        }
                        ?>%
                    </td>
                </tr>
               <?php foreach($value as $keyS=>$valueS){
                    if($keyS!=='data' && $keyS!=='day') {
                        $sum=0;
                        ?>
                        <tr id="plan_work" Plan_level_one="<?php echo $valueS['data']['Plan_level_one']?>">
                            <td style="padding: 0px 0px 0px 20px;">
                                <span style="cursor: pointer;" class="glyphicon glyphicon-eye-open" aria-hidden="true" level="Plan_level_two"  id="<?php echo $valueS['data']['Plan_id'] ?>"></span>
                                <span style="cursor: pointer;" data-toggle="tooltip" data-placement="top" title="<?php echo $valueS['data']['Plan_detail']; ?>"><?php echo $valueS['data']['Plan_title'] ?></span>
                                <?php
                                if(false!==Workdetail::getStaffWorkPlan($valueS['data']['Plan_id'])){
                                    echo '<span data-toggle="tooltip" data-placement="top" title="'.Workdetail::getStaffWorkPlan($valueS['data']['Plan_id']).'" style="padding-left: 8px;padding-top: 2px;" class="glyphicon glyphicon-user" aria-hidden="true"></span>';
                                }
                                ?>
                            </td>
                            <?php for ($i = 1; $i <= Workdetail::checkNumberOfDays($yearNow,$monthNow); $i++) { ?>
                            <td style="padding: 0px;" class="<?php echo (!empty($valueS['day'][$i]['status'])?$valueS['day'][$i]['status']:'') ?>">
                                    <?php
                                    if(!empty($valueS['day'][$i]['icon'])){
                                        echo '<span data-toggle="tooltip" data-placement="top" title="'.$valueS['day'][$i]['status'].'" style="padding-left: 8px;padding-top: 2px;" class="'.$valueS['day'][$i]['icon'].'" aria-hidden="true"></span>';
                                    }
                                    ?>
                            </td>
                            <?php } ?>
                            <td style="text-align: center;padding: 0px;">
                                <span data-toggle="tooltip" data-placement="top" title="เริ่ม <?php echo str_replace(' 00:00:00','',$valueS['data']['Plan_start_date']) ?> ถึง <?php echo str_replace('00:00:00','',$valueS['data']['Plan_end_date']) ?>" >
                                    <?php echo $valueS['data']['Plan_count_day'] ?>
                                </span>
                            </td>
                            <td style="text-align: center;padding: 0px;font-weight: bolder;"><?php echo (Plan::CheckWorkPlanUser($valueS['data']['Plan_id']))."/".$valueS['data']['Plan_hour'] ?><?php echo ($valueS['data']['Plan_status']=='success'?'/C':'')?></td>
                            <td style="text-align: center;padding: 0px;">
                                <?php
                                    if($valueS['data']['Plan_status']=='success'){
                                        echo '100';
                                    }else {
                                        if(Plan::CheckWorkPlanUser($valueS['data']['Plan_id'])>0 && !empty($valueS['data']['Plan_hour']) && $valueS['data']['Plan_hour']>0) {
                                            echo ceil((((Plan::CheckWorkPlanUser($valueS['data']['Plan_id'])) / $valueS['data']['Plan_hour']) * 100));
                                        }else{
                                            echo '0';
                                        }
                                    }
                                ?>%
                            </td>
                        </tr>
                        <?php foreach($valueS as $keySS=>$valueSS){
                            if($keySS!=='data' && $keySS!=='day') {
                                $sum=0;
                                ?>
                                <tr id="plan_work" Plan_level_one="<?php echo $valueS['data']['Plan_level_one']?>" Plan_level_two="<?php echo $valueSS['data']['Plan_level_two']?>">
                                    <td style="padding: 0px 0px 0px 40px">
                                        <span style="cursor: pointer;" class="glyphicon glyphicon-eye-open" aria-hidden="true" level="Plan_level_three"  id="<?php echo $valueSS['data']['Plan_id'] ?>"></span>
                                        <span style="cursor: pointer;" data-toggle="tooltip" data-placement="top" title="<?php echo $valueSS['data']['Plan_detail']; ?>"><?php echo $valueSS['data']['Plan_title'] ?></span>
                                        <?php
                                        if(false!==Workdetail::getStaffWorkPlan($valueSS['data']['Plan_id'])){
                                            echo '<span data-toggle="tooltip" data-placement="top" title="'.Workdetail::getStaffWorkPlan($valueSS['data']['Plan_id']).'" style="padding-left: 8px;padding-top: 2px;" class="glyphicon glyphicon-user" aria-hidden="true"></span>';
                                        }
                                        ?>
                                    </td>
                                    <?php for ($i = 1; $i <= Workdetail::checkNumberOfDays($yearNow,$monthNow); $i++) { ?>
                                    <td style="padding: 0px;" class="<?php echo (!empty($valueSS['day'][$i]['status'])?$valueSS['day'][$i]['status']:'') ?>">
                                        <?php
                                        if(!empty($valueSS['day'][$i]['icon'])){
                                            echo '<span data-toggle="tooltip" data-placement="top" title="'.$valueSS['day'][$i]['status'].'" style="padding-left: 8px;padding-top: 2px;" class="'.$valueSS['day'][$i]['icon'].'" aria-hidden="true"></span>';
                                        }
                                    ?>
                                    </td>
                                    <?php } ?>
                                    <td style="text-align: center;padding: 0px;">
                                        <span data-toggle="tooltip" data-placement="top" title="เริ่ม <?php echo str_replace(' 00:00:00','',$valueSS['data']['Plan_start_date']) ?> ถึง <?php echo str_replace('00:00:00','',$valueSS['data']['Plan_end_date']) ?>" >
                                            <?php echo $valueSS['data']['Plan_count_day'] ?>
                                        </span>
                                    </td>
                                    <td style="padding: 0px;text-align: center;font-weight: bolder;"><?php echo (Plan::CheckWorkPlanUser($valueSS['data']['Plan_id']))."/".$valueSS['data']['Plan_hour'] ?><?php echo ($valueSS['data']['Plan_status']=='success'?'/C':'')?></td>
                                    <td style="text-align: center;padding: 0px;">
                                        <?php
                                        if($valueSS['data']['Plan_status']=='success'){
                                            echo '100';
                                        }else {
                                            if(Plan::CheckWorkPlanUser($valueSS['data']['Plan_id'])>0 && !empty($valueS['data']['Plan_hour']) && $valueS['data']['Plan_hour']>0) {
                                                echo ceil((((Plan::CheckWorkPlanUser($valueSS['data']['Plan_id'])) / $valueSS['data']['Plan_hour']) * 100));
                                            }else{
                                                echo '0';
                                            }
                                        }
                                        ?>%
                                    </td>
                                </tr>
                                <?php foreach ($valueSS as $keySSS => $valueSSS) {
                                    if($keySSS!=='data' && $keySSS!=='day') {   ?>
                                        <tr id="plan_work" Plan_level_one="<?php echo $valueS['data']['Plan_level_one']?>" Plan_level_two="<?php echo $valueSS['data']['Plan_level_two']?>" Plan_level_three="<?php echo $valueSSS['data']['Plan_level_three'] ?>">
                                            <td style="padding: 0px 0px 0px 70px;">
                                                <span style="cursor: pointer;" data-toggle="tooltip" data-placement="top" title="<?php echo $valueSSS['data']['Plan_detail']; ?>"><?php echo $valueSSS['data']['Plan_title'] ?></span>
                                                <?php
                                                if(false!==Workdetail::getStaffWorkPlan($valueSSS['data']['Plan_id'])){
                                                    echo '<span data-toggle="tooltip" data-placement="top" title="'.Workdetail::getStaffWorkPlan($valueSSS['data']['Plan_id']).'" style="padding-left: 8px;padding-top: 2px;" class="glyphicon glyphicon-user" aria-hidden="true"></span>';
                                                }
                                                ?>
                                            </td>
                                            <?php for ($i = 1; $i <= Workdetail::checkNumberOfDays($yearNow,$monthNow); $i++){ ?>
                                                <td style="padding: 0px;" class="<?php echo (!empty($valueSSS['day'][$i]['status'])?$valueSSS['day'][$i]['status']:'') ?>">
                                                    <?php
                                                    if(!empty($valueSSS['day'][$i]['icon'])){
                                                        echo '<span data-toggle="tooltip" data-placement="top" title="'.$valueSSS['day'][$i]['status'].'" style="padding-left: 8px;padding-top: 2px;" class="'.$valueSSS['day'][$i]['icon'].'" aria-hidden="true"></span>';
                                                    }
                                                    ?>
                                                </td>
                                            <?php } ?>
                                            <td style="text-align: center;padding: 0px;">
                                                <span data-toggle="tooltip" data-placement="top" title="เริ่ม <?php echo str_replace(' 00:00:00','',$valueSSS['data']['Plan_start_date']) ?> ถึง <?php echo str_replace('00:00:00','',$valueSSS['data']['Plan_end_date']) ?>" >
                                                    <?php echo $valueSSS['data']['Plan_count_day'] ?>
                                                </span>
                                            </td>
                                            <td style="text-align: center;padding: 0px;font-weight: bolder;"><?php echo (Plan::CheckWorkPlanUser($valueSSS['data']['Plan_id']))."/".$valueSSS['data']['Plan_hour']  ?><?php echo ($valueSSS['data']['Plan_status']=='success'?'/C':'')?></td>
                                            <td style="text-align: center;padding: 0px;">
                                                <?php
                                                if($valueSSS['data']['Plan_status']=='success'){
                                                    echo '100';
                                                }else {
                                                    if(Plan::CheckWorkPlanUser($valueSSS['data']['Plan_id'])>0 && !empty($valueSSS['data']['Plan_hour']) && $valueSSS['data']['Plan_hour']>0) {
                                                        echo ceil((((Plan::CheckWorkPlanUser($valueSSS['data']['Plan_id'])) / $valueSSS['data']['Plan_hour']) * 100));
                                                    }else{
                                                        echo '0';
                                                    }
                                                }
                                                ?>%
                                            </td>
                                        </tr>
                                   <?php }
                                }
                            }
                        }
                    }
                }
            }
            ?>
            </tbody>
        </table>
    </div>
<?php //} ?>
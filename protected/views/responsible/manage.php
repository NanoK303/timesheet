<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
$monthNames = array("เลือกวัน","มกราคม","กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
?>
<div class="row" style="">
    <div class="col-lg-12">
        <form method="post" class="form-inline right">
            <div class="form-group">
                <label for="exampleInputName2">วัน</label>
                <select name="search[day]" class="form-control">
                    <option value="">เลือกวัน</option>
                    <?php for($i=1;$i<=31;$i++){ ?>
                        <option value="<?php echo $i ?>"><?php echo $i ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail2">เดือน</label>
                <select name="search[month]" class="form-control">
<!--                    <option value="">เลือกวัน</option>-->
                    <?php foreach($monthNames as $key=>$value){ ?>
                        <option value="<?php echo $key ?>"><?php echo $value ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail2">ปี</label>
                <select name="search[year]" class="form-control">
                    <option value="">เลือกวัน</option>
                    <option value="2015">2015</option>
                    <option value="2016">2017</option>
                </select>
            </div>
            <button type="submit" class="btn btn-default">ค้นหา</button>
        </form>
    </div>
</div>
<div class="row" style="margin-top: 20px;">
    <div class="col-lg-12">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th>วันที่</th>
                <th>ชื่อผู้ทำงาน</th>
                <th>ชื่อผู้รับผิดชอบแทน</th>
                <th>จัดการ</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($dataResponsible as $key=>$value) { ?>
            <tr>
                <th><?php echo $key+1 ?></th>
                <td><?php echo $value->responsible_day.'-'.$value->responsible_month.'-'.$value->responsible_year ?></td>
                <td><?php echo $value->userName->Name.' '.$value->userName->LastName ?></td>
                <td><?php echo $value->responsibleUserName->Name.' '.$value->responsibleUserName->LastName ?>@mdo</td>
                <td>
                    <a href="/responsible/RemoveResponsible/<?php echo $value->responsible_id ?>" onclick="return confirm('ยืนยันการลบ ความรับผิดชอบ ค่าใช่จ่าย ของ <?php echo $value->userName->Name ?> <?php echo $value->userName->LastName ?> ในวันที่ <?php echo $value->responsible_day ?> เดือน <?php echo $monthNames[$value->responsible_month] ?> ปี <?php echo $value->responsible_year ?>  ')" class="btn btn-danger">ลบ</a>
                </td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<?php
$this->layout='column1';
?>
<div class="row" style="font-weight:bold;font-size: 28px; margin: 0px;padding: 0px;">
    จัดการพนักงาน
</div>

<div class="row">
    <button class="btn btn-primary right"  data-toggle="modal" data-target="#myModalAddStaff" >เพิ่มพนักงาน</button>
    <div style="clear: both"></div>
    <table class="table table-bordered" style="margin-top: 10px;">
        <thead>
        <tr>
            <th>#</th>
            <th>ชื่อ</th>
            <th>ชื่อเล่น</th>
            <th>เบอร์โทร</th>
            <th>ที่อยู่</th>
            <th>อีเมล์</th>
            <th>ตำแหน่ง</th>
            <th>จัดการ</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($dataStaff as $key=>$value){ ?>
            <tr class="viewProject" id="viewStaff_<?php echo $value->UserName ?>">
                <td scope="row"><?php echo $key+1 ?></td>
                <td><?php echo $value->Name.' '.$value->LastName ?></td>
                <td><?php echo $value->Nickname ?></td>
                <td><?php echo $value->Phone ?></td>
                <td><?php echo $value->Address ?></td>
                <td><?php echo $value->Email ?></td>
                <td><?php echo $value->Position ?></td>
                <td>
                    <a class="btn btn-primary" href="/staff/managestaff/username/<?php echo $value->UserName ?>" >แก้ไข</a>
                    <a href="/staff/deletestaff/username/<?php echo $value->UserName ?>" onclick="return confirm('ยืนยันการลบข้อมูล พนักงานนี้ ?')" UserName="<?php echo $value->UserName; ?>" class="btn btn-danger">ลบ</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

<div class="row">
    <div class="modal fade <?php echo (!empty($staff->UserName))?'in':'' ?>"  style="<?php echo (!empty($staff->UserName))?'display:block':'' ?>" id="myModalAddStaff" tabindex="-1" role="dialog" aria-labelledby="myModalAddStaff" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <?php
                $formRegister = $this->beginWidget('CActiveForm', array(
                    'id' => 'formRegister',
                    'htmlOptions' => array('enctype' => 'multipart/form-data'),
                    'enableAjaxValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                    ),
                    'htmlOptions' => array(
                        'class' => 'form-horizontal',
                    ),
                ));
//                echo $form->hiddenField($staff, 'WorkDetail_id');
                ?>
                <div class="modal-header">
<!--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                    <h4 class="modal-title" id="myModalLabel">จัดการข้อมูล พนักงาน</h4>
                </div>
                <div class="modal-body">
                    <div class="form" style="margin-left: 5%">

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">ชื่อ</label>
                            <div class="col-sm-9">
                                <?php echo $formRegister->textField($staff, 'Name', array('class' => 'form-control')); ?>
                                <?php echo $formRegister->error($staff, 'Name'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">นามสกุล</label>
                            <div class="col-sm-9">
                                <?php echo $formRegister->textField($staff, 'LastName', array('class' => 'form-control')); ?>
                                <?php echo $formRegister->error($staff, 'LastName'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">ชื่อเล่น</label>
                            <div class="col-sm-9">
                                <?php echo $formRegister->textField($staff, 'Nickname', array('class' => 'form-control')); ?>
                                <?php echo $formRegister->error($staff, 'Nickname'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">ชื่อเข้าระบบ</label>
                            <div class="col-sm-9">
                                <?php echo $formRegister->textField($staff, 'UserName', array('class' => 'form-control')); ?>
                                <?php echo $formRegister->error($staff, 'UserName'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">รหัสผ่าน</label>
                            <div class="col-sm-9">
                                <?php echo $formRegister->passwordField($staff, 'Password', array('class' => 'form-control')); ?>
                                <?php echo $formRegister->error($staff, 'Password'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">ยืนยันรหัสผ่าน</label>
                            <div class="col-sm-9">
                                <?php echo $formRegister->passwordField($staff, 'Password2', array('class' => 'form-control')); ?>
                                <?php echo $formRegister->error($staff, 'Password2'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">อีเมล์</label>
                            <div class="col-sm-9">
                                <?php echo $formRegister->textField($staff, 'Email', array('class' => 'form-control')); ?>
                                <?php echo $formRegister->error($staff, 'Email'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">เบอร์โทร</label>
                            <div class="col-sm-9">
                                <?php echo $formRegister->textField($staff, 'Phone', array('class' => 'form-control')); ?>
                                <?php echo $formRegister->error($staff, 'Phone'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">ที่อยู่</label>
                            <div class="col-sm-9">
                                <?php echo $formRegister->textArea($staff, 'Address', array('class' => 'form-control')); ?>
                                <?php echo $formRegister->error($staff, 'Address'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">ตำแหน่ง</label>
                            <div class="col-sm-9">
                                <?php echo $formRegister->dropDownList($staff,'Position', array('programmer'=>'โปรแกรมเมอร์','design'=>'ออกแบบ','engineer'=>'วิศวกร','foreman'=>'โฟร์แมน','architect'=>'สถาปนิก','admin'=>'ผู้ดูเเล'), array('empty'=>'เลือกตำแหน่ง','class' => 'form-control')) ?>
                                <?php echo $formRegister->error($staff, 'Position'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="/staff/managestaff" class="btn btn-default" data-dismiss="modal">Close</a>
                    <?php echo CHtml::submitButton(Yii::t('main', 'บันทึก'), array('id' => 'addFormStaff', 'class' => 'btn btn-primary')); ?>
                    <!--					<button type="button" class="btn btn-primary">Save changes</button>-->
                </div>
                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>
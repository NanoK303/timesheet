<?php
$this->layout='column1';
?>
<div class="row" style="font-weight:bold;font-size: 28px; margin: 0px;padding: 0px;">
    จัดการกลุ่มพนักงาน
</div>

<div class="row">
    <div class="col-lg-10"></div>
    <div class="col-lg-2">
        <button style="margin: 5px 0px;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalAddOrganizer">
            เพิ่มกลุ่ม
        </button>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th>ชื่อกลุ่มงาน</th>
                <th>บริษัท</th>
                <th>พนักงาน</th>
                <th>จัดการ</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($dataOrganizer as $key=>$value){
                if($value->Remove=='N'){?>
            <tr>
                <td><?php echo $key+1 ?></td>
                <td><?php echo $value->NameOrganizer ?></td>
                <td><?php echo $value->company->CompanyName ?></td>
                <td>
                    <?php if(!empty($value->staff)){
                        foreach($value->staff as $key=>$value){?>
                        <span class="label label-success" style="cursor: pointer;padding-right: 17px;">
                            <?php echo $value->Name.' '.$value->LastName.'('.$value->Nickname.')'  ?>
                            <a onclick="return confirm('ยืนยันการ ลบ พนักงานออกจากกลุ่มงาน')" href="/staff/removeSubOrganizer/UserName/<?php echo $value->UserName ?>" style="z-index: 20;position: absolute;margin-left: 2px;margin-top: 1px;" type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                            </a>
                        </span>
                        </br>
                    <?php }} ?>
                </td>
                <td>
                    <form method="post" action="/staff/addStaffToOrganizer">
                        <div style="float: left;">
                            <div style="float:left;">
                                <input type="hidden" name="add[SubOrganizer_id]" value="<?php echo $value->SubOrganizer_id; ?>">
                                <select name="add[UserName]" class="form-control">
                                    <option value="">เลือกพนักงาน</option>
                                    <?php foreach($dateStaff as $Staffkey=>$Staffvalue){
                                        if($Staffvalue->SubOrganizer_id != $value->SubOrganizer_id){?>
                                            <option value="<?php echo $Staffvalue['UserName'] ?>"><?php echo $Staffvalue['Name'] ?> <?php echo $Staffvalue['LastName'] ?>(<?php echo $Staffvalue['Nickname'] ?>)</option>
                                    <?php }} ?>
                                </select>
                            </div>
                            <div style="float:left;padding: 0px 5px;">
                                <button class="btn btn-warning" id="btnAddStaff">เพิ่มพนักงาน</button>
                            </div>
                        </div>
                    </form>
                    <a style="margin: 0px 1px;float: left;" class="btn btn-primary" href="/staff/manageOrganizer/<?php echo $value->SubOrganizer_id ?>">แก้ไข</a>
                    <a style="margin: 0px 1px;float: left;" onclick="return confirm('ยืนยันการลบกลุ่มงาน')" class="btn btn-danger" href="/staff/removeOrganizer/<?php echo $value->SubOrganizer_id ?>">ลบ</a>
                </td>
            </tr>
            <?php }} ?>
            </tbody>
        </table>
    </div>
</div>

<div class="row">
    <div class="modal fade <?php echo (!empty($organizer->SubOrganizer_id))?'in':'' ?>"  style="<?php echo (!empty($organizer->SubOrganizer_id))?'display:block':'' ?>" id="myModalAddOrganizer" tabindex="-1" role="dialog" aria-labelledby="myModalAddStaff" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <?php
                $formOrganizer = $this->beginWidget('CActiveForm', array(
                    'id' => 'formOrganizer',
                    'htmlOptions' => array('enctype' => 'multipart/form-data'),
                    'enableAjaxValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                    ),
                    'htmlOptions' => array(
                        'class' => 'form-horizontal',
                    ),
                ));
                ?>
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">จัดการข้อมูล กลุ่มพนักงาน</h4>
                </div>
                <div class="modal-body">
                    <div class="form" style="margin-left: 5%">

                        <div class="form-group">
                            <label for="exampleInputName2" class="col-sm-3 control-label">บริษัท</label>
                            <div class="col-sm-9">
                                <?php
                                echo $formOrganizer->dropDownList($organizer,'Company_id', CHtml::listData(Company::model()->findAll(array('order' => 'Company_id ASC')), 'Company_id', 'CompanyName'), array('empty'=>'กรุณาเลือก','class' => 'form-control'))
                                ?>
                            </div>
                            <div class="form">
                                <?php echo $formOrganizer->error($organizer,'Company_id'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">ชื่อกลุ่มงาน</label>
                            <div class="col-sm-9">
                                <?php echo $formOrganizer->textField($organizer, 'NameOrganizer', array('class' => 'form-control')); ?>
                                <?php echo $formOrganizer->error($organizer, 'NameOrganizer'); ?>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <a href="/staff/manageOrganizer" class="btn btn-default" data-dismiss="modal">Close</a>
                    <?php if(!empty($organizer->SubOrganizer_id)){ ?>
                        <?php echo CHtml::submitButton(Yii::t('main', 'แก้ไข'), array('id' => 'addFormStaff', 'class' => 'btn btn-warning')); ?>
                    <?php }else{ ?>
                        <?php echo CHtml::submitButton(Yii::t('main', 'บันทึก'), array('id' => 'addFormStaff', 'class' => 'btn btn-primary')); ?>
                    <?php } ?>
                </div>
                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>
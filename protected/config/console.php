<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Console Application',
    'timeZone' => 'Asia/Bangkok',
	// preloading 'log' component
	'preload'=>array('log'),
    'import' => array(
        'application.models.*',
        'application.components.*',
        'ext.giix-components.*',
        'ext.SimpleHTMLDOM.*',
        'ext.yii-mail.*',
    ),

	// application components
	'components'=>array(
//        'user'=>array(
//            // enable cookie-based authentication
//            'allowAutoLogin'=>true,
//        ),
		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
        'mail' => array(
            'class' => 'ext.yii-mail.YiiMail',
            'transportType' => 'smtp',
            'transportOptions' => array(
                'host' => 'smtp.gmail.com',
                'username' => 'sootipong303@gmail.com',
                'password' => 'sootipong',
                'port' => '465',
                'encryption'=>'tls',
            ),
            'viewPath' => 'application.views.mail',
            'logging' => true,
            'dryRun' => false
        ),

	),
);

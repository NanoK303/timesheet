$(document).ready(function(){
    //$.datepicker.setDefaults($.datepicker.regional['en']);
    //$('#datepicker_start,#datepicker_end').datetimepicker({
    //    dateFormat: "yy-mm-dd",
    //    timeFormat:"hh:mm:00"
    //})

    //$('#datepicker_start,#datepicker_end').datepicker({
    //    dateFormat: "yy-mm-dd"
    //});
    $('[data-toggle="tooltip"]').tooltip({html:true});

    $(document).on('change', "select#select-leave", function() {
        $('.leave-view').addClass('hide');
        // console.log($(this).val());
        if($(this).val()!==""){
            $('span.'+$(this).val()).removeClass('hide');
        }
    })

    //ตาราสรุปการทำงาน
    $(document).on('click', "a[role=tab]", function() {
        var type=$(this).attr('type');
        var selectMonthSearch=$('#selectMonthSearch').val();
        var selectYearSearch=$('#selectYearSearch').val();
        if(typeof(type)!=='undefined'&&typeof(selectMonthSearch)!=='undefined'&&typeof(selectYearSearch)!=='undefined'){
            $.post("/Site/GetViewTable", {
                type:type,
                selectMonthSearch:selectMonthSearch,
                selectYearSearch:selectYearSearch
            }, function (datalist) {
                $('#table-view-'+type).html(datalist);
            })
        }
        console.log(type);
    })



    $(document).on('click', "#viewLogPlan", function() {
        var Plan_id=$(this).attr('Plan_id');
        if(Plan_id=='all'){
            $('tr.LogPlan').show();
        }else {
            $('tr.LogPlan').hide();
            $("tr#LogPlan_" + Plan_id).each(function () {
                $(this).show();
            });
        }
    })

    $(document).on('click', "#btnSelectAddHour,#btnSelectEditProject,#btnSelectAddStaff", function() {
        $('.mainBtn').show();
        $('.addHouse').hide();
        $('.editNameProject').hide();
        //$('#mainBtn_'+$(this).attr('project_id')).hide();
        //console.log('click');
        if($(this).attr('id')=='btnSelectAddHour') {
            $('#mainBtn_'+$(this).attr('project_id')).hide();
            $('#addHouse_' + $(this).attr('project_id')).show();
            $('input#inputProductId_' + $(this).attr('project_id')).val($(this).attr('project_id'));
            $('input#inputValue_' + $(this).attr('project_id')).focus();
        }else if($(this).attr('id')=='btnSelectEditProject'){
            $('#mainBtn_'+$(this).attr('project_id')).hide();
            $('#editNameProject_' + $(this).attr('project_id')).show();
            $('input#inputEditProductId_' + $(this).attr('project_id')).val($(this).attr('project_id'));
            $('input#inputName_' + $(this).attr('project_id')).val($('td#ProjectName_'+$(this).attr('project_id')).html());
        }else if($(this).attr('id')=='btnSelectAddStaff'){
            $('#mainBtn_'+$(this).attr('project_id')).hide();
            $('#addStaff_' + $(this).attr('project_id')).show();
        }
    });


    $(document).on('click', "#btnChangePublic", function() {
        var ProjectId=$(this).attr('project_id');
        if (confirm("ยืนยันการเปลิ่ยนประเภทโครงการนี้ ") == true) {
            $.post("/Project/ChangePublic", {
                ProjectId: ProjectId
            }, function (datalist) {
                datalist = jQuery.parseJSON(datalist);
                if (datalist['status'] == 'true') {
                    location.reload();
                } else {
                    alert("ไม่สามารถ ย้ายพนักงานออกจากโครงการ");
                }
            })
        }
    })

    $(document).on('click', "#btnDeleteWorkDetail,#btnRemoveStaffProject", function() {

        if($(this).attr('id')=='btnRemoveStaffProject') {
            if(confirm("ยืนยันการย้ายพนักงานออกจากโครงการ")==true){
                var UserName=$(this).attr('UserName');
                var ProjectId=$(this).attr('ProjectId');
                $.post("/Project/removeStaffProject", {
                    UserName:UserName,
                    ProjectId:ProjectId
                }, function (datalist) {
                    datalist = jQuery.parseJSON(datalist);
                    if (datalist['status'] == 'true') {
                        alert("ย้ายพนักงานออกจากโครงการ สำเร็จ");
                        $('#spanStaffProject_'+datalist['data']).remove();
                    } else {
                        alert("ไม่สามารถ ย้ายพนักงานออกจากโครงการ");
                    }
                })
            }
        }else if($(this).attr('id')=='btnDeleteWorkDetail') {
            if (confirm("ยืนยันการลบข้อมูล งาน นี้หรือไม่")) {
                var WorkDetail_id = $(this).attr('WorkDetail_id');
                $.post("/site/deleteWorkDetail", {
                    WorkDetail_id: WorkDetail_id
                }, function (datalist) {
                    datalist = jQuery.parseJSON(datalist);
                    if (datalist['status'] == 'true') {
                        alert("ลบข้อมูลงานสำเร็จ");
                        location.reload();
                    } else {
                        alert("ไม่สามารถลบข้อมูลสำเร็จ");
                    }
                })
            }
        }
    })

    $(document).on('click', "#btnSelectDelete,#btnSelectClose,#btnSelectOpen", function() {
        var projectId=$(this).attr('project_id');
        if($(this).attr('id')=='btnSelectDelete') {
            if (confirm("ยืนยันการลบโครงการนี้ ") == true) {
                $.post("/project/deleteProject", {
                    ProjectId: projectId,
                    type:'Y'
                }, function (datalist) {
                    datalist = jQuery.parseJSON(datalist);
                    if (datalist['status'] == 'true') {
                        $('#viewProject_' + projectId).remove();
                        alert("ลบข้อมูลสำเร็จ");
                    } else {
                        alert("ไม่สามารถลบข้อมูลสำเร็จ");
                    }
                })
            }
        }else if($(this).attr('id')=='btnSelectClose'){
            if (confirm("ยืนยันการปิดโครงการนี้ ") == true) {
                $.post("/project/deleteProject", {
                    ProjectId: projectId,
                    type:'C'
                }, function (datalist) {
                    datalist = jQuery.parseJSON(datalist);
                    if (datalist['status'] == 'true') {
                        alert("ปิดโครงการสำเร็จ");
                        location.reload();
                    } else {
                        alert("ไม่สามารถปิดโครงการได้");
                    }
                })
            }
        }else if($(this).attr('id')=='btnSelectOpen'){
            if (confirm("ยืนยันการเปิดโครงการนี้ ") == true) {
                $.post("/project/deleteProject", {
                    ProjectId: projectId,
                    type:'N'
                }, function (datalist) {
                    datalist = jQuery.parseJSON(datalist);
                    if (datalist['status'] == 'true') {
                        alert("เปิดโครงการสำเร็จ");
                        location.reload();
                    } else {
                        alert("ไม่สามารถเปิดโครงการได้");
                    }
                })
            }
        }
    })

    $(document).on('click', "#btnClose", function() {
        $('.addStaff').hide();
        $('.addHouse').hide();
        $('.editNameProject').hide();
        $('.mainBtn').show();
    })

    $(document).on('click', "#btnAddHour,#btnEditNameProject,#btnAddStaff", function() {
        var projectId=$(this).attr('project_id');

        if($(this).attr('id')=='btnAddStaff'){
            var UserName=$('select#selectUserName_'+projectId).val();

            if(UserName==''){
                alert('กรุณาเลือกชื่อพนักงาน');
            }else{
                $.post("/project/addStaffProject", {
                    ProjectId: projectId,
                    UserName: UserName
                }, function (datalist) {
                    datalist = jQuery.parseJSON(datalist);
                    if (datalist['status'] == 'true') {
                        var string="";
                        string+="<span class='label label-success' style='padding-right: 17px;'>";
                        string+=datalist['data'];
                        string+="<a id='btnRemoveStaffProject' UserName='"+UserName+"' ProjectId='"+projectId+"'  style='z-index: 20;position: absolute;margin-left: 2px;margin-top: 1px;' type='button' class='close' data-dismiss='modal'>";
                        string+="<span aria-hidden='true'>&times;</span>";
                        string+="</a>";
                        string+="</span>";
                        string+="</br>";
                        $('#ListStaff_'+projectId).append(string);
                        $('.addHouse').hide();
                        $('.editNameProject').hide();
                        $('.addStaff').hide();
                        $('.mainBtn').show();
                    } else {
                        alert("ไม่สมารถเพิ่มชั่วโมงทำงานได้")
                    }
                })
            }

        }else if($(this).attr('id')=='btnAddHour') {
            var value = $('#inputValue_' + projectId).val();
            //var ProjectId = $('#inputProductId_' + projectId).val();
            if (!$.isNumeric(value)) {
                alert('กรุณากรอกตัวเลข');
            } else {
                $.post("/project/addHourProject", {
                    ProjectId: projectId,
                    value: value
                }, function (datalist) {
                    datalist = jQuery.parseJSON(datalist);
                    if (datalist['status'] == 'true') {
                        var beforvalue = $('td#MaxTime_' + projectId).html();
                        $('td#MaxTime_' + projectId).html(parseInt(beforvalue) + parseInt(value));
                        $('#inputValue_' + projectId).val('');

                        $('.addHouse').hide();
                        $('.editNameProject').hide();
                        $('.mainBtn').show();
                    } else {
                        alert("ไม่สมารถเพิ่มชั่วโมงทำงานได้")
                    }
                })
            }
        }else if($(this).attr('id')=='btnEditNameProject'){
            var name = $('#inputName_' + projectId).val();
            //var ProjectId = $('#inputEditProductId_' + projectId).val();
            if (name=='') {
                alert('กรุณาชื่อโครงการ');
            } else {
                $.post("/project/editProject", {
                    ProjectId: projectId,
                    name: name
                }, function (datalist) {
                    datalist = jQuery.parseJSON(datalist);
                    if (datalist['status'] == 'true') {
                        $('td#ProjectName_' + projectId).html(name);
                        $('#inputName_' + projectId).val('');

                        $('.addHouse').hide();
                        $('.editNameProject').hide();
                        $('.mainBtn').show();

                    } else {
                        alert("ไม่สมารถเพิ่มชั่วโมงทำงานได้")
                    }
                })
            }
        }
    })

    $(document).on('click', "#btn-Clone-Work", function() {
        var WorkDetail_id=$(this).attr('WorkDetail_id');
        var Project_id=$(this).attr('Project_id');
        var Detail=$('#td_detail_'+WorkDetail_id).html();
        var EndMinute=$(this).attr('EndMinute');
        var EndHour=$(this).attr('EndHour');
        var StartHour=$(this).attr('StartHour');
        var StartMinute=$(this).attr('StartMinute');

        $('#Workdetail_Project_id').val(Project_id);
        $('#Workdetail_Detail').val(Detail);
        $('#Workdetail_StartHour').val(StartHour);
        $('#Workdetail_StartMinute').val(StartMinute);
        $('#Workdetail_EndHour').val(EndHour);
        $('#Workdetail_EndMinute').val(EndMinute);
    })


    $(document).on('change', "#Workdetail_TypeWork", function() {
        if($(this).val()=='work'){
            $('#Workdetail_Project_id').show();
            $('[for="Workdetail_Project_id"]').show();
            $('#Workdetail_TypeLeave').hide();
            $('[for="Workdetail_TypeLeave"]').hide();
        }else if($(this).val()=='leave'){
            $('#Workdetail_Project_id').hide();
            $('[for="Workdetail_Project_id"]').hide();
            $('#Workdetail_TypeLeave').show();
            $('[for="Workdetail_TypeLeave"]').show();
        }
    })

    //********************************************start Select Plan
    $(document).on('click', "select#Workdetail_Plan_level_three", function() {
        if($(this).val()!=='') {
            //console.log($(this).val());
            $('#Workdetail_Detail').val($('select#Workdetail_Plan_level_three option:selected').text());
        }else{
            $('#Workdetail_Detail').val('');
        }
    })
    //********************************************end Select Plan

    //********************************************start ตัวรายงานชั่วโมงทำงาน

    $(document).on('click', "#td_View_Report_Empty", function() {

    })

    $(document).on('click', "#td_View_Report_NotEmpty", function() {
        var selectMonthSearch=$('#selectMonthSearch').val();
        var selectYearSearch=$('#selectYearSearch').val();
        var selectDay=$(this).attr('day');
        var UserName=$(this).attr('UserName');

        $.post("/site/GetWorkDetailPicker",{
            selectMonthSearch:selectMonthSearch,
            selectYearSearch:selectYearSearch,
            selectDay:selectDay,
            UserName:UserName
        },function(datalist){
            datalist=jQuery.parseJSON(datalist);
            if(datalist['status']=='true'){
                $('.view_data').remove();
                $('#sumHour').html(datalist['sum'])
                $.each(datalist['data'], function( key, value ) {
                    if(value['Remove']!=='Y') {
                        var string = "";
                        string += "<tr class='view_data' >";
                        string += "<td>"+ parseInt(parseInt(key) + 1) + "";

                        //if (value['Remove'] == 'E') {
                        //    string += "<a id='btnDeleteWorkDetail' WorkDetail_id='" + value['WorkDetail_id'] + "' style='z-index: 20;position: absolute;margin-top: -7px;' type='button' class='close' data-dismiss='modal'>";
                        //    string += "<span aria-hidden='true'>&times;</span>";
                        //    string += "</a>";
                        //    string += "<a href='/site/index/" + value['WorkDetail_id'] + "' style='z-index: 20;position: absolute;margin-left: -5px;margin-top: 5px;' type='button' class='close' data-dismiss='modal'>";
                        //    string += "<span aria-hidden='true' class='btn-xs'><span class='glyphicon glyphicon-pencil'></span></span>";
                        //    string += "</a>";
                        //}

                        string += "</td>";
                        string += "<td>" + value['StartDate'] + "</td>";
                        string += "<td>" + value['EndDate'] + "</td>";

                        var result=value['Detail'].split(',');

                        if(result.length>1 && (result[0]=='ขาดงาน' || result[0]=='ลาป่วย' || result[0]=='ลาอื่นๆ' || result[0]=='ลาคลอด' || result[0]=='ลาบวช' || result[0]=='ลาสมรส' || result[0]=='ลาพักร้อน' || result[0]=='รออนุมัต')){
                            string += "<td>" + result[0] + "</td>";
                            string += "<td>" + value['UserName'] + "</td>";
                            string += "<td>";
                            string +=result[1];
                        }else {
                            string += "<td>" + value['Project_id'] + "</td>";
                            string += "<td>" + value['UserName'] + "</td>";
                            string += "<td>";
                            string +=value['Detail'];
                        }
                        string += "</td>";

                        string += "<td style='text-align: center'>" + value['Value'];
                        string += "<div class='btn-group btn-group-xs' aria-label='Default button group' role='group' style='margin: 5px;'>";



                        if (typeof(value['EndDate'])==='undefined'){
                            string += "<a href='/site/closework/" + value['WorkDetail_id'] + "' class='btn btn-success' onclick='return confirm('คุณเเน่ใจที่จะปิดงานนี้')' data-toggle='tooltip' data-placement='top' title='ปิดงานนี้' ><span class='glyphicon glyphicon-off' aria-hidden='true'></span></a>"
                        }

                        if (value['Remove'] == 'A'||(value['Remove'] == 'E' && value['TypeWork']!=='leave')) {
                            string += "<a id='btnDeleteWorkDetail'' WorkDetail_id='" + value['WorkDetail_id'] + "' class='btn btn-group btn-danger' data-toggle='tooltip' data-placement='top' title='ลบงานนี้'>";
                            string += "<span class='glyphicon glyphicon-trash' aria-hidden='true'></span>";
                            string += "</a>";
                            string += "<a href='/site/index/" + value['WorkDetail_id'] + "' class='btn btn-group btn-success ' data-toggle='tooltip' data-placement='top' title='แก้ไขข้อมูล' >";
                            string += "<span class='glyphicon glyphicon-pencil'' aria-hidden='true'></span>";
                            string += "</a>";
                        }

                        if(value['TypeWork']!=='leave'){
                            string += "<button class='btn btn-warning' id='btn-Clone-Work' Project_id='" + value['Project_id'] + "'  WorkDetail_id='" + value['WorkDetail_id'] + "' data-toggle='tooltip' data-placement='top' title='สร้างงานใหม่' ><i class=' fa-files-o'></i></button>";
                        }
                        string+='</div>'
                        string += "</td>";
                        string += "</tr>";
                        $('#TableView').append(string);
                        //console.log( key + ": " + value['WorkDetail_id'] );
                    }
                });
            }else{
                alert('ไม่สามารถเพิ่มข้อมูลได้');
            }
        });
    })

    $(document).on('click', "#btn_Back,#btn_Next", function() {
        var type='';
        var sum=$(this).attr('sum');
        var select=$(this).attr('selectMonth');
        var selectYear=$(this).attr('selectYear');
        var monthNames = [ "","มกราคม","กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม" ];
        if($(this).attr('id')=='btn_Back'){
            $( "li[role=presentation]" ).each(function( index ) {
                if($(this).attr('class')==='active'){
                    type=$(this).attr('type');
                }
            });
            if(select==1){
                selectYear--;
                select=13;
            }
            $('#btn_Back').attr('selectMonth',select-1);
            $('#btn_Next').attr('selectMonth',select-1);
            $('#btn_Back').attr('selectYear',selectYear);
            $('#btn_Next').attr('selectYear',selectYear);
            if(typeof(type)!=='undefined'&&typeof(selectMonthSearch)!=='undefined'&&typeof(selectYearSearch)!=='undefined'){
                $('#btn_Back').attr('disabled','disabled');
                $('#btn_Next').attr('disabled','disabled');
                $('#selectMonthSearch').attr('disabled','disabled');
                $('#selectYearSearch').attr('disabled','disabled');
                $.post("/Site/GetViewTable", {
                    type:type,
                    selectMonthSearch:select-1,
                    selectYearSearch:selectYear
                }, function (datalist) {
                    //datalist=jQuery.parseJSON(datalist);
                    //$.each( datalist, function( key, value ) {
                    //    $('#table-view-'+key).html(value);
                    //});
                    $('#table-view-'+type).html(datalist);
                    $('#btn_Back').removeAttr('disabled');
                    $('#btn_Next').removeAttr('disabled');
                    $('#selectMonthSearch').removeAttr('disabled');
                    $('#selectYearSearch').removeAttr('disabled');
                })
            }
            $('#nameMonth').html(monthNames[parseInt(select)-1]+' / '+selectYear);
            //$('.tbody_Month').hide();
            //$('#tbody_All_Month_'+selectYear+""+(select-1)).show();
            //$('#tbody_Leave_Month_'+selectYear+""+(select-1)).show();
            //$('#tbody_Work_Month_'+selectYear+""+(select-1)).show();
            //$('#tbody_Plan_Month_'+selectYear+""+(select-1)).show();
            $('#selectMonthSearch').val(select-1);
            $('#selectYearSearch').val(selectYear);

        }else if($(this).attr('id')=='btn_Next'){
            $( "li[role=presentation]" ).each(function( index ) {
                if($(this).attr('class')==='active'){
                    type=$(this).attr('type');
                }
            });
            if(select==12){
                selectYear++;
                select=0;
            }
            $('#btn_Next').attr('selectMonth',parseInt(select)+1);
            $('#btn_Back').attr('selectMonth',parseInt(select)+1);
            $('#btn_Back').attr('selectYear',selectYear);
            $('#btn_Next').attr('selectYear',selectYear);
            if(typeof(type)!=='undefined'&&typeof(selectMonthSearch)!=='undefined'&&typeof(selectYearSearch)!=='undefined'){
                $.post("/Site/GetViewTable", {
                    type:type,
                    selectMonthSearch:parseInt(select)+1,
                    selectYearSearch:selectYear
                }, function (datalist) {
                    $('#btn_Back').removeAttr('disabled');
                    $('#btn_Next').removeAttr('disabled');
                    $('#selectMonthSearch').removeAttr('disabled');
                    $('#selectYearSearch').removeAttr('disabled');
                    $('#table-view-'+type).html(datalist);
                })
            }
            $('#nameMonth').html(monthNames[parseInt(select)+1]+' / '+selectYear);
            //$('.tbody_Month').hide();
            //$('#tbody_All_Month_'+selectYear+""+(parseInt(select)+1)).show();
            //$('#tbody_Leave_Month_'+selectYear+""+(parseInt(select)+1)).show();
            //$('#tbody_Work_Month_'+selectYear+""+(parseInt(select)+1)).show();
            //$('#tbody_Plan_Month_'+selectYear+""+(parseInt(select)+1)).show();
            $('#selectMonthSearch').val(parseInt(select)+1);
            $('#selectYearSearch').val(selectYear);
        }
    })

    $(document).on('change', "#selectMonthSearch,#selectYearSearch", function() {
        var month=$('#selectMonthSearch').val();
        var year=$('#selectYearSearch').val();
        var monthNames = [ "","มกราคม","กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม" ];
        $( "li[role=presentation]" ).each(function( index ) {
            if($(this).attr('class')==='active'){
                type=$(this).attr('type');
            }
        });
        $('#btn_Next').attr('selectMonth',month);
        $('#btn_Back').attr('selectMonth',month);
        $('#btn_Back').attr('selectYear',year);
        $('#btn_Next').attr('selectYear',year);
        if(typeof(type)!=='undefined'&&typeof(month)!=='undefined'&&typeof(year)!=='undefined'){
            $('#btn_Back').attr('disabled','disabled');
            $('#btn_Next').attr('disabled','disabled');
            $('#selectMonthSearch').attr('disabled','disabled');
            $('#selectYearSearch').attr('disabled','disabled');
            $.post("/Site/GetViewTable", {
                type:type,
                selectMonthSearch:month,
                selectYearSearch:year
            }, function (datalist) {
                $('#btn_Back').removeAttr('disabled');
                $('#btn_Next').removeAttr('disabled');
                $('#selectMonthSearch').removeAttr('disabled');
                $('#selectYearSearch').removeAttr('disabled');
                $('#table-view-'+type).html(datalist);
            })
        }
        $('#nameMonth').html(monthNames[month]+' / '+year);
        //$('.tbody_Month').hide();
        //$('#tbody_Month_'+year+month).show();
    })

    //********************************************End ปุ่มเปลิ่ยนเดือนหน้าเเรก

    $(document).on('click', "#btn-search", function() {
        var Project_id=$('select#Project_id').val();
        var UserName=$('select#UserName').val();
        var datepicker_start=$('#datepicker_start').val();
        var datepicker_end=$('#datepicker_end').val();
        var typeSearch=$('#typeSearch').val();
        var type=$(this).attr('type');
        $.post("/site/GetWorkDetail",{
            Project_id:Project_id,
            UserName:UserName,
            datepicker_start:datepicker_start,
            datepicker_end:datepicker_end,
            typeSearch:typeSearch,
            type:type
        },function(datalist){
            datalist=jQuery.parseJSON(datalist);
            if(datalist['status']=='true'){
                $('.view_data').remove();
                $('#sumHour').html(datalist['sum'])
                $.each(datalist['data'], function( key, value ) {
                    if(value['Remove']!=='Y') {
                        var string = "";
                        string += "<tr class='view_data' >";
                        string += "<td>"+ parseInt(parseInt(key) + 1) + "";

                        //if (value['Remove'] == 'E') {
                        //    string += "<a id='btnDeleteWorkDetail' WorkDetail_id='" + value['WorkDetail_id'] + "' style='z-index: 20;position: absolute;margin-top: -7px;' type='button' class='close' data-dismiss='modal'>";
                        //    string += "<span aria-hidden='true'>&times;</span>";
                        //    string += "</a>";
                        //    string += "<a href='/site/index/" + value['WorkDetail_id'] + "' style='z-index: 20;position: absolute;margin-left: -5px;margin-top: 5px;' type='button' class='close' data-dismiss='modal'>";
                        //    string += "<span aria-hidden='true' class='btn-xs'><span class='glyphicon glyphicon-pencil'></span></span>";
                        //    string += "</a>";
                        //}

                        string += "</td>";
                        string += "<td>" + value['StartDate'] + "</td>";
                        string += "<td>" + value['EndDate'] + "</td>";

                        var result=value['Detail'].split(',');

                        if(result.length>1 && (result[0]=='ขาดงาน' ||  result[0]=='ลาป่วย' || result[0]=='ลาอื่นๆ' || result[0]=='ลาคลอด' || result[0]=='ลาบวช' || result[0]=='ลาสมรส' || result[0]=='ลาพักร้อน' || result[0]=='รออนุมัต')){
                            string += "<td>" + result[0] + "</td>";
                            string += "<td>" + value['UserName'] + "</td>";
                            string += "<td>";
                            string +=result[1];
                        }else {
                            string += "<td>" + value['Project_id'] + "</td>";
                            string += "<td>" + value['UserName'] + "</td>";
                            string += "<td>";
                            string +=value['Detail'];
                        }
                        string += "</td>";
                        string += "<td style='text-align: center'>" + value['Value'];
                        string += "<div class='btn-group btn-group-xs' aria-label='Default button group' role='group' style='margin: 5px;'>";



                        if (typeof(value['EndDate'])==='undefined'){
                            string += "<a href='/site/closework/" + value['WorkDetail_id'] + "' class='btn btn-success' onclick='return confirm('คุณเเน่ใจที่จะปิดงานนี้')' data-toggle='tooltip' data-placement='top' title='ปิดงานนี้' ><span class='glyphicon glyphicon-off' aria-hidden='true'></span></a>"
                        }

                        if (value['Remove'] == 'A'||(value['Remove'] == 'E' && value['TypeWork']!=='leave')) {
                            string += "<a id='btnDeleteWorkDetail'' WorkDetail_id='" + value['WorkDetail_id'] + "' class='btn btn-group btn-danger' data-toggle='tooltip' data-placement='top' title='ลบงานนี้'>";
                            string += "<span class='glyphicon glyphicon-trash' aria-hidden='true'></span>";
                            string += "</a>";
                            string += "<a href='/site/index/" + value['WorkDetail_id'] + "' class='btn btn-group btn-success ' data-toggle='tooltip' data-placement='top' title='แก้ไขข้อมูล' >";
                            string += "<span class='glyphicon glyphicon-pencil'' aria-hidden='true'></span>";
                            string += "</a>";
                        }

                        if((value['TypeWork'])!=='leave'){
                            string += "<button class='btn btn-warning' id='btn-Clone-Work' Project_id='" + value['Project_id'] + "'  WorkDetail_id='" + value['WorkDetail_id'] + "' data-toggle='tooltip' data-placement='top' title='สร้างงานใหม่' ><i class='fa fa-files-o'></i></button>";
                        }

                        string += "</div>";
                        string+ "</td>";
                        string += "</tr>";
                        $('#TableView').append(string);
                        //console.log( key + ": " + value['WorkDetail_id'] );
                    }
                });
            }else{
                alert('ไม่สามารถเพิ่มข้อมูลได้');
            }
        })
    })
});

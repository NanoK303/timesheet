-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.24-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.2.0.4947
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table timesheet.company
DROP TABLE IF EXISTS `company`;
CREATE TABLE IF NOT EXISTS `company` (
  `Company_id` int(10) NOT NULL AUTO_INCREMENT,
  `CompanyName` varchar(10) DEFAULT '0',
  `Remove` enum('Y','N') DEFAULT 'N',
  PRIMARY KEY (`Company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table timesheet.groupstaff
DROP TABLE IF EXISTS `groupstaff`;
CREATE TABLE IF NOT EXISTS `groupstaff` (
  `groupstaff_id` int(10) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) NOT NULL,
  `Project_id` int(11) NOT NULL,
  `Remove` enum('Y','N') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`groupstaff_id`),
  KEY `FK_groupstaff_staff` (`UserName`),
  KEY `FK_groupstaff_project` (`Project_id`),
  CONSTRAINT `FK_groupstaff_project` FOREIGN KEY (`Project_id`) REFERENCES `project` (`Project_id`),
  CONSTRAINT `FK_groupstaff_staff` FOREIGN KEY (`UserName`) REFERENCES `staff` (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table timesheet.holiday
DROP TABLE IF EXISTS `holiday`;
CREATE TABLE IF NOT EXISTS `holiday` (
  `holiday_id` int(10) NOT NULL AUTO_INCREMENT,
  `holiday_detail` varchar(255) DEFAULT NULL,
  `holiday_day` int(10) DEFAULT NULL,
  `holiday_month` int(10) DEFAULT NULL,
  `holiday_year` int(10) DEFAULT NULL,
  `holiday_remove` enum('Y','N') DEFAULT 'N',
  PRIMARY KEY (`holiday_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='วันหยุด';

-- Data exporting was unselected.


-- Dumping structure for table timesheet.log_plan
DROP TABLE IF EXISTS `log_plan`;
CREATE TABLE IF NOT EXISTS `log_plan` (
  `LogPlan_id` int(10) NOT NULL AUTO_INCREMENT,
  `Project_id` int(10) DEFAULT NULL,
  `Plan_id` int(10) DEFAULT NULL,
  `UserName` varchar(50) DEFAULT NULL,
  `LogPlan_detail` text,
  `LogPlan_date` datetime DEFAULT NULL,
  `LogPlan_type` enum('create','edit','delete') DEFAULT NULL,
  `LogPlan_remove` enum('Y','N') DEFAULT 'N',
  PRIMARY KEY (`LogPlan_id`),
  KEY `FK_log_plan_plan` (`Plan_id`),
  KEY `FK_log_plan_staff` (`UserName`),
  KEY `FK_log_plan_project` (`Project_id`),
  CONSTRAINT `FK_log_plan_plan` FOREIGN KEY (`Plan_id`) REFERENCES `plan` (`Plan_id`),
  CONSTRAINT `FK_log_plan_project` FOREIGN KEY (`Project_id`) REFERENCES `project` (`Project_id`),
  CONSTRAINT `FK_log_plan_staff` FOREIGN KEY (`UserName`) REFERENCES `staff` (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table timesheet.plan
DROP TABLE IF EXISTS `plan`;
CREATE TABLE IF NOT EXISTS `plan` (
  `Plan_id` int(10) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) DEFAULT NULL,
  `Project_id` int(10) DEFAULT NULL,
  `Plan_level_one` int(10) DEFAULT NULL,
  `Plan_level_two` int(10) DEFAULT NULL,
  `Plan_level_three` int(10) DEFAULT NULL,
  `Plan_title` varchar(250) DEFAULT NULL,
  `Plan_detail` text,
  `Plan_hour` int(11) DEFAULT NULL,
  `Plan_start_date` datetime DEFAULT NULL,
  `Plan_end_date` datetime DEFAULT NULL,
  `Plan_create_date` datetime DEFAULT NULL,
  `Plan_status` enum('plan') DEFAULT 'plan',
  `Plan_remove` enum('Y','N') DEFAULT 'N',
  PRIMARY KEY (`Plan_id`),
  KEY `FK_plan_staff` (`UserName`),
  KEY `FK_plan_project` (`Project_id`),
  KEY `FK_plan_plan` (`Plan_level_one`),
  KEY `FK_plan_plan_2` (`Plan_level_two`),
  KEY `FK_plan_plan_3` (`Plan_level_three`),
  CONSTRAINT `FK_plan_plan` FOREIGN KEY (`Plan_level_one`) REFERENCES `plan` (`Plan_id`),
  CONSTRAINT `FK_plan_plan_2` FOREIGN KEY (`Plan_level_two`) REFERENCES `plan` (`Plan_id`),
  CONSTRAINT `FK_plan_plan_3` FOREIGN KEY (`Plan_level_three`) REFERENCES `plan` (`Plan_id`),
  CONSTRAINT `FK_plan_project` FOREIGN KEY (`Project_id`) REFERENCES `project` (`Project_id`),
  CONSTRAINT `FK_plan_staff` FOREIGN KEY (`UserName`) REFERENCES `staff` (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table timesheet.plan_staff
DROP TABLE IF EXISTS `plan_staff`;
CREATE TABLE IF NOT EXISTS `plan_staff` (
  `Plan_staff_id` int(10) NOT NULL AUTO_INCREMENT,
  `WorkDetail_id` int(10) DEFAULT NULL,
  `Plan_id` int(10) DEFAULT NULL,
  `UserName` varchar(50) DEFAULT NULL,
  `Plan_staff_hour` int(10) DEFAULT '0',
  `Plan_staff_remove` enum('Y','N') DEFAULT 'N',
  PRIMARY KEY (`Plan_staff_id`),
  KEY `FK_plan_staff_plan` (`Plan_id`),
  KEY `FK_plan_staff_staff` (`UserName`),
  KEY `FK_plan_staff_workdetail` (`WorkDetail_id`),
  CONSTRAINT `FK_plan_staff_plan` FOREIGN KEY (`Plan_id`) REFERENCES `plan` (`Plan_id`),
  CONSTRAINT `FK_plan_staff_staff` FOREIGN KEY (`UserName`) REFERENCES `staff` (`UserName`),
  CONSTRAINT `FK_plan_staff_workdetail` FOREIGN KEY (`WorkDetail_id`) REFERENCES `workdetail` (`WorkDetail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table timesheet.project
DROP TABLE IF EXISTS `project`;
CREATE TABLE IF NOT EXISTS `project` (
  `Project_id` int(10) NOT NULL AUTO_INCREMENT,
  `Company_id` int(10) NOT NULL DEFAULT '0',
  `Project_name` varchar(250) DEFAULT NULL,
  `MaxTime` int(11) DEFAULT NULL,
  `Public` enum('Y','N') DEFAULT 'Y',
  `Remove` enum('Y','N') DEFAULT 'N',
  PRIMARY KEY (`Project_id`),
  KEY `FK_project_company` (`Company_id`),
  CONSTRAINT `FK_project_company` FOREIGN KEY (`Company_id`) REFERENCES `company` (`Company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table timesheet.staff
DROP TABLE IF EXISTS `staff`;
CREATE TABLE IF NOT EXISTS `staff` (
  `UserName` varchar(50) NOT NULL DEFAULT '',
  `SubOrganizer_id` int(10) DEFAULT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `LastName` varchar(250) DEFAULT NULL,
  `Nickname` varchar(250) DEFAULT NULL,
  `Password` varchar(250) DEFAULT NULL,
  `Email` varchar(250) DEFAULT NULL,
  `Phone` varchar(10) DEFAULT NULL,
  `Address` text,
  `Position` enum('admin','programmer','design','manage') DEFAULT NULL,
  `remove` enum('Y','N','C') DEFAULT 'N',
  PRIMARY KEY (`UserName`),
  KEY `FK_staff_sub_organizer` (`SubOrganizer_id`),
  CONSTRAINT `FK_staff_sub_organizer` FOREIGN KEY (`SubOrganizer_id`) REFERENCES `sub_organizer` (`SubOrganizer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table timesheet.sub_organizer
DROP TABLE IF EXISTS `sub_organizer`;
CREATE TABLE IF NOT EXISTS `sub_organizer` (
  `SubOrganizer_id` int(10) NOT NULL AUTO_INCREMENT,
  `Company_id` int(10) DEFAULT NULL,
  `NameOrganizer` varchar(250) DEFAULT NULL,
  `Remove` enum('Y','N') DEFAULT 'N',
  PRIMARY KEY (`SubOrganizer_id`),
  KEY `FK_sub_organizer_company` (`Company_id`),
  CONSTRAINT `FK_sub_organizer_company` FOREIGN KEY (`Company_id`) REFERENCES `company` (`Company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table timesheet.workdetail
DROP TABLE IF EXISTS `workdetail`;
CREATE TABLE IF NOT EXISTS `workdetail` (
  `WorkDetail_id` int(10) NOT NULL AUTO_INCREMENT,
  `Project_id` int(10) DEFAULT NULL,
  `UserName` varchar(50) DEFAULT NULL,
  `StartDate` datetime DEFAULT NULL,
  `EndDate` datetime DEFAULT NULL,
  `Value` double DEFAULT NULL,
  `Detail` text,
  `RecordDate` datetime DEFAULT NULL,
  `TypeWork` enum('work','leave','plan') DEFAULT 'work',
  `Remove` enum('Y','N') DEFAULT 'N',
  PRIMARY KEY (`WorkDetail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

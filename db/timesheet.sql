-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.25a - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2015-02-01 17:21:15
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping structure for table timesheet.company
DROP TABLE IF EXISTS `company`;
CREATE TABLE IF NOT EXISTS `company` (
  `Company_id` int(10) NOT NULL AUTO_INCREMENT,
  `CompanyName` varchar(10) DEFAULT '0',
  `Remove` enum('Y','N') DEFAULT 'N',
  PRIMARY KEY (`Company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table timesheet.project
DROP TABLE IF EXISTS `project`;
CREATE TABLE IF NOT EXISTS `project` (
  `Project_id` int(10) NOT NULL AUTO_INCREMENT,
  `Company_id` int(10) NOT NULL DEFAULT '0',
  `Project_name` varchar(250) DEFAULT NULL,
  `MaxTime` int(11) DEFAULT NULL,
  `Remove` enum('Y','N') DEFAULT 'N',
  PRIMARY KEY (`Project_id`),
  KEY `FK_project_company` (`Company_id`),
  CONSTRAINT `FK_project_company` FOREIGN KEY (`Company_id`) REFERENCES `company` (`Company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table timesheet.staff
DROP TABLE IF EXISTS `staff`;
CREATE TABLE IF NOT EXISTS `staff` (
  `UserName` varchar(50) NOT NULL DEFAULT '',
  `SubOrganizer_id` int(10) DEFAULT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `LastName` varchar(250) DEFAULT NULL,
  `Nickname` varchar(250) DEFAULT NULL,
  `Password` varchar(250) DEFAULT NULL,
  `Position` enum('admin','programmer','design') DEFAULT NULL,
  `remove` enum('Y','N') DEFAULT 'N',
  PRIMARY KEY (`UserName`),
  KEY `FK_staff_sub_organizer` (`SubOrganizer_id`),
  CONSTRAINT `FK_staff_sub_organizer` FOREIGN KEY (`SubOrganizer_id`) REFERENCES `sub_organizer` (`SubOrganizer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table timesheet.sub_organizer
DROP TABLE IF EXISTS `sub_organizer`;
CREATE TABLE IF NOT EXISTS `sub_organizer` (
  `SubOrganizer_id` int(10) NOT NULL AUTO_INCREMENT,
  `Company_id` int(10) DEFAULT NULL,
  `NameOrganizer` varchar(250) DEFAULT NULL,
  `Remove` enum('Y','N') DEFAULT 'N',
  PRIMARY KEY (`SubOrganizer_id`),
  KEY `FK_sub_organizer_company` (`Company_id`),
  CONSTRAINT `FK_sub_organizer_company` FOREIGN KEY (`Company_id`) REFERENCES `company` (`Company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table timesheet.workdetail
DROP TABLE IF EXISTS `workdetail`;
CREATE TABLE IF NOT EXISTS `workdetail` (
  `WorkDetail_id` int(10) NOT NULL AUTO_INCREMENT,
  `Project_id` int(10) NOT NULL DEFAULT '0',
  `UserName` varchar(50) DEFAULT NULL,
  `StartDate` datetime DEFAULT NULL,
  `EndDate` datetime DEFAULT NULL,
  `Value` int(10) DEFAULT NULL,
  `Detail` varchar(250) DEFAULT NULL,
  `RecordDate` datetime DEFAULT NULL,
  `Remove` enum('Y','N') DEFAULT 'N',
  PRIMARY KEY (`WorkDetail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

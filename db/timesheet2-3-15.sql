-- --------------------------------------------------------
-- Host:                         timesheet.hbaan.com
-- Server version:               5.0.51a - Source distribution
-- Server OS:                    unknown-freebsd7.0
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2015-02-03 12:44:49
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping structure for table timesheet_main.company
DROP TABLE IF EXISTS `company`;
CREATE TABLE IF NOT EXISTS `company` (
  `Company_id` int(10) NOT NULL auto_increment,
  `CompanyName` varchar(10) default '0',
  `Remove` enum('Y','N') default 'N',
  PRIMARY KEY  (`Company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table timesheet_main.company: ~1 rows (approximately)
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT INTO `company` (`Company_id`, `CompanyName`, `Remove`) VALUES
	(1, 'X3dev', 'N');
/*!40000 ALTER TABLE `company` ENABLE KEYS */;


-- Dumping structure for table timesheet_main.project
DROP TABLE IF EXISTS `project`;
CREATE TABLE IF NOT EXISTS `project` (
  `Project_id` int(10) NOT NULL auto_increment,
  `Company_id` int(10) NOT NULL default '0',
  `Project_name` varchar(250) default NULL,
  `MaxTime` int(11) default NULL,
  `Remove` enum('Y','N') default 'N',
  PRIMARY KEY  (`Project_id`),
  KEY `FK_project_company` (`Company_id`),
  CONSTRAINT `FK_project_company` FOREIGN KEY (`Company_id`) REFERENCES `company` (`Company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Dumping data for table timesheet_main.project: ~9 rows (approximately)
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` (`Project_id`, `Company_id`, `Project_name`, `MaxTime`, `Remove`) VALUES
	(1, 1, 'TimeSheet', 110, 'N'),
	(2, 1, 'หุ้น', 270, 'N'),
	(3, 1, 'Ssporting', 540, 'N'),
	(4, 1, 'เกม', 810, 'N'),
	(5, 1, 'ยา', 810, 'N'),
	(6, 1, 'ร้านอาหาร', 100, 'N'),
	(7, 1, '1M', 100, 'N'),
	(8, 1, 'ธุรการ', 810, 'N'),
	(9, 1, 'ทำความสะอาดประจำวัน', 810, 'N');
/*!40000 ALTER TABLE `project` ENABLE KEYS */;


-- Dumping structure for table timesheet_main.staff
DROP TABLE IF EXISTS `staff`;
CREATE TABLE IF NOT EXISTS `staff` (
  `UserName` varchar(50) NOT NULL default '',
  `SubOrganizer_id` int(10) default NULL,
  `Name` varchar(250) default NULL,
  `LastName` varchar(250) default NULL,
  `Nickname` varchar(250) default NULL,
  `Password` varchar(250) default NULL,
  `Position` enum('admin','programmer','design') default NULL,
  `remove` enum('Y','N','C') default 'N',
  PRIMARY KEY  (`UserName`),
  KEY `FK_staff_sub_organizer` (`SubOrganizer_id`),
  CONSTRAINT `FK_staff_sub_organizer` FOREIGN KEY (`SubOrganizer_id`) REFERENCES `sub_organizer` (`SubOrganizer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table timesheet_main.staff: ~17 rows (approximately)
/*!40000 ALTER TABLE `staff` DISABLE KEYS */;
INSERT INTO `staff` (`UserName`, `SubOrganizer_id`, `Name`, `LastName`, `Nickname`, `Password`, `Position`, `remove`) VALUES
	('babudo', NULL, 'ศรัณย์', 'ลายกลาง', 'อาร์ม', 'e8eadd8c57c277a3ea014e2229b965b9', 'design', 'N'),
	('Bim', NULL, 'Julaluk', 'Tonghom', 'Bim', '4aefcb71622252c57b3c24f551331eaf', 'design', 'N'),
	('crop', NULL, 'สันติพล', 'ทรัพย์สิน', 'โอ้', '827ccb0eea8a706c4c34a16891f84e7b', 'admin', 'N'),
	('demonixz', NULL, 'สุเชาว์', 'จิระประพจน์', '@@', 'ff0eca571f7670efa4ac4a2c37214c6b', 'programmer', 'N'),
	('heetee', NULL, 'veerasak', 'singkum', 'note/udom', '5719864fab268eebc27bd600e4af996e', 'design', 'N'),
	('indpendents', NULL, 'ภานุพงศ์', 'อุรุโคตร', 'ติ่ง', 'dd649c724529c23bb693a16247621110', 'programmer', 'N'),
	('manaeiei', NULL, 'mana', 'eiei', 'munu', '7aad3ab7ffd5f411049b78364cc53e94', 'programmer', 'N'),
	('mrsyrop', NULL, 'วิถี', 'พงษาวดาร', 'บื่อ', 'fc3ff6565baad0deb575df494191fcf3', 'programmer', 'N'),
	('Muraz', NULL, 'Nukul', 'Muenjihan', 'Au', 'e10adc3949ba59abbe56e057f20f883e', 'programmer', 'N'),
	('muy', NULL, 'กานต์ธีรา', 'สุตัญตั้งใจ', 'มุ้ย', '70410c93da2b5e3ce7977a59bf2d675b', 'programmer', 'N'),
	('NanoK', NULL, 'sootipong', 'taothong', 'NanoK', 'e10adc3949ba59abbe56e057f20f883e', 'admin', 'N'),
	('obe', NULL, 'สุบรรณ', 'วาไชยะ', 'อบ', '59c6f06e471fe1cfaf62afc573fa0bdc', 'programmer', 'N'),
	('superx', NULL, 'ศิวกร', 'อินถาเครือ', 'P\'XL', '261676f286d23f93b21dff383fade78d', 'admin', 'N'),
	('wutthikorn', NULL, 'Wutthikorn', 'Kulkate', 'pele', '6f82976ad44f9d0828583a7f755ef735', 'programmer', 'N'),
	('xsorust', NULL, 'ปรีดา', 'ศรีเสริม', 'โต้ง', 'bb6085dea8961c7824f1a2a842c31b53', 'design', 'N'),
	('ิboy', NULL, 'สิรวิชญ์', 'ทาเงิน', 'บอย', 'd97f8b5d9d7227b668dfffc1c5bf576e', 'programmer', 'N'),
	('แอมมี่', NULL, 'วัชรภรณ์', 'ธรรมสัตย์', 'แอมมี่', '962653292f19ad9fa6e28b729f0e49f4', 'design', 'N');
/*!40000 ALTER TABLE `staff` ENABLE KEYS */;


-- Dumping structure for table timesheet_main.sub_organizer
DROP TABLE IF EXISTS `sub_organizer`;
CREATE TABLE IF NOT EXISTS `sub_organizer` (
  `SubOrganizer_id` int(10) NOT NULL auto_increment,
  `Company_id` int(10) default NULL,
  `NameOrganizer` varchar(250) default NULL,
  `Remove` enum('Y','N') default 'N',
  PRIMARY KEY  (`SubOrganizer_id`),
  KEY `FK_sub_organizer_company` (`Company_id`),
  CONSTRAINT `FK_sub_organizer_company` FOREIGN KEY (`Company_id`) REFERENCES `company` (`Company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table timesheet_main.sub_organizer: ~0 rows (approximately)
/*!40000 ALTER TABLE `sub_organizer` DISABLE KEYS */;
/*!40000 ALTER TABLE `sub_organizer` ENABLE KEYS */;


-- Dumping structure for table timesheet_main.workdetail
DROP TABLE IF EXISTS `workdetail`;
CREATE TABLE IF NOT EXISTS `workdetail` (
  `WorkDetail_id` int(10) NOT NULL auto_increment,
  `Project_id` int(10) NOT NULL default '0',
  `UserName` varchar(50) default NULL,
  `StartDate` datetime default NULL,
  `EndDate` datetime default NULL,
  `Value` double default NULL,
  `Detail` varchar(250) default NULL,
  `RecordDate` datetime default NULL,
  `Remove` enum('Y','N') default 'N',
  PRIMARY KEY  (`WorkDetail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

-- Dumping data for table timesheet_main.workdetail: ~46 rows (approximately)
/*!40000 ALTER TABLE `workdetail` DISABLE KEYS */;
INSERT INTO `workdetail` (`WorkDetail_id`, `Project_id`, `UserName`, `StartDate`, `EndDate`, `Value`, `Detail`, `RecordDate`, `Remove`) VALUES
	(1, 1, 'NanoK', '2015-02-02 08:00:00', '2015-02-02 12:00:00', 4, 'อัพขึ้นโฮสติ้ง http://timesheet.hbaan.com', '2015-02-02 15:12:01', 'N'),
	(2, 1, 'NanoK', '2015-02-02 13:00:00', '2015-02-02 18:00:00', 5, 'ทำหน้า จัดการโครงการ2', '2015-02-02 15:12:31', 'N'),
	(3, 3, 'crop', '2015-02-02 17:49:00', '2015-02-02 18:00:00', 0, 'อัพข่าว', '2015-02-02 15:39:42', 'N'),
	(4, 3, 'wutthikorn', '2015-02-02 09:00:00', '2015-02-02 18:00:00', 9, 'แก้ไขหน้าเล่นเกมให้เป็นระเบียบและเพื่อให้รองรับการแก้ไขในอนาคต', '2015-02-02 17:28:35', 'N'),
	(5, 2, 'babudo', '2015-02-02 08:20:00', '2015-02-02 12:00:00', 3, 'ตัด css /html หน้า sign-in, sign-up', '2015-02-02 17:30:59', 'N'),
	(6, 2, 'babudo', '2015-02-02 13:00:00', '2015-02-02 14:20:00', 1, 'ให้บูม set เครื่องทำโปรเจคหุ้น', '2015-02-02 17:32:04', 'N'),
	(7, 2, 'indpendents', '2015-02-02 08:00:00', '2015-02-02 12:00:00', 4, 'ดูรูปแบบข้อมูล trade รายวันเพื่อแปลงเป็นข้อมูลไว้แสดงกราฟ', '2015-02-02 17:33:47', 'N'),
	(8, 3, 'แอมมี่', '2015-02-02 08:30:00', '2015-02-02 09:30:00', 1, 'ตรวจสอบผลฟุตอบอลเมื่่อคืน พร้อมติดตามข่าวสารเกี่ยวกับฟุตบอล ', '2015-02-02 17:33:50', 'N'),
	(9, 2, 'babudo', '2015-02-02 14:20:00', '2015-02-02 14:40:00', 0, 'แก้ไข/เพิ่มเติม  หน้า sign-in, sign-up (หลังจากที่ set เครื่องใหม่)', '2015-02-02 17:34:09', 'N'),
	(10, 2, 'indpendents', '2015-02-02 13:00:00', '2015-02-02 18:00:00', 5, 'เซ็ตเซิร์ฟเวอร์สำหรับเซอร์วิส', '2015-02-02 17:35:23', 'N'),
	(11, 3, 'แอมมี่', '2015-02-02 09:30:00', '2015-02-02 11:00:00', 1, 'เช็คโปรแกรมการแข่งขันของวันพรุ่งนี้ พร้อมตรวจสอบเวลาแข่ง ช่องทอดการแข่งขัน พร้อมติดตามข่าวสารเกี่ยวกับฟุตบอล ', '2015-02-02 17:35:47', 'N'),
	(12, 5, 'babudo', '2015-02-02 15:00:00', '2015-02-02 17:20:00', 2, 'แก้ไขกล่องยา camacap', '2015-02-02 17:35:59', 'N'),
	(13, 3, 'mrsyrop', '2015-02-02 08:00:00', '2015-02-02 15:00:00', 7, 'ทำระบบแก้รางวัลกิจกรรมประจำวัน', '2015-02-02 17:38:30', 'N'),
	(14, 2, 'แอมมี่', '2015-02-02 11:00:00', '2015-02-02 12:00:00', 1, 'แก้ไขหน้า UI 12SET หน้า Timeline ที่พี่ตั้มส่งมา', '2015-02-02 17:39:36', 'N'),
	(15, 3, 'mrsyrop', '2015-02-02 15:00:00', '2015-02-02 18:00:00', 3, 'แก้การตอบโพสด้วย url', '2015-02-02 17:39:46', 'N'),
	(16, 3, 'heetee', '2015-02-02 08:20:00', '2015-02-02 09:00:00', 0, 'ประกาศรางวัลกิจกรรมใน Ssporting.com โดยการคัดเลือกคอมเม้นคู่บอลที่โดนใจแอดมิน', '2015-02-02 17:40:20', 'N'),
	(17, 2, 'แอมมี่', '2015-02-02 13:00:00', '2015-02-02 14:00:00', 1, 'แก้ไขหน้า UI 12SET หน้า Timeline ที่พี่ตั้มส่งมา', '2015-02-02 17:40:31', 'N'),
	(18, 2, 'Muraz', '2015-02-02 09:00:00', '2015-02-02 12:00:00', 3, 'ลองทำส่วนของ Chart  ในโปรเจท dev ***เกิดปัญหาไม่สามารถทำต่อได้', '2015-02-02 17:41:12', 'N'),
	(19, 2, 'Bim', '2015-02-02 08:30:00', '2015-02-02 11:00:00', 2, 'ปรับเลเอาท์ ห้นหน้าแรก , รวมโปรเจคเลเอาท์หุ้นกับบูม', '2015-02-02 17:41:30', 'N'),
	(20, 2, 'Muraz', '2015-02-02 13:00:00', '2015-02-02 17:00:00', 4, 'เทียบ Option ของ xset.info กับของที่เรามี', '2015-02-02 17:42:12', 'N'),
	(21, 2, 'manaeiei', '2015-02-02 00:00:00', '2015-02-02 00:00:00', 0, 'เริ่มต้น โปรเจค www.xset.info', '2015-02-02 17:42:28', 'N'),
	(22, 2, 'แอมมี่', '2015-02-02 14:00:00', '2015-02-02 17:30:00', 3, 'ทำหน้า UI 12Set หน้า News  ศึกษาข้อมูลหน้า Knoledge', '2015-02-02 17:43:04', 'N'),
	(23, 2, 'Bim', '2015-02-02 12:00:00', '2015-02-02 15:20:00', 3, 'ให้บูมลงโปรแกมสำหรับเปิดโปรเจค, ตัดเลเอาท์หน้าแรกส่วนของ chat ', '2015-02-02 17:44:22', 'N'),
	(24, 3, 'xsorust', '2015-02-02 08:00:00', '2015-02-02 17:30:00', 9, 'อัพเดทข่าวคราวความเคลื่อนไหว เกี่ยวกับฟุตบอล', '2015-02-02 17:45:51', 'N'),
	(25, 3, 'heetee', '2015-02-02 09:00:00', '2015-02-02 10:00:00', 1, 'สร้างกิจกรรมคู่บอลใน Ssporting.com โดยเลือกคู่บอลคู่ใหญ่มาโพสในเว็บ และเพส', '2015-02-02 17:45:53', 'N'),
	(26, 3, 'demonixz', '2015-02-02 16:10:00', '2015-02-02 16:15:00', 0, 'แก้ปัญหาการ reply เป็น url (http://...)', '2015-02-02 17:46:07', 'N'),
	(27, 3, 'xsorust', '2015-02-02 08:30:00', '2015-02-02 08:45:00', 0, 'อัพบอร์ดรูปบอร์ดเซ็กซี่ฟุตบอล', '2015-02-02 17:47:08', 'N'),
	(28, 2, 'demonixz', '2015-02-02 15:00:00', '2015-02-02 15:15:00', 0, 'ติดต่อเรื่อง chart library ของ tradingview', '2015-02-02 17:47:38', 'N'),
	(29, 2, 'Bim', '2015-02-02 15:20:00', '2015-02-02 17:30:00', 2, 'ตัดเลเอาท์หน้าแรกเพิ่มเติม ส่วนของข้อมูลหุ้น (ส่วนที่คลิกแล้วยืดลงมา)', '2015-02-02 17:47:48', 'N'),
	(30, 2, 'ิboy', '2015-02-02 08:20:00', '2015-02-02 17:30:00', 9, 'favorite setalert', '2015-02-02 17:48:16', 'N'),
	(31, 2, 'demonixz', '2015-02-02 11:00:00', '2015-02-02 11:05:00', 0, 'สอบถามเรื่อง license ของ stockchartx (html5)', '2015-02-02 17:48:36', 'N'),
	(32, 2, 'demonixz', '2015-02-02 16:30:00', '2015-02-02 16:41:00', 0, 'config server เพิ่มเว็บ www2.xset.info service.xset.info', '2015-02-02 17:50:04', 'N'),
	(33, 3, 'heetee', '2015-02-02 10:00:00', '2015-02-02 12:00:00', 2, 'หาข้อมูลข่าวสารเกี่ยวกับฟุตบอล จากเว็บไซต์กีฬา Twitter Vine และเพสเฟสบุ๊กกีฬาทั้งในประเทศและต่างประเทศ เพื่อโพสในเพสเฟสบุ๊กและในเว็บ Ssporting.com', '2015-02-02 17:51:48', 'N'),
	(34, 3, 'xsorust', '2015-02-02 10:00:00', '2015-02-02 10:30:00', 0.5, 'หาและอัพคลิปที่น่าสนใจในวงการฟุตบอล', '2015-02-02 17:54:45', 'N'),
	(35, 1, 'แอมมี่', '2015-02-02 17:30:00', '2015-02-02 18:00:00', 0.5, 'Test ระบบ time', '2015-02-02 17:54:52', 'N'),
	(36, 3, 'heetee', '2015-02-02 13:05:00', '2015-02-02 14:30:00', 1.416667, 'คิดมุขเกรียนเพื่อโพสในเพสเฟสบุ๊ก', '2015-02-02 17:57:03', 'N'),
	(37, 3, 'xsorust', '2015-02-02 13:30:00', '2015-02-02 13:45:00', 0.25, 'อัพบอร์ดรูปบอร์ดเซ็กซี่ฟุตบอล', '2015-02-02 17:58:07', 'N'),
	(38, 3, 'heetee', '2015-02-02 14:30:00', '2015-02-02 16:00:00', 1.5, 'หาคู่บอลทีเด็ด และบอลสเตป เพื่อโพสในเพสเฟสบุ๊กและเว็บ Ssporting.com(ทีเด็ดโพสเวลา 16:00 น. /บอลสเตป 16:30 น.)', '2015-02-02 18:01:05', 'N'),
	(39, 3, 'xsorust', '2015-02-02 14:00:00', '2015-02-02 16:00:00', 2, 'เล่นเกมส์ทายผลบอล+แสดงทรรศนะ', '2015-02-02 18:01:42', 'N'),
	(40, 3, 'heetee', '2015-02-02 16:00:00', '2015-02-02 17:30:00', 1.5, 'สร้างกระแสในเว็บ Ssporting.com โดยใช้ User อวตาน', '2015-02-02 18:03:22', 'N'),
	(41, 2, 'indpendents', '2015-02-03 08:00:00', '2015-02-03 09:00:00', 1, 'ติดต่อกับทาง set เกี่ยวกับการเชื่อมตัวตัวเซิร์ฟเวอร์ test,รูปแบบข้อมูลหุ้นรายวัน,เกี่ยวกับบาง test case', '2015-02-03 08:52:11', 'N'),
	(42, 2, 'indpendents', '2015-02-03 09:00:00', '2015-02-03 10:00:00', 1, 'ทำตัวจัดการไฟล์ข้อมูลรายวันเพื่อ insert ข้อมูลลงฐานข้อมูล', '2015-02-03 09:41:08', 'N'),
	(43, 2, 'indpendents', '2015-02-03 10:00:00', '2015-02-03 10:30:00', 0.5, 'ทดสอบตัวจัดการไฟล์ข้อมูลรายวัน', '2015-02-03 09:54:23', 'N'),
	(44, 2, 'indpendents', '2015-02-03 10:30:00', '2015-02-03 11:00:00', 0.5, 'วิเคราะห์ข้อมูลหุ้นรายวันเพื่อทำข้อมูลสำหรับ plot กราฟ', '2015-02-03 09:57:02', 'N'),
	(45, 3, 'obe', '2015-02-03 09:00:00', '2015-02-03 10:20:00', 1.33, 'ios แก้ bug กดจาก getNotifications ประเภท timeline', '2015-02-03 10:12:16', 'N'),
	(46, 2, 'indpendents', '2015-02-03 11:00:00', '2015-02-03 12:00:00', 1, 'เตรียมคำถามสำหรับสอบถามไปยัง SET เกี่ยวกับ test case (Day1)', '2015-02-03 11:01:27', 'N');
/*!40000 ALTER TABLE `workdetail` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
